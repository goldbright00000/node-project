-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 04, 2019 at 01:03 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `winpswebin`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Tablets', 'tablets', NULL, '2019-04-19 13:48:16'),
(2, 'Laptops', 'laptops', NULL, NULL),
(3, 'Mobile Phones', 'mobile-phones', NULL, NULL),
(4, 'Digital Cameras', 'digital-cameras', NULL, NULL),
(5, 'Watches', 'watches', NULL, NULL),
(6, 'Other', 'other', '2019-04-19 16:43:50', '2019-04-19 16:43:50');

-- --------------------------------------------------------

--
-- Table structure for table `category_products`
--

CREATE TABLE `category_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `categories_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_products`
--

INSERT INTO `category_products` (`id`, `product_id`, `categories_id`, `created_at`, `updated_at`) VALUES
(15, 15, 1, NULL, NULL),
(22, 12, 3, NULL, NULL),
(23, 14, 2, NULL, NULL),
(24, 16, 5, NULL, NULL),
(25, 21, 4, NULL, NULL),
(27, 13, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_page`
--

CREATE TABLE `custom_page` (
  `custom_pageid` int(11) NOT NULL,
  `page_name` text,
  `content` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_page`
--

INSERT INTO `custom_page` (`custom_pageid`, `page_name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Footer', '<footer class=\"page-footer d-print-none font-small wd-footer-wrapper pt-1\"><!-- Footer Links -->\r\n<div class=\"container text-center text-md-left\"><!-- Footer links -->\r\n<div class=\"row text-center text-md-left mt-3 \"><!-- Grid column -->\r\n<div class=\"col-md-3 wd-footer-extra-padding mx-auto mt-2 mb-1\">\r\n<div class=\"wd-footer-extra-padding\">\r\n<p><a href=\"https://win.psweb.in/winners\">Winners</a></p>\r\n<p><a href=\"https://win.psweb.in/faqs\">Frequently Asked Questions</a></p>\r\n<p><a href=\"#\">Reviews</a></p>\r\n<p><a href=\"/privacy-policy\">Privacy Policy</a></p>\r\n<p><a href=\"/terms-conditions\">Terms and Conditions</a></p>\r\n<p><a href=\"/refund-policy\">Refund Policy</a></p>\r\n</div>\r\n</div>\r\n<!-- Grid column --> <!-- Grid column -->\r\n<div class=\"col-md-4 mx-auto mb-1\"><a href=\"#\"> <img src=\"https://win.psweb.in/public/images/trust_pilot_white.png\" alt=\"trust pilot reviews\" /> </a></div>\r\n<!-- Grid column --> <!-- Grid column -->\r\n<div class=\"col-md-4 col-lg-3 col-xl-3 mx-auto mt-2\">\r\n<h6 class=\"text-uppercase mb-2 wd-poppins\">We Are Social</h6>\r\n<!-- Social buttons -->\r\n<div class=\"text-md-left text-center mb-3\">&nbsp;</div>\r\n<h6 class=\"text-uppercase  mt-4 mb-4 wd-poppins\">We Accept</h6>\r\n<img class=\"paypalLogo\" src=\"https://win.psweb.in/public/images/paypal-icon.png\" alt=\"credit cards accepted\" /></div>\r\n<!-- Grid column --></div>\r\n<!-- Footer links --></div>\r\n<!-- Footer Links --> <!-- Grid row -->\r\n<div class=\"wd-copyright-section mt-4\">\r\n<div class=\"container\">\r\n<div class=\"row align-items-center pt-3 \"><!-- Grid column -->\r\n<div class=\"col-md-4\"><!--Copyright-->\r\n<p class=\"text-center text-md-left\">&copy; Copyright 2016-2017 WinYourDestinty.com</p>\r\n</div>\r\n<!-- Grid column --> <!-- Grid column -->\r\n<div class=\"col-md-5 offset-md-3\">\r\n<p class=\"text-center text-md-right\">info@winyourdestiny.com</p>\r\n</div>\r\n<!-- Grid column --></div>\r\n</div>\r\n</div>\r\n<!-- Grid row -->\r\n<div id=\"fb-root\" class=\" fb_reset\">\r\n<div style=\"position: absolute; top: -10000px; width: 0px; height: 0px;\">\r\n<div><iframe id=\"fb_xdm_frame_https\" style=\"border: none;\" tabindex=\"-1\" title=\"Facebook Cross Domain Communication Frame\" src=\"https://staticxx.facebook.com/connect/xd_arbiter.php?version=44#channel=f2f9955fccab134&amp;origin=https%3A%2F%2Fwin.psweb.in\" name=\"fb_xdm_frame_https\" frameborder=\"0\" scrolling=\"no\" allowfullscreen=\"true\" aria-hidden=\"true\"></iframe></div>\r\n<div>&nbsp;</div>\r\n</div>\r\n</div>\r\n</footer>', '2019-07-01 12:26:35', '2019-07-01 12:26:35'),
(2, 'AboutUs', '<p>this is about us</p>', '2019-07-01 12:29:28', '2019-07-01 12:30:12');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, NULL, 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '\"\"', 1),
(24, 4, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '\"\"', 2),
(25, 4, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '\"\"', 3),
(26, 4, 'userDiscount', 'number', 'Welcome Credits (The the default credits given to a user upon registration)', 0, 1, 1, 1, 1, 0, '\"\"', 3),
(27, 4, 'salesTax', 'number', 'Sales Tax (No Need for % sign)', 0, 1, 1, 1, 1, 0, '\"\"', 4),
(28, 4, 'maxTickets', 'number', 'The maximum number of tickets that can be played by a user per game', 1, 0, 1, 1, 1, 0, '\"\"', 4),
(29, 4, 'creditNewReferal', 'number', 'New Referral Credit', 0, 1, 1, 1, 1, 0, '\"The percentage of tax applied on the invoice. % symbol is not needed\"', 4),
(30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '\"\"', 1),
(31, 5, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '\"\"', 2),
(32, 5, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '\"\"', 3),
(33, 5, 'name', 'text', 'Coupon Code', 1, 1, 1, 1, 1, 1, '\"The name of the discount coupon\"', 4),
(34, 5, 'value', 'number', 'Discount % (ONLY NUMBER NEED NO NEED FOR % SYMBOL) ', 1, 1, 1, 1, 1, 1, '\"The %age value of the coupon. (ONLY NUMBER NEED NO NEED FOR % SYMBOL)\"', 5),
(35, 5, 'type', 'select_dropdown', 'Type Of Discount', 1, 1, 1, 1, 1, 1, '{\"default\":\"coupon\",\"options\":{\"option1\":\"coupon\"}}', 1),
(36, 5, 'Target', 'select_dropdown', 'Taget Value For Discount', 1, 1, 1, 1, 1, 1, '{\"default\":\"subtotal\",\"options\":{\"option1\":\"subtotal\",\"option2\":\"total\"}}', 5),
(37, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '\"\"', 1),
(38, 6, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '\"\"', 2),
(39, 6, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '\"\"', 3),
(40, 6, 'name', 'text', 'Category Name', 1, 1, 1, 1, 1, 1, '\"\"', 4),
(41, 6, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '\"\"', 5),
(42, 7, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '\"\"', 1),
(43, 7, 'created_at', 'timestamp', 'created_at', 0, 0, 1, 0, 0, 0, '\"\"', 2),
(44, 7, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '\"\"', 3),
(45, 7, 'name', 'text', 'Name Of The Game (Please give a unique name as will bee visible to audience in results)', 1, 1, 1, 0, 1, 1, '\"\"', 4),
(46, 7, 'startDate', 'timestamp', 'Start Date (Select the start date and time for the game) ex: 2019-03-01 00:00:00', 1, 1, 1, 0, 1, 1, '\"\"', 4),
(47, 7, 'endDate', 'timestamp', 'End Date (Select the end date and time for the game) example: 2019-03-01 00:00:00', 1, 1, 1, 0, 1, 1, '\"\"', 4),
(48, 7, 'imageUrl', 'image', 'Image ( The size of the image should be 7160px x 4000px at 150DPI for clarity)', 1, 1, 1, 1, 1, 1, '\"\"', 4),
(49, 7, 'xCords', 'number', 'Winning X-Cordinate (The X Coordinate for the winning position)', 1, 0, 1, 0, 1, 1, '\"\"', 4),
(50, 7, 'yCords', 'number', 'Winning Y-Cordinate (The X Coordinate for the winning position)', 1, 0, 1, 0, 1, 1, '\"\"', 4),
(51, 7, 'winnerDate', 'timestamp', 'Timestamp For Winner Declaration Date example: 2019-03-01 00:00:00', 1, 0, 1, 0, 1, 1, '\"\"', 4),
(52, 7, 'tieDate', 'timestamp', 'Date Till When A Tie Should Be Resolved example: 2019-03-01 00:00:00', 1, 0, 1, 0, 1, 1, '\"\"', 4),
(53, 7, 'runnerUps', 'number', 'The number of runnerups needed for this game', 1, 0, 1, 0, 1, 1, '\"\"', 4),
(54, 8, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '\"\"', 1),
(55, 8, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '\"\"', 2),
(56, 8, 'updated_at', 'timestamp', 'updated_at', 0, 0, 1, 0, 0, 0, '\"\"', 3),
(57, 8, 'user_id', 'number', 'User ID', 1, 1, 0, 0, 0, 0, '\"\"', 3),
(58, 8, 'title', 'text', 'Title', 1, 1, 1, 0, 0, 0, '\"\"', 3),
(59, 8, 'description', 'text_area', 'Description', 0, 0, 0, 0, 0, 0, '\"\"', 3),
(60, 8, 'products', 'code', 'Products', 0, 0, 0, 0, 0, 0, '\"\"', 3),
(61, 8, 'productsTotal', 'text', 'Sub Total', 1, 1, 1, 0, 0, 0, '\"\"', 3),
(62, 8, 'discount', 'text', 'Discount', 1, 1, 1, 0, 0, 0, '\"\"', 3),
(63, 8, 'tax', 'text', 'Tax', 1, 1, 1, 0, 0, 0, '\"\"', 3),
(64, 8, 'creditAdjusted', 'text', 'Credit Adjusted', 1, 1, 1, 0, 0, 0, '\"\"', 3),
(65, 8, 'total', 'text', 'Grand Total', 1, 1, 1, 0, 0, 0, '\"\"', 3),
(66, 8, 'payment_status', 'text', 'Payment Status', 1, 1, 1, 0, 0, 0, '\"\"', 3),
(67, 9, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '\"\"', 1),
(68, 9, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '\"\"', 2),
(69, 9, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '\"\"', 3),
(70, 9, 'name', 'text', 'Product Name', 1, 1, 1, 1, 1, 1, '\"\"', 2),
(71, 9, 'slug', 'text', 'Slug For URL', 1, 1, 1, 1, 1, 1, '\"\"', 2),
(72, 9, 'description', 'text_area', 'Description', 1, 0, 1, 1, 1, 1, '\"\"', 2),
(73, 9, 'price_slab1', 'number', 'Price Slab 1 - Amount Only', 1, 1, 1, 1, 1, 1, '\"\"', 2),
(74, 9, 'discount_slab1', 'number', 'Discount Slab 1', 1, 0, 1, 1, 1, 1, '\"\"', 2),
(75, 9, 'discount_slab2', 'number', 'Discount Slab 2', 1, 0, 1, 1, 1, 1, '\"\"', 2),
(76, 9, 'product_image1', 'image', 'Product Image 1', 1, 0, 1, 1, 1, 1, '\"\"', 2),
(77, 9, 'product_image2', 'image', 'Product Image 2', 1, 0, 1, 1, 1, 1, '\"\"', 2),
(78, 9, 'product_image3', 'image', 'Product Image 3', 1, 0, 1, 1, 1, 1, '\"\"', 2),
(79, 9, 'product_image4', 'image', 'Product Image 4', 1, 0, 1, 1, 1, 1, '\"\"', 2),
(80, 9, 'product_image5', 'image', 'Product Image 5', 1, 0, 1, 1, 1, 1, '\"\"', 2),
(81, 10, 'heroImage', 'image', 'Hompage Hero Image (Size 1920px * 1080px)', 1, 1, 1, 1, 1, 1, '\"\"', 1),
(82, 10, 'heroContent', 'text', 'Win A Macbook Pro® This Week', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(83, 10, 'videoOneTitle', 'text', 'Title For The First Video', 1, 1, 1, 1, 1, 1, '\"\"', 1),
(84, 10, 'videoOne', 'text', 'YouTube ID Of The First Video', 1, 1, 1, 1, 1, 1, '\"\"', 1),
(85, 10, 'videoTwoTitle', 'text', 'Title For The Second Video', 1, 1, 1, 1, 1, 1, '\"\"', 1),
(86, 10, 'videoTwo', 'text', 'YouTube ID Of The Second Video', 1, 1, 1, 1, 1, 1, '\"\"', 1),
(87, 10, 'videoThreeTitle', 'text', 'Title For The Third Video', 1, 1, 1, 1, 1, 1, '\"\"', 1),
(88, 10, 'videoThree', 'text', 'YouTube ID Of The Third Video', 1, 1, 1, 1, 1, 1, '\"\"', 1),
(89, 1, 'lastName', 'text', 'Last Name', 1, 1, 1, 1, 1, 1, '\"\"', 1),
(90, 1, 'gender', 'text', 'Gender', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(91, 1, 'countryCode', 'integer', 'Phone Country Code', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(92, 1, 'mobile', 'integer', 'Phone Number', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(93, 1, 'address1', 'text', 'Address Line 1', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(94, 1, 'address2', 'text', 'Address Line 2', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(95, 1, 'city', 'text', 'City', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(96, 1, 'state', 'text', 'State', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(97, 1, 'zip', 'text', 'Zip', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(98, 1, 'country', 'text', 'Country', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(99, 1, 'initialDiscount', 'number', 'Credits', 1, 0, 1, 1, 1, 1, '\"\"', 1),
(106, 7, 'description', 'text_area', 'Enter the description for the game', 1, 0, 1, 1, 1, 1, '\"\"', 4),
(107, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(108, 12, 'section', 'text', 'Section', 1, 1, 1, 1, 1, 1, '{}', 2),
(109, 12, 'question', 'text', 'Question', 1, 1, 1, 1, 1, 1, '{}', 3),
(110, 12, 'answer', 'text_area', 'Answer', 1, 1, 1, 1, 1, 1, '{}', 4),
(111, 12, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(112, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(113, 9, 'product_belongstomany_category_relationship', 'relationship', 'categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Categories\",\"table\":\"categories\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"category_products\",\"pivot\":\"1\",\"taggable\":null}', 4);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2019-04-10 05:26:25', '2019-04-10 05:26:25'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-04-10 05:26:25', '2019-04-10 05:26:25'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-04-10 05:26:25', '2019-04-10 05:26:25'),
(4, 'global_settings', 'global_settings', 'Global Setting', 'Global Settings', '', 'App\\GlobalSettings', NULL, '', '', 1, 0, NULL, '2019-04-10 05:26:26', '2019-04-10 05:26:26'),
(5, 'dicount_coupons', 'dicount_coupons', 'Discount Coupon', 'Discount Coupons', '', 'App\\DicountCoupon', NULL, '', '', 1, 0, NULL, '2019-04-10 05:26:26', '2019-04-10 05:26:26'),
(6, 'categories', 'categories', 'Categories', 'Categories', '', 'App\\Categories', NULL, '', '', 1, 0, NULL, '2019-04-10 05:26:26', '2019-04-10 05:26:26'),
(7, 'games', 'games', 'Games', 'Games', '', 'App\\Game', NULL, '', '', 1, 0, NULL, '2019-04-10 05:26:27', '2019-04-10 05:26:27'),
(8, 'invoices', 'invoices', 'Invoices', 'Invoices', '', 'App\\Invoice', NULL, '', '', 1, 0, NULL, '2019-04-10 05:26:27', '2019-04-10 05:26:27'),
(9, 'products', 'products', 'Product', 'Products', '', 'App\\Product', NULL, '', '', 1, 0, NULL, '2019-04-10 05:26:27', '2019-04-10 05:26:27'),
(10, 'homepage_settings', 'homepage_settings', 'HomepageSetting', 'HomepageSettings', '', 'App\\HomepageSetting', NULL, '', '', 1, 0, NULL, '2019-04-10 05:26:27', '2019-04-10 05:26:27'),
(12, 'faqs', 'faqs', 'Faq', 'Faqs', NULL, 'App\\Faq', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-04-13 14:35:41', '2019-04-13 14:35:41');

-- --------------------------------------------------------

--
-- Table structure for table `declare_winners`
--

CREATE TABLE `declare_winners` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subTitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `winnerImage1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `winnerImage2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `winnerImage3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `declare_winners`
--

INSERT INTO `declare_winners` (`id`, `title`, `subTitle`, `slug`, `description`, `winnerImage1`, `winnerImage2`, `winnerImage3`, `video`, `created_at`, `updated_at`) VALUES
(1, 'Winner Of Game 1', 'John Doe Won - Porce 911', 'winner-game-1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin facilisis vel nibh vitae commodo. Aliquam maximus commodo dui, porta commodo nisi tempor at. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti. Integer vehicula ante sapien, sit amet volutpat elit facilisis sit amet. Aliquam ut lacinia orci. Fusce vulputate justo id viverra maximus. Maecenas eget condimentum purus.', 'https://win.test/storage/March2019/homepage-hero-image.jpg', 'https://win.test/storage/March2019/homepage-hero-image.jpg', 'https://win.test/storage/March2019/homepage-hero-image.jpg', 'DjIe6U4TyG0', NULL, NULL),
(2, 'Winner Of Game  01', 'The winner take iphone XS', 'Winner-game-01', 'The winner of first game takes iphone XS..\r\n\r\nYou could be the next winner, therefore go and use your skills..', 'phpg8fuMf.png', NULL, NULL, '3cYBfuphkuE', '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(3, 'Winner Of Game 2', 'Bob won - Mac book', 'Winner-game-02', 'The winner of second game takes Macbook.. You could be the next winner, therefore go and use your skills.', 'phpSBUTLs.png', NULL, NULL, 'QupolOeTLPg', '2019-05-15 15:49:47', '2019-05-15 15:49:47');

-- --------------------------------------------------------

--
-- Table structure for table `dicount_coupons`
--

CREATE TABLE `dicount_coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'coupon',
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'subtotal',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dicount_coupons`
--

INSERT INTO `dicount_coupons` (`id`, `name`, `value`, `type`, `target`, `created_at`, `updated_at`) VALUES
(1, 'Disc10', 10, 'coupon', 'subtotal', NULL, NULL),
(2, 'Disc20', 20, 'coupon', 'subtotal', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `section` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `section`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(1, 'HOW DOES IT WORK?', 'How to buy tickets and enter the competion?', 'Simply add tickets to your basket for the prizes you\'d like to win, play Spot the Ball to enter and then proceed to checkout. You can buy tickets from our website at www.winyourdestiny.com ', NULL, NULL),
(2, 'HOW DOES IT WORK?', 'How is the winner decided?', 'At the begiinning of the competition, the \"Spot the Ball\" picture on which you would choose your coordinates is shown to a judging panel. In the presence of an independent lawyer they mark where they think, in their professional opinion, the centre of the ball should be. The winner is simply the person who is closest to the judged position.\n', NULL, NULL),
(3, 'WHO ARE WE?', 'Where is WYUD based?', 'Our Head Office address is 12 Sheryl Drive, Shrewsbury, US, and we can be contacted  via email at info@winyourdestiny.com. Tweet us @Winyourdestiny or get in touch through our Facebook page - facebook.com/WYUD.\n', NULL, NULL),
(4, 'WHO ARE WE?', 'Who are WYUD?', 'WYUD (Win Your Destiny LLC) has been running competitions since 2019. Every month there are guaranteed Product Competition. We\'re a US-based entity , and are therefore required to adhere to the highest legal and audit standards.', NULL, NULL),
(5, 'WINNERS AND PRIZES?', 'Are there winners of every product?', 'Entrants can choose from the multiple products in the competition line-up. At the end of each competition, one product is won. This is determined by matching the winning coordinates from the Spot the Ball game (as chosen by our panel of expert judges) to all the entries.', NULL, NULL),
(6, 'WINNERS AND PRIZES?', 'How can I get further information?', 'Feel free to  email us at info@winyourdestiny.com and we will get straight back to you.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `footer_id` int(11) NOT NULL,
  `content` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`footer_id`, `content`, `created_at`, `updated_at`) VALUES
(2, '<footer class=\"page-footer d-print-none font-small wd-footer-wrapper pt-1\"><!-- Footer Links -->\r\n<div class=\"container text-center text-md-left\"><!-- Footer links -->\r\n<div class=\"row text-center text-md-left mt-3 \"><!-- Grid column -->\r\n<div class=\"col-md-3 wd-footer-extra-padding mx-auto mt-2 mb-1\">\r\n<div class=\"wd-footer-extra-padding\">\r\n<p><a href=\"https://win.psweb.in/winners\">Winners</a></p>\r\n<p><a href=\"https://win.psweb.in/faqs\">Frequently Asked Questions</a></p>\r\n<p><a href=\"#\">Reviews</a></p>\r\n<p><a href=\"/privacy-policy\">Privacy Policy</a></p>\r\n<p><a href=\"/terms-conditions\">Terms and Conditions</a></p>\r\n<p><a href=\"/refund-policy\">Refund Policy</a></p>\r\n</div>\r\n</div>\r\n<!-- Grid column --> <!-- Grid column -->\r\n<div class=\"col-md-4 mx-auto mb-1\"><a href=\"#\"> <img src=\"https://win.psweb.in/public/images/trust_pilot_white.png\" alt=\"trust pilot reviews\" /> </a></div>\r\n<!-- Grid column --> <!-- Grid column -->\r\n<div class=\"col-md-4 col-lg-3 col-xl-3 mx-auto mt-2\">\r\n<h6 class=\"text-uppercase mb-2 wd-poppins\">We Are Social</h6>\r\n<!-- Social buttons -->\r\n<div class=\"text-md-left text-center mb-3\">&nbsp;</div>\r\n<h6 class=\"text-uppercase  mt-4 mb-4 wd-poppins\">We Accept</h6>\r\n<img class=\"paypalLogo\" src=\"https://win.psweb.in/public/images/paypal-icon.png\" alt=\"credit cards accepted\" /></div>\r\n<!-- Grid column --></div>\r\n<!-- Footer links --></div>\r\n<!-- Footer Links --> <!-- Grid row -->\r\n<div class=\"wd-copyright-section mt-4\">\r\n<div class=\"container\">\r\n<div class=\"row align-items-center pt-3 \"><!-- Grid column -->\r\n<div class=\"col-md-4\"><!--Copyright-->\r\n<p class=\"text-center text-md-left\">&copy; Copyright 2016-2017 WinYourDestinty.com</p>\r\n</div>\r\n<!-- Grid column --> <!-- Grid column -->\r\n<div class=\"col-md-5 offset-md-3\">\r\n<p class=\"text-center text-md-right\">info@winyourdestiny.com</p>\r\n</div>\r\n<!-- Grid column --></div>\r\n</div>\r\n</div>\r\n<!-- Grid row -->\r\n<div id=\"fb-root\" class=\" fb_reset\">\r\n<div style=\"position: absolute; top: -10000px; width: 0px; height: 0px;\">\r\n<div><iframe id=\"fb_xdm_frame_https\" style=\"border: none;\" tabindex=\"-1\" title=\"Facebook Cross Domain Communication Frame\" src=\"https://staticxx.facebook.com/connect/xd_arbiter.php?version=44#channel=f2f9955fccab134&amp;origin=https%3A%2F%2Fwin.psweb.in\" name=\"fb_xdm_frame_https\" frameborder=\"0\" scrolling=\"no\" allowfullscreen=\"true\" aria-hidden=\"true\"></iframe></div>\r\n<div>&nbsp;</div>\r\n</div>\r\n</div>\r\n</footer>', '2019-07-01 08:36:28', '2019-07-01 08:36:28');

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `startDate` datetime NOT NULL,
  `endDate` datetime NOT NULL,
  `xCords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yCords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imageUrl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageWidth` int(11) DEFAULT NULL,
  `imageHeight` int(11) DEFAULT NULL,
  `imageMultiplier` int(11) NOT NULL DEFAULT '8',
  `winnerDate` datetime NOT NULL,
  `tieDate` datetime NOT NULL,
  `videoURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `runnerUps` int(11) NOT NULL DEFAULT '20',
  `resultStatus` tinyint(1) NOT NULL DEFAULT '0',
  `declareWinner_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `name`, `startDate`, `endDate`, `xCords`, `yCords`, `imageUrl`, `imageWidth`, `imageHeight`, `imageMultiplier`, `winnerDate`, `tieDate`, `videoURL`, `description`, `runnerUps`, `resultStatus`, `declareWinner_id`, `created_at`, `updated_at`) VALUES
(5, 'Game 1', '2019-04-01 00:00:00', '2019-04-30 11:30:00', '3248', '248', 'games/April2019/M4pO4cu8BJO8Qf04gMMg.png', 1200, 800, 8, '2019-04-30 13:30:00', '2019-04-30 12:30:00', '', 'Contest to judge your skills', 10, 1, 2, NULL, '2019-04-30 13:02:51'),
(6, 'Game 2', '2019-04-30 11:30:00', '2019-07-15 11:30:00', '4992', '1688', 'games/April2019/WFwuyrhJBf5Ilp256oye.jpeg', NULL, NULL, 8, '2019-05-16 13:30:00', '2019-05-16 12:30:00', NULL, 'Contest to judge your skills', 3, 1, 3, '2019-04-29 03:26:58', '2019-05-15 15:49:47'),
(7, 'Game 3', '2019-05-15 11:30:00', '2019-06-12 00:00:00', 'MjMwMA==', 'MTkyMA==', 'games/May2019/ZyGg1UF1Ll4ot8vPPoTx.jpeg', NULL, NULL, 8, '2019-06-01 13:30:00', '2019-06-01 12:30:00', NULL, 'Contest to judge your skills', 5, 0, NULL, '2019-05-15 15:44:47', '2019-05-15 16:01:29');

-- --------------------------------------------------------

--
-- Table structure for table `global_settings`
--

CREATE TABLE `global_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `userDiscount` int(11) NOT NULL DEFAULT '2',
  `salesTax` int(11) NOT NULL DEFAULT '10',
  `maxTickets` int(11) NOT NULL DEFAULT '100',
  `creditNewReferal` double(8,2) NOT NULL DEFAULT '1.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `global_settings`
--

INSERT INTO `global_settings` (`id`, `userDiscount`, `salesTax`, `maxTickets`, `creditNewReferal`, `created_at`, `updated_at`) VALUES
(1, 45, 7, 100, 10.00, NULL, '2019-06-04 16:53:45');

-- --------------------------------------------------------

--
-- Table structure for table `homepage_settings`
--

CREATE TABLE `homepage_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `heroImage` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `heroContent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoOneTitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoOne` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoTwoTitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoTwo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoThreeTitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoThree` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `videoFourTitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoFour` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoFiveTitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoFive` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoSixTitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoSix` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homepage_settings`
--

INSERT INTO `homepage_settings` (`id`, `heroImage`, `heroContent`, `videoOneTitle`, `videoOne`, `videoTwoTitle`, `videoTwo`, `videoThreeTitle`, `videoThree`, `created_at`, `updated_at`, `videoFourTitle`, `videoFour`, `videoFiveTitle`, `videoFive`, `videoSixTitle`, `videoSix`) VALUES
(1, 'homepage_settings/June2019/KYTXuDVlIoZW7HOhcox6.jpg', 'Coming Soon !!!', 'Winner of Game 1a', 'zcz0fOkYVH8', 'Winner of Game 2a', 'zcz0fOkYVH8', 'Winner of Game 3a', 'zcz0fOkYVH8', NULL, '2019-06-21 11:21:39', 'Winner of Game 4', 'zcz0fOkYVH8', 'Winner of Game 5', 'zcz0fOkYVH8', 'Wi6ner of Game 6', 'zcz0fOkYVH8');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `products` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `productsTotal` double(8,2) NOT NULL,
  `discount` double(8,2) NOT NULL,
  `tax` double(8,2) NOT NULL,
  `creditAdjusted` double(8,2) NOT NULL,
  `total` double(8,2) NOT NULL,
  `paymentMethod` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `title`, `description`, `products`, `productsTotal`, `discount`, `tax`, `creditAdjusted`, `total`, `paymentMethod`, `payment_status`, `user_id`, `created_at`, `updated_at`) VALUES
(1145, 'Order ID:75', 'Invoice For Order ID:75', '[{\"name\":\"Car17\",\"price\":\"30.00\",\"qty\":\"1\"}]', 30.00, 5.00, 5.00, 0.00, 30.00, 'PayPal', 'Completed', 1, '2019-02-01 06:58:32', NULL),
(1146, 'Order ID:75', 'Invoice For Order ID:75', '[{\"name\":\"Car17\",\"price\":\"30.00\",\"qty\":\"1\"}]', 30.00, 5.00, 5.00, 0.00, 30.00, 'PayPal', 'Completed', 1, '2019-02-01 06:58:32', NULL),
(1147, 'Order ID:75', 'Invoice For Order ID:75', '[{\"name\":\"Car17\",\"price\":\"30.00\",\"qty\":\"1\"}]', 30.00, 5.00, 5.00, 0.00, 30.00, 'PayPal', 'Pending', 2, '2019-02-01 06:58:32', NULL),
(1148, 'Order ID:75', 'Invoice For Order ID:75', '[{\"name\":\"Car17\",\"price\":\"30.00\",\"qty\":\"1\"}]', 30.00, 5.00, 5.00, 0.00, 30.00, 'PayPal', 'Pending', 1, '2019-02-01 06:58:32', NULL),
(1215, 'Order ID:1215', 'Invoice for the Order ID: 1215', '[{\"name\":\"iphone XS\",\"price\":\"2.88\",\"qty\":\"5\"}]', 14.40, 0.00, 1.01, 2.00, 13.41, 'PayPal', 'Completed', 5, '2019-04-25 05:16:42', '2019-04-25 05:17:32'),
(1216, 'Order ID:1216', 'Invoice for the Order ID: 1216', '[{\"name\":\"iphone XS\",\"price\":\"2.73\",\"qty\":\"10\"}]', 27.30, 0.00, 1.91, 0.00, 29.21, 'PayPal', 'Completed', 5, '2019-04-25 06:22:07', '2019-04-25 06:22:50'),
(1217, 'Order ID:1217', 'Invoice for the Order ID: 1217', '[{\"name\":\"iphone XS\",\"price\":\"3.00\",\"qty\":\"1\"}]', 3.00, 0.00, 0.21, 0.00, 3.21, 'PayPal', 'Completed', 5, '2019-04-25 12:07:40', '2019-04-25 12:08:36'),
(1218, 'Order ID:1218', 'Invoice for the Order ID: 1218', '[{\"name\":\"iphone XS\",\"price\":\"3.00\",\"qty\":1}]', 3.00, 0.00, 0.21, 2.00, 1.21, 'PayPal', 'Completed', 6, '2019-04-25 13:38:22', '2019-04-25 13:39:10'),
(1219, 'Order ID:1219', 'Invoice for the Order ID: 1219', '[{\"name\":\"iPad Air\",\"price\":\"2.91\",\"qty\":\"5\"}]', 14.55, 2.91, 0.81, 0.00, 12.45, NULL, 'Pending', 6, '2019-04-25 13:42:51', '2019-04-25 13:42:51'),
(1220, 'Order ID:1220', 'Invoice for the Order ID: 1220', '[{\"name\":\"Canon DSLR Camera\",\"price\":\"1.00\",\"qty\":\"1\"},{\"name\":\"iWatch\",\"price\":\"2.00\",\"qty\":\"1\"}]', 3.00, 0.60, 0.17, 0.00, 2.57, 'PayPal', 'Completed', 6, '2019-04-25 13:58:27', '2019-04-25 13:59:17'),
(1221, 'Order ID:1221', 'Invoice for the Order ID: 1221', '[{\"name\":\"iphone XS\",\"price\":\"3.00\",\"qty\":\"1\"}]', 3.00, 0.00, 0.21, 0.00, 3.21, 'PayPal', 'Completed', 6, '2019-04-25 14:00:34', '2019-04-25 14:00:56'),
(1222, 'Order ID:1222', 'Invoice for the Order ID: 1222', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":\"1\"}]', 1.00, 0.00, 0.07, 1.07, 0.00, 'Credit Adjusted', 'Completed', 9, '2019-04-26 14:54:03', '2019-04-26 14:54:03'),
(1223, 'Order ID:1223', 'Invoice for the Order ID: 1223', '[{\"name\":\"Beat Headphone\",\"price\":\"1.00\",\"qty\":\"1\"},{\"name\":\"MacBook Pro\",\"price\":\"1.00\",\"qty\":\"1\"}]', 2.00, 0.00, 0.14, 2.14, 0.00, 'Credit Adjusted', 'Completed', 9, '2019-04-26 14:58:36', '2019-04-26 14:58:36'),
(1224, 'Order ID:1224', 'Invoice for the Order ID: 1224', '[{\"name\":\"Canon DSLR Camera\",\"price\":\"1.00\",\"qty\":2}]', 2.00, 0.00, 0.14, 2.14, 0.00, 'Credit Adjusted', 'Completed', 9, '2019-04-26 14:59:56', '2019-04-26 14:59:56'),
(1225, 'Order ID:1225', 'Invoice for the Order ID: 1225', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":\"1\"}]', 1.00, 0.00, 0.07, 1.07, 0.00, 'Credit Adjusted', 'Completed', 9, '2019-04-26 15:00:30', '2019-04-26 15:00:30'),
(1226, 'Order ID:1226', 'Invoice for the Order ID: 1226', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":\"1\"},{\"name\":\"Beat Headphone\",\"price\":\"0.95\",\"qty\":\"5\"}]', 5.75, 0.00, 0.40, 6.15, 0.00, 'Credit Adjusted', 'Completed', 5, '2019-05-04 08:34:00', '2019-05-04 08:34:00'),
(1227, 'Order ID:1227', 'Invoice for the Order ID: 1227', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":\"1\"}]', 1.00, 0.00, 0.07, 1.07, 0.00, 'Credit Adjusted', 'Completed', 5, '2019-05-05 05:49:59', '2019-05-05 05:49:59'),
(1228, 'Order ID:1228', 'Invoice for the Order ID: 1228', '[{\"name\":\"iphone XS\",\"price\":\"0.95\",\"qty\":\"5\"}]', 4.75, 0.00, 0.33, 5.08, 0.00, 'Credit Adjusted', 'Completed', 5, '2019-05-12 12:11:28', '2019-05-12 12:11:28'),
(1229, 'Order ID:1229', 'Invoice for the Order ID: 1229', '[{\"name\":\"iphone XS\",\"price\":\"0.95\",\"qty\":\"5\"}]', 4.75, 0.00, 0.33, 2.00, 3.08, 'PayPal', 'Completed', 24, '2019-05-22 05:31:09', '2019-05-22 05:32:47'),
(1230, 'Order ID:1230', 'Invoice for the Order ID: 1230', '[{\"name\":\"iphone XS\",\"price\":\"0.95\",\"qty\":\"5\"}]', 4.75, 0.00, 0.33, 0.00, 5.08, 'PayPal', 'Completed', 24, '2019-05-22 05:42:54', '2019-05-22 05:43:36'),
(1231, 'Order ID:1231', 'Invoice for the Order ID: 1231', '[{\"name\":\"Beat Headphone\",\"price\":\"1.00\",\"qty\":\"1\"},{\"name\":\"Canon DSLR Camera\",\"price\":\"1.00\",\"qty\":\"1\"},{\"name\":\"MacBook Pro\",\"price\":\"1.00\",\"qty\":\"1\"}]', 3.00, 0.00, 0.21, 3.21, 0.00, 'Credit Adjusted', 'Completed', 5, '2019-05-28 14:05:05', '2019-05-28 14:05:05'),
(1232, 'Order ID:1232', 'Invoice for the Order ID: 1232', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":1}]', 1.00, 0.00, 0.07, 1.07, 0.00, 'Credit Adjusted', 'Completed', 28, '2019-05-30 07:07:23', '2019-05-30 07:07:23'),
(1233, 'Order ID:1233', 'Invoice for the Order ID: 1233', '[{\"name\":\"Beat Headphone\",\"price\":\"0.95\",\"qty\":\"5\"}]', 4.75, 0.00, 0.33, 5.08, 0.00, 'Credit Adjusted', 'Completed', 28, '2019-05-31 07:03:26', '2019-05-31 07:03:26'),
(1234, 'Order ID:1234', 'Invoice for the Order ID: 1234', '[{\"name\":\"iWatch\",\"price\":\"0.90\",\"qty\":\"10\"}]', 9.00, 0.00, 0.63, 6.00, 3.63, NULL, 'Pending', 28, '2019-05-31 08:58:32', '2019-05-31 08:58:32'),
(1235, 'Order ID:1235', 'Invoice for the Order ID: 1235', '[{\"name\":\"iphone XS\",\"price\":\"0.90\",\"qty\":\"10\"}]', 9.00, 0.90, 0.57, 0.00, 8.67, NULL, 'Pending', 28, '2019-05-31 09:36:39', '2019-05-31 09:36:39'),
(1236, 'Order ID:1236', 'Invoice for the Order ID: 1236', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":\"1\"}]', 1.00, 0.00, 0.07, 1.07, 0.00, 'Credit Adjusted', 'Completed', 35, '2019-06-04 16:58:22', '2019-06-04 16:58:22'),
(1237, 'Order ID:1237', 'Invoice for the Order ID: 1237', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":\"1\"}]', 1.00, 0.00, 0.07, 0.00, 1.07, NULL, 'Pending', 28, '2019-06-06 08:47:32', '2019-06-06 08:47:32'),
(1238, 'Order ID:1238', 'Invoice for the Order ID: 1238', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":\"1\"}]', 1.00, 0.00, 0.07, 1.07, 0.00, 'Credit Adjusted', 'Completed', 38, '2019-06-06 09:06:07', '2019-06-06 09:06:07'),
(1239, 'Order ID:1239', 'Invoice for the Order ID: 1239', '[{\"name\":\"MacBook Pro\",\"price\":\"1.00\",\"qty\":\"1\"}]', 1.00, 0.00, 0.07, 1.07, 0.00, 'Credit Adjusted', 'Completed', 38, '2019-06-06 09:25:25', '2019-06-06 09:25:25'),
(1240, 'Order ID:1240', 'Invoice for the Order ID: 1240', '[{\"name\":\"MacBook Pro\",\"price\":\"1.00\",\"qty\":2}]', 2.00, 0.00, 0.14, 2.14, 0.00, 'Credit Adjusted', 'Completed', 38, '2019-06-06 09:27:49', '2019-06-06 09:27:49'),
(1241, 'Order ID:1241', 'Invoice for the Order ID: 1241', '[{\"name\":\"MacBook Pro\",\"price\":\"1.00\",\"qty\":\"1\"}]', 1.00, 0.00, 0.07, 1.07, 0.00, 'Credit Adjusted', 'Completed', 40, '2019-06-10 16:12:15', '2019-06-10 16:12:15'),
(1242, 'Order ID:1242', 'Invoice for the Order ID: 1242', '[{\"name\":\"Beat Headphone\",\"price\":\"1.00\",\"qty\":\"1\"}]', 1.00, 0.00, 0.07, 1.07, 0.00, 'Credit Adjusted', 'Completed', 40, '2019-06-10 16:13:29', '2019-06-10 16:13:29'),
(1243, 'Order ID:1243', 'Invoice for the Order ID: 1243', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":3}]', 3.00, 0.00, 0.00, 3.00, 3.00, NULL, 'Pending', 5, '2019-06-22 06:14:58', '2019-06-22 06:14:58'),
(1244, 'Order ID:1244', 'Invoice for the Order ID: 1244', '[{\"name\":\"iphone XS\",\"price\":\"0.90\",\"qty\":\"10\"}]', 9.00, 0.00, 0.00, 9.00, 9.00, NULL, 'Pending', 5, '2019-06-22 06:21:28', '2019-06-22 06:21:28'),
(1245, 'Order ID:1245', 'Invoice for the Order ID: 1245', '[{\"name\":\"iphone XS\",\"price\":\"0.95\",\"qty\":\"5\"}]', 4.75, 0.00, 0.00, 4.75, 4.75, NULL, 'Pending', 5, '2019-06-22 06:22:21', '2019-06-22 06:22:21'),
(1246, 'Order ID:1246', 'Invoice for the Order ID: 1246', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":2}]', 2.00, 0.00, 0.00, 2.00, 2.00, NULL, 'Pending', 5, '2019-06-23 06:30:29', '2019-06-23 06:30:29'),
(1247, 'Order ID:1247', 'Invoice for the Order ID: 1247', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":2}]', 2.00, 0.00, 0.00, 2.00, 2.00, NULL, 'Pending', 5, '2019-06-28 15:18:17', '2019-06-28 15:18:17'),
(1248, 'Order ID:1248', 'Invoice for the Order ID: 1248', '[{\"name\":\"iphone XS\",\"price\":\"1.00\",\"qty\":4}]', 4.00, 0.00, 0.00, 4.00, 4.00, NULL, 'Pending', 5, '2019-06-28 16:50:50', '2019-06-28 16:50:50');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-04-13 14:34:28', '2019-04-13 14:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-03-29 11:36:41', '2019-03-29 11:36:41', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2019-03-29 11:36:41', '2019-03-29 11:36:41', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2019-03-29 11:36:41', '2019-03-29 11:36:41', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-03-29 11:36:41', '2019-03-29 11:36:41', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2019-03-29 11:36:41', '2019-03-29 11:36:41', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2019-03-29 11:36:41', '2019-03-29 11:36:41', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2019-03-29 11:36:41', '2019-03-29 11:36:41', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2019-03-29 11:36:41', '2019-03-29 11:36:41', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2019-03-29 11:36:41', '2019-03-29 11:36:41', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2019-03-29 11:36:41', '2019-03-29 11:36:41', 'voyager.settings.index', NULL),
(11, 1, 'GlobalSettings', '', '_self', 'voyager-sun', NULL, NULL, 8, '2019-03-29 11:36:42', '2019-03-29 11:36:42', 'voyager.global_settings.index', NULL),
(12, 1, 'Discount Coupons', '', '_self', 'voyager-bell', NULL, NULL, 8, '2019-03-29 11:36:42', '2019-03-29 11:36:42', 'voyager.dicount_coupons.index', NULL),
(13, 1, 'Categories', '', '_self', 'voyager-wand', NULL, NULL, 8, '2019-03-29 11:36:42', '2019-03-29 11:36:42', 'voyager.categories.index', NULL),
(14, 1, 'Games', '', '_self', 'voyager-helm', NULL, NULL, 8, '2019-03-29 11:36:42', '2019-03-29 11:36:42', 'voyager.games.index', NULL),
(15, 1, 'Invoices', '', '_self', 'voyager-wallet', NULL, NULL, 8, '2019-03-29 11:36:42', '2019-03-29 11:36:42', 'voyager.invoices.index', NULL),
(16, 1, 'Products', '', '_self', 'voyager-basket', NULL, NULL, 8, '2019-03-29 11:36:43', '2019-03-29 11:36:43', 'voyager.products.index', NULL),
(17, 1, 'HomepageSettings', '', '_self', 'voyager-basket', NULL, NULL, 8, '2019-03-29 11:36:43', '2019-03-29 11:36:43', 'voyager.homepage_settings.index', NULL),
(18, 1, 'Winners', '/vendor/voyager/gravy', '_self', 'voyager-helm', '#000000', NULL, 15, '2019-03-29 11:54:42', '2019-03-29 11:54:42', NULL, ''),
(20, 1, 'Faqs', '', '_self', NULL, NULL, NULL, 16, '2019-04-13 14:35:41', '2019-04-13 14:35:41', 'voyager.faqs.index', NULL),
(21, 1, 'Reports', '/admin/reports', '_self', 'icon voyager-helm', '#000000', NULL, 17, '2019-05-24 12:33:28', '2019-05-24 12:56:01', NULL, ''),
(23, 1, 'Footer', '/admin/CustomizeFooter', '_self', NULL, '#000000', NULL, 18, '2019-07-01 08:33:47', '2019-07-01 08:33:47', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_01_05_170509_create_products_table', 1),
(24, '2019_01_16_133523_create_sessions_table', 1),
(25, '2019_01_17_192636_discount_coupons', 1),
(26, '2019_01_22_085255_create_categories_table', 1),
(27, '2019_01_22_173933_create_category_products_table', 1),
(28, '2019_01_25_084308_create_global_settings_table', 1),
(29, '2019_01_27_065718_create_invoices_table', 1),
(30, '2019_02_01_131244_create_referrals_table', 1),
(31, '2019_02_07_035004_create_declare_winners_table', 1),
(32, '2019_02_07_065004_create_games_table', 1),
(33, '2019_02_07_085052_create_pointers_table', 1),
(34, '2019_03_18_085325_create_homepage_settings_table', 1),
(35, '2019_03_28_090643_create_tie_breakers_table', 1),
(36, '2019_03_30_131116_create_winners_table', 1),
(37, '2019_04_10_041431_create_faqs_table', 1),
(38, '2019_06_14_065628_create_shoppingcart_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ravikumawat523@gmail.com', '$2y$10$h/dFoJZ0GNPw0f7uaNYAGO4cPxvaERGkz.YRIQDLLggrKe1IyErNS', '2019-05-22 04:10:31'),
('triloksingh2392+ts3@gmail.com', '$2y$10$lI97Pf4c9UK40S4UB/7DTeKHXe6kdB3W55I0xCZ/Ka7tIN7fSDD..', '2019-06-06 10:59:12'),
('triloksingh2392@gmail.com', '$2y$10$hFuQHAPIbwTCCT4qgFFUzOn.U.YkCEo3HXWlJWcpy.AjVEIrbOrq2', '2019-06-06 11:00:39');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(2, 'browse_bread', NULL, '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(3, 'browse_database', NULL, '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(4, 'browse_media', NULL, '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(5, 'browse_compass', NULL, '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(6, 'browse_menus', 'menus', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(7, 'read_menus', 'menus', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(8, 'edit_menus', 'menus', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(9, 'add_menus', 'menus', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(10, 'delete_menus', 'menus', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(11, 'browse_roles', 'roles', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(12, 'read_roles', 'roles', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(13, 'edit_roles', 'roles', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(14, 'add_roles', 'roles', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(15, 'delete_roles', 'roles', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(16, 'browse_users', 'users', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(17, 'read_users', 'users', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(18, 'edit_users', 'users', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(19, 'add_users', 'users', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(20, 'delete_users', 'users', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(21, 'browse_settings', 'settings', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(22, 'read_settings', 'settings', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(23, 'edit_settings', 'settings', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(24, 'add_settings', 'settings', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(25, 'delete_settings', 'settings', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(26, 'browse_global_settings', 'global_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(27, 'read_global_settings', 'global_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(28, 'edit_global_settings', 'global_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(29, 'add_global_settings', 'global_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(30, 'delete_global_settings', 'global_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(31, 'browse_dicount_coupons', 'dicount_coupons', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(32, 'read_dicount_coupons', 'dicount_coupons', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(33, 'edit_dicount_coupons', 'dicount_coupons', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(34, 'add_dicount_coupons', 'dicount_coupons', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(35, 'delete_dicount_coupons', 'dicount_coupons', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(36, 'browse_categories', 'categories', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(37, 'read_categories', 'categories', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(38, 'edit_categories', 'categories', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(39, 'add_categories', 'categories', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(40, 'delete_categories', 'categories', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(41, 'browse_games', 'games', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(42, 'read_games', 'games', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(43, 'edit_games', 'games', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(44, 'add_games', 'games', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(45, 'delete_games', 'games', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(46, 'browse_invoices', 'invoices', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(47, 'read_invoices', 'invoices', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(48, 'edit_invoices', 'invoices', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(49, 'add_invoices', 'invoices', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(50, 'delete_invoices', 'invoices', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(51, 'browse_products', 'products', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(52, 'read_products', 'products', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(53, 'edit_products', 'products', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(54, 'add_products', 'products', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(55, 'delete_products', 'products', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(56, 'browse_homepage_settings', 'homepage_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(57, 'read_homepage_settings', 'homepage_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(58, 'edit_homepage_settings', 'homepage_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(59, 'add_homepage_settings', 'homepage_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(60, 'delete_homepage_settings', 'homepage_settings', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(61, 'browse_faqs', 'faqs', '2019-04-13 14:35:41', '2019-04-13 14:35:41'),
(62, 'read_faqs', 'faqs', '2019-04-13 14:35:41', '2019-04-13 14:35:41'),
(63, 'edit_faqs', 'faqs', '2019-04-13 14:35:41', '2019-04-13 14:35:41'),
(64, 'add_faqs', 'faqs', '2019-04-13 14:35:41', '2019-04-13 14:35:41'),
(65, 'delete_faqs', 'faqs', '2019-04-13 14:35:41', '2019-04-13 14:35:41');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pointers`
--

CREATE TABLE `pointers` (
  `id` int(10) UNSIGNED NOT NULL,
  `xCords` double(8,2) NOT NULL,
  `yCords` double(8,2) NOT NULL,
  `distance` double(8,2) NOT NULL,
  `sessionID` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productID` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `game_id` int(10) UNSIGNED DEFAULT NULL,
  `stampId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stamped` tinyint(1) NOT NULL DEFAULT '0',
  `removed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pointers`
--

INSERT INTO `pointers` (`id`, `xCords`, `yCords`, `distance`, `sessionID`, `productID`, `user_id`, `invoice_id`, `game_id`, `stampId`, `stamped`, `removed`, `created_at`, `updated_at`) VALUES
(1, 2888.00, 3616.00, 1272.79, 'ojx7kx7i1LJUOMTMhgoteD6wZxPBpGSDUbAy83mg', 12, NULL, NULL, 5, 'id11', 1, 0, '2019-04-13 16:06:16', '2019-04-13 16:06:16'),
(8, 1600.00, 3472.00, 2134.19, 'jkrc1KYjSE9fAcrtpdn1Hga5FlFnBUnHbO05Zt0O', 12, NULL, NULL, 5, 'id101', 1, 0, '2019-04-14 13:48:40', '2019-04-14 13:49:16'),
(9, 4032.00, 3568.00, 1193.17, 'jkrc1KYjSE9fAcrtpdn1Hga5FlFnBUnHbO05Zt0O', 12, NULL, NULL, 5, 'id102', 1, 0, '2019-04-14 13:48:42', '2019-04-14 13:49:16'),
(10, 5632.00, 3520.00, 2363.43, 'jkrc1KYjSE9fAcrtpdn1Hga5FlFnBUnHbO05Zt0O', 12, NULL, NULL, 5, 'id103', 1, 0, '2019-04-14 13:48:43', '2019-04-14 13:49:16'),
(11, 1088.00, 2640.00, 2416.06, 'jkrc1KYjSE9fAcrtpdn1Hga5FlFnBUnHbO05Zt0O', 12, NULL, NULL, 5, 'id104', 1, 0, '2019-04-14 13:48:45', '2019-04-14 13:49:16'),
(12, 5392.00, 1496.00, 2141.89, 'jkrc1KYjSE9fAcrtpdn1Hga5FlFnBUnHbO05Zt0O', 12, NULL, NULL, 5, 'id105', 1, 0, '2019-04-14 13:48:47', '2019-04-14 13:49:16'),
(13, 6368.00, 2904.00, 2896.31, 'jkrc1KYjSE9fAcrtpdn1Hga5FlFnBUnHbO05Zt0O', 12, NULL, NULL, 5, 'id106', 1, 0, '2019-04-14 13:48:49', '2019-04-14 13:49:16'),
(16, 3744.00, 1424.00, 1103.32, 'oAAUgQYZ4TW0c0RXm92OHhrdq87xHLGWgMz5alfN', 12, NULL, NULL, 5, 'id11', 1, 0, '2019-04-15 15:16:20', '2019-04-15 15:16:20'),
(17, 3384.00, 904.00, 1600.21, 'oAAUgQYZ4TW0c0RXm92OHhrdq87xHLGWgMz5alfN', 12, NULL, NULL, 5, 'id12', 1, 0, '2019-04-15 15:16:21', '2019-04-15 15:16:21'),
(18, 3920.00, 2688.00, 1460.16, 'xHuiaBZPZDUwor8RjKUZhqBfehm8m4YDmFSJaRz0', 12, NULL, NULL, 5, 'id11', 1, 0, '2019-04-15 15:44:11', '2019-04-15 15:44:11'),
(19, 3608.00, 400.00, 2102.78, 'xQcIcExN5jAyqz5dsvbEw4X1tOSl8sllQHiRVJ66', 12, NULL, NULL, 5, 'id11', 1, 0, '2019-04-16 02:02:29', '2019-04-16 02:02:29'),
(20, 5640.00, 1032.00, 2595.12, 'FFFFv1gP02sNrOIuN7ZH21G6F0Iqs7GBPJlQycpi', 13, NULL, NULL, 5, 'id131', 1, 0, '2019-04-19 15:54:32', '2019-04-19 15:54:32'),
(22, 3384.00, 560.00, 1943.46, '4jIurziP6WjHCmPskJKeh87eIqkYofUolIKrSQTT', 12, NULL, NULL, 5, 'id121', 1, 0, '2019-04-20 14:44:21', '2019-04-20 14:48:24'),
(23, 3040.00, 3320.00, 1940.21, '4jIurziP6WjHCmPskJKeh87eIqkYofUolIKrSQTT', 13, NULL, NULL, 5, 'id131', 1, 0, '2019-04-20 16:29:38', '2019-04-20 16:29:38'),
(24, 2856.00, 992.00, 1639.76, 'h1ytnXOBhGxLJFsQdAMZU68OO2IWiMZbS9nOwSnx', 12, NULL, NULL, 5, 'id121', 1, 0, '2019-04-23 07:46:55', '2019-04-23 07:46:55'),
(26, 1976.00, 1064.00, 2093.96, 't2btIsLtmkcgurJUrbkWXbCGBHO2j9skfOz1dBpF', 12, NULL, NULL, 5, 'id121', 1, 0, '2019-04-23 14:40:12', '2019-04-23 14:40:12'),
(27, 2496.00, 1040.00, 1771.90, 'lZ33sP0o3ZPvLspsDd3YJhRkRorJGAOdOhdKSSEW', 16, NULL, NULL, 5, 'id161', 1, 0, '2019-04-24 12:04:00', '2019-04-24 12:04:00'),
(30, 3248.00, 248.00, 2266.06, 'kcFJuWQ7JKtzBSfRww7EEkKsYby5nFvFrGJaI4h2', 12, NULL, NULL, 5, 'id121', 1, 0, '2019-04-25 03:19:40', '2019-04-25 03:19:40'),
(32, 2504.00, 3712.00, 1568.74, 'TU4SpSNviUOFZ8q20jUwyqbloDXD0VMawtRzxS9a', 12, NULL, NULL, 5, 'id121', 1, 0, '2019-04-25 05:08:19', '2019-04-25 05:08:19'),
(33, 2584.00, 3648.00, 1020.45, '5QHpW3VBvMemfCdhOAMrzbFM5GE6ndMvJxyj4KLI', 12, 5, 1215, 5, 'id121', 1, 0, '2019-04-25 05:14:08', '2019-04-25 05:16:42'),
(34, 1504.00, 2880.00, 1300.86, '5QHpW3VBvMemfCdhOAMrzbFM5GE6ndMvJxyj4KLI', 12, 5, 1215, 5, 'id122', 1, 0, '2019-04-25 05:14:11', '2019-04-25 05:16:42'),
(35, 3424.00, 3656.00, 1921.70, '5QHpW3VBvMemfCdhOAMrzbFM5GE6ndMvJxyj4KLI', 12, 5, 1215, 5, 'id123', 1, 0, '2019-04-25 05:14:14', '2019-04-25 05:16:42'),
(36, 1488.00, 3488.00, 1040.40, '5QHpW3VBvMemfCdhOAMrzbFM5GE6ndMvJxyj4KLI', 12, 5, 1215, 5, 'id124', 1, 0, '2019-04-25 05:14:16', '2019-04-25 05:16:42'),
(37, 2888.00, 3368.00, 1515.55, '5QHpW3VBvMemfCdhOAMrzbFM5GE6ndMvJxyj4KLI', 12, 5, 1215, 5, 'id125', 1, 0, '2019-04-25 05:14:17', '2019-04-25 05:16:42'),
(38, 3784.00, 272.00, 536.54, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id121', 1, 0, '2019-04-25 06:20:11', '2019-04-25 06:22:07'),
(39, 3600.00, 512.00, 440.00, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id122', 1, 0, '2019-04-25 06:20:23', '2019-04-25 06:22:07'),
(40, 3112.00, 592.00, 369.91, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id123', 1, 0, '2019-04-25 06:20:34', '2019-04-25 06:22:07'),
(41, 2912.00, 632.00, 510.25, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id124', 1, 0, '2019-04-25 06:20:44', '2019-04-25 06:22:07'),
(42, 3384.00, 1040.00, 803.59, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id125', 1, 0, '2019-04-25 06:20:59', '2019-04-25 06:22:07'),
(43, 3528.00, 864.00, 676.65, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id126', 1, 0, '2019-04-25 06:21:03', '2019-04-25 06:22:07'),
(44, 3352.00, 400.00, 184.17, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id127', 1, 0, '2019-04-25 06:21:11', '2019-04-25 06:22:07'),
(45, 3160.00, 360.00, 142.44, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id128', 1, 0, '2019-04-25 06:21:20', '2019-04-25 06:22:07'),
(46, 3472.00, 280.00, 226.27, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id129', 1, 0, '2019-04-25 06:21:24', '2019-04-25 06:22:07'),
(47, 3296.00, 608.00, 363.19, '66HsL8OHjv1mlzaMXV5wFzGamCZGwJouY756Jj2o', 12, 5, 1216, 5, 'id1210', 1, 0, '2019-04-25 06:21:29', '2019-04-25 06:22:07'),
(48, 2176.00, 1960.00, 2019.93, 'EEpqo3OBrr3zhjlQ9V7AD6mJavoWjR3uxC9HFK4w', 13, NULL, NULL, 5, 'id131', 1, 0, '2019-04-25 07:10:59', '2019-04-25 07:10:59'),
(49, 2696.00, 392.00, 570.47, 'blLNFuA5Z6gnXb8nWrG8dr7vkkpSbTnvvtRoBfvw', 12, 5, 1217, 5, 'id121', 1, 0, '2019-04-25 12:07:24', '2019-04-25 12:07:40'),
(50, 2856.00, 384.00, 414.92, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 12, 6, 1219, 5, 'id121', 1, 0, '2019-04-25 13:32:31', '2019-04-25 13:44:44'),
(51, 2992.00, 368.00, 282.73, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 12, 6, 1219, 5, 'id122', 1, 0, '2019-04-25 13:33:12', '2019-04-25 13:44:44'),
(52, 2968.00, 240.00, 280.11, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 12, 6, 1219, 5, 'id122', 1, 0, '2019-04-25 13:33:13', '2019-04-25 13:44:44'),
(53, 2808.00, 264.00, 440.29, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 12, 6, 1219, 5, 'id122', 1, 0, '2019-04-25 13:33:14', '2019-04-25 13:44:44'),
(54, 2880.00, 144.00, 382.41, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 12, 6, 1219, 5, 'id122', 1, 0, '2019-04-25 13:33:16', '2019-04-25 13:44:44'),
(55, 3216.00, 368.00, 124.19, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 15, 6, 1219, 5, 'id151', 1, 0, '2019-04-25 13:41:27', '2019-04-25 13:44:44'),
(56, 3312.00, 336.00, 108.81, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 15, 6, 1219, 5, 'id152', 1, 0, '2019-04-25 13:41:36', '2019-04-25 13:44:44'),
(57, 3024.00, 328.00, 237.86, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 15, 6, 1219, 5, 'id153', 1, 0, '2019-04-25 13:41:43', '2019-04-25 13:44:44'),
(58, 3112.00, 352.00, 171.21, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 15, 6, 1219, 5, 'id154', 1, 0, '2019-04-25 13:41:55', '2019-04-25 13:44:44'),
(59, 3024.00, 552.00, 377.61, 'eYXmpJlce6fbpWSKdXz6tHJy1gXzg9aZsqiIudRo', 15, 6, 1219, 5, 'id155', 1, 0, '2019-04-25 13:42:10', '2019-04-25 13:44:44'),
(60, 3328.00, 664.00, 423.62, 'PUNmtaDv9tf0yciDbkDRicVaC98YHcIe4sTXzoSs', 21, 6, 1221, 5, 'id211', 1, 0, '2019-04-25 13:56:33', '2019-04-25 14:00:34'),
(61, 3616.00, 376.00, 389.63, '91777z7ZJnhKoyVE0jgTZLEIL5U9lLIxry59eqCe', 13, NULL, NULL, 5, 'id131', 1, 0, '2019-04-25 13:57:46', '2019-04-25 13:57:46'),
(62, 3160.00, 488.00, 255.62, 'PUNmtaDv9tf0yciDbkDRicVaC98YHcIe4sTXzoSs', 16, 6, 1221, 5, 'id161', 1, 0, '2019-04-25 13:57:47', '2019-04-25 14:00:34'),
(63, 2840.00, 816.00, 699.35, 'PUNmtaDv9tf0yciDbkDRicVaC98YHcIe4sTXzoSs', 12, 6, 1221, 5, 'id121', 1, 0, '2019-04-25 14:00:25', '2019-04-25 14:00:34'),
(91, 3600.00, 256.00, 352.09, 'H445KJ9Nun0AwjTeEs0uiOpZvmdyKFZ7RtK04G6G', 15, NULL, NULL, 5, 'id151', 1, 0, '2019-04-26 15:07:06', '2019-04-26 15:07:06'),
(92, 3704.00, 160.00, 464.41, 'H445KJ9Nun0AwjTeEs0uiOpZvmdyKFZ7RtK04G6G', 15, NULL, NULL, 5, 'id152', 1, 0, '2019-04-26 15:07:10', '2019-04-26 15:07:10'),
(93, 3480.00, 432.00, 296.11, 'H445KJ9Nun0AwjTeEs0uiOpZvmdyKFZ7RtK04G6G', 15, NULL, NULL, 5, 'id153', 1, 0, '2019-04-26 15:07:42', '2019-04-26 15:07:42'),
(94, 3224.00, 840.00, 592.49, 'H445KJ9Nun0AwjTeEs0uiOpZvmdyKFZ7RtK04G6G', 14, NULL, NULL, 5, 'id141', 1, 0, '2019-04-26 15:08:24', '2019-04-26 15:08:24'),
(95, 3632.00, 440.00, 429.33, 'H445KJ9Nun0AwjTeEs0uiOpZvmdyKFZ7RtK04G6G', 14, NULL, NULL, 5, 'id141', 1, 0, '2019-04-26 15:08:25', '2019-04-26 15:08:25'),
(97, 3560.00, 336.00, 324.17, 'TOXPCy9q1TsAP7hIwaNZhvVQo8o8Hao0aAg9iMNm', 12, NULL, NULL, 5, 'id121', 1, 0, '2019-04-26 15:15:23', '2019-04-26 15:15:33'),
(98, 3480.00, 576.00, 401.76, 'TOXPCy9q1TsAP7hIwaNZhvVQo8o8Hao0aAg9iMNm', 14, NULL, NULL, 5, 'id141', 1, 0, '2019-04-26 15:15:39', '2019-04-26 15:15:39'),
(99, 3296.00, 360.00, 121.85, 'TOXPCy9q1TsAP7hIwaNZhvVQo8o8Hao0aAg9iMNm', 12, NULL, NULL, 5, 'id121', 1, 0, '2019-04-26 15:15:48', '2019-04-26 15:15:48'),
(100, 3688.00, 232.00, 440.29, 'IcaaF0Mg5KuoG6yKkMPT01ObrMV4wwWMTmNZEIRg', 12, NULL, NULL, 5, 'id121', 1, 0, '2019-04-29 13:54:09', '2019-04-29 13:54:09'),
(103, 4552.00, 1656.00, 441.16, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 12, 5, 1226, 6, 'id121', 1, 0, '2019-05-04 08:29:14', '2019-05-04 08:34:00'),
(104, 3288.00, 744.00, 1948.01, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 13, 5, 1226, 6, 'id131', 1, 0, '2019-05-04 08:29:25', '2019-05-04 08:34:00'),
(105, 4840.00, 536.00, 1161.98, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 13, 5, 1226, 6, 'id132', 1, 0, '2019-05-04 08:29:25', '2019-05-04 08:34:00'),
(106, 5128.00, 1552.00, 192.33, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 13, 5, 1226, 6, 'id133', 1, 0, '2019-05-04 08:31:21', '2019-05-04 08:34:00'),
(107, 4184.00, 2064.00, 891.20, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 13, 5, 1226, 6, 'id134', 1, 0, '2019-05-04 08:31:28', '2019-05-04 08:34:00'),
(108, 720.00, 3432.00, 4614.27, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 13, 5, 1226, 6, 'id135', 1, 0, '2019-05-04 08:31:30', '2019-05-04 08:34:00'),
(109, 4152.00, 2112.00, 940.94, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-05-04 09:00:19', '2019-05-04 09:04:21'),
(110, 4576.00, 1984.00, 510.56, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-05-04 09:00:19', '2019-05-04 09:04:21'),
(111, 3944.00, 3488.00, 2082.86, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-05-04 09:01:11', '2019-05-04 09:04:21'),
(113, 5480.00, 3512.00, 1888.15, 'r1rW7AuVPJcO1wm3KNbkw8RHqvGSObQcpT3fmPih', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-05-04 09:01:15', '2019-05-04 09:04:21'),
(114, 4736.00, 1728.00, 259.11, 'PxxZhNSO8z0vNiGCPzPBGY9zarage8ZnyFQuWbmV', 12, 5, 1227, 6, 'id121', 1, 0, '2019-05-05 05:49:23', '2019-05-05 05:49:59'),
(115, 2480.00, 3400.00, 3039.92, 'UntDxuo3a4j8zcayrQICYiA734HFcUcWzQ5ACp7t', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-05-05 06:07:13', '2019-05-05 06:07:13'),
(116, 4160.00, 2016.00, 894.32, 'UntDxuo3a4j8zcayrQICYiA734HFcUcWzQ5ACp7t', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-05-05 06:07:23', '2019-05-05 06:07:23'),
(117, 3792.00, 2016.00, 1244.02, 'UntDxuo3a4j8zcayrQICYiA734HFcUcWzQ5ACp7t', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-05-05 06:07:23', '2019-05-05 06:07:23'),
(118, 4072.00, 1888.00, 941.49, 'UntDxuo3a4j8zcayrQICYiA734HFcUcWzQ5ACp7t', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-05-05 06:07:23', '2019-05-05 06:07:23'),
(119, 3904.00, 2552.00, 1389.33, 'UntDxuo3a4j8zcayrQICYiA734HFcUcWzQ5ACp7t', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-05-05 06:07:24', '2019-05-05 06:07:24'),
(120, 3312.00, 2568.00, 1896.52, 'UntDxuo3a4j8zcayrQICYiA734HFcUcWzQ5ACp7t', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-05-05 06:07:24', '2019-05-05 06:07:24'),
(121, 4864.00, 2064.00, 397.19, 'FHlzWlm5azu3RsOJwZPIvBj0Xwo0lAyjnTNlNlDm', 12, 5, 1228, 6, 'id121', 1, 0, '2019-05-12 12:11:03', '2019-05-12 12:11:28'),
(122, 4216.00, 1840.00, 790.75, 'FHlzWlm5azu3RsOJwZPIvBj0Xwo0lAyjnTNlNlDm', 12, 5, 1228, 6, 'id122', 1, 0, '2019-05-12 12:11:04', '2019-05-12 12:11:28'),
(123, 4952.00, 1704.00, 43.08, 'FHlzWlm5azu3RsOJwZPIvBj0Xwo0lAyjnTNlNlDm', 12, 5, 1228, 6, 'id123', 1, 0, '2019-05-12 12:11:06', '2019-05-12 12:11:28'),
(124, 4952.00, 1248.00, 441.81, 'FHlzWlm5azu3RsOJwZPIvBj0Xwo0lAyjnTNlNlDm', 12, 5, 1228, 6, 'id124', 1, 0, '2019-05-12 12:11:08', '2019-05-12 12:11:28'),
(125, 5240.00, 1888.00, 318.60, 'FHlzWlm5azu3RsOJwZPIvBj0Xwo0lAyjnTNlNlDm', 12, 5, 1228, 6, 'id125', 1, 0, '2019-05-12 12:11:10', '2019-05-12 12:11:28'),
(128, 1240.00, 344.00, 1429.84, 'uO0jgIydZk89FdNSAZLqn5Lk7yTCfStymZ6muoES', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-15 15:59:04', '2019-05-15 15:59:04'),
(129, 2296.00, 1920.00, 813.95, 'uO0jgIydZk89FdNSAZLqn5Lk7yTCfStymZ6muoES', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-15 15:59:11', '2019-05-15 15:59:11'),
(133, 2068.00, 1624.00, 376.09, 'g9WXHprrcRHi9O420SiXqPefs8RaCnFFaTY3vV69', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-20 12:55:32', '2019-05-20 12:55:32'),
(134, 2164.00, 2040.00, 181.37, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id121', 1, 0, '2019-05-22 05:22:07', '2019-05-22 05:42:54'),
(135, 4236.00, 2704.00, 2088.72, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id122', 1, 0, '2019-05-22 05:22:30', '2019-05-22 05:42:54'),
(136, 3348.00, 2440.00, 1169.92, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id123', 1, 0, '2019-05-22 05:22:31', '2019-05-22 05:42:54'),
(137, 3780.00, 2184.00, 1503.36, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id124', 1, 0, '2019-05-22 05:22:32', '2019-05-22 05:42:54'),
(138, 4676.00, 1952.00, 2376.22, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id125', 1, 0, '2019-05-22 05:22:33', '2019-05-22 05:42:54'),
(139, 3268.00, 2368.00, 1066.64, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id121', 1, 0, '2019-05-22 05:42:09', '2019-05-22 05:42:54'),
(140, 2548.00, 1944.00, 249.16, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id122', 1, 0, '2019-05-22 05:42:10', '2019-05-22 05:42:54'),
(141, 3348.00, 1704.00, 1070.03, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id123', 1, 0, '2019-05-22 05:42:13', '2019-05-22 05:42:54'),
(142, 4428.00, 2136.00, 2138.93, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id124', 1, 0, '2019-05-22 05:42:15', '2019-05-22 05:42:54'),
(143, 5508.00, 3456.00, 3556.76, 'MykjInyFLDae4TfVJzdyAjRuTt2W2pv0i8qB6lp1', 12, 24, 1230, 7, 'id125', 1, 0, '2019-05-22 05:42:20', '2019-05-22 05:42:54'),
(147, 3680.00, 2728.00, 1599.14, 'tImjWp20nmAPck5Bs4hD26mH9DOTPQk7qrfFcsvY', 15, NULL, NULL, 7, 'id151', 1, 0, '2019-05-22 08:25:13', '2019-05-22 08:25:13'),
(148, 888.00, 2752.00, 1638.89, 'tImjWp20nmAPck5Bs4hD26mH9DOTPQk7qrfFcsvY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-22 08:25:19', '2019-05-22 08:25:19'),
(149, 2728.00, 1448.00, 637.16, 'tImjWp20nmAPck5Bs4hD26mH9DOTPQk7qrfFcsvY', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-22 10:35:07', '2019-05-22 10:35:07'),
(150, 4760.00, 2232.00, 2479.71, 'tImjWp20nmAPck5Bs4hD26mH9DOTPQk7qrfFcsvY', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-22 10:35:13', '2019-05-22 10:35:13'),
(151, 4536.00, 3040.00, 2500.82, 'tImjWp20nmAPck5Bs4hD26mH9DOTPQk7qrfFcsvY', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-05-22 10:35:19', '2019-05-22 10:35:19'),
(152, 5152.00, 3032.00, 3061.12, 'tImjWp20nmAPck5Bs4hD26mH9DOTPQk7qrfFcsvY', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-05-22 10:35:20', '2019-05-22 10:35:20'),
(153, 1872.00, 2160.00, 490.70, 'tImjWp20nmAPck5Bs4hD26mH9DOTPQk7qrfFcsvY', 12, NULL, NULL, 7, 'id126', 1, 0, '2019-05-22 10:35:22', '2019-05-22 10:35:22'),
(154, 572.00, 3032.00, 2054.88, '4mSGUtKo5zlRjllVCxLqmmNt8ZjobVUzrhfR8jdt', 15, NULL, NULL, 7, 'id151', 1, 0, '2019-05-24 13:08:20', '2019-05-24 13:08:20'),
(155, 2164.00, 2440.00, 537.49, '4mSGUtKo5zlRjllVCxLqmmNt8ZjobVUzrhfR8jdt', 15, NULL, NULL, 7, 'id152', 1, 0, '2019-05-24 13:08:24', '2019-05-24 13:08:24'),
(156, 3292.00, 2312.00, 1066.64, '4mSGUtKo5zlRjllVCxLqmmNt8ZjobVUzrhfR8jdt', 15, NULL, NULL, 7, 'id153', 1, 0, '2019-05-24 13:08:33', '2019-05-24 13:08:33'),
(157, 2628.00, 1944.00, 328.88, '4mSGUtKo5zlRjllVCxLqmmNt8ZjobVUzrhfR8jdt', 15, NULL, NULL, 7, 'id154', 1, 0, '2019-05-24 13:08:35', '2019-05-24 13:08:35'),
(158, 2228.00, 1920.00, 72.00, '4mSGUtKo5zlRjllVCxLqmmNt8ZjobVUzrhfR8jdt', 15, NULL, NULL, 7, 'id155', 1, 0, '2019-05-24 13:09:04', '2019-05-24 13:09:04'),
(159, 2004.00, 1712.00, 361.77, '54sMLAX7YBSFuasr1fZSoUE5iMj1sj0ZVkjyrwFT', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-27 05:52:18', '2019-05-27 05:53:11'),
(160, 2700.00, 2328.00, 571.37, '54sMLAX7YBSFuasr1fZSoUE5iMj1sj0ZVkjyrwFT', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-27 05:52:23', '2019-05-27 05:53:11'),
(161, 3100.00, 2328.00, 898.03, '54sMLAX7YBSFuasr1fZSoUE5iMj1sj0ZVkjyrwFT', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-27 05:52:25', '2019-05-27 05:53:11'),
(162, 4444.00, 2472.00, 2213.92, '54sMLAX7YBSFuasr1fZSoUE5iMj1sj0ZVkjyrwFT', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-05-27 05:52:27', '2019-05-27 05:53:11'),
(163, 5212.00, 2576.00, 2984.98, '54sMLAX7YBSFuasr1fZSoUE5iMj1sj0ZVkjyrwFT', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-05-27 05:52:30', '2019-05-27 05:53:11'),
(164, 3932.00, 2704.00, 1810.55, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id131', 1, 0, '2019-05-27 05:57:10', '2019-05-27 05:57:10'),
(165, 4100.00, 1296.00, 1905.09, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id132', 1, 0, '2019-05-27 05:57:17', '2019-05-27 05:57:17'),
(166, 3396.00, 1392.00, 1216.55, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id133', 1, 0, '2019-05-27 05:57:21', '2019-05-27 05:57:21'),
(167, 4108.00, 1512.00, 1853.46, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id134', 1, 0, '2019-05-27 05:57:24', '2019-05-27 05:57:24'),
(168, 3988.00, 1112.00, 1871.42, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id135', 1, 0, '2019-05-27 05:57:27', '2019-05-27 05:57:27'),
(169, 3636.00, 1128.00, 1553.11, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id136', 1, 0, '2019-05-27 05:57:28', '2019-05-27 05:57:28'),
(170, 3564.00, 1152.00, 1479.03, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id136', 1, 0, '2019-05-27 05:57:28', '2019-05-27 05:57:28'),
(171, 1572.00, 256.00, 1816.28, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id137', 1, 0, '2019-05-27 05:57:40', '2019-05-27 05:57:40'),
(172, 4020.00, 1288.00, 1832.44, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id138', 1, 0, '2019-05-27 05:57:50', '2019-05-27 05:57:50'),
(173, 1492.00, 192.00, 1907.58, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id139', 1, 0, '2019-05-27 05:57:57', '2019-05-27 05:57:57'),
(174, 1420.00, 232.00, 1903.61, '0fV3L19mJsSnOdPfMx3FcgcTnatJlOaEU40JQaiD', 13, NULL, NULL, 7, 'id1310', 1, 0, '2019-05-27 05:57:58', '2019-05-27 05:57:58'),
(184, 956.00, 1992.00, 1345.93, 'gJtJYQKFXyFKUPLVRxN46yeggTvaf0b68sKBChLp', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-27 09:00:16', '2019-05-27 09:00:16'),
(185, 1132.00, 1792.00, 1174.99, 'gJtJYQKFXyFKUPLVRxN46yeggTvaf0b68sKBChLp', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-27 09:00:26', '2019-05-27 09:00:26'),
(186, 1276.00, 1312.00, 1190.90, 'gJtJYQKFXyFKUPLVRxN46yeggTvaf0b68sKBChLp', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-27 09:00:33', '2019-05-27 09:00:33'),
(187, 916.00, 1872.00, 1384.83, 'gJtJYQKFXyFKUPLVRxN46yeggTvaf0b68sKBChLp', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-05-27 09:00:50', '2019-05-27 09:00:50'),
(188, 2316.00, 2136.00, 216.59, 'gJtJYQKFXyFKUPLVRxN46yeggTvaf0b68sKBChLp', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-05-27 09:00:57', '2019-05-27 09:00:57'),
(193, 1660.00, 2896.00, 1167.12, 'Xq9a4078udiLjyF8stlZcRFjj6gcZNJI0LkBjJIL', 16, NULL, NULL, 7, 'id161', 1, 0, '2019-05-27 09:32:28', '2019-05-27 09:32:28'),
(194, 2348.00, 2632.00, 713.62, 'Xq9a4078udiLjyF8stlZcRFjj6gcZNJI0LkBjJIL', 16, NULL, NULL, 7, 'id162', 1, 0, '2019-05-27 09:32:31', '2019-05-27 09:32:31'),
(196, 1184.00, 2480.00, 1248.62, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-27 12:18:19', '2019-05-27 12:21:03'),
(197, 1680.00, 3368.00, 1575.15, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-27 12:18:20', '2019-05-27 12:21:03'),
(198, 3296.00, 1936.00, 996.13, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-27 12:18:24', '2019-05-27 12:21:03'),
(199, 528.00, 1600.00, 1800.66, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-05-27 12:18:27', '2019-05-27 12:21:03'),
(200, 1368.00, 744.00, 1500.53, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-05-27 12:18:28', '2019-05-27 12:21:03'),
(201, 1136.00, 2696.00, 1398.95, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-05-27 12:18:28', '2019-05-27 12:21:03'),
(207, 728.00, 2120.00, 1584.67, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id127', 1, 0, '2019-05-27 12:20:44', '2019-05-27 12:21:03'),
(208, 1976.00, 1776.00, 354.56, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id128', 1, 0, '2019-05-27 12:20:44', '2019-05-27 12:21:03'),
(209, 2128.00, 1072.00, 865.27, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id129', 1, 0, '2019-05-27 12:20:45', '2019-05-27 12:21:03'),
(210, 1520.00, 920.00, 1268.23, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1210', 1, 0, '2019-05-27 12:20:46', '2019-05-27 12:21:03'),
(211, 1976.00, 584.00, 1374.73, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1211', 1, 0, '2019-05-27 12:20:47', '2019-05-27 12:21:03'),
(212, 2192.00, 376.00, 1547.77, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1212', 1, 0, '2019-05-27 12:20:47', '2019-05-27 12:21:03'),
(213, 2400.00, 1216.00, 711.07, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1213', 1, 0, '2019-05-27 12:20:48', '2019-05-27 12:21:03'),
(214, 2272.00, 2208.00, 289.36, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1214', 1, 0, '2019-05-27 12:20:49', '2019-05-27 12:21:03'),
(215, 1800.00, 2976.00, 1168.39, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1215', 1, 0, '2019-05-27 12:20:49', '2019-05-27 12:21:03'),
(216, 1648.00, 2112.00, 679.68, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1216', 1, 0, '2019-05-27 12:20:50', '2019-05-27 12:21:03'),
(217, 1240.00, 3040.00, 1542.08, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1217', 1, 0, '2019-05-27 12:20:50', '2019-05-27 12:21:03'),
(218, 1944.00, 3456.00, 1576.72, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1218', 1, 0, '2019-05-27 12:20:51', '2019-05-27 12:21:03'),
(219, 960.00, 3128.00, 1804.12, 'xrBsn63iCgPOWSw8T4vXrB2nJWlaamxiSjveTvWE', 12, NULL, NULL, 7, 'id1219', 1, 0, '2019-05-27 12:20:52', '2019-05-27 12:21:03'),
(220, 1392.00, 2384.00, 1019.69, 'PjCI6g72Zf60sotc38zTYWu6rrENH5lrcbjaASzQ', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-27 12:37:18', '2019-05-27 12:37:18'),
(221, 2712.00, 2664.00, 850.46, 'PjCI6g72Zf60sotc38zTYWu6rrENH5lrcbjaASzQ', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-27 12:37:29', '2019-05-27 12:37:29'),
(222, 5024.00, 2568.00, 2800.01, 'PjCI6g72Zf60sotc38zTYWu6rrENH5lrcbjaASzQ', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-27 12:37:33', '2019-05-27 12:37:33'),
(223, 5784.00, 728.00, 3682.27, 'PjCI6g72Zf60sotc38zTYWu6rrENH5lrcbjaASzQ', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-05-27 12:37:41', '2019-05-27 12:37:41'),
(224, 6200.00, 2176.00, 3908.39, 'PjCI6g72Zf60sotc38zTYWu6rrENH5lrcbjaASzQ', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-05-27 12:37:46', '2019-05-27 12:37:46'),
(225, 2389.12, 1944.00, 92.30, '4DGulJuo1vJT0eyoTpnrVCIMGBOvWUqORcLv3QZG', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-28 06:34:34', '2019-05-28 06:34:34'),
(226, 2589.12, 1520.00, 493.55, '4DGulJuo1vJT0eyoTpnrVCIMGBOvWUqORcLv3QZG', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-28 06:34:36', '2019-05-28 06:34:36'),
(227, 4229.12, 2344.00, 1975.17, '4DGulJuo1vJT0eyoTpnrVCIMGBOvWUqORcLv3QZG', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-28 06:34:38', '2019-05-28 06:34:38'),
(228, 4053.12, 1656.00, 1772.89, '4DGulJuo1vJT0eyoTpnrVCIMGBOvWUqORcLv3QZG', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-05-28 06:34:40', '2019-05-28 06:34:40'),
(229, 5236.00, 1656.00, 2947.85, '4DGulJuo1vJT0eyoTpnrVCIMGBOvWUqORcLv3QZG', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-05-28 06:35:32', '2019-05-28 06:35:32'),
(230, 613.21, 3400.00, 2244.03, 'xVWFc0XX2DWHbxbaX4KO7RgcG33J73gtVA5oSd1D', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-28 07:41:45', '2019-05-28 07:41:45'),
(231, 941.21, 3352.00, 1974.07, 'xVWFc0XX2DWHbxbaX4KO7RgcG33J73gtVA5oSd1D', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-28 07:41:48', '2019-05-28 07:41:48'),
(232, 885.21, 3720.00, 2289.46, 'xVWFc0XX2DWHbxbaX4KO7RgcG33J73gtVA5oSd1D', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-28 07:41:51', '2019-05-28 07:41:51'),
(233, 653.21, 3064.00, 2005.16, 'xVWFc0XX2DWHbxbaX4KO7RgcG33J73gtVA5oSd1D', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-05-28 07:42:00', '2019-05-28 07:42:00'),
(234, 1125.21, 2888.00, 1522.22, 'xVWFc0XX2DWHbxbaX4KO7RgcG33J73gtVA5oSd1D', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-05-28 07:42:09', '2019-05-28 07:42:09'),
(236, 757.21, 3352.00, 2104.95, 'hfg1IKO7ck0malww4HmdZuCD0PKdImUstUYaXcws', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-28 11:01:33', '2019-05-28 11:01:33'),
(239, 1365.21, 2165.33, 966.45, 'ZbAbI132JP78bIBJSDhlxkNHb1qHDSvVwWrbJS5q', 13, 5, 1231, 7, 'id131', 1, 0, '2019-05-28 13:38:56', '2019-05-28 14:05:05'),
(240, 1229.21, 2725.33, 1339.83, 'ZbAbI132JP78bIBJSDhlxkNHb1qHDSvVwWrbJS5q', 21, 5, 1231, 7, 'id211', 1, 0, '2019-05-28 13:39:05', '2019-05-28 14:05:05'),
(241, 2861.21, 2237.33, 644.71, 'ZbAbI132JP78bIBJSDhlxkNHb1qHDSvVwWrbJS5q', 14, 5, 1231, 7, 'id141', 1, 0, '2019-05-28 14:02:50', '2019-05-28 14:05:05'),
(242, 1477.21, 1738.67, 842.54, 'ZbAbI132JP78bIBJSDhlxkNHb1qHDSvVwWrbJS5q', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-28 14:24:10', '2019-05-28 14:24:10'),
(243, 2037.21, 1744.00, 316.28, 'GXOG2t3G91nCxpx1Sv16VHRRi3VKrNBcjrpCX1Aa', 21, NULL, NULL, 7, 'id211', 1, 0, '2019-05-29 04:10:35', '2019-05-29 04:10:35'),
(244, 1404.00, 1496.00, 991.26, 'fpv4Me4QGiGZvnH0zS1qTfSdLmQsnCmwhZCpfKq1', 21, NULL, NULL, 7, 'id211', 1, 0, '2019-05-29 04:10:49', '2019-05-29 06:01:00'),
(247, 2484.00, 920.00, 1016.79, 'fpv4Me4QGiGZvnH0zS1qTfSdLmQsnCmwhZCpfKq1', 14, NULL, NULL, 7, 'id141', 1, 0, '2019-05-29 05:49:49', '2019-05-29 06:01:00'),
(248, 4645.21, 1120.00, 2477.90, 'GXOG2t3G91nCxpx1Sv16VHRRi3VKrNBcjrpCX1Aa', 13, NULL, NULL, 7, 'id131', 1, 0, '2019-05-29 05:54:34', '2019-05-29 05:54:34'),
(249, 1349.21, 1688.00, 978.69, 'GXOG2t3G91nCxpx1Sv16VHRRi3VKrNBcjrpCX1Aa', 13, NULL, NULL, 7, 'id132', 1, 0, '2019-05-29 05:55:45', '2019-05-29 05:55:45'),
(250, 2197.12, 1608.00, 328.52, 'gKD19uln1kWD3nrU7H30pcWaG2lNZHlzd05QMtln', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-29 11:20:59', '2019-05-29 11:20:59'),
(259, 3028.00, 1928.00, 728.04, '3FIdGx10uJ0y4N6SZ2E7FZHzpMRGMtPOYp4R0R8p', 12, 28, 1232, 7, 'id121', 1, 0, '2019-05-30 06:51:02', '2019-05-30 07:07:23'),
(267, 1293.12, 3040.00, 1506.05, 'KkMVspyHkhKSi3cyJsTu9GOjm0U6Zmzn0JBuw7YR', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-30 12:19:55', '2019-05-30 12:19:55'),
(268, 2901.12, 3296.00, 1501.57, 'KkMVspyHkhKSi3cyJsTu9GOjm0U6Zmzn0JBuw7YR', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-30 12:20:02', '2019-05-30 12:20:02'),
(269, 805.12, 3424.00, 2120.53, 'KkMVspyHkhKSi3cyJsTu9GOjm0U6Zmzn0JBuw7YR', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-30 12:20:10', '2019-05-30 12:20:10'),
(270, 629.12, 2264.00, 1705.92, 'VyBFSedfqaqWuohSi6p9n8uMCUUWzu7j6n4JHrRV', 14, NULL, NULL, 7, 'id141', 1, 0, '2019-05-30 12:29:23', '2019-05-30 12:29:23'),
(271, 1944.00, 3296.00, 1421.31, 'BcOYNuE3xBuIyvfVLVQK3XzjeHB5dsZXIOrxH5au', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-30 16:11:54', '2019-05-30 16:16:05'),
(275, 3448.00, 2320.00, 1215.69, 'BcOYNuE3xBuIyvfVLVQK3XzjeHB5dsZXIOrxH5au', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-30 16:15:49', '2019-05-30 16:16:05'),
(277, 1301.21, 2152.00, 1025.38, 'deu9dKXvfsu9xq1VpRUWs4gmA1Fk0EObYO2wRe2x', 16, NULL, NULL, 7, 'id161', 1, 0, '2019-05-30 16:24:40', '2019-05-30 16:24:40'),
(278, 4640.00, 2624.00, 2443.61, 'BcOYNuE3xBuIyvfVLVQK3XzjeHB5dsZXIOrxH5au', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-30 16:30:37', '2019-05-30 16:30:37'),
(279, 4688.00, 3408.00, 2813.66, 'BcOYNuE3xBuIyvfVLVQK3XzjeHB5dsZXIOrxH5au', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-30 16:30:54', '2019-05-30 16:30:54'),
(298, 476.00, 3816.00, 2630.93, 'RoY74Eg0rIqKP9S6yjGLJBO3KQ6TUlHFEVvuMNYw', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-30 20:26:22', '2019-05-30 20:26:22'),
(300, 4012.00, 3808.00, 2548.62, 'RoY74Eg0rIqKP9S6yjGLJBO3KQ6TUlHFEVvuMNYw', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-30 20:26:27', '2019-05-30 20:26:27'),
(301, 1692.00, 3704.00, 1884.76, 'RoY74Eg0rIqKP9S6yjGLJBO3KQ6TUlHFEVvuMNYw', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-30 20:26:48', '2019-05-30 20:26:48'),
(302, 4724.00, 3816.00, 3077.43, 'RoY74Eg0rIqKP9S6yjGLJBO3KQ6TUlHFEVvuMNYw', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-30 20:26:59', '2019-05-30 20:26:59'),
(303, 2748.00, 3760.00, 1893.75, 'RoY74Eg0rIqKP9S6yjGLJBO3KQ6TUlHFEVvuMNYw', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-05-30 20:27:02', '2019-05-30 20:27:02'),
(310, 3180.00, 2976.00, 1374.60, '8faU1cpFy9X9AKSXexDEWzdlTNwrTg8AR9zluHiE', 13, NULL, NULL, 7, 'id132', 1, 0, '2019-05-31 03:44:50', '2019-05-31 03:44:50'),
(311, 1124.00, 2352.00, 1252.84, '8faU1cpFy9X9AKSXexDEWzdlTNwrTg8AR9zluHiE', 13, NULL, NULL, 7, 'id131', 1, 0, '2019-05-31 03:45:10', '2019-05-31 03:45:10'),
(312, 3100.00, 1808.00, 807.80, '8faU1cpFy9X9AKSXexDEWzdlTNwrTg8AR9zluHiE', 13, NULL, NULL, 7, 'id132', 1, 0, '2019-05-31 03:45:10', '2019-05-31 03:45:10'),
(313, 5044.00, 2984.00, 2943.07, '8faU1cpFy9X9AKSXexDEWzdlTNwrTg8AR9zluHiE', 13, NULL, NULL, 7, 'id133', 1, 0, '2019-05-31 03:45:11', '2019-05-31 03:45:11'),
(314, 3164.00, 3416.00, 1727.57, '8faU1cpFy9X9AKSXexDEWzdlTNwrTg8AR9zluHiE', 13, NULL, NULL, 7, 'id134', 1, 0, '2019-05-31 03:45:11', '2019-05-31 03:45:11'),
(315, 1812.00, 1760.00, 513.56, 'Jf5pix3DeeMycd9bL6PwC7NQiuznQ2aqjSyA1BtL', 13, 28, 1233, 7, 'id131', 1, 0, '2019-05-31 06:57:50', '2019-05-31 07:03:26'),
(316, 1436.00, 1592.00, 924.16, 'Jf5pix3DeeMycd9bL6PwC7NQiuznQ2aqjSyA1BtL', 13, 28, 1233, 7, 'id132', 1, 0, '2019-05-31 06:57:54', '2019-05-31 07:03:26'),
(317, 2100.00, 1944.00, 201.43, 'Jf5pix3DeeMycd9bL6PwC7NQiuznQ2aqjSyA1BtL', 13, 28, 1233, 7, 'id133', 1, 0, '2019-05-31 06:58:03', '2019-05-31 07:03:26'),
(318, 908.00, 1920.00, 1392.00, 'Jf5pix3DeeMycd9bL6PwC7NQiuznQ2aqjSyA1BtL', 13, 28, 1233, 7, 'id134', 1, 0, '2019-05-31 06:58:06', '2019-05-31 07:03:26'),
(319, 956.00, 1496.00, 1409.29, 'Jf5pix3DeeMycd9bL6PwC7NQiuznQ2aqjSyA1BtL', 13, 28, 1233, 7, 'id135', 1, 0, '2019-05-31 06:58:13', '2019-05-31 07:03:26'),
(321, 4276.00, 3808.00, 2732.97, 'omrNgLwn8FcjOWD2DzRBhSZBQeAd6wwZWKa0SgqH', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-31 07:09:58', '2019-05-31 07:22:31'),
(322, 6500.00, 2312.00, 4218.25, 'omrNgLwn8FcjOWD2DzRBhSZBQeAd6wwZWKa0SgqH', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-31 07:10:16', '2019-05-31 07:22:31'),
(323, 3748.00, 2024.00, 1451.73, '5jyXA38RuUE1RUX42GtUO14ePB6AnjGYDyNdo6wk', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-31 07:34:45', '2019-05-31 07:38:41'),
(324, 4516.00, 3776.00, 2890.57, '5jyXA38RuUE1RUX42GtUO14ePB6AnjGYDyNdo6wk', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-31 07:36:05', '2019-05-31 07:38:41'),
(325, 2876.00, 2184.00, 633.62, 'K7sO02S7lPWV6GF9ldUAMS7xiB76XY76Q7BwVSTj', 21, NULL, NULL, 7, 'id211', 1, 0, '2019-05-31 08:47:45', '2019-05-31 08:47:45'),
(326, 2828.00, 1880.00, 529.51, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id161', 1, 0, '2019-05-31 08:49:26', '2019-05-31 08:58:32'),
(327, 2724.00, 2208.00, 512.56, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id162', 1, 0, '2019-05-31 08:49:29', '2019-05-31 08:58:32'),
(328, 2812.00, 1560.00, 625.89, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id163', 1, 0, '2019-05-31 08:49:31', '2019-05-31 08:58:32'),
(329, 3188.00, 1784.00, 898.35, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id164', 1, 0, '2019-05-31 08:49:33', '2019-05-31 08:58:32'),
(330, 3132.00, 2104.00, 852.10, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id165', 1, 0, '2019-05-31 08:49:35', '2019-05-31 08:58:32'),
(331, 2372.00, 1816.00, 126.49, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id166', 1, 0, '2019-05-31 08:49:37', '2019-05-31 08:58:32'),
(332, 2036.00, 1800.00, 289.99, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id167', 1, 0, '2019-05-31 08:49:39', '2019-05-31 08:58:32'),
(333, 2588.00, 2024.00, 306.20, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id168', 1, 0, '2019-05-31 08:49:49', '2019-05-31 08:58:32'),
(334, 2060.00, 2016.00, 258.49, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id169', 1, 0, '2019-05-31 08:49:58', '2019-05-31 08:58:32'),
(335, 964.00, 1976.00, 1337.17, '1RyqaxeeQ4jiP7BHtLZaYtuqEnfsdl7MSBkMg1qD', 16, 28, 1234, 7, 'id1610', 1, 0, '2019-05-31 08:50:08', '2019-05-31 08:58:32'),
(349, 2812.00, 1872.00, 514.25, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id121', 1, 0, '2019-05-31 09:30:49', '2019-05-31 09:36:39'),
(351, 3244.00, 2352.00, 1038.15, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id123', 1, 0, '2019-05-31 09:30:53', '2019-05-31 09:36:39'),
(352, 3676.00, 2056.00, 1382.70, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id122', 1, 0, '2019-05-31 09:32:06', '2019-05-31 09:36:39'),
(353, 3548.00, 2400.00, 1337.13, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id124', 1, 0, '2019-05-31 09:32:12', '2019-05-31 09:36:39'),
(354, 3972.00, 2104.00, 1682.09, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id125', 1, 0, '2019-05-31 09:32:14', '2019-05-31 09:36:39'),
(355, 3884.00, 1776.00, 1590.53, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id126', 1, 0, '2019-05-31 09:32:15', '2019-05-31 09:36:39'),
(356, 3556.00, 1768.00, 1265.16, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id127', 1, 0, '2019-05-31 09:32:16', '2019-05-31 09:36:39'),
(357, 3396.00, 1976.00, 1097.43, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id128', 1, 0, '2019-05-31 09:32:18', '2019-05-31 09:36:39'),
(358, 3884.00, 2384.00, 1650.56, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id129', 1, 0, '2019-05-31 09:32:20', '2019-05-31 09:36:39'),
(359, 4364.00, 1960.00, 2064.39, 'SH5ntVWxdF059NIJSZq8qPmdA6lWrMMBPc9BT9di', 12, 28, 1235, 7, 'id1210', 1, 0, '2019-05-31 09:32:23', '2019-05-31 09:36:39'),
(382, 1828.00, 3536.00, 1683.52, 'qnOIRbjCLBIH6fA5ni9UcRLyOx8caCeyrAQx6KY0', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-05-31 10:35:15', '2019-05-31 10:35:15'),
(383, 3300.00, 3096.00, 1543.69, 'qnOIRbjCLBIH6fA5ni9UcRLyOx8caCeyrAQx6KY0', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-05-31 10:35:16', '2019-05-31 10:35:16'),
(384, 5092.00, 2984.00, 2987.87, 'qnOIRbjCLBIH6fA5ni9UcRLyOx8caCeyrAQx6KY0', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-05-31 10:35:17', '2019-05-31 10:35:17'),
(385, 4124.00, 3056.00, 2148.83, 'qnOIRbjCLBIH6fA5ni9UcRLyOx8caCeyrAQx6KY0', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-05-31 10:35:19', '2019-05-31 10:35:19'),
(386, 5852.00, 3456.00, 3869.88, 'qnOIRbjCLBIH6fA5ni9UcRLyOx8caCeyrAQx6KY0', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-05-31 10:35:22', '2019-05-31 10:35:22'),
(401, 3708.00, 3208.00, 1908.25, 'ERYuCAu4QGLxp2VxbksIoNt63sIJDaLoimIBDOf3', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-03 09:13:30', '2019-06-03 09:45:03'),
(402, 5548.00, 2896.00, 3391.47, 'ERYuCAu4QGLxp2VxbksIoNt63sIJDaLoimIBDOf3', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-03 09:13:35', '2019-06-03 09:45:03'),
(403, 3716.00, 2704.00, 1618.55, 'ERYuCAu4QGLxp2VxbksIoNt63sIJDaLoimIBDOf3', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-03 09:13:37', '2019-06-03 09:45:03'),
(404, 2140.00, 2704.00, 800.16, 'ERYuCAu4QGLxp2VxbksIoNt63sIJDaLoimIBDOf3', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-03 09:13:41', '2019-06-03 09:45:03'),
(423, 548.00, 3632.00, 2449.58, 'MW9ieK4vQj3i9nvxPftBPVyAnmwukqWL5pBU7bUE', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-03 13:07:22', '2019-06-03 13:07:22'),
(424, 228.00, 112.00, 2749.92, 'MW9ieK4vQj3i9nvxPftBPVyAnmwukqWL5pBU7bUE', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-03 13:17:28', '2019-06-03 13:17:28'),
(425, 164.00, 464.00, 2585.04, 'MW9ieK4vQj3i9nvxPftBPVyAnmwukqWL5pBU7bUE', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-03 13:17:33', '2019-06-03 13:17:33'),
(426, 68.00, 696.00, 2545.58, 'MW9ieK4vQj3i9nvxPftBPVyAnmwukqWL5pBU7bUE', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-03 13:17:57', '2019-06-03 13:17:57'),
(432, 4333.12, 1904.00, 2033.19, 'MqgQArUeyZpV1hLLGoPeZmm42EFbEaMfgW7b9TME', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-04 08:22:18', '2019-06-04 08:22:18'),
(433, 2325.12, 2224.00, 305.04, 'MqgQArUeyZpV1hLLGoPeZmm42EFbEaMfgW7b9TME', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-04 08:22:22', '2019-06-04 08:22:22'),
(434, 5381.12, 1384.00, 3127.40, 'MqgQArUeyZpV1hLLGoPeZmm42EFbEaMfgW7b9TME', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-04 08:22:24', '2019-06-04 08:22:24'),
(435, 3149.12, 992.00, 1257.85, 'MqgQArUeyZpV1hLLGoPeZmm42EFbEaMfgW7b9TME', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-04 08:22:24', '2019-06-04 08:22:24'),
(436, 6037.12, 2656.00, 3808.91, 'MqgQArUeyZpV1hLLGoPeZmm42EFbEaMfgW7b9TME', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-04 08:22:25', '2019-06-04 08:22:25'),
(442, 3140.00, 2472.00, 1005.14, 'oPujb364IQH0dxaDuT5ltrMThjVGgKqw0orGdsle', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-04 09:18:35', '2019-06-04 09:19:34'),
(443, 540.00, 2648.00, 1904.62, 'oPujb364IQH0dxaDuT5ltrMThjVGgKqw0orGdsle', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-04 09:18:37', '2019-06-04 09:19:34'),
(444, 5348.00, 824.00, 3239.06, 'oPujb364IQH0dxaDuT5ltrMThjVGgKqw0orGdsle', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-04 09:20:01', '2019-06-04 09:20:01'),
(447, 1928.00, 2752.00, 911.38, '0fTzrR0KstzCdqvxhAoTwpzQppSzI7psVTbmE2GB', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-04 14:17:44', '2019-06-04 16:27:59'),
(449, 4544.00, 3496.00, 2742.14, '0fTzrR0KstzCdqvxhAoTwpzQppSzI7psVTbmE2GB', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-04 15:45:49', '2019-06-04 16:27:59'),
(451, 1264.00, 2176.00, 1067.16, '0fTzrR0KstzCdqvxhAoTwpzQppSzI7psVTbmE2GB', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-04 15:46:24', '2019-06-04 16:27:59'),
(452, 5000.00, 2216.00, 2716.18, '0fTzrR0KstzCdqvxhAoTwpzQppSzI7psVTbmE2GB', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-04 16:06:06', '2019-06-04 16:27:59'),
(453, 2528.00, 2464.00, 589.85, '0fTzrR0KstzCdqvxhAoTwpzQppSzI7psVTbmE2GB', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-04 16:27:32', '2019-06-04 16:27:59'),
(454, 3728.00, 2704.00, 1629.06, '0fTzrR0KstzCdqvxhAoTwpzQppSzI7psVTbmE2GB', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-04 16:35:54', '2019-06-04 16:35:54'),
(455, 4672.00, 2832.00, 2541.28, '0fTzrR0KstzCdqvxhAoTwpzQppSzI7psVTbmE2GB', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-04 16:35:57', '2019-06-04 16:35:57'),
(456, 5224.00, 2880.00, 3077.56, '0fTzrR0KstzCdqvxhAoTwpzQppSzI7psVTbmE2GB', 12, NULL, NULL, 7, 'id126', 1, 0, '2019-06-04 16:35:59', '2019-06-04 16:35:59'),
(462, 1768.00, 1776.00, 551.14, 'mECFjaCkJ1Y7PaJaPdDUNq6kQq6GS1LSlCzVUDo6', 12, 35, 1236, 7, 'id121', 1, 0, '2019-06-04 16:57:46', '2019-06-04 16:58:22'),
(463, 3416.00, 2168.00, 1143.22, '0RSjFlFSTILhFmSOyNKJEGvtvfz06VLuH2ZymXTN', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-04 17:16:24', '2019-06-04 17:16:24'),
(464, 4888.00, 2384.00, 2629.27, '2KxAxpYEZkLU1hZkbfDzDlesBWGT1Ms0M2ukUAM4', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-05 08:08:21', '2019-06-05 08:08:21'),
(465, 1960.00, 3096.00, 1224.16, 'USkMdgvdOV1ofNBYybYdBOiieDrID1JNykKAGTxB', 21, NULL, NULL, 7, 'id211', 1, 0, '2019-06-05 20:37:17', '2019-06-05 20:37:17'),
(466, 720.00, 2080.00, 1588.08, 'USkMdgvdOV1ofNBYybYdBOiieDrID1JNykKAGTxB', 21, NULL, NULL, 7, 'id212', 1, 0, '2019-06-05 20:37:22', '2019-06-05 20:37:22'),
(467, 2280.00, 1688.00, 232.86, 'USkMdgvdOV1ofNBYybYdBOiieDrID1JNykKAGTxB', 21, NULL, NULL, 7, 'id213', 1, 0, '2019-06-05 20:37:25', '2019-06-05 20:37:25'),
(468, 120.00, 1688.00, 2192.31, 'USkMdgvdOV1ofNBYybYdBOiieDrID1JNykKAGTxB', 21, NULL, NULL, 7, 'id214', 1, 0, '2019-06-05 20:37:27', '2019-06-05 20:37:27'),
(469, 1344.00, 2416.00, 1077.01, 'USkMdgvdOV1ofNBYybYdBOiieDrID1JNykKAGTxB', 21, NULL, NULL, 7, 'id215', 1, 0, '2019-06-05 20:37:32', '2019-06-05 20:37:32'),
(470, 312.00, 2152.00, 2001.49, 'USkMdgvdOV1ofNBYybYdBOiieDrID1JNykKAGTxB', 15, NULL, NULL, 7, 'id151', 1, 0, '2019-06-05 20:37:41', '2019-06-05 20:37:41'),
(471, 760.00, 1784.00, 1545.99, 'USkMdgvdOV1ofNBYybYdBOiieDrID1JNykKAGTxB', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-05 20:37:46', '2019-06-05 20:37:46'),
(473, 948.00, 1256.00, 1506.25, 'LIMmJUfSvcDob6NSMglWoZ8JrOGS7tQKQIzBJgvf', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-06 08:42:53', '2019-06-06 08:42:53'),
(474, 1372.00, 1552.00, 998.30, 'LIMmJUfSvcDob6NSMglWoZ8JrOGS7tQKQIzBJgvf', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-06 08:42:54', '2019-06-06 08:42:54'),
(475, 1932.00, 1360.00, 670.09, 'LIMmJUfSvcDob6NSMglWoZ8JrOGS7tQKQIzBJgvf', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-06 08:43:06', '2019-06-06 08:43:06'),
(476, 1700.00, 1976.00, 602.61, 'LIMmJUfSvcDob6NSMglWoZ8JrOGS7tQKQIzBJgvf', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-06 08:43:06', '2019-06-06 08:43:06'),
(477, 2508.00, 1776.00, 252.98, 'LIMmJUfSvcDob6NSMglWoZ8JrOGS7tQKQIzBJgvf', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-06 08:43:07', '2019-06-06 08:43:07'),
(478, 1460.00, 1936.00, 840.15, 'N66RkwsZhunxBaaNpaLazz3F25ZeLxE8XjTPZFTT', 12, 28, 1237, 7, 'id121', 1, 0, '2019-06-06 08:47:17', '2019-06-06 08:47:32'),
(480, 1540.00, 1752.00, 778.35, 'N66RkwsZhunxBaaNpaLazz3F25ZeLxE8XjTPZFTT', 13, NULL, NULL, 7, 'id131', 1, 0, '2019-06-06 08:51:48', '2019-06-06 08:51:48'),
(481, 1556.00, 1608.00, 806.77, 'sIwE3TvRgga3hZQxX7isahi9glgDnZ5n0Hxht7fp', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-06 08:57:48', '2019-06-06 08:57:48'),
(482, 1668.00, 1712.00, 665.35, 'xnwLbDNfCIz7q4WeAaG4es4KwS2nxoqvIH3ZL4Ww', 12, 38, 1240, 7, 'id121', 1, 0, '2019-06-06 09:03:04', '2019-06-06 09:27:49'),
(483, 1692.00, 1678.53, 654.19, 'hV72cYRQxyQMatpxOWVGeuAiBcSH4bMfUtPf8nOz', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-06 09:05:10', '2019-06-06 09:05:10'),
(484, 1564.00, 1624.00, 793.29, 'xnwLbDNfCIz7q4WeAaG4es4KwS2nxoqvIH3ZL4Ww', 14, 38, 1240, 7, 'id141', 1, 0, '2019-06-06 09:25:01', '2019-06-06 09:27:49'),
(485, 1660.00, 1800.00, 651.15, 'xnwLbDNfCIz7q4WeAaG4es4KwS2nxoqvIH3ZL4Ww', 14, 38, 1240, 7, 'id141', 1, 0, '2019-06-06 09:27:33', '2019-06-06 09:27:49'),
(486, 1996.00, 2176.00, 397.43, 'xnwLbDNfCIz7q4WeAaG4es4KwS2nxoqvIH3ZL4Ww', 14, 38, 1240, 7, 'id142', 1, 0, '2019-06-06 09:27:34', '2019-06-06 09:27:49'),
(487, 1332.00, 1760.00, 981.13, 'bcaz5e503TPNHuCNwlSSvZgvCv54NGYGAsvq5Y0M', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-06 10:47:27', '2019-06-06 10:47:27'),
(488, 1420.00, 1800.00, 888.14, 'eq3TgCMz8JP38IWOdTu1kS1HbyGpv2SB0JdhMV67', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-06 11:09:20', '2019-06-06 11:09:20'),
(489, 2004.00, 920.00, 1042.89, 'eq3TgCMz8JP38IWOdTu1kS1HbyGpv2SB0JdhMV67', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-06 11:09:36', '2019-06-06 11:09:36'),
(490, 2492.00, 2424.00, 539.33, 'eq3TgCMz8JP38IWOdTu1kS1HbyGpv2SB0JdhMV67', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-06 11:11:57', '2019-06-06 11:11:57'),
(491, 2700.00, 1888.00, 401.28, 'ukJX5Jt5ODVEaHTe0Oxw73bdjXa0RSdAonkw23Y7', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-06 12:57:22', '2019-06-06 12:57:22'),
(492, 4084.00, 1632.00, 1807.10, 'ukJX5Jt5ODVEaHTe0Oxw73bdjXa0RSdAonkw23Y7', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-06 12:57:23', '2019-06-06 12:57:23'),
(493, 4180.00, 2024.00, 1882.87, 'ukJX5Jt5ODVEaHTe0Oxw73bdjXa0RSdAonkw23Y7', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-06 12:57:23', '2019-06-06 12:57:23'),
(494, 4948.00, 1072.00, 2780.47, 'ukJX5Jt5ODVEaHTe0Oxw73bdjXa0RSdAonkw23Y7', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-06 12:57:24', '2019-06-06 12:57:24'),
(495, 1400.00, 2488.00, 1064.25, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id211', 1, 0, '2019-06-07 23:32:04', '2019-06-07 23:32:04'),
(496, 1837.12, 1704.00, 510.79, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id212', 1, 0, '2019-06-07 23:32:08', '2019-06-07 23:32:08'),
(497, 1333.12, 2480.00, 1117.34, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id213', 1, 0, '2019-06-07 23:32:11', '2019-06-07 23:32:11'),
(498, 1485.12, 2560.00, 1036.16, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id214', 1, 0, '2019-06-07 23:32:12', '2019-06-07 23:32:12'),
(499, 2869.12, 2136.00, 608.74, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id215', 1, 0, '2019-06-07 23:32:13', '2019-06-07 23:32:13'),
(500, 2661.12, 1800.00, 380.54, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id216', 1, 0, '2019-06-07 23:32:13', '2019-06-07 23:32:13'),
(501, 2221.12, 2504.00, 589.30, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id217', 1, 0, '2019-06-07 23:32:14', '2019-06-07 23:32:14'),
(502, 3349.12, 2376.00, 1143.94, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id218', 1, 0, '2019-06-07 23:32:14', '2019-06-07 23:32:14'),
(503, 3893.12, 1768.00, 1600.36, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id218', 1, 0, '2019-06-07 23:32:15', '2019-06-07 23:32:15'),
(504, 4045.12, 1640.00, 1767.44, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id219', 1, 0, '2019-06-07 23:32:15', '2019-06-07 23:32:15'),
(505, 3805.12, 2904.00, 1798.24, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id2110', 1, 0, '2019-06-07 23:32:15', '2019-06-07 23:32:15'),
(506, 4373.12, 2056.00, 2077.58, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id2111', 1, 0, '2019-06-07 23:32:16', '2019-06-07 23:32:16'),
(507, 4725.12, 1712.00, 2434.03, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id2112', 1, 0, '2019-06-07 23:32:16', '2019-06-07 23:32:16'),
(508, 509.12, 2088.00, 1798.74, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id2113', 1, 0, '2019-06-07 23:32:17', '2019-06-07 23:32:17'),
(509, 2661.12, 1416.00, 620.02, 'jquVnh8PzIaXPhLfcdlFNmZSiZBw2a2HA8LlDerx', 21, NULL, NULL, 7, 'id2114', 1, 0, '2019-06-07 23:32:17', '2019-06-07 23:32:17'),
(510, 1712.00, 2096.00, 613.78, 't8kRLN15FHAzYG2yAvdsiC8j8NqraFjM5tc50iy4', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-08 07:42:20', '2019-06-08 07:42:20'),
(511, 3304.00, 2816.00, 1345.67, 'kgNo1H4R5eSBBAghgXcDpa2CB1eZyhOgvCDVAUUh', 13, NULL, NULL, 7, 'id131', 1, 0, '2019-06-08 08:07:16', '2019-06-08 08:07:16'),
(512, 3244.00, 2912.00, 1369.38, 'xWSHjGDhEYQD4glUNHvQy6e0RJcbEq09GMXrYiMn', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-08 09:10:27', '2019-06-08 09:10:27'),
(513, 1300.00, 1576.00, 1057.51, 'xWSHjGDhEYQD4glUNHvQy6e0RJcbEq09GMXrYiMn', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-08 09:10:27', '2019-06-08 09:10:27'),
(515, 1460.00, 2376.00, 955.79, 'xWSHjGDhEYQD4glUNHvQy6e0RJcbEq09GMXrYiMn', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-08 09:10:29', '2019-06-08 09:10:29'),
(516, 2484.00, 2608.00, 712.18, 'xWSHjGDhEYQD4glUNHvQy6e0RJcbEq09GMXrYiMn', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-08 09:10:30', '2019-06-08 09:10:30'),
(517, 4576.00, 1880.00, 2276.35, 'qE1gR0KMEoYQSL5GfPNEBcT7PRnKxJsuw3mllJsn', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-10 11:35:37', '2019-06-10 11:35:37'),
(518, 4712.00, 1848.00, 2413.07, 'XjA0wZg72iuLW6Svdjtmw6e1jwEm9fPuPRCUEbvM', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-10 11:43:35', '2019-06-10 11:43:35'),
(519, 1684.00, 1136.00, 997.05, 'hWRgCtJhB1ot4FqpZ6bVAD8RMnxQjaPtsgfuTlKQ', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-10 15:03:38', '2019-06-10 15:03:38'),
(520, 1844.00, 3176.00, 1336.22, 'hWRgCtJhB1ot4FqpZ6bVAD8RMnxQjaPtsgfuTlKQ', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-10 15:03:52', '2019-06-10 15:03:52'),
(521, 3500.00, 3232.00, 1778.02, 'hWRgCtJhB1ot4FqpZ6bVAD8RMnxQjaPtsgfuTlKQ', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-10 15:03:52', '2019-06-10 15:03:52'),
(522, 4996.00, 3144.00, 2960.84, 'hWRgCtJhB1ot4FqpZ6bVAD8RMnxQjaPtsgfuTlKQ', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-10 15:03:52', '2019-06-10 15:03:52'),
(523, 4372.00, 800.00, 2355.33, 'swZ166LPyujJ4zVCa4pcbMVxLsSyeBH9galSRT7H', 14, 40, 1242, 7, 'id141', 1, 0, '2019-06-10 16:04:28', '2019-06-10 16:13:29'),
(524, 1372.00, 2736.00, 1235.73, 'swZ166LPyujJ4zVCa4pcbMVxLsSyeBH9galSRT7H', 13, 40, 1242, 7, 'id131', 1, 0, '2019-06-10 16:13:19', '2019-06-10 16:13:29'),
(525, 2048.00, 1384.00, 592.28, 'PKXtmVleSK4qttz79K1g2OGNAckzso1DiAsj24cA', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 08:26:44', '2019-06-11 08:26:44'),
(526, 1072.00, 1808.00, 1233.10, 'PKXtmVleSK4qttz79K1g2OGNAckzso1DiAsj24cA', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-11 08:26:45', '2019-06-11 08:26:45'),
(534, 1764.00, 960.00, 1099.50, 'RjfkrU6yNscxpwDVF84FfiwhVp7ayCSIV7fIUsSj', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 10:57:54', '2019-06-11 10:59:02'),
(535, 5564.00, 640.00, 3506.01, 'RjfkrU6yNscxpwDVF84FfiwhVp7ayCSIV7fIUsSj', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-11 10:57:56', '2019-06-11 10:59:02'),
(536, 3572.00, 1128.00, 1498.42, 'RjfkrU6yNscxpwDVF84FfiwhVp7ayCSIV7fIUsSj', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-11 10:57:57', '2019-06-11 10:59:02'),
(537, 460.00, 608.00, 2259.85, 'RjfkrU6yNscxpwDVF84FfiwhVp7ayCSIV7fIUsSj', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-11 10:57:59', '2019-06-11 10:59:02'),
(538, 1620.00, 1504.00, 797.15, 'RjfkrU6yNscxpwDVF84FfiwhVp7ayCSIV7fIUsSj', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-11 10:58:00', '2019-06-11 10:59:02'),
(539, 996.00, 1848.00, 1305.99, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 11:00:47', '2019-06-11 11:00:47'),
(540, 2412.00, 1352.00, 578.94, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-11 11:00:48', '2019-06-11 11:00:48');
INSERT INTO `pointers` (`id`, `xCords`, `yCords`, `distance`, `sessionID`, `productID`, `user_id`, `invoice_id`, `game_id`, `stampId`, `stamped`, `removed`, `created_at`, `updated_at`) VALUES
(541, 2068.00, 1928.00, 232.14, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-11 11:00:49', '2019-06-11 11:00:49'),
(542, 3188.00, 1600.00, 943.90, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-11 11:00:49', '2019-06-11 11:00:49'),
(543, 3004.00, 2168.00, 746.40, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-11 11:00:50', '2019-06-11 11:00:50'),
(544, 4396.00, 1568.00, 2125.35, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id126', 1, 0, '2019-06-11 11:00:51', '2019-06-11 11:00:51'),
(545, 3612.00, 2120.00, 1327.16, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id127', 1, 0, '2019-06-11 11:00:52', '2019-06-11 11:00:52'),
(546, 4268.00, 1416.00, 2031.51, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id128', 1, 0, '2019-06-11 11:00:53', '2019-06-11 11:00:53'),
(547, 5924.00, 1248.00, 3685.78, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id129', 1, 0, '2019-06-11 11:00:54', '2019-06-11 11:00:54'),
(548, 5540.00, 1192.00, 3320.78, 'UdJJVb4BRw0kYTvMjvxeiIpxyJ3D48ZxWmVXvyzM', 12, NULL, NULL, 7, 'id1210', 1, 0, '2019-06-11 11:00:55', '2019-06-11 11:00:55'),
(549, 1124.00, 840.00, 1596.68, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 14, NULL, NULL, 7, 'id141', 1, 0, '2019-06-11 12:09:33', '2019-06-11 12:09:33'),
(550, 5372.00, 1160.00, 3164.61, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 14, NULL, NULL, 7, 'id142', 1, 0, '2019-06-11 12:09:34', '2019-06-11 12:09:34'),
(551, 3756.00, 2264.00, 1496.09, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 14, NULL, NULL, 7, 'id143', 1, 0, '2019-06-11 12:09:35', '2019-06-11 12:09:35'),
(552, 1804.00, 2112.00, 531.86, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 14, NULL, NULL, 7, 'id144', 1, 0, '2019-06-11 12:09:35', '2019-06-11 12:09:35'),
(553, 3324.00, 3008.00, 1494.10, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 14, NULL, NULL, 7, 'id145', 1, 0, '2019-06-11 12:09:39', '2019-06-11 12:09:39'),
(554, 5380.00, 3096.00, 3296.87, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 12:09:39', '2019-06-11 12:09:42'),
(555, 668.00, 888.00, 1930.92, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-11 12:10:16', '2019-06-11 12:10:16'),
(556, 1364.00, 832.00, 1435.21, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-11 12:10:16', '2019-06-11 12:10:16'),
(557, 1988.00, 520.00, 1434.34, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-11 12:10:17', '2019-06-11 12:10:17'),
(558, 2452.00, 504.00, 1424.13, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-11 12:10:17', '2019-06-11 12:10:17'),
(559, 3388.00, 504.00, 1785.72, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-11 12:10:17', '2019-06-11 12:10:17'),
(560, 4324.00, 480.00, 2483.98, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 12, NULL, NULL, 7, 'id126', 1, 0, '2019-06-11 12:10:18', '2019-06-11 12:10:18'),
(561, 5324.00, 488.00, 3345.92, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 12, NULL, NULL, 7, 'id126', 1, 0, '2019-06-11 12:10:18', '2019-06-11 12:10:18'),
(562, 5492.00, 480.00, 3501.78, 'aM61dVGx24oyZqsnoecp9jx4pfvZCFwnNggWStJC', 12, NULL, NULL, 7, 'id127', 1, 0, '2019-06-11 12:10:18', '2019-06-11 12:10:18'),
(563, 3264.00, 2048.00, 972.46, 'PTty1Y5fxo8ODhEQD8IcfPXtIseCXIeYTAEjy47L', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 13:15:55', '2019-06-11 13:15:55'),
(564, 2468.00, 1536.00, 419.14, 'ZIAzJTxCqQupSEhn9YWkgKXZO20XGnTQX7XVIsOT', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 13:21:03', '2019-06-11 13:26:22'),
(565, 3876.00, 1488.00, 1634.14, 'ZIAzJTxCqQupSEhn9YWkgKXZO20XGnTQX7XVIsOT', 12, NULL, NULL, 7, 'id122', 1, 0, '2019-06-11 13:21:04', '2019-06-11 13:26:22'),
(566, 5652.00, 472.00, 3651.38, 'ZIAzJTxCqQupSEhn9YWkgKXZO20XGnTQX7XVIsOT', 12, NULL, NULL, 7, 'id123', 1, 0, '2019-06-11 13:21:05', '2019-06-11 13:26:22'),
(568, 4556.00, 864.00, 2490.92, 'ZIAzJTxCqQupSEhn9YWkgKXZO20XGnTQX7XVIsOT', 12, NULL, NULL, 7, 'id125', 1, 0, '2019-06-11 13:21:07', '2019-06-11 13:26:22'),
(569, 4108.00, 2024.00, 1810.99, 'ZIAzJTxCqQupSEhn9YWkgKXZO20XGnTQX7XVIsOT', 12, NULL, NULL, 7, 'id124', 1, 0, '2019-06-11 13:24:20', '2019-06-11 13:26:22'),
(575, 5516.00, 1608.00, 3231.10, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 15:38:41', '2019-06-11 19:38:49'),
(578, 1044.00, 480.00, 1910.79, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 16:08:32', '2019-06-11 19:38:49'),
(579, 5700.00, 608.00, 3644.36, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 16:48:45', '2019-06-11 19:38:49'),
(580, 5580.00, 1600.00, 3295.57, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:10', '2019-06-11 19:38:49'),
(581, 5780.00, 1136.00, 3567.22, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:12', '2019-06-11 19:38:49'),
(582, 3820.00, 1448.00, 1591.60, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:13', '2019-06-11 19:38:49'),
(583, 2204.00, 1520.00, 411.36, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:14', '2019-06-11 19:38:49'),
(584, 2132.00, 808.00, 1124.62, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:14', '2019-06-11 19:38:49'),
(585, 4060.00, 2640.00, 1901.58, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:15', '2019-06-11 19:38:49'),
(586, 5612.00, 2216.00, 3325.20, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:15', '2019-06-11 19:38:49'),
(587, 3140.00, 2304.00, 923.61, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:16', '2019-06-11 19:38:49'),
(588, 1772.00, 2528.00, 805.26, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:16', '2019-06-11 19:38:49'),
(589, 1260.00, 1544.00, 1105.88, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:16', '2019-06-11 19:38:49'),
(590, 1316.00, 1368.00, 1128.26, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:17', '2019-06-11 19:38:49'),
(591, 2468.00, 1288.00, 653.95, 'pifEbUMl466J8dk320UaFrrBIlcdb2HVmmbFIyBY', 12, NULL, NULL, 7, 'id121', 1, 0, '2019-06-11 19:37:17', '2019-06-11 19:38:49'),
(592, 372.00, 1720.00, 4624.11, 'f8tyXYejc1e53xamNE84WZ1nB27OimaEn3icizU4', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-13 13:25:32', '2019-06-13 13:26:43'),
(593, 1172.00, 1400.00, 3834.83, 'f8tyXYejc1e53xamNE84WZ1nB27OimaEn3icizU4', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-13 13:25:37', '2019-06-13 13:26:43'),
(594, 2420.00, 896.00, 2695.00, 'f8tyXYejc1e53xamNE84WZ1nB27OimaEn3icizU4', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-13 13:25:39', '2019-06-13 13:26:43'),
(595, 3316.00, 1120.00, 1773.42, 'f8tyXYejc1e53xamNE84WZ1nB27OimaEn3icizU4', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-13 13:25:41', '2019-06-13 13:26:43'),
(596, 4620.00, 1160.00, 648.20, 'f8tyXYejc1e53xamNE84WZ1nB27OimaEn3icizU4', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-13 13:25:42', '2019-06-13 13:26:43'),
(597, 2116.00, 1264.00, 2911.04, 'cJWTncn67tWzWqi0r1HOjkqexXdAn5ZRTGMGDgyX', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-13 13:27:00', '2019-06-13 13:27:15'),
(598, 3636.00, 1360.00, 1398.99, 'cJWTncn67tWzWqi0r1HOjkqexXdAn5ZRTGMGDgyX', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-13 13:27:01', '2019-06-13 13:27:15'),
(599, 5268.00, 968.00, 769.66, 'cJWTncn67tWzWqi0r1HOjkqexXdAn5ZRTGMGDgyX', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-13 13:27:01', '2019-06-13 13:27:15'),
(600, 3372.00, 704.00, 1898.85, 'cJWTncn67tWzWqi0r1HOjkqexXdAn5ZRTGMGDgyX', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-13 13:27:02', '2019-06-13 13:27:15'),
(601, 2932.00, 2536.00, 2231.41, 'cJWTncn67tWzWqi0r1HOjkqexXdAn5ZRTGMGDgyX', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-13 13:27:03', '2019-06-13 13:27:15'),
(610, 716.00, 1568.00, 4281.68, '8Z5v588PK4rlmNK5Qm28GRpPkj8I1UEsRfKs9PT8', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-14 06:45:19', '2019-06-14 06:45:19'),
(611, 1304.00, 824.00, 3787.85, 'FIWTZeXppKyUWTzfwcSSks4Yq55Pvw7m8kNSO3Ip', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-14 12:43:25', '2019-06-14 12:43:25'),
(612, 4144.00, 1816.00, 857.61, 'P6JvT6lsC0SP8smLV8lsgmHkCxd5PqzJu0nw8rbD', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-14 12:52:38', '2019-06-14 12:52:38'),
(614, 2104.00, 2128.00, 2921.33, 'oXGodyA4ap1toZtNnhV66NsGvPvNrdYpF3m8KY6L', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-14 12:55:14', '2019-06-14 12:56:56'),
(615, 752.00, 3464.00, 4596.93, 'oXGodyA4ap1toZtNnhV66NsGvPvNrdYpF3m8KY6L', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-14 13:03:25', '2019-06-14 13:03:25'),
(616, 3256.00, 2184.00, 1805.47, 'oXGodyA4ap1toZtNnhV66NsGvPvNrdYpF3m8KY6L', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-14 13:04:48', '2019-06-14 13:04:48'),
(617, 2096.00, 2144.00, 2931.68, '3B6QwlQR2zSpYi5NXg6PvaPrGuBUOVXPjZjJ2uqj', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-15 03:32:23', '2019-06-15 03:32:23'),
(618, 2656.00, 1128.00, 2402.19, '3QEiVR1sGMhAmQ6ZZBcla093J46vGSrTWuYw3O1c', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-18 10:27:33', '2019-06-18 10:27:33'),
(619, 1488.00, 3656.00, 4018.84, '3QEiVR1sGMhAmQ6ZZBcla093J46vGSrTWuYw3O1c', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-18 10:29:06', '2019-06-18 10:29:06'),
(620, 4580.00, 1384.00, 515.24, 'NKc7DmhaOC9i6sqwYozKvm232mqcxniFGwUe7Vz0', 21, NULL, NULL, 6, 'id211', 1, 0, '2019-06-18 10:57:35', '2019-06-18 10:57:35'),
(623, 4636.00, 1264.00, 556.22, 'TuKaUpOckhPL2FoEdD0T22NbGK5Q6SUXDbr8GiC2', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-18 12:56:06', '2019-06-18 12:56:06'),
(625, 3976.00, 2048.00, 1077.89, '8mYFYtQTzAjhrHVeirV8Q0ZJ1Abt0XbAJ0L8odvJ', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-18 15:27:45', '2019-06-18 15:27:45'),
(627, 576.00, 3056.00, 4623.04, '8mYFYtQTzAjhrHVeirV8Q0ZJ1Abt0XbAJ0L8odvJ', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-18 15:29:08', '2019-06-18 15:29:08'),
(628, 1180.00, 2208.00, 3851.27, 'Jouzdu3X9vM1SUqyioE7KWkOG1ViSw5fIIDe1bLD', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-19 12:29:40', '2019-06-19 12:29:40'),
(629, 2988.00, 2160.00, 2062.73, 'Jouzdu3X9vM1SUqyioE7KWkOG1ViSw5fIIDe1bLD', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-19 12:29:41', '2019-06-19 12:29:41'),
(630, 3500.00, 3208.00, 2132.70, 'Jouzdu3X9vM1SUqyioE7KWkOG1ViSw5fIIDe1bLD', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-19 12:29:43', '2019-06-19 12:29:43'),
(631, 4996.00, 2016.00, 328.00, 'Jouzdu3X9vM1SUqyioE7KWkOG1ViSw5fIIDe1bLD', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-19 12:29:44', '2019-06-19 12:29:44'),
(632, 5412.00, 1544.00, 440.22, 'Jouzdu3X9vM1SUqyioE7KWkOG1ViSw5fIIDe1bLD', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-19 12:29:45', '2019-06-19 12:29:45'),
(633, 2128.00, 2152.00, 2901.34, 'PLD4pVMsIs3r6grKZIbBeVr0BIPyCdxlTlaMDPRZ', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-21 11:04:56', '2019-06-21 11:04:56'),
(634, 3296.00, 2200.00, 1771.60, 'PLD4pVMsIs3r6grKZIbBeVr0BIPyCdxlTlaMDPRZ', 13, NULL, NULL, 6, 'id131', 1, 0, '2019-06-21 11:05:04', '2019-06-21 11:05:04'),
(635, 2856.00, 2936.00, 2473.86, 'PLD4pVMsIs3r6grKZIbBeVr0BIPyCdxlTlaMDPRZ', 14, NULL, NULL, 6, 'id141', 1, 0, '2019-06-21 11:05:09', '2019-06-21 11:05:09'),
(636, 2144.00, 3536.00, 3395.03, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 13, NULL, NULL, 6, 'id131', 1, 0, '2019-06-21 11:08:20', '2019-06-21 11:20:44'),
(638, 720.00, 3344.00, 4581.74, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 13, NULL, NULL, 6, 'id133', 1, 0, '2019-06-21 11:08:23', '2019-06-21 11:20:44'),
(639, 2864.00, 3472.00, 2776.88, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 13, NULL, NULL, 6, 'id134', 1, 0, '2019-06-21 11:08:25', '2019-06-21 11:20:44'),
(640, 4768.00, 3624.00, 1948.92, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 13, NULL, NULL, 6, 'id135', 1, 0, '2019-06-21 11:08:26', '2019-06-21 11:20:44'),
(641, 3912.00, 3512.00, 2119.76, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 13, NULL, NULL, 6, 'id132', 1, 0, '2019-06-21 11:10:19', '2019-06-21 11:20:44'),
(642, 3800.00, 3232.00, 1950.59, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-21 11:12:23', '2019-06-21 11:20:44'),
(643, 4968.00, 1664.00, 33.94, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-21 11:13:09', '2019-06-21 11:20:44'),
(644, 3992.00, 2056.00, 1065.56, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-21 11:26:35', '2019-06-21 11:26:35'),
(645, 808.00, 3064.00, 4404.46, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-21 11:26:38', '2019-06-21 11:26:38'),
(646, 1128.00, 3704.00, 4358.30, 'bUUI8nmUDvemQhAHZ4O3c8dqVGWoGJyft4RAVfAy', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-21 11:26:40', '2019-06-21 11:26:40'),
(647, 744.00, 3104.00, 4477.79, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id121', 1, 0, '2019-06-22 06:13:41', '2019-06-22 06:21:28'),
(648, 2752.00, 1288.00, 2275.43, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id122', 1, 0, '2019-06-22 06:13:42', '2019-06-22 06:21:28'),
(649, 3768.00, 3456.00, 2150.35, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id123', 1, 0, '2019-06-22 06:13:46', '2019-06-22 06:21:28'),
(650, 5064.00, 1816.00, 146.86, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id124', 1, 0, '2019-06-22 06:13:48', '2019-06-22 06:21:28'),
(651, 504.00, 2720.00, 4605.12, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id125', 1, 0, '2019-06-22 06:13:51', '2019-06-22 06:21:28'),
(652, 1152.00, 3288.00, 4160.00, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id126', 1, 0, '2019-06-22 06:13:52', '2019-06-22 06:21:28'),
(653, 2712.00, 1512.00, 2286.78, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id127', 1, 0, '2019-06-22 06:13:54', '2019-06-22 06:21:28'),
(654, 2224.00, 896.00, 2879.08, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id121', 1, 0, '2019-06-22 06:21:00', '2019-06-22 06:21:28'),
(655, 3976.00, 3080.00, 1723.35, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id122', 1, 0, '2019-06-22 06:21:05', '2019-06-22 06:21:28'),
(656, 1496.00, 3296.00, 3848.07, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id123', 1, 0, '2019-06-22 06:21:06', '2019-06-22 06:21:28'),
(657, 816.00, 3328.00, 4486.49, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id124', 1, 0, '2019-06-22 06:21:07', '2019-06-22 06:21:28'),
(658, 4320.00, 3536.00, 1966.39, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id125', 1, 0, '2019-06-22 06:21:08', '2019-06-22 06:21:28'),
(659, 2888.00, 3608.00, 2848.37, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id126', 1, 0, '2019-06-22 06:21:09', '2019-06-22 06:21:28'),
(660, 4640.00, 3768.00, 2109.57, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id127', 1, 0, '2019-06-22 06:21:10', '2019-06-22 06:21:28'),
(661, 5352.00, 3608.00, 1953.46, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id128', 1, 0, '2019-06-22 06:21:12', '2019-06-22 06:21:28'),
(662, 984.00, 3008.00, 4219.77, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id129', 1, 0, '2019-06-22 06:21:16', '2019-06-22 06:21:28'),
(663, 496.00, 2808.00, 4633.40, 'wbPhzE6me5fzG178tmr1wxcPd427J55icrif8Jgc', 12, 5, 1244, 6, 'id1210', 1, 0, '2019-06-22 06:21:18', '2019-06-22 06:21:28'),
(664, 3304.00, 1152.00, 1771.06, 'y8FCKc2aXEfqe73hmHfcs8oqm7gqADn5MJSbzEFR', 12, 5, 1245, 6, 'id121', 1, 0, '2019-06-22 06:21:51', '2019-06-22 06:22:21'),
(665, 4608.00, 864.00, 909.08, 'y8FCKc2aXEfqe73hmHfcs8oqm7gqADn5MJSbzEFR', 12, 5, 1245, 6, 'id122', 1, 0, '2019-06-22 06:21:52', '2019-06-22 06:22:21'),
(666, 4608.00, 1696.00, 384.08, 'y8FCKc2aXEfqe73hmHfcs8oqm7gqADn5MJSbzEFR', 12, 5, 1245, 6, 'id123', 1, 0, '2019-06-22 06:21:53', '2019-06-22 06:22:21'),
(667, 1000.00, 2744.00, 4129.31, 'y8FCKc2aXEfqe73hmHfcs8oqm7gqADn5MJSbzEFR', 12, 5, 1245, 6, 'id124', 1, 0, '2019-06-22 06:21:55', '2019-06-22 06:22:21'),
(668, 688.00, 1120.00, 4341.32, 'y8FCKc2aXEfqe73hmHfcs8oqm7gqADn5MJSbzEFR', 12, 5, 1245, 6, 'id125', 1, 0, '2019-06-22 06:21:57', '2019-06-22 06:22:21'),
(670, 1872.00, 3584.00, 3650.92, 'y8FCKc2aXEfqe73hmHfcs8oqm7gqADn5MJSbzEFR', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-22 08:34:55', '2019-06-22 08:34:55'),
(671, 2272.00, 2296.00, 2787.12, 'y8FCKc2aXEfqe73hmHfcs8oqm7gqADn5MJSbzEFR', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-22 08:36:08', '2019-06-22 08:36:08'),
(672, 2464.00, 1816.00, 2531.24, 'y8FCKc2aXEfqe73hmHfcs8oqm7gqADn5MJSbzEFR', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-22 08:38:14', '2019-06-22 08:38:14'),
(673, 3296.00, 2192.00, 1769.30, 'y8FCKc2aXEfqe73hmHfcs8oqm7gqADn5MJSbzEFR', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-22 08:39:25', '2019-06-22 08:39:25'),
(676, 3428.00, 1976.00, 1594.23, 'F7FNna6Ym7ajrhmK8cIQ2LvXEZYvcMbGkJiMoPh6', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-22 11:46:55', '2019-06-22 12:09:21'),
(678, 1228.00, 1496.00, 3772.89, 'F7FNna6Ym7ajrhmK8cIQ2LvXEZYvcMbGkJiMoPh6', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-22 11:46:56', '2019-06-22 12:09:21'),
(680, 1664.00, 3496.00, 3787.41, 'eJrXaJwtX5WCyE99HXGBsQ4Nxfp0KgRMTItsYRQB', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-23 03:05:09', '2019-06-23 03:12:14'),
(681, 2080.00, 808.00, 3042.06, 'eJrXaJwtX5WCyE99HXGBsQ4Nxfp0KgRMTItsYRQB', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-23 03:20:04', '2019-06-23 03:20:04'),
(682, 3928.00, 3632.00, 2216.13, 'eJrXaJwtX5WCyE99HXGBsQ4Nxfp0KgRMTItsYRQB', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-23 03:20:07', '2019-06-23 03:20:07'),
(683, 648.00, 3320.00, 4640.45, 'eJrXaJwtX5WCyE99HXGBsQ4Nxfp0KgRMTItsYRQB', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-23 03:20:09', '2019-06-23 03:20:09'),
(684, 336.00, 2832.00, 4794.48, 'eJrXaJwtX5WCyE99HXGBsQ4Nxfp0KgRMTItsYRQB', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-23 03:20:11', '2019-06-23 03:20:11'),
(685, 856.00, 2936.00, 4320.19, 'LB42xn4cMszRJLoRleEnQsiS8eMFC3LLe6tG5ztG', 15, NULL, NULL, 6, 'id151', 1, 0, '2019-06-23 06:09:17', '2019-06-23 06:09:17'),
(686, 1912.00, 2048.00, 3100.97, 'wJLp2DYGQH41rNjbro4oVxnEDkyysqi4YjG36ydx', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-23 06:10:42', '2019-06-23 06:10:42'),
(730, 4576.00, 976.00, 824.62, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-23 07:07:39', '2019-06-23 07:08:04'),
(731, 3608.00, 2736.00, 1736.02, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-23 07:07:41', '2019-06-23 07:08:04'),
(732, 904.00, 2776.00, 4230.31, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-23 07:07:43', '2019-06-23 07:08:04'),
(733, 4912.00, 1776.00, 118.93, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-23 07:08:08', '2019-06-23 07:08:08'),
(734, 3888.00, 3624.00, 2228.66, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-23 07:08:25', '2019-06-23 07:08:25'),
(735, 1224.00, 3384.00, 4132.10, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-06-23 07:08:29', '2019-06-23 07:08:29'),
(736, 3832.00, 1872.00, 1174.50, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-06-23 07:08:32', '2019-06-23 07:08:32'),
(737, 5096.00, 3632.00, 1946.78, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id128', 1, 0, '2019-06-23 07:08:33', '2019-06-23 07:08:33'),
(738, 5312.00, 2016.00, 458.24, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id129', 1, 0, '2019-06-23 07:08:35', '2019-06-23 07:08:35'),
(740, 4432.00, 3800.00, 2184.98, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id1210', 1, 0, '2019-06-23 07:10:35', '2019-06-23 07:10:35'),
(741, 5176.00, 3800.00, 2120.00, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id1211', 1, 0, '2019-06-23 07:10:37', '2019-06-23 07:10:37'),
(742, 6280.00, 3824.00, 2494.28, 'qqFB4M5Gl82OYO0Boipi37B7Pij9HIiarp7JPaSC', 12, NULL, NULL, 6, 'id1212', 1, 0, '2019-06-23 07:10:39', '2019-06-23 07:10:39'),
(743, 3746.67, 1296.00, 1310.54, 'nXY41pXeVLX9tXFS19dXXkTwZmArvRkKd258Hqv6', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-23 13:09:29', '2019-06-23 13:09:29'),
(744, 3986.67, 1464.00, 1034.35, 'nXY41pXeVLX9tXFS19dXXkTwZmArvRkKd258Hqv6', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-06-23 13:09:29', '2019-06-23 13:09:29'),
(745, 3418.67, 944.00, 1746.22, 'nXY41pXeVLX9tXFS19dXXkTwZmArvRkKd258Hqv6', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-23 13:09:30', '2019-06-23 13:09:30'),
(746, 3098.67, 1928.00, 1910.14, 'nXY41pXeVLX9tXFS19dXXkTwZmArvRkKd258Hqv6', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-23 13:09:49', '2019-06-23 13:09:49'),
(747, 2028.00, 1200.00, 3007.85, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-23 13:21:50', '2019-06-23 13:21:50'),
(748, 2980.00, 1792.00, 2018.68, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-23 13:21:55', '2019-06-23 13:21:55'),
(749, 3132.00, 2088.00, 1906.44, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-23 13:21:56', '2019-06-23 13:21:56'),
(750, 3092.00, 1928.00, 1919.07, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-23 13:21:57', '2019-06-23 13:21:57'),
(751, 3180.00, 2400.00, 1950.59, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-23 13:21:59', '2019-06-23 13:21:59'),
(752, 2868.00, 2584.00, 2308.94, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-06-23 13:21:59', '2019-06-23 13:21:59'),
(753, 3636.00, 3720.00, 2445.12, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-06-23 13:21:59', '2019-06-23 13:21:59'),
(754, 5092.00, 2528.00, 845.47, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id128', 1, 0, '2019-06-23 13:22:25', '2019-06-23 13:22:25'),
(755, 4060.00, 1344.00, 997.21, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id129', 1, 0, '2019-06-23 13:22:26', '2019-06-23 13:22:26'),
(756, 5892.00, 1288.00, 981.23, 'Px4a7pLa08hNXMNhNfOX6CglusJR8JWciELGVp6D', 12, NULL, NULL, 6, 'id1210', 1, 0, '2019-06-23 13:22:26', '2019-06-23 13:22:26'),
(757, 2348.00, 2768.00, 2859.77, 'FM5rtHy0KBOxpwhtMsyIJuJFQY7l4qmPlM8K8xk7', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-24 05:44:21', '2019-06-24 05:53:37'),
(764, 3821.33, 2421.33, 1383.10, 'FM5rtHy0KBOxpwhtMsyIJuJFQY7l4qmPlM8K8xk7', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-24 05:53:08', '2019-06-24 05:53:37'),
(765, 717.33, 1245.33, 4303.38, 'FM5rtHy0KBOxpwhtMsyIJuJFQY7l4qmPlM8K8xk7', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-24 05:53:10', '2019-06-24 05:53:37'),
(767, 2909.33, 2421.33, 2211.27, 'FM5rtHy0KBOxpwhtMsyIJuJFQY7l4qmPlM8K8xk7', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-24 05:53:14', '2019-06-24 05:53:37'),
(768, 2949.33, 2045.33, 2078.03, 'FM5rtHy0KBOxpwhtMsyIJuJFQY7l4qmPlM8K8xk7', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-24 05:53:16', '2019-06-24 05:53:37'),
(769, 5181.33, 2773.33, 1095.56, 'FM5rtHy0KBOxpwhtMsyIJuJFQY7l4qmPlM8K8xk7', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-06-24 05:58:58', '2019-06-24 05:58:58'),
(777, 2116.00, 1200.00, 2921.05, 't5kgeg0bHRKLGrWq7WoKIsLv9Vl9sPWwkaBez5rN', 13, NULL, NULL, 6, 'id131', 1, 0, '2019-06-24 07:15:43', '2019-06-24 07:15:43'),
(786, 2700.00, 960.00, 2408.65, 't5kgeg0bHRKLGrWq7WoKIsLv9Vl9sPWwkaBez5rN', 13, NULL, NULL, 6, 'id132', 1, 0, '2019-06-24 07:19:50', '2019-06-24 07:19:50'),
(787, 2812.00, 1608.00, 2185.46, 't5kgeg0bHRKLGrWq7WoKIsLv9Vl9sPWwkaBez5rN', 13, NULL, NULL, 6, 'id132', 1, 0, '2019-06-24 07:19:51', '2019-06-24 07:19:51'),
(788, 3452.00, 2256.00, 1645.16, 't5kgeg0bHRKLGrWq7WoKIsLv9Vl9sPWwkaBez5rN', 13, NULL, NULL, 6, 'id133', 1, 0, '2019-06-24 07:19:54', '2019-06-24 07:19:54'),
(789, 2180.00, 1224.00, 2853.97, 't5kgeg0bHRKLGrWq7WoKIsLv9Vl9sPWwkaBez5rN', 13, NULL, NULL, 6, 'id134', 1, 0, '2019-06-24 07:19:55', '2019-06-24 07:19:55'),
(795, 1148.00, 912.00, 3925.47, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-24 07:24:00', '2019-06-24 07:24:00'),
(796, 5500.00, 2712.00, 1141.31, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-24 07:24:03', '2019-06-24 07:24:03'),
(797, 1068.00, 952.00, 3996.36, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-24 07:26:31', '2019-06-24 07:26:31'),
(798, 748.00, 1656.00, 4248.12, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-24 07:26:32', '2019-06-24 07:26:32'),
(799, 428.00, 2624.00, 4662.91, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-24 07:26:33', '2019-06-24 07:26:33'),
(800, 4812.00, 2976.00, 1301.08, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-06-24 07:26:37', '2019-06-24 07:26:37'),
(801, 5988.00, 2968.00, 1619.40, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-06-24 07:27:04', '2019-06-24 07:27:04'),
(802, 5412.00, 2824.00, 1209.77, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id128', 1, 0, '2019-06-24 07:27:05', '2019-06-24 07:27:05'),
(803, 780.00, 800.00, 4308.50, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id129', 1, 0, '2019-06-24 07:31:17', '2019-06-24 07:31:17'),
(804, 1100.00, 1088.00, 3941.93, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id1210', 1, 0, '2019-06-24 07:31:23', '2019-06-24 07:31:23'),
(805, 4124.00, 2352.00, 1096.03, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id1211', 1, 0, '2019-06-24 07:31:24', '2019-06-24 07:31:24'),
(806, 1004.00, 1016.00, 4048.17, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id1212', 1, 0, '2019-06-24 07:31:26', '2019-06-24 07:31:26'),
(807, 1564.00, 1024.00, 3495.64, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id1213', 1, 0, '2019-06-24 07:31:27', '2019-06-24 07:31:27'),
(808, 4564.00, 2712.00, 1111.40, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id1214', 1, 0, '2019-06-24 07:31:28', '2019-06-24 07:31:28'),
(809, 660.00, 488.00, 4498.99, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-24 07:34:12', '2019-06-24 07:34:12'),
(810, 4348.00, 1248.00, 783.26, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-24 07:34:55', '2019-06-24 07:34:55'),
(812, 1156.00, 3248.00, 4144.78, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-24 07:35:01', '2019-06-24 07:35:01'),
(813, 1636.00, 3240.00, 3701.12, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-24 07:35:01', '2019-06-24 07:35:01'),
(814, 2948.00, 3256.00, 2579.33, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 21, NULL, NULL, 6, 'id211', 1, 0, '2019-06-24 07:35:02', '2019-06-24 07:35:02'),
(815, 3572.00, 3256.00, 2118.11, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 21, NULL, NULL, 6, 'id212', 1, 0, '2019-06-24 07:35:02', '2019-06-24 07:35:02'),
(816, 972.00, 3704.00, 4500.76, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 21, NULL, NULL, 6, 'id213', 1, 0, '2019-06-24 07:35:03', '2019-06-24 07:35:03'),
(817, 2948.00, 2536.00, 2216.62, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 21, NULL, NULL, 6, 'id214', 1, 0, '2019-06-24 07:35:04', '2019-06-24 07:35:04'),
(818, 1252.00, 2336.00, 3799.66, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 21, NULL, NULL, 6, 'id215', 1, 0, '2019-06-24 07:35:04', '2019-06-24 07:35:04'),
(819, 2548.00, 536.00, 2705.51, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-24 07:35:43', '2019-06-24 07:35:43'),
(820, 4156.00, 1808.00, 848.53, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 13, NULL, NULL, 6, 'id131', 1, 0, '2019-06-24 07:35:46', '2019-06-24 07:35:46'),
(821, 4740.00, 1720.00, 257.99, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 13, NULL, NULL, 6, 'id132', 1, 0, '2019-06-24 07:35:49', '2019-06-24 07:35:49'),
(822, 3180.00, 1632.00, 1816.86, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 13, NULL, NULL, 6, 'id133', 1, 0, '2019-06-24 07:35:51', '2019-06-24 07:35:51'),
(823, 3916.00, 768.00, 1418.73, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 13, NULL, NULL, 6, 'id134', 1, 0, '2019-06-24 07:35:53', '2019-06-24 07:35:53'),
(824, 2892.00, 968.00, 2223.78, '8P63vfpdcCGY4kNKSUIAv0COBtGS81SLXQxKkANh', 13, NULL, NULL, 6, 'id134', 1, 0, '2019-06-24 07:35:54', '2019-06-24 07:35:54'),
(828, 4672.00, 2760.00, 1118.74, 'J8nTvQXUPHsy1ERV7vW9qDkjxAiU3uzeVWOHBkwR', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-24 07:49:28', '2019-06-24 07:49:52'),
(830, 4816.00, 2864.00, 1189.10, 'J8nTvQXUPHsy1ERV7vW9qDkjxAiU3uzeVWOHBkwR', 14, NULL, NULL, 6, 'id141', 1, 0, '2019-06-24 07:50:37', '2019-06-24 07:50:37'),
(831, 1208.00, 1056.00, 3836.41, 'J8nTvQXUPHsy1ERV7vW9qDkjxAiU3uzeVWOHBkwR', 14, NULL, NULL, 6, 'id142', 1, 0, '2019-06-24 07:50:39', '2019-06-24 07:50:39'),
(832, 1064.00, 848.00, 4016.81, 'J8nTvQXUPHsy1ERV7vW9qDkjxAiU3uzeVWOHBkwR', 14, NULL, NULL, 6, 'id143', 1, 0, '2019-06-24 07:50:53', '2019-06-24 07:50:53'),
(833, 4384.00, 2528.00, 1036.95, 'J8nTvQXUPHsy1ERV7vW9qDkjxAiU3uzeVWOHBkwR', 14, NULL, NULL, 6, 'id144', 1, 0, '2019-06-24 07:50:54', '2019-06-24 07:50:54'),
(834, 5640.00, 2768.00, 1259.49, 'J8nTvQXUPHsy1ERV7vW9qDkjxAiU3uzeVWOHBkwR', 14, NULL, NULL, 6, 'id145', 1, 0, '2019-06-24 07:51:07', '2019-06-24 07:51:07'),
(835, 4496.00, 2584.00, 1024.12, 'J8nTvQXUPHsy1ERV7vW9qDkjxAiU3uzeVWOHBkwR', 14, NULL, NULL, 6, 'id146', 1, 0, '2019-06-24 07:59:33', '2019-06-24 07:59:33'),
(836, 4496.00, 2496.00, 948.09, 'J8nTvQXUPHsy1ERV7vW9qDkjxAiU3uzeVWOHBkwR', 14, NULL, NULL, 6, 'id147', 1, 0, '2019-06-24 07:59:36', '2019-06-24 07:59:36'),
(837, 4680.00, 2536.00, 903.58, 'J8nTvQXUPHsy1ERV7vW9qDkjxAiU3uzeVWOHBkwR', 14, NULL, NULL, 6, 'id148', 1, 0, '2019-06-24 07:59:37', '2019-06-24 07:59:37'),
(838, 2300.00, 2368.00, 2780.43, 'hyA8qRWStuEMOH3xbjARmfxm9bxVrikC7S0NK6Cv', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-24 08:00:40', '2019-06-24 08:00:40'),
(839, 6844.00, 3232.00, 2408.12, 'hyA8qRWStuEMOH3xbjARmfxm9bxVrikC7S0NK6Cv', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-24 08:01:11', '2019-06-24 08:01:11'),
(840, 6748.00, 3312.00, 2388.91, 'hyA8qRWStuEMOH3xbjARmfxm9bxVrikC7S0NK6Cv', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-24 08:01:13', '2019-06-24 08:01:13'),
(841, 6668.00, 3408.00, 2398.75, 'hyA8qRWStuEMOH3xbjARmfxm9bxVrikC7S0NK6Cv', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-24 08:01:15', '2019-06-24 08:01:15'),
(842, 6604.00, 3488.00, 2413.64, 'hyA8qRWStuEMOH3xbjARmfxm9bxVrikC7S0NK6Cv', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-24 08:01:17', '2019-06-24 08:01:17'),
(843, 4684.00, 2032.00, 464.41, 'vytnG5FjXv3hF8i35moVLeUzlKmFa2w2sfzKLDUD', 12, NULL, NULL, 6, 'id1215', 1, 0, '2019-06-24 08:06:03', '2019-06-24 08:06:03'),
(844, 1683.20, 1075.20, 3368.80, 'qqYbTZmHdqVNj5LVfuLg7gHEfPuOp69ouUzcJlgn', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-24 08:15:04', '2019-06-24 08:15:04'),
(845, 1979.20, 1355.20, 3034.66, 'qqYbTZmHdqVNj5LVfuLg7gHEfPuOp69ouUzcJlgn', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-06-24 08:15:05', '2019-06-24 08:15:05'),
(846, 3675.20, 2475.20, 1535.27, 'qqYbTZmHdqVNj5LVfuLg7gHEfPuOp69ouUzcJlgn', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-24 08:15:39', '2019-06-24 08:15:39'),
(847, 1612.00, 992.00, 3454.83, 'H9tyBLM60bO5rN9r4ealAHxEugguMYZtzlNItMdv', 13, NULL, NULL, 6, 'id131', 1, 0, '2019-06-24 08:43:59', '2019-06-24 08:43:59'),
(848, 2044.00, 1256.00, 2983.44, 'H9tyBLM60bO5rN9r4ealAHxEugguMYZtzlNItMdv', 13, NULL, NULL, 6, 'id132', 1, 0, '2019-06-24 08:44:04', '2019-06-24 08:44:04'),
(849, 2468.00, 2040.00, 2552.39, 'H9tyBLM60bO5rN9r4ealAHxEugguMYZtzlNItMdv', 13, NULL, NULL, 6, 'id133', 1, 0, '2019-06-24 08:44:06', '2019-06-24 08:44:06'),
(850, 2660.00, 1632.00, 2336.67, 'H9tyBLM60bO5rN9r4ealAHxEugguMYZtzlNItMdv', 13, NULL, NULL, 6, 'id133', 1, 0, '2019-06-24 08:44:07', '2019-06-24 08:44:07'),
(851, 3188.00, 1936.00, 1824.93, 'H9tyBLM60bO5rN9r4ealAHxEugguMYZtzlNItMdv', 13, NULL, NULL, 6, 'id134', 1, 0, '2019-06-24 08:44:07', '2019-06-24 08:44:07'),
(853, 1928.00, 2856.00, 3279.07, 'u4DnhdVKUeadm495806uNUPsQk6h57d8dkebAsxv', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-24 10:35:09', '2019-06-24 10:35:09'),
(877, 348.00, 504.00, 4796.43, 'oiCXixdNAu0LHf0tIz1uUo9BkSAfxP2NYcwgY1x6', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-24 14:42:14', '2019-06-24 14:42:14'),
(878, 1284.00, 520.00, 3891.42, 'oiCXixdNAu0LHf0tIz1uUo9BkSAfxP2NYcwgY1x6', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-06-24 14:42:15', '2019-06-24 14:42:15'),
(879, 3316.00, 584.00, 2010.28, 'oiCXixdNAu0LHf0tIz1uUo9BkSAfxP2NYcwgY1x6', 13, NULL, NULL, 6, 'id131', 1, 0, '2019-06-24 14:42:16', '2019-06-24 14:42:16'),
(880, 4857.88, 1596.19, 166.47, 'lf6yd5z3XZkcG4X5oPsXmw6vq4NjOSfX2wbFhSKZ', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-25 12:35:32', '2019-06-25 12:39:37'),
(897, 2788.00, 968.00, 2322.43, 'SKodE516Xri2aoTLLcnMB9Ht4miD2blbsLmvHJ2i', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-26 17:15:45', '2019-06-26 17:15:45'),
(898, 448.00, 2776.00, 4672.44, 'WG6Ewe23dI0vZG02iZFfno4R93wBRhV5iqPUWFMO', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-27 09:25:48', '2019-06-27 09:26:27'),
(899, 2672.00, 1216.00, 2367.53, 'WG6Ewe23dI0vZG02iZFfno4R93wBRhV5iqPUWFMO', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-27 09:43:28', '2019-06-27 09:43:28'),
(900, 1074.67, 3360.00, 4261.69, 'BDPpeq5Nk2OqI9gYZrnLyBB1MsBOoiyk0BpIauEQ', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-27 09:49:43', '2019-06-27 09:49:43'),
(919, 1604.00, 1424.00, 3402.26, 'U83g7m9b8JBKBVNZiZMJYncoDM343xODnUg5Vm8F', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-27 16:27:19', '2019-06-27 16:27:19'),
(920, 1300.00, 1752.00, 3696.55, 'U83g7m9b8JBKBVNZiZMJYncoDM343xODnUg5Vm8F', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-27 16:27:20', '2019-06-27 16:27:20'),
(936, 369.38, 528.00, 4767.28, 'U83g7m9b8JBKBVNZiZMJYncoDM343xODnUg5Vm8F', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-06-27 16:59:17', '2019-06-27 16:59:17'),
(937, 1036.00, 2448.00, 4032.27, 'U83g7m9b8JBKBVNZiZMJYncoDM343xODnUg5Vm8F', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-27 18:48:38', '2019-06-27 18:48:38'),
(938, 1452.00, 2336.00, 3602.75, 'U83g7m9b8JBKBVNZiZMJYncoDM343xODnUg5Vm8F', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-27 18:48:43', '2019-06-27 18:48:43'),
(939, 3536.00, 1320.00, 1501.79, 'XGATbFbNUHpVl4Mq1As9I2lCtcZlHREWWNgeHgqO', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-27 22:49:34', '2019-06-27 22:49:34'),
(940, 4060.00, 2448.00, 1205.69, 'ZLzQebpSg4dCq7xKkogV5rpAELZc3xvZwMhnHrLr', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 01:14:07', '2019-06-28 01:14:07'),
(957, 1004.00, 1760.00, 3992.65, 'BT8oL9kRCpe3LI44mJQvWVTeSpcrgkkMZgDhAhvY', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 06:25:51', '2019-06-28 06:25:51'),
(958, 2900.00, 1976.00, 2115.69, 'BT8oL9kRCpe3LI44mJQvWVTeSpcrgkkMZgDhAhvY', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-06-28 06:25:54', '2019-06-28 06:25:54'),
(959, 3836.00, 1728.00, 1160.69, 'BT8oL9kRCpe3LI44mJQvWVTeSpcrgkkMZgDhAhvY', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-06-28 06:25:56', '2019-06-28 06:25:56'),
(960, 3244.00, 792.00, 1967.82, 'BT8oL9kRCpe3LI44mJQvWVTeSpcrgkkMZgDhAhvY', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-06-28 06:25:57', '2019-06-28 06:25:57'),
(961, 2724.00, 1432.00, 2286.38, 'BT8oL9kRCpe3LI44mJQvWVTeSpcrgkkMZgDhAhvY', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 06:25:57', '2019-06-28 06:25:57'),
(962, 3524.00, 1224.00, 1543.40, 'BT8oL9kRCpe3LI44mJQvWVTeSpcrgkkMZgDhAhvY', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 06:25:58', '2019-06-28 06:25:58'),
(963, 4716.00, 936.00, 802.44, 'BT8oL9kRCpe3LI44mJQvWVTeSpcrgkkMZgDhAhvY', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 06:25:58', '2019-06-28 06:25:58'),
(988, 7060.00, 1448.00, 2077.91, 'i9d8DONEEjcDjEvgPicv5f4Id0xlVxkBC5Da82fo', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 10:46:02', '2019-06-28 10:46:02'),
(990, 4044.00, 992.00, 1179.29, 'i9d8DONEEjcDjEvgPicv5f4Id0xlVxkBC5Da82fo', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 10:48:47', '2019-06-28 10:48:47'),
(991, 3148.00, 1648.00, 1848.43, 'i9d8DONEEjcDjEvgPicv5f4Id0xlVxkBC5Da82fo', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 10:48:48', '2019-06-28 10:48:48'),
(992, 2324.00, 984.00, 2763.19, 'i9d8DONEEjcDjEvgPicv5f4Id0xlVxkBC5Da82fo', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 10:48:48', '2019-06-28 10:48:48'),
(996, 3524.00, 2376.00, 1624.85, 'mu4Lh7jG5hYoRlF5w2Mywf1j7X8FuWg2YMETZTY2', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 11:02:24', '2019-06-28 11:02:24'),
(997, 3196.00, 1632.00, 1800.87, 'mu4Lh7jG5hYoRlF5w2Mywf1j7X8FuWg2YMETZTY2', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 11:02:32', '2019-06-28 11:02:32'),
(998, 5316.00, 1488.00, 377.36, 'mu4Lh7jG5hYoRlF5w2Mywf1j7X8FuWg2YMETZTY2', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-28 11:02:34', '2019-06-28 11:02:34'),
(1000, 1636.00, 832.00, 3467.32, 'mu4Lh7jG5hYoRlF5w2Mywf1j7X8FuWg2YMETZTY2', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-28 11:02:35', '2019-06-28 11:02:35'),
(1001, 3316.00, 984.00, 1821.54, 'KV0cjW6WtWmfM4qVq51gRVwsnvb9iasHUO6pVpr3', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 11:03:02', '2019-06-28 11:03:15'),
(1002, 4724.00, 1320.00, 457.61, 'KV0cjW6WtWmfM4qVq51gRVwsnvb9iasHUO6pVpr3', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 11:03:03', '2019-06-28 11:03:15'),
(1005, 4604.00, 2248.00, 683.57, 'KV0cjW6WtWmfM4qVq51gRVwsnvb9iasHUO6pVpr3', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-28 11:03:05', '2019-06-28 11:03:15'),
(1007, 3936.00, 768.00, 1400.55, 'XiTu14SBuD4hFsLpAAlgpdViKqDeid9pcrdRKL1U', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 12:54:38', '2019-06-28 12:54:50'),
(1008, 2848.00, 1360.00, 2168.94, '1e8UJEtvFhFS2k4TROjBCznnzS8mi8YARzc5Q9aH', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 15:02:00', '2019-06-28 15:05:37'),
(1009, 2112.00, 2152.00, 2917.14, '1e8UJEtvFhFS2k4TROjBCznnzS8mi8YARzc5Q9aH', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 15:04:51', '2019-06-28 15:05:37'),
(1013, 3920.00, 3552.00, 2150.27, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 16:07:18', '2019-06-28 16:07:18'),
(1014, 1744.00, 3520.00, 3729.04, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 16:07:20', '2019-06-28 16:07:20'),
(1016, 5200.00, 3560.00, 1883.52, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-28 16:07:25', '2019-06-28 16:07:25'),
(1017, 6296.00, 3648.00, 2354.15, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-28 16:07:29', '2019-06-28 16:07:29'),
(1018, 4424.00, 3696.00, 2086.79, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-28 16:08:02', '2019-06-28 16:08:02'),
(1019, 2400.00, 3544.00, 3187.98, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-06-28 16:14:03', '2019-06-28 16:14:03'),
(1020, 976.00, 3200.00, 4291.20, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-06-28 16:14:05', '2019-06-28 16:14:05'),
(1021, 688.00, 3672.00, 4739.27, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id128', 1, 0, '2019-06-28 16:14:06', '2019-06-28 16:14:06'),
(1022, 1280.00, 3672.00, 4208.94, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id129', 1, 0, '2019-06-28 16:14:06', '2019-06-28 16:14:06'),
(1023, 1280.00, 3416.00, 4094.50, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1210', 1, 0, '2019-06-28 16:14:07', '2019-06-28 16:14:07'),
(1024, 3832.00, 3112.00, 1836.68, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1211', 1, 0, '2019-06-28 16:14:22', '2019-06-28 16:14:22'),
(1025, 752.00, 2816.00, 4387.48, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1212', 1, 0, '2019-06-28 16:14:24', '2019-06-28 16:14:24'),
(1026, 1208.00, 2912.00, 3977.04, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1213', 1, 0, '2019-06-28 16:14:25', '2019-06-28 16:14:25'),
(1027, 400.00, 3048.00, 4789.16, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1214', 1, 0, '2019-06-28 16:14:26', '2019-06-28 16:14:26'),
(1028, 424.00, 2592.00, 4656.59, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1215', 1, 0, '2019-06-28 16:14:27', '2019-06-28 16:14:27'),
(1029, 2728.00, 2616.00, 2446.81, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1216', 1, 0, '2019-06-28 16:14:27', '2019-06-28 16:14:27'),
(1030, 3664.00, 2944.00, 1827.87, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1216', 1, 0, '2019-06-28 16:14:28', '2019-06-28 16:14:28'),
(1031, 4096.00, 3264.00, 1812.90, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1216', 1, 0, '2019-06-28 16:14:28', '2019-06-28 16:14:28'),
(1032, 2840.00, 3080.00, 2562.96, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1217', 1, 0, '2019-06-28 16:14:29', '2019-06-28 16:14:29'),
(1033, 2016.00, 2880.00, 3205.84, 'MQPekgUNinW7UL1x93ewmstWvtmDGWqSEi5yryIt', 12, NULL, NULL, 6, 'id1218', 1, 0, '2019-06-28 16:14:30', '2019-06-28 16:14:30'),
(1034, 3024.00, 1288.00, 2008.24, 'BnEhA1IKEqmR3jN0McKeJI9qar7yS9spDKP2iVnz', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 16:29:54', '2019-06-28 16:29:54'),
(1035, 4200.00, 3552.00, 2025.28, 'BnEhA1IKEqmR3jN0McKeJI9qar7yS9spDKP2iVnz', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 16:29:56', '2019-06-28 16:29:56'),
(1036, 1088.00, 3536.00, 4319.30, 'BnEhA1IKEqmR3jN0McKeJI9qar7yS9spDKP2iVnz', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-28 16:30:00', '2019-06-28 16:30:00'),
(1037, 4616.00, 3864.00, 2208.25, 'BnEhA1IKEqmR3jN0McKeJI9qar7yS9spDKP2iVnz', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-28 16:30:02', '2019-06-28 16:30:02'),
(1038, 5264.00, 3696.00, 2026.34, 'BnEhA1IKEqmR3jN0McKeJI9qar7yS9spDKP2iVnz', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-28 16:30:04', '2019-06-28 16:30:04'),
(1039, 3704.00, 3536.00, 2252.56, '2MduvHZMIgSSzghTbFmyfNpRyJ84yfKeh3c35cf9', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 16:34:44', '2019-06-28 16:39:16'),
(1042, 6368.00, 3720.00, 2454.06, '2MduvHZMIgSSzghTbFmyfNpRyJ84yfKeh3c35cf9', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-28 16:35:00', '2019-06-28 16:39:16'),
(1043, 5520.00, 3712.00, 2091.74, '2MduvHZMIgSSzghTbFmyfNpRyJ84yfKeh3c35cf9', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-06-28 16:35:03', '2019-06-28 16:39:16'),
(1044, 4816.00, 3624.00, 1943.98, '2MduvHZMIgSSzghTbFmyfNpRyJ84yfKeh3c35cf9', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-28 16:35:48', '2019-06-28 16:39:16'),
(1045, 568.00, 3560.00, 4803.77, '2MduvHZMIgSSzghTbFmyfNpRyJ84yfKeh3c35cf9', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 16:36:22', '2019-06-28 16:39:16'),
(1046, 552.00, 2856.00, 4591.06, '2MduvHZMIgSSzghTbFmyfNpRyJ84yfKeh3c35cf9', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-06-28 16:38:22', '2019-06-28 16:39:16'),
(1047, 728.00, 3576.00, 4663.29, '2MduvHZMIgSSzghTbFmyfNpRyJ84yfKeh3c35cf9', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-06-28 16:38:29', '2019-06-28 16:39:16'),
(1048, 3040.00, 872.00, 2115.69, 'SqLkywweqASnpIOhGhGOKBVYFHz4Ry13zomINX9L', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 16:45:20', '2019-06-28 16:45:39'),
(1049, 4776.00, 3488.00, 1812.91, 'SqLkywweqASnpIOhGhGOKBVYFHz4Ry13zomINX9L', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 16:45:25', '2019-06-28 16:45:39'),
(1050, 440.00, 3120.00, 4771.93, 'SqLkywweqASnpIOhGhGOKBVYFHz4Ry13zomINX9L', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-28 16:45:26', '2019-06-28 16:45:39'),
(1051, 4360.00, 3560.00, 1975.81, 'SqLkywweqASnpIOhGhGOKBVYFHz4Ry13zomINX9L', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-28 16:45:28', '2019-06-28 16:45:39'),
(1052, 1576.00, 952.00, 3494.39, 'bnyfUcy4oLvwk0WsIvZMVbWDwsP37aC2Y87FZZZZ', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-28 16:48:51', '2019-06-28 16:48:51'),
(1053, 4008.00, 3520.00, 2079.54, 'bnyfUcy4oLvwk0WsIvZMVbWDwsP37aC2Y87FZZZZ', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-06-28 16:48:54', '2019-06-28 16:48:54'),
(1054, 4656.00, 3544.00, 1886.17, 'bnyfUcy4oLvwk0WsIvZMVbWDwsP37aC2Y87FZZZZ', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-06-28 16:48:56', '2019-06-28 16:48:56'),
(1055, 5632.00, 2000.00, 712.00, 'bnyfUcy4oLvwk0WsIvZMVbWDwsP37aC2Y87FZZZZ', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-06-28 16:48:58', '2019-06-28 16:48:58'),
(1056, 3152.00, 1024.00, 1956.14, 'vgJKx38Q7I5M5zAM3l7mtpUwJpNDx4WUYkbeLBA6', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-06-29 14:32:33', '2019-06-29 14:32:47'),
(1062, 5724.00, 2696.00, 1243.40, '7R9az1rLaIVKZt6hLaM6ng9RF2kmsuFJTbABHNfs', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 01:57:04', '2019-07-01 01:57:04'),
(1063, 2108.00, 1544.00, 2891.59, 'xzgNkO34XMzeZCGSeFgP3bWhKjyo1Iq9e3idss7V', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 04:47:09', '2019-07-01 04:47:09'),
(1066, 1020.00, 1448.00, 3983.24, 'xzgNkO34XMzeZCGSeFgP3bWhKjyo1Iq9e3idss7V', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-01 04:47:16', '2019-07-01 04:47:16'),
(1067, 1276.00, 1800.00, 3721.69, 'xzgNkO34XMzeZCGSeFgP3bWhKjyo1Iq9e3idss7V', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-01 04:47:16', '2019-07-01 04:47:16'),
(1097, 1044.00, 2608.00, 4057.67, 's5dNvIsTj1e1OtE2LLWjHTdCxJvT2oeDSl3Hd9Bl', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 06:21:37', '2019-07-01 06:21:37'),
(1098, 2132.00, 2568.00, 2996.15, 's5dNvIsTj1e1OtE2LLWjHTdCxJvT2oeDSl3Hd9Bl', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-07-01 06:21:38', '2019-07-01 06:21:38'),
(1099, 2388.00, 2152.00, 2648.95, 's5dNvIsTj1e1OtE2LLWjHTdCxJvT2oeDSl3Hd9Bl', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-01 06:21:38', '2019-07-01 06:21:38'),
(1100, 3404.00, 2248.00, 1687.62, 's5dNvIsTj1e1OtE2LLWjHTdCxJvT2oeDSl3Hd9Bl', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-01 06:21:38', '2019-07-01 06:21:38'),
(1101, 3508.00, 2624.00, 1757.91, 's5dNvIsTj1e1OtE2LLWjHTdCxJvT2oeDSl3Hd9Bl', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-01 06:21:39', '2019-07-01 06:21:39'),
(1102, 2852.00, 2928.00, 2476.76, 's5dNvIsTj1e1OtE2LLWjHTdCxJvT2oeDSl3Hd9Bl', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-01 06:21:39', '2019-07-01 06:21:39'),
(1106, 2476.00, 1072.00, 2594.20, 's5dNvIsTj1e1OtE2LLWjHTdCxJvT2oeDSl3Hd9Bl', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-07-01 06:41:39', '2019-07-01 06:41:39'),
(1118, 2028.00, 2576.00, 3097.99, 'BOfz8NlQlDTzPmT0z0513jpIb5jzw9K4ySLZfA0p', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 06:50:57', '2019-07-01 06:50:57'),
(1119, 2540.00, 2808.00, 2699.32, 'BOfz8NlQlDTzPmT0z0513jpIb5jzw9K4ySLZfA0p', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-01 06:51:21', '2019-07-01 06:51:21'),
(1120, 2588.00, 2512.00, 2545.08, 'BOfz8NlQlDTzPmT0z0513jpIb5jzw9K4ySLZfA0p', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-07-01 06:51:22', '2019-07-01 06:51:22'),
(1121, 2540.00, 2016.00, 2477.81, 'BOfz8NlQlDTzPmT0z0513jpIb5jzw9K4ySLZfA0p', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-01 06:51:23', '2019-07-01 06:51:23'),
(1122, 3068.00, 3136.00, 2411.20, 'BOfz8NlQlDTzPmT0z0513jpIb5jzw9K4ySLZfA0p', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-01 06:51:25', '2019-07-01 06:51:25'),
(1123, 3756.00, 2824.00, 1681.69, 'BOfz8NlQlDTzPmT0z0513jpIb5jzw9K4ySLZfA0p', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-01 06:51:30', '2019-07-01 06:51:30'),
(1124, 2948.00, 2544.00, 2219.69, 'BOfz8NlQlDTzPmT0z0513jpIb5jzw9K4ySLZfA0p', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-07-01 06:51:38', '2019-07-01 06:51:38'),
(1132, 1228.00, 1152.00, 3805.93, '9Y092YThGZUfDmSVcQ97kSoztC7ezNskUT7PeNU3', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 06:59:23', '2019-07-01 06:59:23'),
(1133, 1964.00, 1120.00, 3084.74, '9Y092YThGZUfDmSVcQ97kSoztC7ezNskUT7PeNU3', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-07-01 06:59:24', '2019-07-01 06:59:24'),
(1134, 2852.00, 1048.00, 2237.48, '9Y092YThGZUfDmSVcQ97kSoztC7ezNskUT7PeNU3', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-01 06:59:24', '2019-07-01 06:59:24'),
(1135, 3388.00, 1032.00, 1736.66, '9Y092YThGZUfDmSVcQ97kSoztC7ezNskUT7PeNU3', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-01 06:59:25', '2019-07-01 06:59:25'),
(1136, 3980.00, 1040.00, 1205.06, '9Y092YThGZUfDmSVcQ97kSoztC7ezNskUT7PeNU3', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-01 06:59:26', '2019-07-01 06:59:26'),
(1137, 2548.00, 1448.00, 2459.74, '9Y092YThGZUfDmSVcQ97kSoztC7ezNskUT7PeNU3', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-01 06:59:30', '2019-07-01 06:59:30'),
(1138, 2053.33, 1061.33, 3011.07, 'JvbghY2MQODzg5fuF9xLfNJE6zSBdqgJdSzRSOs0', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 07:02:32', '2019-07-01 07:02:32'),
(1139, 2981.33, 1141.33, 2090.21, 'JvbghY2MQODzg5fuF9xLfNJE6zSBdqgJdSzRSOs0', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-01 07:02:32', '2019-07-01 07:02:32'),
(1143, 4517.33, 3445.33, 1816.56, 'JvbghY2MQODzg5fuF9xLfNJE6zSBdqgJdSzRSOs0', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-01 07:05:01', '2019-07-01 07:05:01'),
(1145, 5197.33, 1949.33, 324.86, 'JvbghY2MQODzg5fuF9xLfNJE6zSBdqgJdSzRSOs0', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-01 07:07:15', '2019-07-01 07:07:15'),
(1146, 5280.00, 3624.00, 1957.30, 'PvcsWxTwPKkHpRJQOvunYwJiXCpLRbhKRzlDw6tJ', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 07:08:36', '2019-07-01 07:08:36'),
(1147, 3536.00, 2184.00, 1538.17, 'PvcsWxTwPKkHpRJQOvunYwJiXCpLRbhKRzlDw6tJ', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-01 07:09:45', '2019-07-01 07:09:45'),
(1148, 2272.00, 2288.00, 2785.39, 'PvcsWxTwPKkHpRJQOvunYwJiXCpLRbhKRzlDw6tJ', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-01 07:09:56', '2019-07-01 07:09:56'),
(1154, 1836.00, 1288.00, 3185.22, 'XJSc39IcYGYv3h9zrKWlcvqhBb08jor3dSrY4ylE', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 07:16:17', '2019-07-01 07:16:17'),
(1155, 2204.00, 1368.00, 2810.28, 'XJSc39IcYGYv3h9zrKWlcvqhBb08jor3dSrY4ylE', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-07-01 07:16:18', '2019-07-01 07:16:18');
INSERT INTO `pointers` (`id`, `xCords`, `yCords`, `distance`, `sessionID`, `productID`, `user_id`, `invoice_id`, `game_id`, `stampId`, `stamped`, `removed`, `created_at`, `updated_at`) VALUES
(1156, 2764.00, 1424.00, 2247.56, 'XJSc39IcYGYv3h9zrKWlcvqhBb08jor3dSrY4ylE', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-01 07:16:19', '2019-07-01 07:16:19'),
(1157, 1156.00, 1776.00, 3841.01, 'XJSc39IcYGYv3h9zrKWlcvqhBb08jor3dSrY4ylE', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-01 07:16:21', '2019-07-01 07:16:21'),
(1158, 1716.00, 1864.00, 3284.72, 'XJSc39IcYGYv3h9zrKWlcvqhBb08jor3dSrY4ylE', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-01 07:16:22', '2019-07-01 07:16:22'),
(1159, 2252.00, 2144.00, 2781.63, 'XJSc39IcYGYv3h9zrKWlcvqhBb08jor3dSrY4ylE', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-01 07:16:25', '2019-07-01 07:16:25'),
(1160, 1064.00, 3608.00, 4372.14, 'EFsO6mOCS3jZv8zTWem3V1SBTn9IRP2Vzb4QtUsp', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 07:50:44', '2019-07-01 07:50:44'),
(1161, 3708.00, 832.00, 1546.51, '3PIhPDhopLEtPj6LEu3PjwmJwcqZx34AmMbxOlMa', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-01 08:03:22', '2019-07-01 08:03:22'),
(1162, 2612.00, 1288.00, 2417.32, '3PIhPDhopLEtPj6LEu3PjwmJwcqZx34AmMbxOlMa', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-01 08:03:56', '2019-07-01 08:03:56'),
(1163, 3244.00, 816.00, 1957.01, '3PIhPDhopLEtPj6LEu3PjwmJwcqZx34AmMbxOlMa', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-01 08:06:24', '2019-07-01 08:06:24'),
(1186, 816.00, 3296.00, 4474.89, '2GzSJbKkMwx7FavTN9KJWkJaHn5vEnDYtAn3eMuo', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-02 03:46:14', '2019-07-02 03:46:14'),
(1187, 472.00, 3000.00, 4706.56, '2GzSJbKkMwx7FavTN9KJWkJaHn5vEnDYtAn3eMuo', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-02 03:46:46', '2019-07-02 03:46:46'),
(1188, 4472.00, 3672.00, 2051.01, 'f9FwhFyOc8EC0XOzAeEyE8DmhgPbaIAqRhkTl7h3', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-02 08:24:01', '2019-07-02 08:24:01'),
(1236, 3428.00, 792.00, 1805.95, 'y2JHuUAj1zOpdYiuSzq4tvAFHu3QMHBapq5rb2tn', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-02 10:29:16', '2019-07-02 10:29:16'),
(1237, 3868.00, 1448.00, 1153.25, 'y2JHuUAj1zOpdYiuSzq4tvAFHu3QMHBapq5rb2tn', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-02 10:31:44', '2019-07-02 10:31:44'),
(1238, 3868.00, 1656.00, 1128.45, 'y2JHuUAj1zOpdYiuSzq4tvAFHu3QMHBapq5rb2tn', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-02 10:31:45', '2019-07-02 10:31:45'),
(1240, 2796.00, 880.00, 2343.69, 'y2JHuUAj1zOpdYiuSzq4tvAFHu3QMHBapq5rb2tn', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-02 10:44:14', '2019-07-02 10:44:14'),
(1243, 6544.00, 3728.00, 2563.26, 'f9FwhFyOc8EC0XOzAeEyE8DmhgPbaIAqRhkTl7h3', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-02 10:45:42', '2019-07-02 10:45:42'),
(1244, 6976.00, 3832.00, 2921.13, 'f9FwhFyOc8EC0XOzAeEyE8DmhgPbaIAqRhkTl7h3', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-02 10:45:47', '2019-07-02 10:45:47'),
(1256, 2564.00, 1536.00, 2436.75, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-02 21:19:42', '2019-07-02 21:19:42'),
(1257, 1964.00, 2064.00, 3055.23, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-02 21:19:43', '2019-07-02 21:19:43'),
(1258, 2260.00, 2184.00, 2780.60, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-02 21:19:44', '2019-07-02 21:19:44'),
(1259, 3100.00, 1872.00, 1904.91, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-02 21:19:45', '2019-07-02 21:19:45'),
(1260, 4036.00, 1704.00, 960.13, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-02 21:19:46', '2019-07-02 21:19:46'),
(1261, 1964.00, 3320.00, 3443.32, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-07-02 21:19:57', '2019-07-02 21:19:57'),
(1262, 2788.00, 3384.00, 2784.18, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-07-02 21:20:01', '2019-07-02 21:20:01'),
(1263, 3148.00, 3472.00, 2568.61, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id128', 1, 0, '2019-07-02 21:20:04', '2019-07-02 21:20:04'),
(1264, 3732.00, 2184.00, 1357.83, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id129', 1, 0, '2019-07-02 21:20:11', '2019-07-02 21:20:11'),
(1265, 5172.00, 800.00, 905.27, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id1210', 1, 0, '2019-07-02 21:21:08', '2019-07-02 21:21:08'),
(1266, 3068.00, 1688.00, 1928.00, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id1211', 1, 0, '2019-07-02 21:39:24', '2019-07-02 21:39:24'),
(1267, 3508.00, 1168.00, 1576.24, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id1212', 1, 0, '2019-07-02 21:39:24', '2019-07-02 21:39:24'),
(1268, 3964.00, 1408.00, 1069.31, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id1213', 1, 0, '2019-07-02 21:39:26', '2019-07-02 21:39:26'),
(1269, 1636.00, 1400.00, 3372.32, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id1214', 1, 0, '2019-07-02 21:41:24', '2019-07-02 21:41:24'),
(1270, 4868.00, 984.00, 715.54, 'ejOgLiXhZQyPBZRjDfsvMrCaMJ61pvnsT5xxKfGD', 12, NULL, NULL, 6, 'id1215', 1, 0, '2019-07-02 21:41:26', '2019-07-02 21:41:26'),
(1271, 5764.00, 2696.00, 1267.24, 'iDHnQpqaVmtH4NHv1MTljX9uUYpBu5dIfhiNKWsI', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-03 01:26:33', '2019-07-03 01:26:33'),
(1272, 5836.00, 2712.00, 1324.45, 'iDHnQpqaVmtH4NHv1MTljX9uUYpBu5dIfhiNKWsI', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-03 01:26:59', '2019-07-03 01:26:59'),
(1295, 1537.88, 2084.19, 3478.16, 'G14NVZuq5zgsgthplSJYBI6mH0WcRXMELvsk8jkT', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-03 06:38:57', '2019-07-03 06:38:57'),
(1296, 3657.88, 1796.19, 1340.04, 'G14NVZuq5zgsgthplSJYBI6mH0WcRXMELvsk8jkT', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-03 06:40:24', '2019-07-03 06:40:24'),
(1299, 2476.00, 1648.00, 2520.32, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-03 06:45:30', '2019-07-03 06:45:30'),
(1300, 3044.00, 1712.00, 1952.15, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-03 06:45:31', '2019-07-03 06:45:31'),
(1301, 3204.00, 1264.00, 1841.48, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-03 06:45:32', '2019-07-03 06:45:32'),
(1302, 2532.00, 768.00, 2630.15, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-03 06:45:33', '2019-07-03 06:45:33'),
(1303, 1316.00, 632.00, 3828.52, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-03 06:45:34', '2019-07-03 06:45:34'),
(1304, 3700.00, 1568.00, 1301.54, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-07-03 06:46:29', '2019-07-03 06:46:29'),
(1305, 4828.00, 1712.00, 169.71, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-07-03 06:46:31', '2019-07-03 06:46:31'),
(1306, 4188.00, 1976.00, 857.79, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-07-03 06:46:32', '2019-07-03 06:46:32'),
(1307, 4492.00, 936.00, 905.27, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id128', 1, 0, '2019-07-03 06:46:33', '2019-07-03 06:46:33'),
(1308, 5260.00, 1272.00, 492.70, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id129', 1, 0, '2019-07-03 06:46:34', '2019-07-03 06:46:34'),
(1309, 4076.00, 2928.00, 1544.02, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1210', 1, 0, '2019-07-03 06:46:37', '2019-07-03 06:46:37'),
(1310, 4084.00, 936.00, 1182.05, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1211', 1, 0, '2019-07-03 06:52:15', '2019-07-03 06:52:15'),
(1311, 3292.00, 1120.00, 1796.17, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1212', 1, 0, '2019-07-03 06:52:16', '2019-07-03 06:52:16'),
(1312, 385.88, 2372.19, 4657.90, 'G14NVZuq5zgsgthplSJYBI6mH0WcRXMELvsk8jkT', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-03 07:06:57', '2019-07-03 07:06:57'),
(1313, 3980.00, 840.00, 1323.39, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1213', 1, 0, '2019-07-03 07:23:19', '2019-07-03 07:23:19'),
(1314, 1092.00, 2312.00, 3953.55, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1214', 1, 0, '2019-07-03 07:28:21', '2019-07-03 07:28:21'),
(1315, 1716.00, 2368.00, 3349.75, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-07-03 07:28:21', '2019-07-03 07:28:21'),
(1322, 2284.00, 448.00, 2982.04, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1215', 1, 0, '2019-07-03 08:00:15', '2019-07-03 08:00:15'),
(1323, 3988.00, 448.00, 1598.02, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', NULL, NULL, NULL, 6, NULL, 1, 0, '2019-07-03 08:00:15', '2019-07-03 08:00:15'),
(1324, 3340.00, 1040.00, 1778.27, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1216', 1, 0, '2019-07-03 08:00:16', '2019-07-03 08:00:16'),
(1325, 2084.00, 728.00, 3066.16, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1216', 1, 0, '2019-07-03 08:00:16', '2019-07-03 08:00:16'),
(1326, 1452.00, 736.00, 3669.64, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1217', 1, 0, '2019-07-03 08:00:17', '2019-07-03 08:00:17'),
(1327, 1620.00, 416.00, 3607.68, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1218', 1, 0, '2019-07-03 08:00:17', '2019-07-03 08:00:17'),
(1328, 1868.00, 336.00, 3407.68, 'vYMJhhJaPx1Gg1QP38GOvpRTiAeVybJtOOHgSZI9', 12, NULL, NULL, 6, 'id1219', 1, 0, '2019-07-03 08:00:20', '2019-07-03 08:00:20'),
(1329, 636.00, 3360.00, 4669.60, 'nQiDgBeO3hdVU9iWh0hEAju52eoibXmQBoQWj3bY', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-03 08:05:55', '2019-07-03 08:05:55'),
(1330, 6708.00, 1552.00, 1717.39, 'nQiDgBeO3hdVU9iWh0hEAju52eoibXmQBoQWj3bY', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-03 08:06:05', '2019-07-03 08:06:05'),
(1331, 5116.00, 456.00, 1237.83, 'nQiDgBeO3hdVU9iWh0hEAju52eoibXmQBoQWj3bY', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-03 08:06:12', '2019-07-03 08:06:12'),
(1332, 3452.00, 1064.00, 1665.33, 'nQiDgBeO3hdVU9iWh0hEAju52eoibXmQBoQWj3bY', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-03 08:06:19', '2019-07-03 08:06:19'),
(1333, 3876.00, 3336.00, 1992.56, 'nQiDgBeO3hdVU9iWh0hEAju52eoibXmQBoQWj3bY', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-03 08:06:22', '2019-07-03 08:06:22'),
(1334, 5588.00, 2824.00, 1281.00, 'Ac64WkpcIY34AmzSKqcOxdKZ6l8wamxhcl5xSK26', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-03 08:07:01', '2019-07-03 08:07:01'),
(1335, 932.00, 3112.00, 4306.26, 'Ac64WkpcIY34AmzSKqcOxdKZ6l8wamxhcl5xSK26', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-03 08:09:39', '2019-07-03 08:09:39'),
(1337, 904.00, 3504.00, 4473.21, 'ZInuWULsiCFxculmOmVv8u4MmeJbKSyJMqUmmAlb', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-03 09:26:41', '2019-07-03 09:26:41'),
(1338, 1868.00, 920.00, 3220.90, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-03 17:33:14', '2019-07-03 19:37:56'),
(1339, 2460.00, 984.00, 2631.90, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-03 17:33:15', '2019-07-03 19:37:56'),
(1340, 2364.00, 840.00, 2765.24, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-03 17:33:17', '2019-07-03 19:37:56'),
(1341, 2492.00, 1488.00, 2511.97, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-03 18:00:31', '2019-07-03 19:37:56'),
(1342, 2956.00, 1584.00, 2042.65, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-03 18:00:32', '2019-07-03 19:37:56'),
(1343, 2332.00, 2952.00, 2948.66, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-07-03 18:03:10', '2019-07-03 19:37:56'),
(1344, 2828.00, 3320.00, 2713.60, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-07-03 18:03:11', '2019-07-03 19:37:56'),
(1345, 3660.00, 3288.00, 2084.44, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id128', 1, 0, '2019-07-03 18:03:13', '2019-07-03 19:37:56'),
(1346, 4716.00, 3432.00, 1766.33, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id129', 1, 0, '2019-07-03 18:03:15', '2019-07-03 19:37:56'),
(1347, 5316.00, 3400.00, 1741.65, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id1210', 1, 0, '2019-07-03 18:03:18', '2019-07-03 19:37:56'),
(1348, 1540.00, 1024.00, 3519.21, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id1211', 1, 0, '2019-07-03 18:21:45', '2019-07-03 19:37:56'),
(1349, 1540.00, 1240.00, 3484.92, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id1212', 1, 0, '2019-07-03 18:21:46', '2019-07-03 19:37:56'),
(1350, 860.00, 1016.00, 4190.24, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id1213', 1, 0, '2019-07-03 19:36:06', '2019-07-03 19:37:56'),
(1351, 4316.00, 1336.00, 765.70, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id1215', 1, 0, '2019-07-03 19:37:23', '2019-07-03 19:37:56'),
(1352, 2860.00, 1160.00, 2200.29, 'GznrvBSK5H1aP6V1uOXaZAIoXzWVu74ULxk6HBT7', 12, NULL, NULL, 6, 'id1215', 1, 0, '2019-07-03 19:37:23', '2019-07-03 19:37:56'),
(1353, 2212.00, 3552.00, 3350.40, 'hWnD0X5n9rcbLUN8PKadBBPP3Iy38o2mxVv6a9LD', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-04 01:09:52', '2019-07-04 01:10:29'),
(1354, 5164.00, 3520.00, 1839.69, 'hWnD0X5n9rcbLUN8PKadBBPP3Iy38o2mxVv6a9LD', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-04 01:09:56', '2019-07-04 01:10:29'),
(1355, 1980.00, 3520.00, 3528.81, 'hWnD0X5n9rcbLUN8PKadBBPP3Iy38o2mxVv6a9LD', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-04 01:09:59', '2019-07-04 01:10:29'),
(1356, 1452.00, 2896.00, 3744.22, 'hWnD0X5n9rcbLUN8PKadBBPP3Iy38o2mxVv6a9LD', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-04 01:10:00', '2019-07-04 01:10:29'),
(1357, 3036.00, 2432.00, 2096.46, 'hWnD0X5n9rcbLUN8PKadBBPP3Iy38o2mxVv6a9LD', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-04 01:10:02', '2019-07-04 01:10:29'),
(1358, 4236.00, 1912.00, 792.32, 'hWnD0X5n9rcbLUN8PKadBBPP3Iy38o2mxVv6a9LD', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-07-04 01:10:05', '2019-07-04 01:10:29'),
(1360, 6188.00, 1752.00, 1193.72, 'hWnD0X5n9rcbLUN8PKadBBPP3Iy38o2mxVv6a9LD', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-07-04 01:10:07', '2019-07-04 01:10:29'),
(1361, 2748.00, 776.00, 2425.95, 'hWnD0X5n9rcbLUN8PKadBBPP3Iy38o2mxVv6a9LD', 12, NULL, NULL, 6, 'id128', 1, 0, '2019-07-04 01:10:08', '2019-07-04 01:10:29'),
(1362, 132.00, 3792.00, 5299.56, 'hWnD0X5n9rcbLUN8PKadBBPP3Iy38o2mxVv6a9LD', 12, NULL, NULL, 6, 'id129', 1, 0, '2019-07-04 01:10:10', '2019-07-04 01:10:29'),
(1364, 1524.00, 1128.00, 3516.87, 'il8zY5pJJKzX2hCmgiHpgT22HeTW0P2zkZk6kAdz', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-04 05:13:55', '2019-07-04 05:13:55'),
(1365, 1052.00, 3040.00, 4169.30, 'il8zY5pJJKzX2hCmgiHpgT22HeTW0P2zkZk6kAdz', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-04 05:22:23', '2019-07-04 05:22:23'),
(1366, 1940.00, 3144.00, 3385.13, 'il8zY5pJJKzX2hCmgiHpgT22HeTW0P2zkZk6kAdz', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-04 05:23:00', '2019-07-04 05:23:00'),
(1367, 2660.00, 1160.00, 2394.93, 'il8zY5pJJKzX2hCmgiHpgT22HeTW0P2zkZk6kAdz', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-04 05:43:46', '2019-07-04 05:43:46'),
(1368, 2916.00, 1152.00, 2147.95, 'il8zY5pJJKzX2hCmgiHpgT22HeTW0P2zkZk6kAdz', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-04 05:43:47', '2019-07-04 05:43:47'),
(1369, 2420.00, 368.00, 2894.51, 'Lg48LcPtW7MwiBHYCSkZTGgV7bS1ooqPKuXoqbQh', 12, NULL, NULL, 6, 'id121', 1, 0, '2019-07-04 12:30:41', '2019-07-04 12:30:41'),
(1370, 4924.00, 536.00, 1154.25, 'Lg48LcPtW7MwiBHYCSkZTGgV7bS1ooqPKuXoqbQh', 12, NULL, NULL, 6, 'id122', 1, 0, '2019-07-04 12:32:12', '2019-07-04 12:32:12'),
(1371, 1732.00, 1104.00, 3315.83, 'Lg48LcPtW7MwiBHYCSkZTGgV7bS1ooqPKuXoqbQh', 12, NULL, NULL, 6, 'id123', 1, 0, '2019-07-04 12:32:13', '2019-07-04 12:32:13'),
(1372, 2580.00, 968.00, 2521.00, 'Lg48LcPtW7MwiBHYCSkZTGgV7bS1ooqPKuXoqbQh', 12, NULL, NULL, 6, 'id124', 1, 0, '2019-07-04 12:33:04', '2019-07-04 12:33:04'),
(1373, 4604.00, 880.00, 898.07, 'Lg48LcPtW7MwiBHYCSkZTGgV7bS1ooqPKuXoqbQh', 12, NULL, NULL, 6, 'id125', 1, 0, '2019-07-04 12:33:38', '2019-07-04 12:33:38'),
(1374, 4884.00, 384.00, 1308.80, 'Lg48LcPtW7MwiBHYCSkZTGgV7bS1ooqPKuXoqbQh', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-07-04 12:35:08', '2019-07-04 12:35:08'),
(1375, 4324.00, 288.00, 1552.93, 'Lg48LcPtW7MwiBHYCSkZTGgV7bS1ooqPKuXoqbQh', 12, NULL, NULL, 6, 'id126', 1, 0, '2019-07-04 12:35:09', '2019-07-04 12:35:09'),
(1376, 3108.00, 1592.00, 1890.44, 'Lg48LcPtW7MwiBHYCSkZTGgV7bS1ooqPKuXoqbQh', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-07-04 12:35:11', '2019-07-04 12:35:11'),
(1377, 3620.00, 1568.00, 1381.22, 'Lg48LcPtW7MwiBHYCSkZTGgV7bS1ooqPKuXoqbQh', 12, NULL, NULL, 6, 'id127', 1, 0, '2019-07-04 12:35:12', '2019-07-04 12:35:12');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_slab1` double(8,2) NOT NULL,
  `discount_slab1` double(8,2) NOT NULL,
  `discount_slab2` double(8,2) NOT NULL,
  `product_image1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_image4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_image5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `description`, `price_slab1`, `discount_slab1`, `discount_slab2`, `product_image1`, `product_image2`, `product_image3`, `product_image4`, `product_image5`, `created_at`, `updated_at`) VALUES
(12, 'iphone XS', 'iphone-xs', 'Super Retina. In big and bigger. \r\nThe custom OLED displays on iPhone XS deliver the most accurate colour in the industry, HDR and true blacks. \r\nAnd iPhone XS Max has our largest display ever on an iPhone.', 1.00, 5.00, 10.00, 'products/April2019/nyIlxM71OPmzsZwH5Uhw.jpeg', 'products/April2019/RbBiQNFTIS2aDuf1QRd2.jpeg', 'products/April2019/jcPFjXvb64EyAUBXW30w.jpeg', 'products/April2019/jDOQf2QZQq0W57uIRkxl.jpeg', 'products/April2019/mJfpprNml6VLmHpdG8Ed.jpeg', '2019-04-13 14:34:29', '2019-04-25 16:28:41'),
(13, 'Beat Headphone', 'Beat-headphone', 'Beats EP on-ear headphone delivers masterfully tuned sound. Its battery-free design offers unlimited playback and its sleek, durable frame is reinforced with lightweight stainless steel. Beats EP is an ideal introduction to Beats for any music lover seeking a dynamic listening experience.', 1.00, 5.00, 10.00, 'products/April2019/t192DkAeWEsJa18DT9fP.jpeg', 'products/April2019/AKmM0O0F8bcFhOirVkfv.jpeg', 'products/April2019/ZlwFYpFMiHvE1ZYnquaF.jpeg', 'products/April2019/gI2c43pI7draQi4b9tg9.jpeg', 'products/April2019/604OCYMS80rRvFAmqnjV.jpeg', '2019-04-13 14:34:29', '2019-04-19 16:43:33'),
(14, 'MacBook Pro', 'Macbook-pro', 'With great power comes great capability.\r\nMacBook Pro elevates the notebook to a whole new level of performance and portability. Wherever your ideas take you, you’ll get there faster than ever with high-performance processors and memory, advanced graphics, blazing-fast storage and more.', 1.00, 5.00, 10.00, 'products/April2019/z9aWQb1h8bMcgT1dfp6g.jpg', 'products/April2019/UTjTJ1UTYsLvy7B50Nv3.jpeg', 'products/April2019/FgbVl9B7HwS6KoYCKCjD.jpeg', 'products/April2019/BwyZ3kKbVKQq9BG2JoKt.jpeg', 'products/April2019/DYETwrKc7Kk0xZ08qzGi.jpeg', '2019-04-13 14:34:29', '2019-04-25 16:29:16'),
(15, 'iPad Air', 'iPad-Air', 'iPad Air brings more of our most powerful technologies to more people than ever. A12 Bionic chip with Neural Engine. A 10.5-inch Retina display with True Tone. Support for Apple Pencil and the Smart Keyboard. And at just 456 grams and 6.1 millimetres thin, carrying all that power is effortless.', 1.00, 5.00, 10.00, 'products/April2019/59Pg2iTICY6ZQYhYiChv.jpeg', 'products/April2019/Svd7EYWH4TeJCEbbP1nC.jpeg', 'products/April2019/d7Ui7xVZxeRcJgz0pdyl.jpeg', 'products/April2019/s5XZSlU0mLGU7zCcLIRz.jpeg', 'products/April2019/BIbafwKWX2dSIHiR529Y.jpeg', '2019-04-13 14:34:29', '2019-04-25 16:29:36'),
(16, 'iWatch', 'iwatch-new', 'Apple Watch Series 4. Fundamentally redesigned and re‑engineered to help you be even more active, healthy and connected.', 1.00, 5.00, 10.00, 'products/April2019/OdscS5OkqbC5RzLeYIMP.jpeg', 'products/April2019/iU9UIyahX30hGhcLYV5e.jpeg', 'products/April2019/YccmdLWuVOKrEnzagA9t.jpeg', 'products/April2019/WprofnMmejZZKIrFY9zg.jpeg', 'products/April2019/qArpmU0X2iTx8As4vBGY.jpeg', '2019-04-13 14:34:29', '2019-04-25 16:29:48'),
(21, 'Canon DSLR Camera', 'Canon-EOS-77D-Camera', 'The Canon EOS 77D 18-55 DSLR Camera features a 24.2 MP image sensor that lets you capture high-quality photographs and videos. Its built-in Movie digital image stabiliser alleviates shakes and judders caused from handheld usage, thereby giving you sharp, blur-free videos. Furthermore, this camera comes with an EF-S 18-55 mm lens.\r\nEffective Megapixels: 24.2 MP\r\nSensor Type: CMOS\r\nScreen Size: 7.7 cm (3 Inch) TFT LCD Display\r\nConnectivity: USB,HDMI,Bluetooth,Wi-Fi,NFC', 2.00, 5.00, 10.00, 'products/April2019/BdtpJf0QtWAx61X00tbu.jpeg', 'products/April2019/7DO4sXkAxXHc5ESVezg6.jpeg', 'products/April2019/qwkWXckuloBDjmopQiJY.jpeg', 'products/April2019/PbiKNFTL16NaxFRgJivS.jpeg', 'products/April2019/wgeXT8lgxQFeBdM5tPLn.jpeg', '2019-04-19 16:11:25', '2019-06-21 11:24:41');

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `accepted_on` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `referrals`
--

INSERT INTO `referrals` (`id`, `name`, `email`, `message`, `status`, `user_id`, `accepted_on`, `created_at`, `updated_at`) VALUES
(1, 'Quinten Walsh', 'marvin.hayes@hotmail.com', 'Quasi aspernatur similique et. Omnis repellendus at aliquam temporibus voluptatem.', 1, 2, '2019-03-25 10:57:52', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(2, 'Norene Gerlach', 'evolkman@kuphal.com', 'Excepturi placeat quas excepturi eum nam minus libero. Minus accusantium reiciendis et sit itaque aut.', 1, 1, '2019-04-04 13:18:54', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(3, 'Keegan Jenkins', 'eerdman@schroeder.com', 'Ut quos et veritatis. Qui mollitia facilis id illo veritatis natus. Iusto veniam et accusantium inventore omnis fugit.', 1, 3, '2019-03-31 23:29:13', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(4, 'Isom Borer', 'marks.amber@yost.biz', 'Natus et quidem harum ad. Accusamus eos cumque et et aliquid. A ut debitis fugit. Sequi modi autem voluptatum vel cum ut ea. Dolores est voluptatem deleniti quia provident.', 0, 1, '2019-04-02 20:00:16', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(5, 'Maxime Kuphal', 'kilback.clarabelle@hotmail.com', 'Quidem reprehenderit consectetur ullam rerum recusandae. Culpa repellendus sit quia sed et qui est. Sapiente voluptas magni non ut voluptate velit.', 1, 2, '2019-04-01 09:17:29', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(6, 'Clair Brakus', 'jakubowski.nicklaus@casper.com', 'Voluptatem aut eum deleniti voluptatem asperiores dolorem sequi. Atque ipsam voluptas sapiente perspiciatis autem placeat qui. Delectus a enim vel et quae veniam quae.', 1, 2, '2019-03-22 16:02:03', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(7, 'Salvador Dibbert', 'pollich.florencio@gmail.com', 'Dolor vero repellendus sit sit minima. Deserunt aut nobis aliquid magnam. Deserunt et eveniet ut quis.', 0, 1, '2019-03-27 12:48:30', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(8, 'Dr. Estella Torp', 'christiansen.laney@eichmann.com', 'Natus eligendi nemo culpa voluptas fugiat id. Hic adipisci iste unde sit sequi. Quaerat recusandae voluptas commodi et qui asperiores.', 0, 3, '2019-04-08 02:05:50', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(9, 'Kari Cartwright PhD', 'charlotte33@gmail.com', 'Voluptas sunt quibusdam et qui itaque. Soluta vel animi laboriosam enim nobis amet. Sed ut dolor eum enim beatae vero est voluptates. Est accusantium eligendi voluptas quasi at optio.', 1, 1, '2019-04-01 20:04:46', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(10, 'Creola Frami', 'toy.viva@gmail.com', 'Nam in molestias dicta earum. Qui voluptatem repellat est veritatis omnis. Dolore quod eveniet dolorum inventore cupiditate optio sunt. Deserunt error magni nesciunt ducimus.', 0, 2, '2019-03-19 02:50:27', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(11, 'Mr. Rex Schmidt', 'pouros.elinore@yahoo.com', 'Pariatur quis corporis aut ullam voluptatem dolor pariatur. Placeat aut sit repudiandae iusto. In assumenda praesentium ducimus deserunt aperiam.', 0, 1, '2019-04-04 05:54:44', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(12, 'Dr. Tyrell Keebler III', 'schoen.frieda@von.net', 'Corrupti dignissimos dolorem quis sapiente. Nesciunt molestiae rerum qui reiciendis sed dolor quis. Qui eos tempore quibusdam.', 1, 1, '2019-04-10 20:18:06', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(13, 'Ms. Marcia Brakus', 'maggie28@boyle.com', 'Nisi maxime officia quae sit consequatur aspernatur at. Dolor at quo aut beatae quo explicabo voluptas. Perferendis doloribus ea commodi neque asperiores aut mollitia.', 0, 2, '2019-04-07 02:37:19', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(14, 'Aliyah Purdy', 'santiago.bernhard@gmail.com', 'Error sed ratione atque dolorem ut voluptatem eveniet. Ut in alias libero quaerat blanditiis perspiciatis. Consequatur eius harum et dolores. Repellat velit rerum velit voluptatem harum.', 0, 3, '2019-04-07 04:24:18', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(15, 'Hardy Ritchie', 'berta.hodkiewicz@zboncak.net', 'Iste ipsum perferendis non qui eaque. Quia et dolores nobis amet quidem fugit atque. Voluptas sapiente vel iste odio doloribus incidunt aut.', 0, 3, '2019-04-02 14:29:57', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(16, 'Estella Braun', 'lrutherford@yahoo.com', 'Eum qui reprehenderit ratione et dolores eius minus. Quae alias aliquid omnis eligendi quos saepe. Consectetur fuga deserunt ut animi asperiores corporis error.', 1, 3, '2019-04-12 14:52:29', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(17, 'Prof. Aurelio Lesch', 'maudie68@hotmail.com', 'Assumenda eaque ut est perspiciatis quis ut. Est aut doloremque quo. Excepturi veniam aperiam repellendus enim consequuntur quasi. Velit nobis et numquam illo eveniet molestiae.', 0, 3, '2019-03-19 00:55:45', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(18, 'Kyler Mills', 'awilliamson@yahoo.com', 'Non similique eius quidem velit. Vel amet vel nisi soluta et. Et et consequatur adipisci et. Veniam totam perferendis ut est veniam provident deserunt.', 0, 1, '2019-03-22 02:47:00', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(19, 'Mr. Isadore Keebler', 'joyce.howe@yahoo.com', 'Eligendi occaecati dolorem ducimus deleniti libero aut est. Qui et aliquid placeat enim recusandae facere amet. Impedit deserunt fugiat occaecati vel fugiat sed harum sunt.', 0, 2, '2019-04-05 23:51:26', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(20, 'Marcelo Carroll', 'mavis.hill@yahoo.com', 'Dignissimos explicabo placeat doloribus nemo et saepe exercitationem. Saepe est non autem veniam nesciunt consequatur. Dolorem nisi sit explicabo illo ab odio dolorem.', 1, 3, '2019-04-04 19:09:05', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(21, 'Mr. Mallory Gerlach I', 'hoyt.leuschke@hauck.com', 'Ducimus iste deserunt ipsam. Quia dolore quis culpa et quidem laboriosam. Et sit sit sunt illo praesentium ad corrupti.', 1, 2, '2019-04-02 14:32:55', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(22, 'Abe Reinger', 'emmy45@schaden.com', 'Minus facere minima magnam. Labore excepturi fugiat voluptas consequatur occaecati. Rerum eveniet aliquid aut autem voluptas.', 0, 2, '2019-03-23 15:13:20', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(23, 'Dr. Vaughn Boyer', 'cnienow@gmail.com', 'Doloribus culpa non et voluptate. Est voluptatem et eveniet consectetur.', 0, 1, '2019-03-14 06:10:25', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(24, 'Russel Schneider', 'gleichner.anya@hotmail.com', 'Aut delectus eum ex nobis earum ullam quod. Aliquam tenetur officia delectus dolore voluptatum. Cupiditate corrupti quam incidunt non minus est.', 0, 1, '2019-04-11 05:39:09', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(25, 'Abel Nikolaus', 'dpredovic@rippin.com', 'Dolor cumque doloribus blanditiis autem magnam. Eaque ipsam quo dolores odit sed. Est modi error ab dolor laudantium suscipit cumque. Et sint ut provident blanditiis.', 1, 3, '2019-03-18 06:35:05', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(26, 'Prof. Golden Stoltenberg', 'klindgren@yahoo.com', 'Aut explicabo eius neque. Libero nihil mollitia dolorem velit autem. Dolores qui dolore voluptas qui consequatur ut. Commodi nulla corporis omnis sed.', 1, 2, '2019-04-02 16:12:17', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(27, 'Kyla Koch', 'leopold01@schmeler.com', 'Necessitatibus blanditiis non eos quidem odit dolor. Cupiditate eius ratione architecto quo quia dicta. Omnis quia odio et laudantium omnis laborum. Accusamus saepe dolor sunt nemo non et modi.', 1, 1, '2019-03-31 06:43:52', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(28, 'Rod Doyle', 'mack23@metz.biz', 'Quae consequuntur esse dolores odit corporis voluptas magnam. Nemo voluptas omnis quod voluptas ducimus animi vel. Alias voluptatibus sed eos. Ipsum error repellat illum dicta deserunt hic.', 1, 1, '2019-03-23 18:49:06', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(29, 'Mrs. Graciela Bins', 'magdalena68@gmail.com', 'Quis odio culpa sed repudiandae voluptas laborum. Pariatur blanditiis vitae accusantium. Quasi quia dolorem molestias sint id quaerat. Voluptatem voluptas voluptatem vel ipsum.', 0, 3, '2019-03-21 10:54:59', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(30, 'Dr. Mikayla Kunde', 'katelin.powlowski@yahoo.com', 'Molestias voluptatem totam fugiat tenetur. Temporibus suscipit illum repudiandae necessitatibus. Adipisci eos odit et ut.', 0, 1, '2019-03-23 15:14:05', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(31, 'Mr. Rickie Turner DDS', 'qgoodwin@hotmail.com', 'Laboriosam explicabo dolor totam quis deleniti facilis quo. Veritatis est accusantium quis alias est. Eius qui quia et modi qui et dicta.', 1, 2, '2019-04-01 13:14:49', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(32, 'Misty Baumbach', 'hermann.jamil@runte.org', 'Ut laudantium quo sed velit unde debitis id. Non perferendis quod accusantium adipisci nobis. Ipsam blanditiis aut aut. Saepe aliquid in est aut. Quo iure at rerum nobis.', 0, 1, '2019-03-25 04:46:38', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(33, 'Gavin Kuhlman V', 'roberta00@block.com', 'Nihil praesentium nam sapiente fuga possimus at. Praesentium quidem consequuntur cupiditate dicta iusto in. Debitis earum aut occaecati est.', 1, 2, '2019-03-23 00:42:33', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(34, 'Enoch Goldner', 'pagac.abbey@kuphal.com', 'Maiores et tempora omnis fugit voluptatem. Qui a expedita nostrum tempora magnam aliquam. Corrupti mollitia ipsa quasi asperiores.', 0, 1, '2019-04-10 01:37:26', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(35, 'Mrs. Tiara Durgan', 'lakin.arianna@gmail.com', 'Iusto saepe nobis debitis enim. Mollitia quo ducimus porro. Quae quam vero mollitia totam. Neque et blanditiis libero minus.', 1, 3, '2019-03-24 21:55:20', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(36, 'Mrs. Orpha Altenwerth', 'oma75@yahoo.com', 'Perferendis dolor consectetur est inventore dignissimos. Accusamus fuga alias expedita maiores non expedita.', 0, 3, '2019-04-04 17:58:05', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(37, 'Ms. Kara Carter IV', 'johnathon55@hotmail.com', 'Neque harum odit beatae assumenda. Quas amet natus voluptatem libero sapiente aperiam. Ea magni voluptatum fugiat.', 1, 3, '2019-03-14 09:00:29', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(38, 'Emilie Kuvalis DDS', 'monica24@bauch.info', 'Reprehenderit veniam nihil ut est aut et. Qui earum rem minus placeat explicabo excepturi repudiandae non. Sequi error voluptate odio vero voluptatum.', 0, 2, '2019-03-28 13:57:44', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(39, 'Allene Schaefer', 'smith.kyla@hotmail.com', 'Porro et voluptatem et omnis. Consectetur possimus ut ea. Aliquid nihil ratione voluptatum vel sunt error. Doloremque ducimus fugit consectetur magnam voluptate quidem perspiciatis.', 1, 3, '2019-03-20 20:53:31', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(40, 'Destini Huel', 'cwiegand@rolfson.info', 'Velit ut minus cumque eos. Dolorum omnis nihil numquam optio dolorem. At tenetur dolorem ex occaecati dolor voluptate molestias. Nobis ut minus ut laborum expedita non.', 0, 2, '2019-04-11 02:24:59', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(41, 'Mr. Raymond Moen', 'rossie.wiegand@gmail.com', 'Rerum impedit et doloribus repellendus eligendi. Aut tempore deserunt delectus et repudiandae fugiat iste quisquam. Quis sequi dolor sit.', 0, 1, '2019-03-22 19:33:22', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(42, 'Garfield Roob', 'amari.murray@huel.com', 'Eaque dolorum autem veniam. Maxime aut omnis quasi cum modi voluptates est. Adipisci quia sit ab nihil eligendi pariatur. Non ea quisquam at at quisquam doloribus.', 0, 1, '2019-03-23 19:31:18', '2019-04-13 14:34:29', '2019-04-13 14:34:29'),
(43, 'Corene Satterfield', 'tressa.huel@gmail.com', 'Reiciendis quia voluptas nesciunt vero dicta voluptas. Explicabo aut nulla et velit aliquam excepturi assumenda atque. Ut voluptates eius ex consequatur ipsa iusto.', 0, 1, '2019-03-13 21:27:10', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(44, 'Tyrese Stiedemann', 'vblick@rohan.com', 'Voluptas facilis soluta vel nemo iste. Dignissimos magni incidunt magnam temporibus unde vero. Aut inventore rerum consequatur aut. A voluptates ut aliquam velit ducimus ut sapiente.', 0, 3, '2019-04-07 20:30:44', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(45, 'Prof. Marc Botsford II', 'abernathy.rory@prosacco.com', 'Nemo eum debitis qui totam. Unde enim incidunt fuga rerum. Illo nobis quia velit.', 1, 3, '2019-04-07 11:16:33', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(46, 'Jessica Skiles', 'kim34@yahoo.com', 'Asperiores esse enim incidunt incidunt dolorem distinctio. Saepe voluptatem enim non iusto ipsum exercitationem nobis cumque. Sit atque tempora reiciendis nostrum et error aut.', 0, 2, '2019-03-24 22:42:48', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(47, 'Victoria Herman', 'fritsch.clemmie@gmail.com', 'Tenetur iure magnam ullam qui. Quam quis ea dolorum maxime. Et consequatur dolor inventore ut libero sed.', 0, 1, '2019-03-20 18:47:54', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(48, 'Miss Janae Kihn', 'jamil.powlowski@pacocha.com', 'Placeat ipsa consequatur nulla dolorum et. Beatae doloribus quod culpa dignissimos quas animi sunt dolorem. Enim aliquid non quos et. Quibusdam doloribus id ut perferendis consectetur nobis.', 0, 2, '2019-03-13 23:16:46', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(49, 'Prof. Iva Prohaska I', 'dell69@yahoo.com', 'Sit unde doloremque et ut non. Non nihil explicabo quae in. Ipsum iste qui dolorem magni sint. Commodi sed quibusdam ducimus et. Sint dolorum quibusdam consequatur quia.', 0, 1, '2019-03-18 21:04:16', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(50, 'Lily Renner', 'tfeest@dare.com', 'Qui sit magnam sint error eos accusamus repudiandae. Vel velit id quos est temporibus. Minima quaerat occaecati illum optio velit quidem. Ut et aliquid omnis voluptatem est distinctio totam.', 1, 3, '2019-03-28 15:44:07', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(51, 'Dulce Rodriguez Jr.', 'gordon81@gmail.com', 'Dolor neque perspiciatis corrupti rerum eaque beatae natus. Corrupti non incidunt quaerat voluptates assumenda omnis praesentium et. Ipsum voluptas sint qui aut magnam aut cumque.', 0, 1, '2019-03-28 08:11:47', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(52, 'Dr. Jeromy Gutmann DVM', 'bins.ericka@carter.biz', 'Consequatur ipsam at est. Et et fugiat et nulla tempore. Beatae exercitationem sed occaecati nostrum amet porro enim molestiae.', 0, 1, '2019-03-30 23:13:49', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(53, 'Kathleen Shanahan', 'clementina87@boyle.com', 'Et neque maxime quibusdam consequuntur. Incidunt culpa itaque aut atque deleniti inventore. Repellendus magni quibusdam commodi ab fugiat fugit dignissimos unde. Numquam quam dolore sunt eos amet.', 0, 1, '2019-04-08 23:21:22', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(54, 'Maryse Hickle', 'marcia75@hotmail.com', 'Ratione ut rerum quo qui. Aliquid placeat reprehenderit ratione. Cumque enim aspernatur ut ipsum accusamus facere.', 1, 3, '2019-04-03 05:56:20', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(55, 'Orland Kunde Sr.', 'blick.alejandra@schultz.com', 'Beatae dolore libero fuga veritatis tenetur sint doloribus. Nobis repudiandae asperiores molestiae illum reprehenderit qui. Itaque tenetur et qui ipsam et. Provident nemo est eaque ipsa.', 0, 1, '2019-03-22 02:47:17', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(56, 'Jaquan Rau', 'blick.craig@block.com', 'Architecto culpa doloribus blanditiis vel velit et. Dolores voluptas perferendis sapiente facilis. Et ut quibusdam enim dolore.', 1, 2, '2019-04-02 00:07:44', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(57, 'Gilberto Ryan', 'myriam96@orn.com', 'Itaque dicta qui facilis nulla officia dignissimos et libero. Dolorem velit perferendis vel. Quaerat dolore nostrum dignissimos dolorem dolores deserunt.', 1, 3, '2019-03-29 12:28:57', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(58, 'Linnea Will DVM', 'helga24@gmail.com', 'Omnis unde deleniti non. Et corrupti reiciendis labore. Dolorum ea est est sed aliquid.', 1, 3, '2019-03-20 01:44:19', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(59, 'Prof. Ezekiel Will', 'fadel.kayli@yahoo.com', 'In ex quasi sit nisi ea totam minima. Voluptas recusandae cumque eum deleniti soluta similique. Quae magni vel sit odio aut commodi.', 0, 3, '2019-03-22 18:46:51', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(60, 'Dr. Lyric Robel', 'igoodwin@paucek.org', 'Ratione ut eum non. Ut quos eveniet reiciendis possimus minima quia saepe incidunt. Voluptatum et dolore hic quisquam explicabo. Illo sunt non sint aut sit.', 0, 3, '2019-04-10 22:35:27', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(61, 'Prof. Wilhelm Morar', 'savion.rice@batz.com', 'Officiis delectus natus quam quidem enim. Et qui odit dolor voluptates similique cumque. Voluptatem commodi nihil adipisci sit sunt et.', 0, 3, '2019-03-29 22:15:39', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(62, 'Mrs. Addison Corwin', 'dayton.hirthe@wyman.org', 'Aliquid ipsum aperiam vel qui non. Nisi totam ut ipsam eos adipisci voluptatum incidunt. Corrupti ea in libero. Esse ut quae ipsa cumque. Quo quia adipisci ut.', 1, 2, '2019-04-06 13:59:06', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(63, 'Herman O\'Conner', 'oswaldo47@yahoo.com', 'Quas non neque laboriosam aut possimus facilis. Dolorum quis ut ea consequuntur qui et.', 1, 1, '2019-03-26 05:00:24', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(64, 'Korbin Ratke', 'tillman.tremblay@hotmail.com', 'Odit minus odio quisquam. Sapiente qui dolorum voluptatem pariatur. Praesentium qui molestias blanditiis iste iste ut et.', 1, 3, '2019-04-11 02:47:51', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(65, 'Mr. Napoleon Reilly II', 'carole.waters@hotmail.com', 'Ratione aliquid voluptatem veritatis rerum possimus in. Ratione enim sint magni laboriosam ipsum et provident. Aliquam dolore occaecati earum aliquid et.', 1, 1, '2019-03-21 12:38:49', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(66, 'Marina Brown', 'virginia.trantow@witting.com', 'Eum dolorem eos voluptatem atque amet. Eos sunt delectus delectus. Consequuntur ducimus enim non facilis repudiandae.', 1, 1, '2019-03-21 00:16:41', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(67, 'Amparo Schultz', 'huel.aletha@kuhn.com', 'Unde corrupti sed deleniti hic aut incidunt. Inventore tempore distinctio tempora at qui aut. Cum ea illum ab cumque ipsa ex.', 1, 2, '2019-03-17 22:13:41', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(68, 'Bertha Jacobs', 'angela76@kihn.com', 'Aut cum eligendi labore ut non. Nostrum sunt voluptatum nesciunt quas. Et sint voluptas voluptas. Quos quasi rerum dignissimos consectetur.', 1, 3, '2019-03-23 13:07:41', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(69, 'Dr. Vida Collins', 'klocko.tessie@beer.com', 'Enim vero vel nemo odit dolorem repellat esse. Quia excepturi quaerat maiores nulla adipisci.', 1, 2, '2019-03-16 00:00:47', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(70, 'Darren Bauch', 'ankunding.hester@hotmail.com', 'Quaerat sint omnis quo. Dolorem quisquam ut molestias totam voluptatum quos. Nemo magni cumque numquam eos et nam accusantium consequatur.', 1, 1, '2019-03-27 05:57:46', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(71, 'Marianne O\'Kon', 'tremblay.jordane@yahoo.com', 'Omnis et quas et inventore dolores cumque. Optio cumque sunt repudiandae incidunt. Aut placeat sit ullam molestiae ea ea qui. Esse et sed nisi consequatur dolorem accusantium.', 0, 3, '2019-03-17 11:47:28', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(72, 'Margaretta Friesen', 'nat47@yahoo.com', 'Numquam libero minima voluptatibus nemo facilis et. Voluptatem molestiae maiores culpa dolores unde omnis distinctio non. Illo cupiditate aut non labore.', 1, 3, '2019-03-19 01:01:49', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(73, 'Johnpaul Predovic', 'ullrich.francesco@hotmail.com', 'Quo voluptatem officia commodi. Est sed ut praesentium sit blanditiis nesciunt.', 1, 3, '2019-04-02 06:37:35', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(74, 'Jedediah Gleichner', 'kaitlyn06@hotmail.com', 'Sed molestias et asperiores omnis architecto corporis aut. Maiores cumque omnis et earum perspiciatis. Consectetur minima culpa impedit et. Culpa repellendus sed quisquam eum neque.', 0, 1, '2019-04-02 06:44:22', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(75, 'Susanna Kuvalis', 'tod.reilly@kirlin.com', 'Velit ut nam quaerat reprehenderit sint molestiae odit. Saepe non minus magni neque non consequatur et. Qui nostrum quaerat nihil. Natus et recusandae totam doloribus cupiditate.', 1, 1, '2019-03-17 19:46:50', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(76, 'Baby Shanahan', 'francisco.prohaska@yahoo.com', 'Aliquam qui repudiandae consequatur. Asperiores ut et rerum aliquid ea voluptatem. Ut dolorem voluptates laborum qui ipsa officiis accusantium. Voluptas corrupti repellendus non et porro.', 0, 3, '2019-03-19 01:20:32', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(77, 'Mrs. Destany Fritsch', 'crist.yesenia@hotmail.com', 'Id dolorem numquam commodi aliquid aspernatur ipsa. Laborum debitis temporibus atque vel quo sed ipsa earum. Molestias quos et maiores magni quibusdam esse. Cum itaque quos aut ratione nostrum quod.', 1, 1, '2019-04-11 07:24:07', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(78, 'Ms. Sophie Collins Jr.', 'drake.lockman@gmail.com', 'Et non itaque itaque laudantium quo vel. Mollitia ut aliquam ullam. Quia sequi dolorem eos sunt id. Qui non ut et doloribus repudiandae dolores iste.', 1, 1, '2019-04-04 14:58:00', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(79, 'Abigale Gutkowski', 'else.boyle@hotmail.com', 'Ea assumenda neque quasi non quo. Molestiae quia inventore dolore. Pariatur a deleniti voluptatum et dignissimos vel.', 0, 1, '2019-04-06 05:21:02', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(80, 'Arnold Aufderhar', 'tillman.lockman@yahoo.com', 'Itaque mollitia tempora nihil doloribus. Repellendus sed consectetur voluptatibus illo recusandae. Mollitia doloremque quis vero numquam quam id nesciunt. Est voluptates atque necessitatibus illo.', 1, 3, '2019-03-26 22:30:21', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(81, 'Mr. Trace Leannon II', 'jeanette68@frami.com', 'Ut ut numquam autem dignissimos ratione. Quae et voluptatem et vel assumenda atque soluta. Voluptas voluptatibus optio maiores inventore.', 1, 3, '2019-04-09 17:13:53', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(82, 'Clara Bayer', 'cleo06@hotmail.com', 'Amet eos at voluptatem. Alias dolorem odit consequatur dolorem beatae. Voluptas soluta veniam itaque voluptatem.', 0, 3, '2019-04-12 00:27:23', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(83, 'Miss Lauretta Schowalter', 'jones.gerard@gmail.com', 'Facere quae alias et. Non et doloribus distinctio et dolorum officia quo. Omnis fugit deserunt quia.', 1, 3, '2019-03-22 02:04:52', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(84, 'Stanford Macejkovic V', 'cwhite@spencer.com', 'Impedit possimus nulla optio eius ipsum error. Omnis quia amet fuga similique sapiente atque id. Facere aut quo voluptatem consequatur.', 1, 3, '2019-03-25 20:06:07', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(85, 'Aletha Lindgren', 'hane.tressa@hotmail.com', 'Et et totam temporibus. Voluptate voluptatem sed et error repudiandae.', 0, 2, '2019-04-12 11:28:43', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(86, 'Rhett Stiedemann', 'marc63@rutherford.com', 'Nihil quidem voluptatem tempore labore officiis repellat iste. Veritatis exercitationem praesentium voluptas et et. Eum et quia natus sequi enim.', 0, 3, '2019-04-13 00:06:48', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(87, 'Dr. Mollie Schaefer', 'lakin.lindsey@weber.biz', 'Esse quod corporis magnam molestiae. Eaque vel rem qui. Unde itaque qui ut tempora. Corrupti quod totam omnis velit ut est adipisci. Sit iste qui repudiandae maxime sint repellendus.', 0, 1, '2019-03-26 22:15:33', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(88, 'Dr. Hanna Gerhold II', 'abernathy.brannon@yahoo.com', 'Qui esse quis aspernatur sint maxime nobis. Eligendi fugiat voluptas voluptatum dolores. Cumque sapiente perferendis quisquam ipsa voluptas blanditiis.', 1, 1, '2019-03-29 22:25:56', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(89, 'Otilia Zemlak', 'orn.frieda@hotmail.com', 'Eveniet porro nesciunt fugiat enim sequi. Delectus harum et saepe illum earum consequatur. Nostrum nihil voluptatibus perspiciatis. Dolores sed vel at et voluptatem sed dolor.', 1, 3, '2019-03-25 20:22:41', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(90, 'Melody Schowalter', 'nbashirian@turcotte.org', 'Suscipit quia similique sapiente dolores accusamus. Sapiente delectus necessitatibus ab quia. Pariatur dicta et nobis.', 1, 2, '2019-04-07 01:48:43', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(91, 'Fausto Gleichner', 'harmon44@gmail.com', 'Ducimus possimus amet illum numquam. Reprehenderit laudantium sed impedit consectetur qui. Quod aliquam aut ea architecto.', 0, 3, '2019-03-31 16:20:31', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(92, 'Gracie Sporer', 'mosciski.diego@gmail.com', 'Quae eius repellendus assumenda dolorum minima. Provident sapiente eligendi laborum ut. Minus doloribus tempore et ipsam. Et velit eligendi repudiandae aliquam nobis.', 1, 1, '2019-04-12 08:27:46', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(93, 'Frank Wolff', 'mcclure.myron@nienow.com', 'Voluptas magni doloribus dolores ratione aut beatae. Dolorem quo voluptatem dolor impedit dolorem id. Repellat et libero qui nihil et amet et voluptates. Velit odit sint ex molestiae.', 1, 3, '2019-04-13 12:51:56', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(94, 'Dr. Wallace Stokes', 'fatima23@yahoo.com', 'Sunt quaerat impedit dignissimos esse quasi ipsum voluptatem. Et sunt cupiditate tenetur ad. Aperiam repudiandae natus quis dolor repudiandae. Eveniet mollitia qui vel sit et dolore explicabo.', 0, 2, '2019-03-24 21:44:22', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(95, 'Eusebio Kozey', 'runolfsson.emelia@gmail.com', 'Temporibus voluptas temporibus eligendi architecto minus. Qui id commodi magnam ipsum quam amet. Hic ipsum tempora aut.', 1, 1, '2019-04-04 06:45:33', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(96, 'Quincy Jast', 'cbruen@osinski.info', 'Aut quidem expedita alias magnam ut provident et placeat. Odit consequatur id et harum fugit.', 0, 3, '2019-04-07 22:21:12', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(97, 'Ms. Anastasia Rice', 'donny58@hotmail.com', 'Repellat voluptas ut fugiat autem eum. Vitae aliquam consectetur sint velit consequatur perferendis impedit. Mollitia dolorem eaque ex et et. Non qui officia eligendi voluptas.', 1, 2, '2019-03-19 23:59:00', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(98, 'Miss Marge Gaylord', 'sschaden@west.org', 'Consectetur inventore laboriosam architecto cumque quos earum ea. Dicta aut iusto ut perspiciatis beatae sed error. Non voluptas aut numquam enim placeat molestiae eum eum.', 1, 1, '2019-03-14 07:44:48', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(99, 'Kayley Schultz', 'zrippin@gmail.com', 'Numquam omnis sequi laudantium facere voluptas libero. Aliquam perferendis vel veritatis vitae. Eos officia pariatur atque perspiciatis incidunt.', 1, 3, '2019-03-15 05:05:51', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(100, 'Adriel Dach', 'alana.johnson@auer.com', 'Et maiores iste tempora earum consequatur quia. Voluptatum quaerat laudantium id aut. Quia ullam dicta voluptate ut dicta libero. Ducimus aut et voluptatibus atque.', 1, 1, '2019-03-31 22:52:06', '2019-04-13 14:34:30', '2019-04-13 14:34:30'),
(101, 'Trilok3', 'triloksingh2392+ts3@gmail.com', NULL, 1, 28, '2019-05-31 06:02:04', '2019-05-31 05:07:17', '2019-05-31 06:02:04');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-04-13 14:34:28', '2019-04-13 14:34:28'),
(2, 'user', 'Normal User', '2019-04-13 14:34:28', '2019-04-13 14:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('G9mXmvM0k21yUhOHRBaFxBVOPqDSJTdkQ30H93t8', NULL, '91.210.107.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'YToxMjp7czo2OiJfdG9rZW4iO3M6NDA6IkxnNDhMY1B0VzdNd2lCSFlDU2taVEdnVjdiUzFvb3FQS3VYb3FiUWgiO3M6MTI6Im1pbmltdW1QcmljZSI7ZDoxO3M6MTI6Im1heGltdW1QcmljZSI7ZDoyO3M6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6ODoicHJvZHVjdHMiO086Mjk6IklsbHVtaW5hdGVcU3VwcG9ydFxDb2xsZWN0aW9uIjoxOntzOjg6IgAqAGl0ZW1zIjthOjIwOntpOjA7YTo2OntzOjU6InJvd0lkIjtzOjMyOiI3Yzg3ODgzZjJlNDRkMTcwYTc1MjkwM2IwOGMyZjI0ZCI7czo5OiJwcm9kdWN0SUQiO3M6MjoiMTIiO3M6NDoibmFtZSI7czo5OiJpcGhvbmUgWFMiO3M6NToic3RhbXAiO3M6NToiaWQxMjEiO3M6NToiaW1hZ2UiO3M6NDQ6InByb2R1Y3RzL0FwcmlsMjAxOS9ueUlseE03MU9QbXpzWndINVVody5qcGVnIjtzOjc6InN0YW1wZWQiO086MTE6IkFwcFxQb2ludGVyIjoyNjp7czoxMToiACoAZmlsbGFibGUiO2E6MTp7aTowO3M6ODoiZGlzdGFuY2UiO31zOjEzOiIAKgBjb25uZWN0aW9uIjtzOjU6Im15c3FsIjtzOjg6IgAqAHRhYmxlIjtzOjg6InBvaW50ZXJzIjtzOjEzOiIAKgBwcmltYXJ5S2V5IjtzOjI6ImlkIjtzOjEwOiIAKgBrZXlUeXBlIjtzOjM6ImludCI7czoxMjoiaW5jcmVtZW50aW5nIjtiOjE7czo3OiIAKgB3aXRoIjthOjA6e31zOjEyOiIAKgB3aXRoQ291bnQiO2E6MDp7fXM6MTA6IgAqAHBlclBhZ2UiO2k6MTU7czo2OiJleGlzdHMiO2I6MTtzOjE4OiJ3YXNSZWNlbnRseUNyZWF0ZWQiO2I6MDtzOjEzOiIAKgBhdHRyaWJ1dGVzIjthOjE0OntzOjI6ImlkIjtpOjEzNjk7czo2OiJ4Q29yZHMiO2Q6MjQyMDtzOjY6InlDb3JkcyI7ZDozNjg7czo4OiJkaXN0YW5jZSI7ZDoyODk0LjUxO3M6OToic2Vzc2lvbklEIjtzOjQwOiJMZzQ4TGNQdFc3TXdpQkhZQ1NrWlRHZ1Y3YlMxb29xUEt1WG9xYlFoIjtzOjk6InByb2R1Y3RJRCI7aToxMjtzOjc6InVzZXJfaWQiO047czoxMDoiaW52b2ljZV9pZCI7TjtzOjc6ImdhbWVfaWQiO2k6NjtzOjc6InN0YW1wSWQiO3M6NToiaWQxMjEiO3M6Nzoic3RhbXBlZCI7aToxO3M6NzoicmVtb3ZlZCI7aTowO3M6MTA6ImNyZWF0ZWRfYXQiO3M6MTk6IjIwMTktMDctMDQgMTI6MzA6NDEiO3M6MTA6InVwZGF0ZWRfYXQiO3M6MTk6IjIwMTktMDctMDQgMTI6MzA6NDEiO31zOjExOiIAKgBvcmlnaW5hbCI7YToxNDp7czoyOiJpZCI7aToxMzY5O3M6NjoieENvcmRzIjtkOjI0MjA7czo2OiJ5Q29yZHMiO2Q6MzY4O3M6ODoiZGlzdGFuY2UiO2Q6Mjg5NC41MTtzOjk6InNlc3Npb25JRCI7czo0MDoiTGc0OExjUHRXN013aUJIWUNTa1pUR2dWN2JTMW9vcVBLdVhvcWJRaCI7czo5OiJwcm9kdWN0SUQiO2k6MTI7czo3OiJ1c2VyX2lkIjtOO3M6MTA6Imludm9pY2VfaWQiO047czo3OiJnYW1lX2lkIjtpOjY7czo3OiJzdGFtcElkIjtzOjU6ImlkMTIxIjtzOjc6InN0YW1wZWQiO2k6MTtzOjc6InJlbW92ZWQiO2k6MDtzOjEwOiJjcmVhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjMwOjQxIjtzOjEwOiJ1cGRhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjMwOjQxIjt9czoxMDoiACoAY2hhbmdlcyI7YTowOnt9czo4OiIAKgBjYXN0cyI7YTowOnt9czo4OiIAKgBkYXRlcyI7YTowOnt9czoxMzoiACoAZGF0ZUZvcm1hdCI7TjtzOjEwOiIAKgBhcHBlbmRzIjthOjA6e31zOjE5OiIAKgBkaXNwYXRjaGVzRXZlbnRzIjthOjA6e31zOjE0OiIAKgBvYnNlcnZhYmxlcyI7YTowOnt9czoxMjoiACoAcmVsYXRpb25zIjthOjA6e31zOjEwOiIAKgB0b3VjaGVzIjthOjA6e31zOjEwOiJ0aW1lc3RhbXBzIjtiOjE7czo5OiIAKgBoaWRkZW4iO2E6MDp7fXM6MTA6IgAqAHZpc2libGUiO2E6MDp7fXM6MTA6IgAqAGd1YXJkZWQiO2E6MTp7aTowO3M6MToiKiI7fX19aToxO2E6Njp7czo1OiJyb3dJZCI7czozMjoiN2M4Nzg4M2YyZTQ0ZDE3MGE3NTI5MDNiMDhjMmYyNGQiO3M6OToicHJvZHVjdElEIjtzOjI6IjEyIjtzOjQ6Im5hbWUiO3M6OToiaXBob25lIFhTIjtzOjU6InN0YW1wIjtzOjU6ImlkMTIyIjtzOjU6ImltYWdlIjtzOjQ0OiJwcm9kdWN0cy9BcHJpbDIwMTkvbnlJbHhNNzFPUG16c1p3SDVVaHcuanBlZyI7czo3OiJzdGFtcGVkIjtPOjExOiJBcHBcUG9pbnRlciI6MjY6e3M6MTE6IgAqAGZpbGxhYmxlIjthOjE6e2k6MDtzOjg6ImRpc3RhbmNlIjt9czoxMzoiACoAY29ubmVjdGlvbiI7czo1OiJteXNxbCI7czo4OiIAKgB0YWJsZSI7czo4OiJwb2ludGVycyI7czoxMzoiACoAcHJpbWFyeUtleSI7czoyOiJpZCI7czoxMDoiACoAa2V5VHlwZSI7czozOiJpbnQiO3M6MTI6ImluY3JlbWVudGluZyI7YjoxO3M6NzoiACoAd2l0aCI7YTowOnt9czoxMjoiACoAd2l0aENvdW50IjthOjA6e31zOjEwOiIAKgBwZXJQYWdlIjtpOjE1O3M6NjoiZXhpc3RzIjtiOjE7czoxODoid2FzUmVjZW50bHlDcmVhdGVkIjtiOjA7czoxMzoiACoAYXR0cmlidXRlcyI7YToxNDp7czoyOiJpZCI7aToxMzcwO3M6NjoieENvcmRzIjtkOjQ5MjQ7czo2OiJ5Q29yZHMiO2Q6NTM2O3M6ODoiZGlzdGFuY2UiO2Q6MTE1NC4yNTtzOjk6InNlc3Npb25JRCI7czo0MDoiTGc0OExjUHRXN013aUJIWUNTa1pUR2dWN2JTMW9vcVBLdVhvcWJRaCI7czo5OiJwcm9kdWN0SUQiO2k6MTI7czo3OiJ1c2VyX2lkIjtOO3M6MTA6Imludm9pY2VfaWQiO047czo3OiJnYW1lX2lkIjtpOjY7czo3OiJzdGFtcElkIjtzOjU6ImlkMTIyIjtzOjc6InN0YW1wZWQiO2k6MTtzOjc6InJlbW92ZWQiO2k6MDtzOjEwOiJjcmVhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjMyOjEyIjtzOjEwOiJ1cGRhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjMyOjEyIjt9czoxMToiACoAb3JpZ2luYWwiO2E6MTQ6e3M6MjoiaWQiO2k6MTM3MDtzOjY6InhDb3JkcyI7ZDo0OTI0O3M6NjoieUNvcmRzIjtkOjUzNjtzOjg6ImRpc3RhbmNlIjtkOjExNTQuMjU7czo5OiJzZXNzaW9uSUQiO3M6NDA6IkxnNDhMY1B0VzdNd2lCSFlDU2taVEdnVjdiUzFvb3FQS3VYb3FiUWgiO3M6OToicHJvZHVjdElEIjtpOjEyO3M6NzoidXNlcl9pZCI7TjtzOjEwOiJpbnZvaWNlX2lkIjtOO3M6NzoiZ2FtZV9pZCI7aTo2O3M6Nzoic3RhbXBJZCI7czo1OiJpZDEyMiI7czo3OiJzdGFtcGVkIjtpOjE7czo3OiJyZW1vdmVkIjtpOjA7czoxMDoiY3JlYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMjoxMiI7czoxMDoidXBkYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMjoxMiI7fXM6MTA6IgAqAGNoYW5nZXMiO2E6MDp7fXM6ODoiACoAY2FzdHMiO2E6MDp7fXM6ODoiACoAZGF0ZXMiO2E6MDp7fXM6MTM6IgAqAGRhdGVGb3JtYXQiO047czoxMDoiACoAYXBwZW5kcyI7YTowOnt9czoxOToiACoAZGlzcGF0Y2hlc0V2ZW50cyI7YTowOnt9czoxNDoiACoAb2JzZXJ2YWJsZXMiO2E6MDp7fXM6MTI6IgAqAHJlbGF0aW9ucyI7YTowOnt9czoxMDoiACoAdG91Y2hlcyI7YTowOnt9czoxMDoidGltZXN0YW1wcyI7YjoxO3M6OToiACoAaGlkZGVuIjthOjA6e31zOjEwOiIAKgB2aXNpYmxlIjthOjA6e31zOjEwOiIAKgBndWFyZGVkIjthOjE6e2k6MDtzOjE6IioiO319fWk6MjthOjY6e3M6NToicm93SWQiO3M6MzI6IjdjODc4ODNmMmU0NGQxNzBhNzUyOTAzYjA4YzJmMjRkIjtzOjk6InByb2R1Y3RJRCI7czoyOiIxMiI7czo0OiJuYW1lIjtzOjk6ImlwaG9uZSBYUyI7czo1OiJzdGFtcCI7czo1OiJpZDEyMyI7czo1OiJpbWFnZSI7czo0NDoicHJvZHVjdHMvQXByaWwyMDE5L255SWx4TTcxT1BtenNad0g1VWh3LmpwZWciO3M6Nzoic3RhbXBlZCI7TzoxMToiQXBwXFBvaW50ZXIiOjI2OntzOjExOiIAKgBmaWxsYWJsZSI7YToxOntpOjA7czo4OiJkaXN0YW5jZSI7fXM6MTM6IgAqAGNvbm5lY3Rpb24iO3M6NToibXlzcWwiO3M6ODoiACoAdGFibGUiO3M6ODoicG9pbnRlcnMiO3M6MTM6IgAqAHByaW1hcnlLZXkiO3M6MjoiaWQiO3M6MTA6IgAqAGtleVR5cGUiO3M6MzoiaW50IjtzOjEyOiJpbmNyZW1lbnRpbmciO2I6MTtzOjc6IgAqAHdpdGgiO2E6MDp7fXM6MTI6IgAqAHdpdGhDb3VudCI7YTowOnt9czoxMDoiACoAcGVyUGFnZSI7aToxNTtzOjY6ImV4aXN0cyI7YjoxO3M6MTg6Indhc1JlY2VudGx5Q3JlYXRlZCI7YjowO3M6MTM6IgAqAGF0dHJpYnV0ZXMiO2E6MTQ6e3M6MjoiaWQiO2k6MTM3MTtzOjY6InhDb3JkcyI7ZDoxNzMyO3M6NjoieUNvcmRzIjtkOjExMDQ7czo4OiJkaXN0YW5jZSI7ZDozMzE1LjgzO3M6OToic2Vzc2lvbklEIjtzOjQwOiJMZzQ4TGNQdFc3TXdpQkhZQ1NrWlRHZ1Y3YlMxb29xUEt1WG9xYlFoIjtzOjk6InByb2R1Y3RJRCI7aToxMjtzOjc6InVzZXJfaWQiO047czoxMDoiaW52b2ljZV9pZCI7TjtzOjc6ImdhbWVfaWQiO2k6NjtzOjc6InN0YW1wSWQiO3M6NToiaWQxMjMiO3M6Nzoic3RhbXBlZCI7aToxO3M6NzoicmVtb3ZlZCI7aTowO3M6MTA6ImNyZWF0ZWRfYXQiO3M6MTk6IjIwMTktMDctMDQgMTI6MzI6MTMiO3M6MTA6InVwZGF0ZWRfYXQiO3M6MTk6IjIwMTktMDctMDQgMTI6MzI6MTMiO31zOjExOiIAKgBvcmlnaW5hbCI7YToxNDp7czoyOiJpZCI7aToxMzcxO3M6NjoieENvcmRzIjtkOjE3MzI7czo2OiJ5Q29yZHMiO2Q6MTEwNDtzOjg6ImRpc3RhbmNlIjtkOjMzMTUuODM7czo5OiJzZXNzaW9uSUQiO3M6NDA6IkxnNDhMY1B0VzdNd2lCSFlDU2taVEdnVjdiUzFvb3FQS3VYb3FiUWgiO3M6OToicHJvZHVjdElEIjtpOjEyO3M6NzoidXNlcl9pZCI7TjtzOjEwOiJpbnZvaWNlX2lkIjtOO3M6NzoiZ2FtZV9pZCI7aTo2O3M6Nzoic3RhbXBJZCI7czo1OiJpZDEyMyI7czo3OiJzdGFtcGVkIjtpOjE7czo3OiJyZW1vdmVkIjtpOjA7czoxMDoiY3JlYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMjoxMyI7czoxMDoidXBkYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMjoxMyI7fXM6MTA6IgAqAGNoYW5nZXMiO2E6MDp7fXM6ODoiACoAY2FzdHMiO2E6MDp7fXM6ODoiACoAZGF0ZXMiO2E6MDp7fXM6MTM6IgAqAGRhdGVGb3JtYXQiO047czoxMDoiACoAYXBwZW5kcyI7YTowOnt9czoxOToiACoAZGlzcGF0Y2hlc0V2ZW50cyI7YTowOnt9czoxNDoiACoAb2JzZXJ2YWJsZXMiO2E6MDp7fXM6MTI6IgAqAHJlbGF0aW9ucyI7YTowOnt9czoxMDoiACoAdG91Y2hlcyI7YTowOnt9czoxMDoidGltZXN0YW1wcyI7YjoxO3M6OToiACoAaGlkZGVuIjthOjA6e31zOjEwOiIAKgB2aXNpYmxlIjthOjA6e31zOjEwOiIAKgBndWFyZGVkIjthOjE6e2k6MDtzOjE6IioiO319fWk6MzthOjY6e3M6NToicm93SWQiO3M6MzI6IjdjODc4ODNmMmU0NGQxNzBhNzUyOTAzYjA4YzJmMjRkIjtzOjk6InByb2R1Y3RJRCI7czoyOiIxMiI7czo0OiJuYW1lIjtzOjk6ImlwaG9uZSBYUyI7czo1OiJzdGFtcCI7czo1OiJpZDEyNCI7czo1OiJpbWFnZSI7czo0NDoicHJvZHVjdHMvQXByaWwyMDE5L255SWx4TTcxT1BtenNad0g1VWh3LmpwZWciO3M6Nzoic3RhbXBlZCI7TzoxMToiQXBwXFBvaW50ZXIiOjI2OntzOjExOiIAKgBmaWxsYWJsZSI7YToxOntpOjA7czo4OiJkaXN0YW5jZSI7fXM6MTM6IgAqAGNvbm5lY3Rpb24iO3M6NToibXlzcWwiO3M6ODoiACoAdGFibGUiO3M6ODoicG9pbnRlcnMiO3M6MTM6IgAqAHByaW1hcnlLZXkiO3M6MjoiaWQiO3M6MTA6IgAqAGtleVR5cGUiO3M6MzoiaW50IjtzOjEyOiJpbmNyZW1lbnRpbmciO2I6MTtzOjc6IgAqAHdpdGgiO2E6MDp7fXM6MTI6IgAqAHdpdGhDb3VudCI7YTowOnt9czoxMDoiACoAcGVyUGFnZSI7aToxNTtzOjY6ImV4aXN0cyI7YjoxO3M6MTg6Indhc1JlY2VudGx5Q3JlYXRlZCI7YjowO3M6MTM6IgAqAGF0dHJpYnV0ZXMiO2E6MTQ6e3M6MjoiaWQiO2k6MTM3MjtzOjY6InhDb3JkcyI7ZDoyNTgwO3M6NjoieUNvcmRzIjtkOjk2ODtzOjg6ImRpc3RhbmNlIjtkOjI1MjE7czo5OiJzZXNzaW9uSUQiO3M6NDA6IkxnNDhMY1B0VzdNd2lCSFlDU2taVEdnVjdiUzFvb3FQS3VYb3FiUWgiO3M6OToicHJvZHVjdElEIjtpOjEyO3M6NzoidXNlcl9pZCI7TjtzOjEwOiJpbnZvaWNlX2lkIjtOO3M6NzoiZ2FtZV9pZCI7aTo2O3M6Nzoic3RhbXBJZCI7czo1OiJpZDEyNCI7czo3OiJzdGFtcGVkIjtpOjE7czo3OiJyZW1vdmVkIjtpOjA7czoxMDoiY3JlYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMzowNCI7czoxMDoidXBkYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMzowNCI7fXM6MTE6IgAqAG9yaWdpbmFsIjthOjE0OntzOjI6ImlkIjtpOjEzNzI7czo2OiJ4Q29yZHMiO2Q6MjU4MDtzOjY6InlDb3JkcyI7ZDo5Njg7czo4OiJkaXN0YW5jZSI7ZDoyNTIxO3M6OToic2Vzc2lvbklEIjtzOjQwOiJMZzQ4TGNQdFc3TXdpQkhZQ1NrWlRHZ1Y3YlMxb29xUEt1WG9xYlFoIjtzOjk6InByb2R1Y3RJRCI7aToxMjtzOjc6InVzZXJfaWQiO047czoxMDoiaW52b2ljZV9pZCI7TjtzOjc6ImdhbWVfaWQiO2k6NjtzOjc6InN0YW1wSWQiO3M6NToiaWQxMjQiO3M6Nzoic3RhbXBlZCI7aToxO3M6NzoicmVtb3ZlZCI7aTowO3M6MTA6ImNyZWF0ZWRfYXQiO3M6MTk6IjIwMTktMDctMDQgMTI6MzM6MDQiO3M6MTA6InVwZGF0ZWRfYXQiO3M6MTk6IjIwMTktMDctMDQgMTI6MzM6MDQiO31zOjEwOiIAKgBjaGFuZ2VzIjthOjA6e31zOjg6IgAqAGNhc3RzIjthOjA6e31zOjg6IgAqAGRhdGVzIjthOjA6e31zOjEzOiIAKgBkYXRlRm9ybWF0IjtOO3M6MTA6IgAqAGFwcGVuZHMiO2E6MDp7fXM6MTk6IgAqAGRpc3BhdGNoZXNFdmVudHMiO2E6MDp7fXM6MTQ6IgAqAG9ic2VydmFibGVzIjthOjA6e31zOjEyOiIAKgByZWxhdGlvbnMiO2E6MDp7fXM6MTA6IgAqAHRvdWNoZXMiO2E6MDp7fXM6MTA6InRpbWVzdGFtcHMiO2I6MTtzOjk6IgAqAGhpZGRlbiI7YTowOnt9czoxMDoiACoAdmlzaWJsZSI7YTowOnt9czoxMDoiACoAZ3VhcmRlZCI7YToxOntpOjA7czoxOiIqIjt9fX1pOjQ7YTo2OntzOjU6InJvd0lkIjtzOjMyOiI3Yzg3ODgzZjJlNDRkMTcwYTc1MjkwM2IwOGMyZjI0ZCI7czo5OiJwcm9kdWN0SUQiO3M6MjoiMTIiO3M6NDoibmFtZSI7czo5OiJpcGhvbmUgWFMiO3M6NToic3RhbXAiO3M6NToiaWQxMjUiO3M6NToiaW1hZ2UiO3M6NDQ6InByb2R1Y3RzL0FwcmlsMjAxOS9ueUlseE03MU9QbXpzWndINVVody5qcGVnIjtzOjc6InN0YW1wZWQiO086MTE6IkFwcFxQb2ludGVyIjoyNjp7czoxMToiACoAZmlsbGFibGUiO2E6MTp7aTowO3M6ODoiZGlzdGFuY2UiO31zOjEzOiIAKgBjb25uZWN0aW9uIjtzOjU6Im15c3FsIjtzOjg6IgAqAHRhYmxlIjtzOjg6InBvaW50ZXJzIjtzOjEzOiIAKgBwcmltYXJ5S2V5IjtzOjI6ImlkIjtzOjEwOiIAKgBrZXlUeXBlIjtzOjM6ImludCI7czoxMjoiaW5jcmVtZW50aW5nIjtiOjE7czo3OiIAKgB3aXRoIjthOjA6e31zOjEyOiIAKgB3aXRoQ291bnQiO2E6MDp7fXM6MTA6IgAqAHBlclBhZ2UiO2k6MTU7czo2OiJleGlzdHMiO2I6MTtzOjE4OiJ3YXNSZWNlbnRseUNyZWF0ZWQiO2I6MDtzOjEzOiIAKgBhdHRyaWJ1dGVzIjthOjE0OntzOjI6ImlkIjtpOjEzNzM7czo2OiJ4Q29yZHMiO2Q6NDYwNDtzOjY6InlDb3JkcyI7ZDo4ODA7czo4OiJkaXN0YW5jZSI7ZDo4OTguMDc7czo5OiJzZXNzaW9uSUQiO3M6NDA6IkxnNDhMY1B0VzdNd2lCSFlDU2taVEdnVjdiUzFvb3FQS3VYb3FiUWgiO3M6OToicHJvZHVjdElEIjtpOjEyO3M6NzoidXNlcl9pZCI7TjtzOjEwOiJpbnZvaWNlX2lkIjtOO3M6NzoiZ2FtZV9pZCI7aTo2O3M6Nzoic3RhbXBJZCI7czo1OiJpZDEyNSI7czo3OiJzdGFtcGVkIjtpOjE7czo3OiJyZW1vdmVkIjtpOjA7czoxMDoiY3JlYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMzozOCI7czoxMDoidXBkYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMzozOCI7fXM6MTE6IgAqAG9yaWdpbmFsIjthOjE0OntzOjI6ImlkIjtpOjEzNzM7czo2OiJ4Q29yZHMiO2Q6NDYwNDtzOjY6InlDb3JkcyI7ZDo4ODA7czo4OiJkaXN0YW5jZSI7ZDo4OTguMDc7czo5OiJzZXNzaW9uSUQiO3M6NDA6IkxnNDhMY1B0VzdNd2lCSFlDU2taVEdnVjdiUzFvb3FQS3VYb3FiUWgiO3M6OToicHJvZHVjdElEIjtpOjEyO3M6NzoidXNlcl9pZCI7TjtzOjEwOiJpbnZvaWNlX2lkIjtOO3M6NzoiZ2FtZV9pZCI7aTo2O3M6Nzoic3RhbXBJZCI7czo1OiJpZDEyNSI7czo3OiJzdGFtcGVkIjtpOjE7czo3OiJyZW1vdmVkIjtpOjA7czoxMDoiY3JlYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMzozOCI7czoxMDoidXBkYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozMzozOCI7fXM6MTA6IgAqAGNoYW5nZXMiO2E6MDp7fXM6ODoiACoAY2FzdHMiO2E6MDp7fXM6ODoiACoAZGF0ZXMiO2E6MDp7fXM6MTM6IgAqAGRhdGVGb3JtYXQiO047czoxMDoiACoAYXBwZW5kcyI7YTowOnt9czoxOToiACoAZGlzcGF0Y2hlc0V2ZW50cyI7YTowOnt9czoxNDoiACoAb2JzZXJ2YWJsZXMiO2E6MDp7fXM6MTI6IgAqAHJlbGF0aW9ucyI7YTowOnt9czoxMDoiACoAdG91Y2hlcyI7YTowOnt9czoxMDoidGltZXN0YW1wcyI7YjoxO3M6OToiACoAaGlkZGVuIjthOjA6e31zOjEwOiIAKgB2aXNpYmxlIjthOjA6e31zOjEwOiIAKgBndWFyZGVkIjthOjE6e2k6MDtzOjE6IioiO319fWk6NTthOjY6e3M6NToicm93SWQiO3M6MzI6IjdjODc4ODNmMmU0NGQxNzBhNzUyOTAzYjA4YzJmMjRkIjtzOjk6InByb2R1Y3RJRCI7czoyOiIxMiI7czo0OiJuYW1lIjtzOjk6ImlwaG9uZSBYUyI7czo1OiJzdGFtcCI7czo1OiJpZDEyNiI7czo1OiJpbWFnZSI7czo0NDoicHJvZHVjdHMvQXByaWwyMDE5L255SWx4TTcxT1BtenNad0g1VWh3LmpwZWciO3M6Nzoic3RhbXBlZCI7TzoxMToiQXBwXFBvaW50ZXIiOjI2OntzOjExOiIAKgBmaWxsYWJsZSI7YToxOntpOjA7czo4OiJkaXN0YW5jZSI7fXM6MTM6IgAqAGNvbm5lY3Rpb24iO3M6NToibXlzcWwiO3M6ODoiACoAdGFibGUiO3M6ODoicG9pbnRlcnMiO3M6MTM6IgAqAHByaW1hcnlLZXkiO3M6MjoiaWQiO3M6MTA6IgAqAGtleVR5cGUiO3M6MzoiaW50IjtzOjEyOiJpbmNyZW1lbnRpbmciO2I6MTtzOjc6IgAqAHdpdGgiO2E6MDp7fXM6MTI6IgAqAHdpdGhDb3VudCI7YTowOnt9czoxMDoiACoAcGVyUGFnZSI7aToxNTtzOjY6ImV4aXN0cyI7YjoxO3M6MTg6Indhc1JlY2VudGx5Q3JlYXRlZCI7YjowO3M6MTM6IgAqAGF0dHJpYnV0ZXMiO2E6MTQ6e3M6MjoiaWQiO2k6MTM3NDtzOjY6InhDb3JkcyI7ZDo0ODg0O3M6NjoieUNvcmRzIjtkOjM4NDtzOjg6ImRpc3RhbmNlIjtkOjEzMDguODtzOjk6InNlc3Npb25JRCI7czo0MDoiTGc0OExjUHRXN013aUJIWUNTa1pUR2dWN2JTMW9vcVBLdVhvcWJRaCI7czo5OiJwcm9kdWN0SUQiO2k6MTI7czo3OiJ1c2VyX2lkIjtOO3M6MTA6Imludm9pY2VfaWQiO047czo3OiJnYW1lX2lkIjtpOjY7czo3OiJzdGFtcElkIjtzOjU6ImlkMTI2IjtzOjc6InN0YW1wZWQiO2k6MTtzOjc6InJlbW92ZWQiO2k6MDtzOjEwOiJjcmVhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjM1OjA4IjtzOjEwOiJ1cGRhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjM1OjA4Ijt9czoxMToiACoAb3JpZ2luYWwiO2E6MTQ6e3M6MjoiaWQiO2k6MTM3NDtzOjY6InhDb3JkcyI7ZDo0ODg0O3M6NjoieUNvcmRzIjtkOjM4NDtzOjg6ImRpc3RhbmNlIjtkOjEzMDguODtzOjk6InNlc3Npb25JRCI7czo0MDoiTGc0OExjUHRXN013aUJIWUNTa1pUR2dWN2JTMW9vcVBLdVhvcWJRaCI7czo5OiJwcm9kdWN0SUQiO2k6MTI7czo3OiJ1c2VyX2lkIjtOO3M6MTA6Imludm9pY2VfaWQiO047czo3OiJnYW1lX2lkIjtpOjY7czo3OiJzdGFtcElkIjtzOjU6ImlkMTI2IjtzOjc6InN0YW1wZWQiO2k6MTtzOjc6InJlbW92ZWQiO2k6MDtzOjEwOiJjcmVhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjM1OjA4IjtzOjEwOiJ1cGRhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjM1OjA4Ijt9czoxMDoiACoAY2hhbmdlcyI7YTowOnt9czo4OiIAKgBjYXN0cyI7YTowOnt9czo4OiIAKgBkYXRlcyI7YTowOnt9czoxMzoiACoAZGF0ZUZvcm1hdCI7TjtzOjEwOiIAKgBhcHBlbmRzIjthOjA6e31zOjE5OiIAKgBkaXNwYXRjaGVzRXZlbnRzIjthOjA6e31zOjE0OiIAKgBvYnNlcnZhYmxlcyI7YTowOnt9czoxMjoiACoAcmVsYXRpb25zIjthOjA6e31zOjEwOiIAKgB0b3VjaGVzIjthOjA6e31zOjEwOiJ0aW1lc3RhbXBzIjtiOjE7czo5OiIAKgBoaWRkZW4iO2E6MDp7fXM6MTA6IgAqAHZpc2libGUiO2E6MDp7fXM6MTA6IgAqAGd1YXJkZWQiO2E6MTp7aTowO3M6MToiKiI7fX19aTo2O2E6Njp7czo1OiJyb3dJZCI7czozMjoiN2M4Nzg4M2YyZTQ0ZDE3MGE3NTI5MDNiMDhjMmYyNGQiO3M6OToicHJvZHVjdElEIjtzOjI6IjEyIjtzOjQ6Im5hbWUiO3M6OToiaXBob25lIFhTIjtzOjU6InN0YW1wIjtzOjU6ImlkMTI3IjtzOjU6ImltYWdlIjtzOjQ0OiJwcm9kdWN0cy9BcHJpbDIwMTkvbnlJbHhNNzFPUG16c1p3SDVVaHcuanBlZyI7czo3OiJzdGFtcGVkIjtPOjExOiJBcHBcUG9pbnRlciI6MjY6e3M6MTE6IgAqAGZpbGxhYmxlIjthOjE6e2k6MDtzOjg6ImRpc3RhbmNlIjt9czoxMzoiACoAY29ubmVjdGlvbiI7czo1OiJteXNxbCI7czo4OiIAKgB0YWJsZSI7czo4OiJwb2ludGVycyI7czoxMzoiACoAcHJpbWFyeUtleSI7czoyOiJpZCI7czoxMDoiACoAa2V5VHlwZSI7czozOiJpbnQiO3M6MTI6ImluY3JlbWVudGluZyI7YjoxO3M6NzoiACoAd2l0aCI7YTowOnt9czoxMjoiACoAd2l0aENvdW50IjthOjA6e31zOjEwOiIAKgBwZXJQYWdlIjtpOjE1O3M6NjoiZXhpc3RzIjtiOjE7czoxODoid2FzUmVjZW50bHlDcmVhdGVkIjtiOjA7czoxMzoiACoAYXR0cmlidXRlcyI7YToxNDp7czoyOiJpZCI7aToxMzc2O3M6NjoieENvcmRzIjtkOjMxMDg7czo2OiJ5Q29yZHMiO2Q6MTU5MjtzOjg6ImRpc3RhbmNlIjtkOjE4OTAuNDQ7czo5OiJzZXNzaW9uSUQiO3M6NDA6IkxnNDhMY1B0VzdNd2lCSFlDU2taVEdnVjdiUzFvb3FQS3VYb3FiUWgiO3M6OToicHJvZHVjdElEIjtpOjEyO3M6NzoidXNlcl9pZCI7TjtzOjEwOiJpbnZvaWNlX2lkIjtOO3M6NzoiZ2FtZV9pZCI7aTo2O3M6Nzoic3RhbXBJZCI7czo1OiJpZDEyNyI7czo3OiJzdGFtcGVkIjtpOjE7czo3OiJyZW1vdmVkIjtpOjA7czoxMDoiY3JlYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozNToxMSI7czoxMDoidXBkYXRlZF9hdCI7czoxOToiMjAxOS0wNy0wNCAxMjozNToxMSI7fXM6MTE6IgAqAG9yaWdpbmFsIjthOjE0OntzOjI6ImlkIjtpOjEzNzY7czo2OiJ4Q29yZHMiO2Q6MzEwODtzOjY6InlDb3JkcyI7ZDoxNTkyO3M6ODoiZGlzdGFuY2UiO2Q6MTg5MC40NDtzOjk6InNlc3Npb25JRCI7czo0MDoiTGc0OExjUHRXN013aUJIWUNTa1pUR2dWN2JTMW9vcVBLdVhvcWJRaCI7czo5OiJwcm9kdWN0SUQiO2k6MTI7czo3OiJ1c2VyX2lkIjtOO3M6MTA6Imludm9pY2VfaWQiO047czo3OiJnYW1lX2lkIjtpOjY7czo3OiJzdGFtcElkIjtzOjU6ImlkMTI3IjtzOjc6InN0YW1wZWQiO2k6MTtzOjc6InJlbW92ZWQiO2k6MDtzOjEwOiJjcmVhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjM1OjExIjtzOjEwOiJ1cGRhdGVkX2F0IjtzOjE5OiIyMDE5LTA3LTA0IDEyOjM1OjExIjt9czoxMDoiACoAY2hhbmdlcyI7YTowOnt9czo4OiIAKgBjYXN0cyI7YTowOnt9czo4OiIAKgBkYXRlcyI7YTowOnt9czoxMzoiACoAZGF0ZUZvcm1hdCI7TjtzOjEwOiIAKgBhcHBlbmRzIjthOjA6e31zOjE5OiIAKgBkaXNwYXRjaGVzRXZlbnRzIjthOjA6e31zOjE0OiIAKgBvYnNlcnZhYmxlcyI7YTowOnt9czoxMjoiACoAcmVsYXRpb25zIjthOjA6e31zOjEwOiIAKgB0b3VjaGVzIjthOjA6e31zOjEwOiJ0aW1lc3RhbXBzIjtiOjE7czo5OiIAKgBoaWRkZW4iO2E6MDp7fXM6MTA6IgAqAHZpc2libGUiO2E6MDp7fXM6MTA6IgAqAGd1YXJkZWQiO2E6MTp7aTowO3M6MToiKiI7fX19aTo3O2E6Njp7czo1OiJyb3dJZCI7czozMjoiN2M4Nzg4M2YyZTQ0ZDE3MGE3NTI5MDNiMDhjMmYyNGQiO3M6OToicHJvZHVjdElEIjtzOjI6IjEyIjtzOjQ6Im5hbWUiO3M6OToiaXBob25lIFhTIjtzOjU6InN0YW1wIjtzOjU6ImlkMTI4IjtzOjU6ImltYWdlIjtzOjQ0OiJwcm9kdWN0cy9BcHJpbDIwMTkvbnlJbHhNNzFPUG16c1p3SDVVaHcuanBlZyI7czo3OiJzdGFtcGVkIjtOO31pOjg7YTo2OntzOjU6InJvd0lkIjtzOjMyOiI3Yzg3ODgzZjJlNDRkMTcwYTc1MjkwM2IwOGMyZjI0ZCI7czo5OiJwcm9kdWN0SUQiO3M6MjoiMTIiO3M6NDoibmFtZSI7czo5OiJpcGhvbmUgWFMiO3M6NToic3RhbXAiO3M6NToiaWQxMjkiO3M6NToiaW1hZ2UiO3M6NDQ6InByb2R1Y3RzL0FwcmlsMjAxOS9ueUlseE03MU9QbXpzWndINVVody5qcGVnIjtzOjc6InN0YW1wZWQiO047fWk6OTthOjY6e3M6NToicm93SWQiO3M6MzI6IjdjODc4ODNmMmU0NGQxNzBhNzUyOTAzYjA4YzJmMjRkIjtzOjk6InByb2R1Y3RJRCI7czoyOiIxMiI7czo0OiJuYW1lIjtzOjk6ImlwaG9uZSBYUyI7czo1OiJzdGFtcCI7czo2OiJpZDEyMTAiO3M6NToiaW1hZ2UiO3M6NDQ6InByb2R1Y3RzL0FwcmlsMjAxOS9ueUlseE03MU9QbXpzWndINVVody5qcGVnIjtzOjc6InN0YW1wZWQiO047fWk6MTA7YTo2OntzOjU6InJvd0lkIjtzOjMyOiI3Yzg3ODgzZjJlNDRkMTcwYTc1MjkwM2IwOGMyZjI0ZCI7czo5OiJwcm9kdWN0SUQiO3M6MjoiMTIiO3M6NDoibmFtZSI7czo5OiJpcGhvbmUgWFMiO3M6NToic3RhbXAiO3M6NjoiaWQxMjExIjtzOjU6ImltYWdlIjtzOjQ0OiJwcm9kdWN0cy9BcHJpbDIwMTkvbnlJbHhNNzFPUG16c1p3SDVVaHcuanBlZyI7czo3OiJzdGFtcGVkIjtOO31pOjExO2E6Njp7czo1OiJyb3dJZCI7czozMjoiN2M4Nzg4M2YyZTQ0ZDE3MGE3NTI5MDNiMDhjMmYyNGQiO3M6OToicHJvZHVjdElEIjtzOjI6IjEyIjtzOjQ6Im5hbWUiO3M6OToiaXBob25lIFhTIjtzOjU6InN0YW1wIjtzOjY6ImlkMTIxMiI7czo1OiJpbWFnZSI7czo0NDoicHJvZHVjdHMvQXByaWwyMDE5L255SWx4TTcxT1BtenNad0g1VWh3LmpwZWciO3M6Nzoic3RhbXBlZCI7Tjt9aToxMjthOjY6e3M6NToicm93SWQiO3M6MzI6IjdjODc4ODNmMmU0NGQxNzBhNzUyOTAzYjA4YzJmMjRkIjtzOjk6InByb2R1Y3RJRCI7czoyOiIxMiI7czo0OiJuYW1lIjtzOjk6ImlwaG9uZSBYUyI7czo1OiJzdGFtcCI7czo2OiJpZDEyMTMiO3M6NToiaW1hZ2UiO3M6NDQ6InByb2R1Y3RzL0FwcmlsMjAxOS9ueUlseE03MU9QbXpzWndINVVody5qcGVnIjtzOjc6InN0YW1wZWQiO047fWk6MTM7YTo2OntzOjU6InJvd0lkIjtzOjMyOiI3Yzg3ODgzZjJlNDRkMTcwYTc1MjkwM2IwOGMyZjI0ZCI7czo5OiJwcm9kdWN0SUQiO3M6MjoiMTIiO3M6NDoibmFtZSI7czo5OiJpcGhvbmUgWFMiO3M6NToic3RhbXAiO3M6NjoiaWQxMjE0IjtzOjU6ImltYWdlIjtzOjQ0OiJwcm9kdWN0cy9BcHJpbDIwMTkvbnlJbHhNNzFPUG16c1p3SDVVaHcuanBlZyI7czo3OiJzdGFtcGVkIjtOO31pOjE0O2E6Njp7czo1OiJyb3dJZCI7czozMjoiN2M4Nzg4M2YyZTQ0ZDE3MGE3NTI5MDNiMDhjMmYyNGQiO3M6OToicHJvZHVjdElEIjtzOjI6IjEyIjtzOjQ6Im5hbWUiO3M6OToiaXBob25lIFhTIjtzOjU6InN0YW1wIjtzOjY6ImlkMTIxNSI7czo1OiJpbWFnZSI7czo0NDoicHJvZHVjdHMvQXByaWwyMDE5L255SWx4TTcxT1BtenNad0g1VWh3LmpwZWciO3M6Nzoic3RhbXBlZCI7Tjt9aToxNTthOjY6e3M6NToicm93SWQiO3M6MzI6IjdjODc4ODNmMmU0NGQxNzBhNzUyOTAzYjA4YzJmMjRkIjtzOjk6InByb2R1Y3RJRCI7czoyOiIxMiI7czo0OiJuYW1lIjtzOjk6ImlwaG9uZSBYUyI7czo1OiJzdGFtcCI7czo2OiJpZDEyMTYiO3M6NToiaW1hZ2UiO3M6NDQ6InByb2R1Y3RzL0FwcmlsMjAxOS9ueUlseE03MU9QbXpzWndINVVody5qcGVnIjtzOjc6InN0YW1wZWQiO047fWk6MTY7YTo2OntzOjU6InJvd0lkIjtzOjMyOiI3Yzg3ODgzZjJlNDRkMTcwYTc1MjkwM2IwOGMyZjI0ZCI7czo5OiJwcm9kdWN0SUQiO3M6MjoiMTIiO3M6NDoibmFtZSI7czo5OiJpcGhvbmUgWFMiO3M6NToic3RhbXAiO3M6NjoiaWQxMjE3IjtzOjU6ImltYWdlIjtzOjQ0OiJwcm9kdWN0cy9BcHJpbDIwMTkvbnlJbHhNNzFPUG16c1p3SDVVaHcuanBlZyI7czo3OiJzdGFtcGVkIjtOO31pOjE3O2E6Njp7czo1OiJyb3dJZCI7czozMjoiN2M4Nzg4M2YyZTQ0ZDE3MGE3NTI5MDNiMDhjMmYyNGQiO3M6OToicHJvZHVjdElEIjtzOjI6IjEyIjtzOjQ6Im5hbWUiO3M6OToiaXBob25lIFhTIjtzOjU6InN0YW1wIjtzOjY6ImlkMTIxOCI7czo1OiJpbWFnZSI7czo0NDoicHJvZHVjdHMvQXByaWwyMDE5L255SWx4TTcxT1BtenNad0g1VWh3LmpwZWciO3M6Nzoic3RhbXBlZCI7Tjt9aToxODthOjY6e3M6NToicm93SWQiO3M6MzI6IjdjODc4ODNmMmU0NGQxNzBhNzUyOTAzYjA4YzJmMjRkIjtzOjk6InByb2R1Y3RJRCI7czoyOiIxMiI7czo0OiJuYW1lIjtzOjk6ImlwaG9uZSBYUyI7czo1OiJzdGFtcCI7czo2OiJpZDEyMTkiO3M6NToiaW1hZ2UiO3M6NDQ6InByb2R1Y3RzL0FwcmlsMjAxOS9ueUlseE03MU9QbXpzWndINVVody5qcGVnIjtzOjc6InN0YW1wZWQiO047fWk6MTk7YTo2OntzOjU6InJvd0lkIjtzOjMyOiI3Yzg3ODgzZjJlNDRkMTcwYTc1MjkwM2IwOGMyZjI0ZCI7czo5OiJwcm9kdWN0SUQiO3M6MjoiMTIiO3M6NDoibmFtZSI7czo5OiJpcGhvbmUgWFMiO3M6NToic3RhbXAiO3M6NjoiaWQxMjIwIjtzOjU6ImltYWdlIjtzOjQ0OiJwcm9kdWN0cy9BcHJpbDIwMTkvbnlJbHhNNzFPUG16c1p3SDVVaHcuanBlZyI7czo3OiJzdGFtcGVkIjtOO319fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjMyOiJodHRwczovL3dpbi5wc3dlYi5pbi9hZG1pbi9sb2dpbiI7fXM6OToiY2FydEFkZGVkIjtzOjU6InZhbHVlIjtzOjQ6ImNhcnQiO2E6MTp7czo3OiJkZWZhdWx0IjtPOjI5OiJJbGx1bWluYXRlXFN1cHBvcnRcQ29sbGVjdGlvbiI6MTp7czo4OiIAKgBpdGVtcyI7YToxOntzOjMyOiI3Yzg3ODgzZjJlNDRkMTcwYTc1MjkwM2IwOGMyZjI0ZCI7TzozMjoiR2xvdWRlbWFuc1xTaG9wcGluZ2NhcnRcQ2FydEl0ZW0iOjg6e3M6NToicm93SWQiO3M6MzI6IjdjODc4ODNmMmU0NGQxNzBhNzUyOTAzYjA4YzJmMjRkIjtzOjI6ImlkIjtzOjI6IjEyIjtzOjM6InF0eSI7aToyMDtzOjQ6Im5hbWUiO3M6OToiaXBob25lIFhTIjtzOjU6InByaWNlIjtkOjAuOTtzOjc6Im9wdGlvbnMiO086Mzk6Ikdsb3VkZW1hbnNcU2hvcHBpbmdjYXJ0XENhcnRJdGVtT3B0aW9ucyI6MTp7czo4OiIAKgBpdGVtcyI7YToxOntzOjEwOiJhdHRyaWJ1dGVzIjthOjI6e3M6MTI6InByb2R1Y3RfYXR0ciI7YTozOntzOjI6ImlkIjtpOjEyO3M6NDoic2x1ZyI7czo5OiJpcGhvbmUteHMiO3M6NToiaW1hZ2UiO3M6NDQ6InByb2R1Y3RzL0FwcmlsMjAxOS9ueUlseE03MU9QbXpzWndINVVody5qcGVnIjt9czo5OiJzaXplX2F0dHIiO2E6Mjp7czo1OiJsYWJlbCI7czozOiJ4eGwiO3M6NToicHJpY2UiO2Q6MTU7fX19fXM6NDk6IgBHbG91ZGVtYW5zXFNob3BwaW5nY2FydFxDYXJ0SXRlbQBhc3NvY2lhdGVkTW9kZWwiO047czo0MToiAEdsb3VkZW1hbnNcU2hvcHBpbmdjYXJ0XENhcnRJdGVtAHRheFJhdGUiO2k6MjE7fX19fXM6MTU6InN1Y2Nlc3NfbWVzc2FnZSI7czo1NzoiVGhlIHRpY2tldChzKSBmb3IgaXBob25lIFhTIGhhdmUgYmVlbiBhZGRlZCB0byB5b3VyIGNhcnQgIjtzOjE0OiJpbnRyb0Rpc3BsYXllZCI7YjoxO3M6MTI6Im5leHRQb3NpdGlvbiI7aTo3O3M6MzoidXJsIjthOjE6e3M6ODoiaW50ZW5kZWQiO3M6MjY6Imh0dHBzOi8vd2luLnBzd2ViLmluL2FkbWluIjt9fQ==', 1562244745);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shoppingcart`
--

INSERT INTO `shoppingcart` (`identifier`, `instance`, `content`, `created_at`, `updated_at`) VALUES
('4', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:0:{}}', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tie_breakers`
--

CREATE TABLE `tie_breakers` (
  `id` int(10) UNSIGNED NOT NULL,
  `pointCount` int(11) NOT NULL,
  `game_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `breakerPlayed` tinyint(1) NOT NULL DEFAULT '0',
  `breakerDistance` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `countryCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ageConfirmation` tinyint(1) DEFAULT NULL,
  `newsConfirmation` tinyint(1) DEFAULT NULL,
  `profileImage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/avatar.png',
  `initialDiscount` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `lastName`, `email`, `avatar`, `gender`, `email_verified_at`, `password`, `provider_id`, `provider`, `remember_token`, `settings`, `created_at`, `updated_at`, `mobile`, `countryCode`, `address1`, `address2`, `city`, `state`, `zip`, `country`, `ageConfirmation`, `newsConfirmation`, `profileImage`, `initialDiscount`) VALUES
(1, NULL, 'John', 'Doe', 'john@win.com', 'phpwLrEBK.png', 'male', NULL, '$2y$10$62sGrrTlqRJ70ZjLjVa3MezO/L93tx5RWH2keK2SUJias0.YoDTpq', NULL, NULL, 'ufaVauxXTnfEfKUXZ7OgihpxAcBXAkuX0p8C8P8o5QtU64vipwKtMR4zXvGz', NULL, NULL, '2019-04-13 16:04:43', '1234567890', '+1', 'Address Line One Added In Database', 'Address Line Two Added In Database', 'New York', 'New York', '12345', 'United States Of America', NULL, NULL, 'phpwLrEBK.png', 100),
(2, NULL, 'Mark', 'Doe', 'mark@win.com', 'phphTb7hh.jpg', 'male', NULL, '$2y$10$860nTJld/6SnVxAimREBGuik0katIhNOGX0S6Id9tVy/HrnhuNTgm', NULL, NULL, 'krC4Yh5jYWVVqi48ENFoJa1s0UZUFn1RVhZPcyJODfbPT5jm30hcqZeNBuOp', NULL, NULL, '2019-04-13 16:05:44', '1234567890', '+1', 'Address Line One Added In Database', 'Address Line Two Added In Database', 'New York', 'New York', '12345', 'United States Of America', NULL, NULL, 'phphTb7hh.jpg', 100),
(3, NULL, 'Bob', 'Doe', 'bob@win.com', 'users/default.png', 'male', NULL, '$2y$10$vRDGbkwY2G7A4KMeqIlVNuRyBUXtl3GhXIjXi51wjtAf.jtoA5Asa', NULL, NULL, 'C9e8WsCvRwoMRhq3nQS8kOUsPIBkbGCp8a8FL9yyi53T05EsYIIRTtG0q1x7', NULL, NULL, NULL, '1234567890', '+1', 'Address Line One Added In Database', 'Address Line Two Added In Database', 'New York', 'New York', '12345', 'United States Of America', NULL, NULL, 'images/avatar.png', 100),
(4, 1, 'Anuj', 'Singhal', 'admin@win.com', 'users/default.png', 'male', NULL, '$2y$10$JldE2zVAV1qPm/sekmULUuSri4VgKmxQv0BkbalNArGGsIttumXH6', NULL, NULL, '7o3D8qdIqG2qlkyC4pzSEDcqeB17Qm12c0TgcadFy8sKZOh47QisVfUkGXH2', '{\"locale\":\"en\"}', NULL, '2019-06-13 12:44:32', '1234567890', '+1', 'Address Line One Added In Database', 'Address Line Two Added In Database', 'New York', 'New York', '12345', 'United States Of America', NULL, NULL, 'images/avatar.png', 100),
(5, 2, 'Anuj', 'Singhal', 'anujsinghal100@hotmail.com', 'php9QzQLx.png', 'male', '2019-04-25 05:16:02', '$2y$10$mPkJOxDyG9r7Br8mpzUrLuIlshaaaxyRJU1pV3jdwlZeD1XlIRRlO', NULL, NULL, 'MJkBsiZz4Z1dK98vem5zAnEBNhDC4681hBA6H3gQbYcVwTMKQaYlN8VjuLVy', '{\"locale\":\"en\"}', '2019-04-14 08:36:30', '2019-06-28 16:49:45', '23435536', '+1', 'New', 'New', 'DElhi', 'DElhi', '123334', 'United States Of America', 1, 1, 'php9QzQLx.png', 10),
(6, 2, 'Ansh', 'Singhal', 'ansh.singhal2002@gmail.com', 'users/default.png', 'male', '2019-04-25 13:37:09', '$2y$10$5mKfArDYtTkKAgNc3O9g2.DAbYaBPkYgACC7RAPWf9NJ.1u7IMOiC', NULL, NULL, 'xfgcLDWD0pTVwh89fPl4C695meS5A92qgO0X4s7Cz6sywkEXqVSqsejkfDPG', '{\"locale\":\"en\"}', '2019-04-25 13:36:46', '2019-04-25 14:09:51', '9599508747', '+91', 'NEw', 'Delhi', 'Delhi', 'Haryana', '1245', 'United States Of America', 1, 1, 'images/avatar.png', 100),
(7, 2, 'Suruchi', 'Singh', 'suchisinghal100@yahoo.com', 'users/default.png', 'female', NULL, '$2y$10$UPMWFEyyWdzKwdc8JyjhYu6VGev7Ood.4l4grjbnPcQ459HjjaW3y', NULL, NULL, 'fF3rzUWhIJD6yjl7bAqkzcU7joQ5LomIwmQrEg58dnc2dgMTwgGWLU6qZqcc', '{\"locale\":\"en\"}', '2019-04-25 14:10:48', '2019-04-25 14:12:53', '23435536', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 50),
(8, 2, 'Anuj', 'JC', 'anuj.singhal@justiceandcare.org', 'users/default.png', 'male', '2019-04-25 16:25:16', '$2y$10$RP4eBTGWZdwPVvREUZ/pa.CDcQgC1G7lStWtdE.P8xyzNfQQnaH4C', NULL, NULL, 'meYMqEGuuMfbk9WQ8V1FRpCdtd1qvgzGOfuwhzLSnbvyIcnbIIfbJkEcWAvx', NULL, '2019-04-25 16:24:57', '2019-04-25 16:25:16', '23435536', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(9, 2, 'ayesha', 'singhal', 'ayeshasinghal100@gmail.com', 'users/default.png', 'female', '2019-04-26 14:53:16', '$2y$10$0CzrlitEErLSMWlT4pm8VujRWiu4Mn/Z3uni.MszjB.3OwOaDMCTu', NULL, NULL, 'bFmABJUi5RAFgWgSoif2A3zRhFXWv5eyFM502CdiNY5nXldQqdl84VBiuFOw', '{\"locale\":\"en\"}', '2019-04-26 14:52:53', '2019-04-26 15:00:30', '9599588747', '91', 'NEw', 'Delhi', 'Delhi', 'Haryana', '1179', 'United States Of America', 1, 1, 'images/avatar.png', 45),
(10, 2, 'EOTechpjj', 'swusalmeutqccaaGP', 'support@vdsina.ru', 'users/default.png', 'male', NULL, '$2y$10$2MyAJr52md4jXi1vfcro8efAeNr/xlA39B63kMS.9uBg/5SaA5pOW', NULL, NULL, NULL, NULL, '2019-05-03 10:49:26', '2019-05-03 10:49:26', '81788463394', 'Jordan', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(11, 2, 'Vitamixoqh', 'xwusafmesmxxx2hGP', 'torririli@gmail.com', 'users/default.png', 'male', NULL, '$2y$10$lGaNj5.LYBcElB19F9MJ7.cjfaEOreJiXim95vw6a1.X/Y1gMlAXq', NULL, NULL, NULL, NULL, '2019-05-04 14:34:07', '2019-05-04 14:34:07', '83424263819', 'Belize', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(12, 2, 'Premiumdbr', 'swusalmerttfdqxGP', 'bill@vdsina.ru', 'users/default.png', 'male', NULL, '$2y$10$5qcAUNeOayI1s9ZsLsQaseVjW5oO9IUHt6NXsDXRyHZQ19WItYx22', NULL, NULL, NULL, NULL, '2019-05-06 20:58:18', '2019-05-06 20:58:18', '82364672758', 'Lithuania', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(24, 2, 'Ravi', 'Kumawat', 'ravikumawat523@gmail.com', 'users/default.png', 'male', '2019-05-22 05:23:18', '$2y$10$YLv3zNfyr5C/YM7cQm2uzuOwI/fMsjlKM1.7zIbSSRaNi2oFJPhVS', NULL, NULL, 'rdaCORN48TetrqURKx20Kb41x6eB3JhYhfVE9sX6RWBm2Pf0L6czQZggqk42', NULL, '2019-05-22 04:09:09', '2019-05-22 05:42:54', '7877666111', '+91', 'Address', 'Address2', 'city', 'State', '112345', 'United States Of America', 1, 1, 'images/avatar.png', 0),
(25, 2, 'Pankti', 'Patel', 'pankti.patel283@gmail.com', 'users/default.png', 'female', '2019-05-22 06:48:50', '$2y$10$Be0MNoe60BxmCmGOUhBLy.UtBxNF24pZCoxbJUusLcd3TCX6MR0Fi', NULL, NULL, 'gO1tcn8HFIicRD6ddMFAMRuvk2OEv7Uui7iBeLdLDQSFcEJ3QdpXrAga7zml', NULL, '2019-05-22 06:48:10', '2019-05-22 06:48:50', '2345678564', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(26, 2, 'Kajal', 'Gupta', 'kajal@bel-technology.com', 'users/default.png', 'female', NULL, '$2y$10$zA7i15ZUNqn6UROdhMYuCOtIGziIn1QPxST9q1Fib/43h6EbyLr1i', NULL, NULL, 'kCEwMLFbP3vEPGt4rmRdNNB70iZKswkfFJQLizzknjzeB7fc5RItLRS2Xhtj', NULL, '2019-05-27 05:47:51', '2019-05-27 05:47:51', '9001748188', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(27, 2, 'Ravi', 'Kumawat', 'beltechnologyclient@gmail.com', 'users/default.png', 'male', '2019-05-27 08:14:26', '$2y$10$jlrxAYPhet6Cd6qC6JPnmOhLyFr2127PcQQ14f8FWSCI2wfNcEZVO', NULL, NULL, 'FMTLoW6vAkeVxB1loYob5T53Pg6ukn2lHlBiRl7FeQDY6aZr4cRsHxSci3Hf', NULL, '2019-05-27 08:04:33', '2019-05-27 08:20:12', '7792834063', '+91', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(28, 2, 'Trilok', 'Parmar', 'triloksingh2392@gmail.com', 'phptMwxiW.jpg', 'male', '2019-05-29 12:43:43', '$2y$10$xHrv6Hks4cxJPDEp6kJ8j.eScdUqszn5avWzyoCuijVXmtfFXwy9e', NULL, NULL, 'mOW8XwFyTQQLCdv41zTDpKWMgSqco9m7WDuwtrI1VG85aY25L3FHT7XJsysS', NULL, '2019-05-29 12:21:08', '2019-06-06 08:47:32', '1234567891', '+1', 'test address 11', 'test address 21', 'test city1', 'test state1', '12346', 'United States Of America', 1, 0, 'phptMwxiW.jpg', 0),
(29, 2, 'Trilok1', 'Parmar1', 'triloksingh2392+ts1@gmail.com', 'users/default.png', 'male', '2019-05-30 04:17:44', '$2y$10$3Ouy68vY.XDu.gweG/2ehOSTmTKdnVwfXiQw9U/h/kei0upLoBPDm', NULL, NULL, 'Pj9v9JLiY2SJYkT788WcSqCebDCkhaDGKp7cuTnam1ggM0Kc1QzAhqNpHu8D', NULL, '2019-05-30 04:12:48', '2019-05-30 04:17:44', '3456789123', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(30, 2, 'Trilok3', 'Parmar3', 'triloksingh2392+ts3@gmail.com', 'users/default.png', 'male', NULL, '$2y$10$6PnC2T0w0koro/7DrOen2ez/kCRYlMWNzi5NJtfSnkSZV.0HW4olW', NULL, NULL, 'bekluP5vVGyU4UfEwSComVvA640120QBxLWzH4S5G9I7B6i2Mmgn8BOiRgyD', NULL, '2019-05-31 06:02:03', '2019-05-31 06:02:03', '4567891234', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(31, 2, 'Trilok4', 'Parmar4', 'triloksingh2392+ts4@gmail.com', 'users/default.png', 'male', NULL, '$2y$10$FiKKGyfGehvr8qaHq7Yh.uR5AH.lmZCFQCyUhnTiJE.g.gKb6r0Pe', NULL, NULL, 'mMHptbQprzd04lKF5irs4AmcDJaREufIXfi6KTfuJUryy9kwW01rDBlI1Crv', NULL, '2019-05-31 06:36:39', '2019-05-31 06:36:39', '5678912345', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(32, 2, 'Pankti', 'Patel', 'ritsbogs1@gmail.com', 'users/default.png', 'female', NULL, '$2y$10$pppBiWAHyG//u1uZ9EFtyuO9YEvfyqsuy1oGG2qLNIUuBlZUrAsRS', NULL, NULL, NULL, NULL, '2019-05-31 07:30:05', '2019-05-31 07:30:05', '4534236789', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 2),
(33, 2, 'Kajal', 'referal', 'kajal1295gupta@gmail.com', 'users/default.png', 'female', NULL, '$2y$10$3ugyQ47hP6Exb1JhTIURGOVjV3fu4OrjihTEdd6KwXxb6i1BKcP8i', NULL, NULL, 'JkOVbXG4NSzZqIA1mXXX3OGb7gt33kakIZ8pJfxDhfujqh6Kn7dj9jY7jCe4', NULL, '2019-06-03 12:54:38', '2019-06-03 12:54:38', '9001748188', '+91', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 50),
(34, 2, 'Kumar', 'Ravi', 'ravikumawat523+1@gmail.com', 'users/default.png', 'male', NULL, '$2y$10$Jlh/PiCDG9Lpsd3n44WTzOUdKluAfpiZigiltLNCZ46XpfTGROlXK', NULL, NULL, NULL, NULL, '2019-06-04 09:39:07', '2019-06-04 09:39:07', '8279295217', '+91', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 50),
(35, 2, 'Ansh', 'Singhal', 'anshayeshasuchi@gmail.com', 'users/default.png', 'male', '2019-06-04 16:57:24', '$2y$10$1eG7W4r8ldaEeU8HBB4BPevf9XOexefoULG2waGXlFgVC/fhseD/K', NULL, NULL, 'KmckSYTshKsOJCiNm7MDrN3z4qSquRulnJdT5kFFW1isOU6yGpgihtGhsFjm', NULL, '2019-06-04 16:52:38', '2019-06-04 16:58:22', '9599588747', '+1', 'US', 'US', 'New York', 'New York', '12345', 'United States Of America', 1, 1, 'images/avatar.png', 49),
(36, 2, 'Such', 'Singhal', 'suchisinghal@yahoo.com', 'users/default.png', 'female', NULL, '$2y$10$EwUC7gnN9K1JqJWfbsarxeYCs5X6WaxZZLa8.lReEH9R8J/vksDZu', NULL, NULL, '4o4YmDlLFePOcf5KRnxVu3IxWEVKWVozlcaIgx1JSPtjv65f0V1i2FEk1Fg8', NULL, '2019-06-04 16:54:57', '2019-06-04 16:54:57', '9599508747', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 45),
(37, 2, 'A', 'A', 'anuj@xyz123.com', 'users/default.png', 'male', NULL, '$2y$10$9FmFtiNUfBQJqDjBaVhwg.fmNtqanl1.zFK9H48l79c7X4xtgGZ8i', NULL, NULL, 'o5o3V811y84FTojncGYVb3YaaRgDmIFmUt8dyLgJNRMKnkaOgDDlkUwb5jjo', NULL, '2019-06-05 08:13:47', '2019-06-05 08:13:47', '9599588747', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 45),
(38, 2, 'Trilokts6', 'Parmarts6', 'triloksingh2392+ts6@gmail.com', 'users/default.png', 'male', '2019-06-06 09:04:57', '$2y$10$I3bX8lXa25JI8CQ6/HNLUuOVXdwbLKg8z56h4wwwjma/TW/tMw0Mm', NULL, NULL, '3mzeyGEOuz7Z1GKiNdGIgBCeDhCGs3GEczFz4p9UM8cgvp38s596EvCgZidJ', NULL, '2019-06-06 09:02:20', '2019-06-06 09:27:51', '1234567891', '+1', 'test address 1', 'test address 2', 'test city', 'test state', '12345', 'United States Of America', 1, 1, 'images/avatar.png', 41),
(39, 2, 'Trilokst7', 'Parmarst7', 'triloksingh2392+st7@gmail.com', 'users/default.png', 'male', NULL, '$2y$10$ryOVAqVzC1AFgoTx1AznHe5WSANXq5FO9f.V0v6k0IgzkOVhmBLKm', NULL, NULL, 'DagRy31T1spN9ASt0i9MK4t42TT4CmHX54sopgfji9hXLAjjb2j03yO3TBXf', NULL, '2019-06-06 11:36:22', '2019-06-06 11:36:22', '1234567891', '+1', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 45),
(40, 2, 'ab', 'cd', 'ab@mailinator.com', 'users/default.png', 'male', '2019-06-10 16:11:32', '$2y$10$wfwYQp/WJ.w0s3zUhcYSLeq1IuycYBlouZUxhReVRheqAHIAGFgNO', NULL, NULL, NULL, NULL, '2019-06-10 16:10:34', '2019-06-10 16:13:30', '9887451414', '+1', 's', 's', 's', 's', '123456', 'United States Of America', 1, 1, 'images/avatar.png', 43),
(41, 2, 'Nitin', 'Johnson', 'nitin.johnson2000@gmail.com', 'users/default.png', 'male', '2019-06-11 19:40:32', '$2y$10$i7Y/46fPO4B/ob4yXMurMe8baieG4RGlEFd8lxeQeOcTafdoiN9uO', NULL, NULL, 'e1tvPklZs09RLihhyWayL1yqJJgkOaW7YHmamwRy4e2WwGHhXexxYsi1yVVC', NULL, '2019-06-11 19:35:31', '2019-06-11 19:40:32', '123456789', 'India', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 45),
(42, 2, 'Atul', 'Gupta', 'gupta.atul18@gmail.com', 'users/default.png', 'male', NULL, '$2y$10$RbWhwsSkUoOkD22h6oklRu3ercMQ6B5IJvZ2l54Zm5R.mR4fQAkTG', NULL, NULL, NULL, NULL, '2019-06-24 04:53:41', '2019-06-24 04:53:41', '9899902424', '+91', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 45),
(43, 2, 'lee', 'miki', 'pro3116214@mail.ru', 'users/default.png', 'female', NULL, '$2y$10$Bo6.fxTdu4t95WpP3P8LieFgMX4GJy5NiqdywsDDTOf7lJtQMyocu', NULL, NULL, NULL, NULL, '2019-06-24 10:57:45', '2019-06-24 12:48:50', '(921) 747-9082', '+7', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'images/avatar.png', 45);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `winners`
--

CREATE TABLE `winners` (
  `id` int(10) UNSIGNED NOT NULL,
  `position` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `game_id` int(10) UNSIGNED NOT NULL,
  `pointer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `winners`
--

INSERT INTO `winners` (`id`, `position`, `user_id`, `game_id`, `pointer_id`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 5, 45, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(2, 2, 5, 5, 44, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(3, 3, 5, 5, 46, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(4, 4, 6, 5, 62, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(5, 5, 5, 5, 47, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(6, 6, 5, 5, 40, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(7, 7, 6, 5, 60, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(8, 8, 5, 5, 39, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(9, 9, 5, 5, 41, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(10, 10, 5, 5, 38, '2019-04-30 13:02:51', '2019-04-30 13:02:51'),
(11, 1, 5, 6, 123, '2019-05-15 15:49:47', '2019-05-15 15:49:47'),
(12, 2, 5, 6, 106, '2019-05-15 15:49:47', '2019-05-15 15:49:47'),
(13, 3, 5, 6, 114, '2019-05-15 15:49:47', '2019-05-15 15:49:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `category_products`
--
ALTER TABLE `category_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_products_product_id_foreign` (`product_id`),
  ADD KEY `category_products_categories_id_foreign` (`categories_id`);

--
-- Indexes for table `custom_page`
--
ALTER TABLE `custom_page`
  ADD PRIMARY KEY (`custom_pageid`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `declare_winners`
--
ALTER TABLE `declare_winners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dicount_coupons`
--
ALTER TABLE `dicount_coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dicount_coupons_name_unique` (`name`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`footer_id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`),
  ADD KEY `games_declarewinner_id_foreign` (`declareWinner_id`);

--
-- Indexes for table `global_settings`
--
ALTER TABLE `global_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage_settings`
--
ALTER TABLE `homepage_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_user_id_foreign` (`user_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `pointers`
--
ALTER TABLE `pointers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pointers_user_id_foreign` (`user_id`),
  ADD KEY `pointers_invoice_id_foreign` (`invoice_id`),
  ADD KEY `pointers_game_id_foreign` (`game_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_name_unique` (`name`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referrals_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `tie_breakers`
--
ALTER TABLE `tie_breakers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tie_breakers_game_id_foreign` (`game_id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_email_verified_at_unique` (`email_verified_at`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `winners`
--
ALTER TABLE `winners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `winners_user_id_foreign` (`user_id`),
  ADD KEY `winners_game_id_foreign` (`game_id`),
  ADD KEY `winners_pointer_id_foreign` (`pointer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category_products`
--
ALTER TABLE `category_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `custom_page`
--
ALTER TABLE `custom_page`
  MODIFY `custom_pageid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `declare_winners`
--
ALTER TABLE `declare_winners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dicount_coupons`
--
ALTER TABLE `dicount_coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `footer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `global_settings`
--
ALTER TABLE `global_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `homepage_settings`
--
ALTER TABLE `homepage_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1249;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `pointers`
--
ALTER TABLE `pointers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1378;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tie_breakers`
--
ALTER TABLE `tie_breakers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `winners`
--
ALTER TABLE `winners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_products`
--
ALTER TABLE `category_products`
  ADD CONSTRAINT `category_products_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `games_declarewinner_id_foreign` FOREIGN KEY (`declareWinner_id`) REFERENCES `declare_winners` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pointers`
--
ALTER TABLE `pointers`
  ADD CONSTRAINT `pointers_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  ADD CONSTRAINT `pointers_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `pointers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `referrals`
--
ALTER TABLE `referrals`
  ADD CONSTRAINT `referrals_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tie_breakers`
--
ALTER TABLE `tie_breakers`
  ADD CONSTRAINT `tie_breakers_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `winners`
--
ALTER TABLE `winners`
  ADD CONSTRAINT `winners_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  ADD CONSTRAINT `winners_pointer_id_foreign` FOREIGN KEY (`pointer_id`) REFERENCES `pointers` (`id`),
  ADD CONSTRAINT `winners_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

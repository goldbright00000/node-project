<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TieBreakers extends Model
{
	/**
	 * Function to define one to one replationship with Games
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function games()
	{
		return $this->hasOne('App\Game');
	}
}

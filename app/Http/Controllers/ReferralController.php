<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class ReferralController extends Controller {
	/*
	 * Adding a constructor
	 * Added the Auth middleware to ensure that only logged in users can access the views
	 */

	public $user;
	public $sentRequests;
	public $acceptedRequests;


	public function __construct() {
		$this->middleware( 'auth' );
	}

	private function setProperties() {
		$this->user             = Auth::user();
		$this->sentRequests     = User::find( Auth::user()->id )->referrals->where( 'status', '=', 0 );
		$this->acceptedRequests = User::find( Auth::user()->id )->referrals->where( 'status', '=', 1 );
	}

	/**
	 * Method to control the refferal Dashbaord
	 *
	 */
	public function index() {

		// Set the Class Properties needed for the method
		$this->setProperties();

		// Return View With Data
		return view( 'pages.credits-history' )->with( [
			'sentRequests'     => $this->sentRequests,
			'acceptedRequests' => $this->acceptedRequests,
			'user'             => $this->user,
		] );

	}


	/**
	 * Fucntion to Send Referral request
	 */
	public function store( Request $request ) {
		// Validate the incoming request data
		$validatedData = $request->validate( [
			'name'  => 'required|max:255',
			'email' => 'required|unique:referrals|max:255',
		] );

		$this->user = Auth::user();

		// Execute the referal Request
		sendReferralRequest( $request, $this->user );

		// Add confirmation to the session
		session()->put( 'success', 'Your Request Has Been Sent Successfully' );
		session()->save();

		// Set the properties needed for the method
		$this->setProperties();

		return back()->with( [
			'user'             => $this->user,
			'sentRequests'     => $this->sentRequests,
			'acceptedRequests' => $this->acceptedRequests,
		] );
	}


}

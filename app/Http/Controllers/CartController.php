<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Darryldecode\Cart\CartCondition;
use Log;
class CartController extends Controller {

	/**
	 * Important prpoperties used by the class
	 */
	private $user;


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$products = Product::inRandomOrder()->take( 4 )->get();

		return view( 'pages.cart' )->with( 'products', $products );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request, $product_id ) {
        $product_id_check=null;
        if(empty($product_id)){
            $product_id_check = $request->get('product_id');
        } else if(is_numeric($product_id)){
            $product_id_check = $request->get('product_id');
        } else {
            $temp_cart= json_decode(json_encode(\Cart::content()), true);
            $product_id_check = $temp_cart[$product_id]['options']['attributes']['product_attr']['id'];
        }   
        
        if($request['headerAddCard'])
		{
			session(['cartAdded' => $request['headerAddCard']]);
		}
		else{
			session(['cartAdded' => 'value']);
		}
        
        
        $qty = $request->get('product_quantity');   
        
        $this->user      = Auth::user();
		$productQuantity = $qty;
		$ticketStatus    = CheckMaximumTickets( $this->user, $productQuantity, FindCurrentGame() );
		$cart            = \Cart::content();
		$products        = Product::inRandomOrder()->take( 4 )->get();

		if ( $ticketStatus === false ) {
			return redirect( route( 'product-listing' ) );
		}
        
        $selectedProduct = \DB::table( 'products' )->where( 'id', '=', $product_id_check )->get();
        if($request->get('to_do')=='updateCartVal'){
            $current_qty=null;
            foreach(\Cart::content() as $row){
                $current_qty=$row->qty;
            }
            \Cart::update($product_id, $current_qty+$qty);      
        } else {
            $base_price_multiplier=null;
            if($qty<=4){
                $base_price_multiplier=1;
            } else if($qty>=5 && $qty<=9){
                $base_price_multiplier=0.95;
            } else if($qty>9){
                $base_price_multiplier=0.90;
            }

            \Cart::add(
                $product_id_check, 
                $selectedProduct->first()->name,
                $qty, 
                $base_price_multiplier,
                [
                    'attributes' => [
                        'product_attr' => [
                            'id'    => $selectedProduct->first()->id,
                            'slug'  => $selectedProduct->first()->slug,
                            'image' => $selectedProduct->first()->product_image1,
                        ],
                        'size_attr'    => [
                            'label' => 'xxl',
                            'price' => 15.00,
                        ]
                    ]                
                ]
            );
        }
        
        
        
		$success_message = 'The ticket(s) for ' . $selectedProduct->first()->name . ' have been added to your cart ';
		session()->put( 'success_message', $success_message );
		//Saving the session...
		session()->save();
        
		return redirect()->route( 'product-listing' );
        
	}


	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		\Cart::remove( $id );
		removePointers();
		$removed_message = 'The selected ticket(s) have been removed from your cart';
		session()->put( 'removed_message', $removed_message );
		//Saving the session...
		session()->save();

		return back()->withErrors(['cartRemove']);
	}

}

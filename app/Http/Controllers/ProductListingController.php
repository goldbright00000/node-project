<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;
use App\Product;
use Log;
class ProductListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function index(Request $request) {        
		/*$cartVal = false;
		$cartHeaderAdd = "";
		if ($request->session()->has('cartAdded')) {
			$cartVal = 'cart';
			$cartAdded = $request->session()->get('cartAdded');
			if($cartAdded == 'headerAddCard')
			{
				$cartHeaderAdd = $cartAdded;
			}
			\Session::forget('cartAdded');
			
		}*/
		
    	//Set minimum price and save in the session
	    $minimumPrice = Product::min('price_slab1');
	    $maximumPrice = Product::max('price_slab1');

	    session()->put('minimumPrice', $minimumPrice);
	    session()->put('maximumPrice', $maximumPrice);
	    session()->save();


    	if ($request->has('filter')){

    		// Get and save the price requested by the client in the session
		    $lastPrice = $request->price;
		    session()->put('lastPrice', $lastPrice);
		    session()->save();

    		// Filtered The Requested Categories From The Request
		    $requestedCategories = collect($request->except('price', 'filter'));

		    // Flash request categories to the session
			session()->put('requestedCategories', $requestedCategories);
			session()->save();

			// Extract ID's of the requested categories
		    $categoriesID = $requestedCategories->keys();

		    // Check if Requested Categories Are More Than 0
			if ($requestedCategories->count() > 0){
			    $products = Product::with('categories')
		                       ->whereBetween('price_slab1', [0, $request->price])
		                       ->whereHas('categories',
			                       function($query) use ($categoriesID) {
			                            $query->whereIn('categories_id', $categoriesID);
		                            })
		                       ->paginate(9);
			}
			else {
				$products = Product::orderBy('id')->whereBetween('price_slab1', [0, $request->price])->paginate(9);
			}
		    $categories = Categories::all();

	    }
    	else {
		    $products = Product::orderBy('id')->paginate(9);
		    $categories = Categories::all();
	    }

	    return view('pages.products')->withInput($request)
         ->with([
		    'products' => $products,
		    'categories' => $categories,
            //'cartVal' => $cartVal,
		    //'cartHeaderAdd' => $cartHeaderAdd,
	    ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        return view('pages.product-detail')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the listings product listings on the page
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Invoice;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;


class HomeController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'auth' );
	}


	/**
	 * Validates user data added for password change
	 */

	public function admin_credential_rules( array $data ) {
		$messages = [
			'current-password.required' => 'Please enter current password',
			'password.required'         => 'Please enter password',
		];

		$validator = validator()->make( $data, [
			'current-password'      => 'required',
			'password'              => 'required|same:password',
			'password_confirmation' => 'required|same:password',
		], $messages );

		return $validator;
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$userid      = Auth::id();
		$userDetails = User::where( 'id', '=', $userid )->first();

		//dd($userDetails);
		return view( 'home' )->with( 'user', $userDetails );
	}

	/**
	 * Display the Order History Of The User
	 */
	public function orderHistory() {
		$userid   = Auth::id();
		$invoices = User::find( $userid )->invoices;

		return view( 'pages.order-history' )->with( 'invoices', $invoices );
	}


	/**
	 * Function to return invoice view and Generate Invoice
	 *
	 * @param $request  invoice ID
	 * @param Request the requested invoice ID
	 *
	 * @return Returns the invoice view along with the invoice details
	 */
	public function invoice( Request $request ) {
		$invoiceID   = $request->invoice_id;
		$userid      = Auth::id();
		$user        = Auth::user();
		$invoiceUser = Invoice::find( $invoiceID )->user;
		$invoice     = Invoice::where( 'id', '=', $invoiceID )->first();
		$dbUserID    = $invoiceUser->id;

		// Check if the invoice belongs to the user who requested it
		if ( $userid != $dbUserID ) {
			session()->put( 'failure', 'Sorry you do not have the permissions to view this page' );
			session()->save();

			return view( 'pages.result' );
		} else {
			return view( 'pages.invoice' )->with( [
				'invoice' => $invoice,
				'user'    => $user,
			] );
		}
	}

	/**
	 * Function to update the user information in the database
	 *
	 * @param Request
	 */
	public function update( Request $request ) {

		// Get the current user
		$currentUser = Auth::user()->id;


		// Update the user Details Based on the changes that have been requested by the user
		if ( $request->firstName != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'name' => $request->firstName,
			] );
		}
		if ( $request->lastName != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'lastName' => $request->lastName,
			] );
		}
		if ( $request->phone != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'mobile' => $request->phone,
			] );
		}
		if ( $request->gender != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'gender' => $request->gender,
			] );
		}

		// Fetch the user details once again
		$userDetails = User::where( 'id', '=', $currentUser )->first();

		return back()->with( 'user', $userDetails );

	}

	/**
	 * Function to update the billing address in the database
	 *
	 */
	public function updateAddress( Request $request ) {

		// Get the current user
		$currentUser = Auth::user()->id;

		// Update the user Details Based on the changes that have been requested by the user
		if ( $request->address1 != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'address1' => $request->address1,
			] );
		}

		if ( $request->address2 != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'address2' => $request->address2,
			] );
		}

		if ( $request->city != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'city' => $request->city,
			] );
		}

		if ( $request->state != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'state' => $request->state,
			] );
		}

		if ( $request->zip != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'zip' => $request->zip,
			] );
		}

		if ( $request->country != null ) {
			Auth::user()->where( 'id', '=', $currentUser )->update( [
				'country' => $request->country,
			] );
		}


		// Fetch the user details once again
		$userDetails = User::where( 'id', '=', $currentUser )->first();

		return back()->with( 'user', $userDetails );
	}

	/**
	 * Function to update the password
	 */

	public function updatePassword( Request $request ) {
		// Get the current user
		$currentUser = Auth::user()->id;

		// Fetch the user details once again
		$userDetails = User::where( 'id', '=', $currentUser )->first();

		if ( Auth::Check() ) {
			$request_data = $request->All();
			$validator    = $this->admin_credential_rules( $request_data );

			if ( $validator->fails() ) {
				//dd('Validator Has Failed');
				return response()->json( array( 'error' => $validator->getMessageBag()->toArray() ), 400 );
			} else {
				$current_password = Auth::User()->password;
				if ( Hash::check( $request_data['current-password'], $current_password ) ) {
					$user_id            = Auth::User()->id;
					$obj_user           = User::find( $user_id );
					$obj_user->password = Hash::make( $request_data['password'] );;
					$obj_user->save();

					session()->put( 'successPassword', 'Your Password Has Been Updated Successfully' );
					session()->save();


					return back()->with( 'user', $userDetails );
				} else {
					session()->put( 'passwordError', 'Please enter correct current password' );
					session()->save();

					return back()->with( 'user', $userDetails );
				}
			}
		} else {
			return redirect()->to( '/' );
		}
	}

	public function profilePic( Request $request ) {

		// Fetch the image from the form request
		$picture = $request->file( 'picture' );

		$picture         = $request->file( 'picture' );
		$imageExtension = $picture->getClientOriginalExtension();
		Storage::disk( 'public' )->put( $picture->getFilename() . '.' . $imageExtension, File::get( $picture ) );

		// Get the current user
		$userID = Auth:: user();

		$image = $picture->getFilename() . '.' . $imageExtension;

		//dd($image);

		// Update the database with the image
		User:: where( 'id', '=', $userID->id )->update( [
			'profileImage' => $image,
			'avatar'       => $image,
		] );

		//dd(User:: where( 'id', '=', $userID->id )->first());

		return redirect()->route('home');
	}
}

<?php

namespace App\Http\Controllers;

use App\Mail\NotifyWinners;
use App\Pointer;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Game;
use App\DeclareWinner;
use App\User;
use App\Product;
use App\TieBreakers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Winner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Response;


class GameController extends Controller {

	// Variables used across the class
	private $allGames;
	private $allDeclareWinners;

	// Function to set the class properties
	private function setClassProperties() {

		// Get All The Games From The Database
		$this->allGames = Game::orderBy( 'endDate', 'DESC' )->paginate( 10 );

		// Get All Declare Winner Information
		$this->allDeclareWinners = DeclareWinner::all();

	}


	public function index() {

		// Set all The Required Class Properties
		$this->setClassProperties();

		return view( 'pages.winners' )->with( [
			'games'   => $this->allGames,
			'winners' => $this->allDeclareWinners,
		] );
	}


	public function detail( $slug ) {

		$winnerDetail = DeclareWinner::where( 'slug', '=', $slug )->first();
		$game         = Game::where( 'declareWinner_id', '=', $winnerDetail->id )->first();

		// Get the winners
		$winners = Winner::where( 'game_id', '=', $game->id )->get();

		// Collect the winning pointers
		$pointers = collect( [] );
		foreach ( $winners as $winner ) {
			// get all winning pointers
			$pointers[] = Pointer:: where( 'id', '=', $winner->pointer_id )->first();
		}

		// Get the login user
		$user       = Auth::user();
		$userPoints = null;

		// Fetch the points of the current user for the current game
		if ( $user != null ) {
			// Get all The points of the user
			$userPoints = Pointer:: where( [
				'game_id' => $game->id,
				'user_id' => $user->id,
			] )->get();
		}

		// Assign Variables For Collecting The Runner Up Details
		$userDetails    = [];
		$productDetails = [];


		foreach ( $winners as $win ) {

			$userDetails [] = collect( [
				'name'       => User:: where( 'id', '=', $win->user_id )->first()->name . ' ' . User:: where( 'id', '=', $win->user_id )->first()->lastName,
				'state'      => User:: where( 'id', '=', $win->user_id )->first()->state,
				'city'       => User:: where( 'id', '=', $win->user_id )->first()->city,
				'user_id'    => $win->user_id,
				'pointer_id' => $win->pointer_id,
			] );

			$product          = Pointer:: where( 'id', '=', $win->pointer_id )->first()->productID;
			$productDetails[] = [
				'product'    => Product:: where( 'id', '=', $product )->first()->name,
				'pointer_id' => $win->pointer_id,
			];
		}

		return view( 'pages.winners-detail' )->with( [
			'game'           => $game,
			'winner'         => $winnerDetail,
			'positions'      => $winners,
			'pointers'       => $pointers,
			'userPoints'     => $userPoints,
			'userDetails'    => $userDetails,
			'productDetails' => $productDetails,
		] );
	}

	// Function for Game Winner Admin View
	// https://medium.com/@matthewcoatney/wiring-laravel-voyager-custom-menus-6d75d637842
	public function gravy() {

		// Set all The Required Class Properties
		$this->setClassProperties();

		return view( '/vendor/voyager/gravy' )->with( [
			'games' => $this->allGames,
			'winners' => $this->allDeclareWinners,
		] );
	}

	public function winners( Request $request ) {
		$game       = Game:: where( 'id', '=', $request->game )->first();
		$winnerData = findWinners( $game );
		$users      = collect( [] );
		$products   = collect( [] );



		foreach ( $winnerData['pointers'] as $point ) {
			$users []    = User:: where( 'id', '=', $point->user_id )->first();
			$products [] = Product:: where( 'id', '=', $point->productID )->first();
		}
		$uniqueUsers    = $users->unique();
		$uniqueProducts = $products->unique();

		$tieBreakers = TieBreakers:: where( [
			'game_id' => $request->game,
		] )->get();


		return view( 'vendor.voyager.winners-list' )->with( [
			'winnerData'  => $winnerData,
			'users'       => $uniqueUsers,
			'products'    => $uniqueProducts,
			'tieBreakers' => $tieBreakers,
			'game_id'     => $request->game,
		] );
	}

	/**
	 * Public Function to declare the winners and publish them in the winner
	 * section of the website
	 *
	 * @param Request $request The Request variable from the form
	 *
	 * @return view() winners.declare
	 */

	public function declare( Request $request ) {
		$game_id = $request->game;
		$game    = Game:: where( 'id', '=', $game_id )->first();

		return view( 'vendor.voyager.winners-delcare' )->with( 'game', $game );
	}


	/**
	 * Public Function to post the results
	 */

	public function post( Request $request ) {

		$game_id = $request->gameid;
		$game    = Game:: where( 'id', '=', $game_id )->first();

		$winnerData = findWinners( $game );
		$users      = collect( [] );
		$products   = collect( [] );

		foreach ( $winnerData['pointers'] as $point ) {
			$users []    = User:: where( 'id', '=', $point->user_id )->first();
			$products [] = Product:: where( 'id', '=', $point->productID )->first();
		}

		$uniqueUsers    = $users->unique();
		$uniqueProducts = $products->unique();

		// Save Image 1
		$image1          = $request->file( 'image1' );
		$image1Extension = $image1->getClientOriginalExtension();
		Storage::disk( 'public' )->put( $image1->getFilename() . '.' . $image1Extension, File::get( $image1 ) );

		// Save Image 2
		if ( $request->image2 != null ) {
			$image2          = $request->file( 'image2' );
			$image2Extension = $image2->getClientOriginalExtension();
			Storage::disk( 'public' )->put( $image2->getFilename() . '.' . $image2Extension, File::get( $image2 ) );
		}

		// Save Image 3
		if ( $request->image3 != null ) {
			$image3          = $request->file( 'image3' );
			$image3Extension = $image3->getClientOriginalExtension();
			Storage::disk( 'public' )->put( $image3->getFilename() . '.' . $image3Extension, File::get( $image3 ) );
		}

		// Database Insertion For Declare Winner
		$declareWinners               = new DeclareWinner;
		$declareWinners->title        = $request->title;
		$declareWinners->subTitle     = $request->subTitle;
		$declareWinners->slug         = $request->slug;
		$declareWinners->description  = $request->description;
		$declareWinners->winnerImage1 = $image1->getFilename() . '.' . $image1Extension;
		if ( $request->image2 != null ) {
			$declareWinners->winnerImage2 = $image2->getFilename() . '.' . $image2Extension;
		}
		if ( $request->image3 != null ) {
			$declareWinners->winnerImage3 = $image3->getFilename() . '.' . $image3Extension;
		}
		$declareWinners->video = $request->video;
		$declareWinners->save();


		$loopcount = 1;
		foreach ( $winnerData['pointers'] as $point ) {
			$winners             = new Winner;
			$winners->position   = $loopcount ++;
			$winners->user_id    = $point->user_id;
			$winners->game_id    = $point->game_id;
			$winners->pointer_id = $point->id;
			$winners->save();
		}


		// Update the result Status
		Game::where( 'id', '=', $game_id )->update( [
			'resultStatus'     => 1,
			'declareWinner_id' => $declareWinners->id,
		] );

		session()->put( 'success', 'Winners Page Has Been Published Successfully' );
		session()->save();

		// Find All Players Who played the game
		$allPointers = Pointer:: where( [
			[ 'game_id', '=', $game_id, ],
			[ 'user_id', '!=', null ],
		] )->get();

		$allplayers = collect( [] );

		foreach ( $allPointers as $pointer ) {
			$player        = User:: where( 'id', '=', $pointer->user_id )->first();
			$allplayers [] = $player;
		}
		$uniquePlayers = $allplayers->unique();
		foreach ( $uniquePlayers as $player ) {
			Mail::to( $player->email )->send( new NotifyWinners( $game, $declareWinners, $player ) );
		}
		return redirect( route( 'voyager.games' ) );

	}
	
	
	public function reports() {		
		// Set all The Required Class Properties
		$userList = User::paginate( 10 );
		return view( '/vendor/voyager/reports' )->with( [
			'userList' => $userList,
		] );
	}
	
	public function userCsv(){

		$table = User::get();

		$filename = "Users.csv";
		$handle = fopen($filename, 'w+');
		fputcsv($handle, array('Id','Name' ,'Email','Gender','Mobile'));
	
		foreach($table as $key => $value) {
			fputcsv($handle, array($value->id,$value->name,$value->email,$value->gender,$value->mobile));
		}

		fclose($handle);

		$headers = array(
			'Content-Type' => 'text/csv',
		);

		return Response::download($filename, 'Users.csv', $headers);
	}
}

<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index(){
		$faqs = Faq::all();

		$sections = collect([]);

		foreach ($faqs as $faq){
			$sections [] = $faq->section;
		}
		$sections = $sections->unique();

    	return view('pages.faqs')->with([
    		'sections' => $sections,
    		'faqs' => $faqs,
	    ]);
    }
}

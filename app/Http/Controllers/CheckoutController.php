<?php

namespace App\Http\Controllers;

use App\GlobalSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Darryldecode\Cart\CartCondition;


class CheckoutController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		if ( checkVerification() == null ) {
			return redirect( route( 'verification' ) );
		}

		$user       = Auth::user();
		$newTickets = 0;

		// Check Maximum number of playable tickets reached or not
		$ticketStatus = CheckMaximumTickets( $user, $newTickets, FindCurrentGame() );

		// Redirect if the maximum number has been reached 
		if ( $ticketStatus === false ) {
			return redirect( route( 'product-listing' ) );
		}

		// Update Pointers Based on Previous Session
		updatePointerSession();

		// Check if the sales tax has been applied previously
		$taxExist = \Cart::content();

		if ( $taxExist == null ) {
			// Apply Sales Tax On The Invoice
			$saleTax      = GlobalSettings::first()->salesTax;
			$taxCondition = new CartCondition ( [
				'name'   => 'Sales Tax',
				'type'   => 'tax',
				'target' => 'total',
				'value'  => '+' . $saleTax . '%',
				'order'  => '5',
			] );
			\Cart::content();
		}


		//$ticketStatus = CheckMaximumTickets( $user, $request, FindCurrentGame() );
		if ( session()->has( 'creditAdjusted' ) ) {
			// Credit Adjusted Already
		} else {
			CreditAdjuster( $user );
		}

		return view( 'pages.checkout' )->with( [
			'user' => $user,
		] );
	}

	/**
	 * Apply discount coupons on the cart value
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return $cart
	 */
	public function discount( Request $request ) {
		$type            = 'coupon';
		$conditions      = \Cart::content();
		$coupon          = $request->discountCoupon;
		$couponObject    = DB::table( 'dicount_coupons' )->where( 'name', '=', $coupon )->first();
		$couponCondition = null;
		$user            = Auth::user();


		/**
		 * The function checks if the Coupon Inserted is correct and applies the discount condition
		 *
		 * @param $couponQuery
		 *
		 * @return CartCondition|null
		 */
		function buildCondition( $couponQuery, $conditions ) {
			if ( $couponQuery == null ) {
				session()->put( 'couponFlash', 'The Discount Coupon Entered is not valid' );
				session()->save();

				return null;
			} elseif ( $conditions->count() > 0 || session()->has( 'dontDiscount' ) ) {
				session()->put( 'couponFlash', 'One Of The Discount Coupons Or Credits Have Already been Applied' );
				session()->save();

				return null;

			} elseif ( session()->has( 'dontDiscount' ) ) {
				session()->put( 'couponFlash', 'One Of The Discount Coupons Or Credits Have Already been Applied' );
				session()->save();

				return null;
			} elseif ( $conditions->count() == 0 && $couponQuery != null ) {
				session()->put( 'couponSuccess', 'Discount Applied Successfully' );
				session()->save();
				$CouponCondition = new CartCondition( [
					'name'   => $couponQuery->name,
					'type'   => $couponQuery->type,
					'target' => $couponQuery->target,
					'value'  => '-' . $couponQuery->value . '%',
					'order'  => '1',
				] );

				\Cart::content();
			} else {
				session()->put( 'couponFlash', 'The Discount Condition Does Not Meet The Requirement' );
				session()->save();

				return null;
			}
		}

		buildCondition( $couponObject, $conditions );

		return view( 'pages.checkout' )->with( 'user', $user );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}

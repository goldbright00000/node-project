<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Darryldecode\Cart\CartCondition;
use App\User;
use App\DatabaseStorageModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

	/**
	 * @return string
	 */
	public function redirectTo()
	{
		if (request()->has('previous')) {
			return $this->redirectTo = request()->previous;
		}
		else {
			return $this->redirectTo = '/home';
		}
	}


	/**
	 * Redirect the user to the GitHub authentication page.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function redirectToProvider($provider)
	{
		return Socialite::driver($provider)->redirect();
	}


	/**
	 * Obtain the user information from GitHub.
	 *
	 * @return \Illuminate\Http\Response
	 * @param $provider LoginController the Social Login Provider
	 */
	public function handleProviderCallback($provider)
	{
		$socialUser = Socialite::driver($provider)->user();
		//dd($socialUser);

		$user = User::where('provider_id', $socialUser->getId())->first();
		if (!$user) {
			// add user to database
			$user = User::create([
				'email' => $socialUser->getEmail(),
				'name' => $socialUser->getName(),
				'provider_id' => $socialUser->getId(),
				'provider' => $provider,
			]);
		}
		// login the user
		Auth::login($user, true);	
		// $user->token;
	}
    
    public function login(Request $request){
        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            \Cart::restore(Auth::user()->id);
            return redirect($this->redirectTo);
        } else {
            $error_message = 'Invalid login credentials';
            session()->put( 'error_message', $error_message );
            session()->save();
            return redirect($this->redirectTo);
        }
        
    }
    
    public function logout(Request $request){
        if(!empty(\Cart::content())){
            \DB::table('shoppingcart')->where('identifier', Auth::user()->id)->delete();
            \Cart::store(Auth::user()->id);    
        }        
        Auth::logout();
        \Session::flush();
        return redirect()->route( 'product-listing' );
    }

}

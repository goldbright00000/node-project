<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use DB;
use Log;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/email-confirm';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'lastName' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
	        'countryCode' => ['required', 'string'],
	        'gender' => ['required', 'string'],
            'mobile' => ['required', 'string'],
        ]);
    }


    public function validate_news(array $data){
    	if(isset($data['newsConfirmation'])){
    		return $data;
	    } else {
		    $data['newsConfirmation'] = "0";
		    return $data;
	    }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)  {
		$ticketInfo = DB::table('global_settings')->where('id',1)->orderBy('id','ASC')->first();
		$userDiscount = "";
		if($ticketInfo)
		{
			$userDiscount = $ticketInfo->userDiscount;
		}
        $user = User::create([
            'name' => $data['name'],
            'lastName' => $data['lastName'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'gender' => strval($data['gender']),
            'countryCode' => $data['countryCode'],
            'mobile' => $data['mobile'],
            'initialDiscount' => $userDiscount,
            'ageConfirmation' => $data['ageConfirmation'],
            'newsConfirmation' => $this->validate_news($data)['newsConfirmation'],
        ]);

	    Mail::to($data['email'])->send(new WelcomeMail());
	    $user->sendEmailVerificationNotification();
	    return $user;
    }

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function register(Request $request) {


		$this->validator($request->all())->validate();
		event(new Registered($user = $this->create($request->all())));
		$this->guard()->login($user);

		// Check And Add Credits To Refererrs Account
		CheckReferral( $request );

		//return $this->registered($request, $user)?: redirect($this->redirectPath());
		return redirect($this->redirectPath());
	}

}

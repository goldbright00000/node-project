<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;
use App\Product;
use App\HomepageSetting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Log;

class HomePageController extends Controller {
	public function __construct() {

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$products = Product::inRandomOrder()->take( 4 )->get();
		$settings = HomepageSetting::first();
		$game     = FindCurrentGame();
		Log::info($game);
		$endDate = Carbon::createFromFormat('Y-m-d H:i:s', $game->endDate);
		Log::info($endDate);

		return view( 'pages.home' )->with( [
			'products'     => $products,
			'homeSettings' => $settings,
			'game'         => $game,
			'endDate'=> $endDate,
		] );
	}

	/**
	 * Function to resend an email verification email
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function verify() {
		$user = Auth::user();
		$user->sendEmailVerificationNotification();

		return view('pages.email-confirm');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}

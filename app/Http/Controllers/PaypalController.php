<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\User;
use App\Invoice;



class PaypalController extends Controller {

	protected $provider;

	public function __construct() {
		$this->provider = new ExpressCheckout();

	}

	public function expressCheckout( Request $request ) {
        
		$user = Auth::user();
		$cart = \Cart::content();
		$invoice = null;

		// Add Billing Address To The Database
		updateBillingDetails($request, $user);

		// Comparing and saving user details
		compareUserDetails( $user, $request );

		// Straightaway Process the invoice if the invoice is an existing invoice
		if($request->has('invoice_id')){
			$invoice = Invoice::where('id', '=', $request->invoice_id )->first();
		}
		else {
			// Generate invoice and prepare the data For PayPal
			$invoice = generateInvoice( $user, $cart );
		}

		// Link The invoice to the relevant pointer table
		addInvoicePointer($invoice);

		// Complete the process if the invoice value is Zero
		if (  $invoice->total == 0 ) {
			// Process The Invoice With Credits
			processInvoiceWithCredits($invoice, $request);

			// Update The Credits Adjusted In Transaction In Database
			UpdateUserCredit( $user, session()->get( 'creditAdjusted' ) );

			//Perform Post Checkout Actions
			postCheckoutActions($request,$invoice);

			// Return to the results page and display success message
			return view( 'pages.result' );
		}

		// Prepare the data variable for PayPal
		$data = preparePaypalData( $invoice );

		// Update The Credits Adjusted In Transaction In Database
		UpdateUserCredit( $user, session()->get( 'creditAdjusted' ) );

		//Perform Post Checkout Actions
		postCheckoutActions($request,$invoice);
        $data['shipping_discount'] = 0;
        $data['cancel_url'] = route('paypal.failure');

		// Send Request To PayPal
		$response = $this->provider->setCurrency( 'USD' )->setExpressCheckout( $data );
        
        //return redirect($response['paypal_link']);

		/*if ( ! $response['paypal_link'] ) {
			return redirect( '/' )->with( [ 'code' => 'danger', 'message' => 'Something went wrong with PayPal' ] );
		}*/

		// Saving The $data to session
		session()->put( 'data', $data );
		session()->save();

		// This will redirect user to PayPal
		return redirect( $response['paypal_link'] );

	}


	/**
	 * Function to handle Response from PayPal
	 */
	public function expressCheckoutSuccess( Request $request ) {
    
		$token     = $request->get( 'token' );
		$PayerID   = $request->get( 'PayerID' );
		$data      = session()->get( 'data' );
		$invoiceID = session()->get( 'data' )['invoice_id'];
		$invoice   = Invoice::where( 'id', '=', $invoiceID )->first();
		$user      = Auth::user();

		// Perform The Transaction On PayPal
		$response = $this->provider->doExpressCheckoutPayment( $data, $token, $PayerID );

		//Check if the payment went through
		if ( $response['ACK'] == 'Success' ) {

			// Update the Payment Status Of The Invoice
			Invoice::where( 'id', '=', $invoiceID )->update( [
				'paymentMethod'  => 'PayPal',
				'payment_status' => 'Completed'
			] );

			// Set Success Message in the Session
			session()->put( 'success', 'Your Payment Was Successful. Happy Playing!' );
			session()->save();
		} else {
			// Set Failure Message In The Session
			session()->put( 'failure', 'Your Payment Did Not Go Through, Please contact the Admin or Try again' );
			session()->save();
		}

		return view( 'pages.result' );
	}
    
    public function GeFailNotification(Request $request){
        session()->put( 'failure', 'Payment cancelled by user.' );
        session()->save();
        return view( 'pages.result' );
    }

}

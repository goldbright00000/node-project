<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'products' => 'array',
	];


	protected $fillable = ['title', 'total', 'payment_status'];

	public function getPaidAttribute() {
		if ($this->payment_status == 'Invalid') {
			return false;
		}
		return true;
	}

	/**
	 * Function to associate Invoices with User
	 */
	public function user(){
		return $this->belongsTo('App\User');
	}

	/**
	 * Function to define one to many replationship with pointer model
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function pointers()
	{
		return $this->hasMany('App\Pointer');
	}


}

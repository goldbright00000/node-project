<?php

use Illuminate\Support\Facades\Auth;

if (!function_exists('checkVerification')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function checkVerification() {
		$user = Auth::user();
		$verified = $user->email_verified_at;

		return $verified;
    }
}

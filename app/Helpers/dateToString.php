<?php

use Carbon\Carbon;

if (!function_exists('dateToString')) {

    /**
     * Function to convert date to string format
     *
     * @param
     * @return
     */
    function dateToString($date){
	    $formatDate = Carbon::createFromFormat('Y-m-d H:i:s', $date)->toFormattedDateString();
        return $formatDate;
    }
}

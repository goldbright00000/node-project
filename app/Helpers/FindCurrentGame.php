<?php
use App\Game;
use Carbon\Carbon;

if (!function_exists('FindCurrentGame')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function FindCurrentGame() {
	    $currentTimeStamp = Carbon::now();
	    $currentGame = Game::where('startDate', '<=', $currentTimeStamp)
	                       ->where('endDate', '>=', $currentTimeStamp)
	                       ->first();
	    return $currentGame;
    }
}

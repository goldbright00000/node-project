<?php

use App\Pointer;

if (!function_exists('updatePointerSession')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function updatePointerSession() {
		if(session()->has('previousSession')){

			$previousToken = session()->get( 'previousSession' );
			$newSession    = session()->get('_token');

			// Get Pointers with Old Session ID
			$pointers = Pointer::where( 'sessionID', '=', $previousToken )->get();

			// Update all Pointers
			Pointer::where( 'sessionID', '=', $previousToken )->update([
				'sessionID' => $newSession,
			]);

			session()->forget('previousSession');
			session()->save();

		}
    }
}

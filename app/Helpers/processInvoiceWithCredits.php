<?php
use App\Mail\SuccessfulOrder;
use App\Mail\OrderInvoice;
use Illuminate\Support\Facades\Mail;
use App\Invoice;

if (!function_exists('processInvoiceWithCredits')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function processInvoiceWithCredits($invoice, $request) {
	    // Update the Payment Status Of The Invoice And Mark As Paid
	    Invoice::where( 'id', '=', $invoice->id )->update( [
		    'paymentMethod'  => 'Credit Adjusted',
		    'payment_status' => 'Completed'
	    ] );

	    // Set Success Message in the Session
	    session()->put( 'success', 'We have Adjusted Your Credits And You Are All Done. Happy Playing!' );
	    session()->save();

	    // Send A Confirmation Email to the user
	    Mail::to( $request->user() )->send( new SuccessfulOrder( $invoice->id ) );
	    Mail::to( $request->user() )->send( new OrderInvoice( $invoice ) );
    }
}

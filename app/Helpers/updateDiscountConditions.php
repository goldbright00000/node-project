<?php

use App\Product;
use Darryldecode\Cart\CartCondition;

if (!function_exists('updateDiscountConditions')) {

    /**
     * Function to update the discount conditions from the cart once
     * a prodi
     *
     * @param
     * @return
     */
    function updateDiscountConditions($cartItem) {

	    // Clear All Cart Conditions
    	\Cart::clearItemConditions($cartItem->id);

    	// Get the product
	    $product = Product::where('id','=',$cartItem->id)->first();


	    // Discount 1 Condition
	    $discount1 = new CartCondition( [
		    'name'  => '5 Tickets Discount',
		    'type'  => 'volume',
		    'value' => '-' . $product->discount_slab1 . '%',
	    ]);

	    // Discount 2 Condition
	    $discount2 = new CartCondition( [
		    'name'  => '10 Tickets Discount',
		    'type'  => 'volume',
		    'value' => '-' . $product->discount_slab2 . '%',
	    ]);


    	if($cartItem->quantity >= 5 && $cartItem->quantity < 10  ){
		    \Cart::addItemCondition($cartItem->id, $discount1);
	    }
    	elseif ($cartItem->quantity >= 10) {
		    //dd('Less than 10 is true now');
    		\Cart::addItemCondition($cartItem->id, $discount2);
	    }

    }
}

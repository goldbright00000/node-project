<?php

use App\Product;

if (!function_exists('stampProducts')) {

    /**
     * Function to stamp all products with Unique ID
     *
     * @param $cart - Use the shoipping cart to stamp all the products.
     * @return
     */
    function stampProducts($cart){
		$cart = json_decode(json_encode($cart), true);
	    $stampedProducts = collect( [] );
	    foreach ( $cart as $item ) {
		    for ( $x = 1; $x <= $item['qty']; $x ++ ) {
			    $stampedProducts->push( [
					'rowId' => $item['rowId'],
				    'productID' => $item['id'],
				    'name'      => $item['name'],
				    'stamp'     => 'id' . $item['id'] . $x,
				    'image'     => Product::where( 'id', '=', $item['id'] )->first()->product_image1,
				    'stamped'   => gameStatus( 'id' . $item['id'] . $x ),
			    ] );
		    }
	    }
	    session()->put( 'products', $stampedProducts );
	    session()->save();

	    return $stampedProducts;
    }
}

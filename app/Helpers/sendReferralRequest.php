<?php

use App\Referral;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendReferral;

if (!function_exists('sendReferralRequest')) {

    /**
     * Function to Send a Rererral Request
     *
     * @param
     * @return
     */
    function sendReferralRequest($request, $user ){

		$referral = new Referral;
	    $referral->name = $request->name;
	    $referral->email = $request->email;
	    $referral->message = $request->message;
	    $referral->user_id = $user->id;
	    $referral->save();

	    // Prepareing the $sendReferral collection
	    $sendReferral = collect([
	    	'referrer' => $user->name,
		    'message' => $request->message,
	    ]);


	    // Sending the email to the Invitee
	    Mail::to( $request->email )->send( new SendReferral( $sendReferral ) );
    }
}

<?php

use App\Pointer;

if ( ! function_exists( 'pointerCollectionFromDb' ) ) {

	/**
	 * description
	 *
	 * @param
	 *
	 * @return
	 */
	function pointerCollectionFromDb( $request ) {

		// Get the session token for the current session of the user
		$sessionID = session()->get( '_token' );

		// Get all the pointers set For Current Session
		$pointers = Pointer::where( [
			[ 'sessionID', '=', $sessionID ],
			[ 'invoice_id', '=', null ],
		] )->get();

		// Get the coordinates only for the current Product
		$productId          = $request->productID;
		$stampId            = $request->stampId;
		$productCoordinates = $pointers->where('productID', '=', $productId);

		return $productCoordinates;
	}
}

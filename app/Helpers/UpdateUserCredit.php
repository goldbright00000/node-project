<?php
use App\User;

if (!function_exists('UpdateUserCredit')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function UpdateUserCredit($user, $creditAdjusted) {
			$currentCredits = $user->initialDiscount;
			$updatedValue = $currentCredits - $creditAdjusted;
			User::where('id', '=', $user->id)->update(['initialDiscount'=> $updatedValue]);
    }
}

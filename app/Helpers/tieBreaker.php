<?php

use App\Pointer;
use App\TieBreakers;
use App\Game;

if ( ! function_exists( 'tieBreaker' ) ) {

	/**
	 * description
	 *
	 * @param $game - The complete Game Object
	 * @param $players - The collection of players in a tie
	 *
	 * @return
	 */
	function tieBreaker( $game, $players ) {

		// Setting the required variables
		$gameID            = $game->id;
		$newDistancePoints = collect( [] );

		/**
		 * Function to check the distance of points played by the players in the new game
		 *
		 * @param $playerID - The ID of the player in question
		 * @param $gameID - The ID of the game for which the result is needed
		 *
		 * @return $newDistancePoints
		 */
		function findNewGameDistance( $playerID, $gameID ) {
			$currentGame   = FindCurrentGame();
			$alreadyPlayed = TieBreakers:: where( [
				'user_id' => $playerID,
				'game_id' => $gameID,
			] )->first()->pointCount;

			// Fetch all The Pointers Of The Current Game For Particular User
			$currentGamePointers = Pointer:: where( [
				'user_id' => $playerID,
				'game_id' => $currentGame->id,
			] )->get();

			// Check The Count And Pick Pointer Accordingly
			if ( $currentGamePointers->count() > $alreadyPlayed ) {
				$newDistancePoints = $currentGamePointers->pull( $alreadyPlayed );
			} else {
				$newDistancePoints = null;
			}

			return $newDistancePoints;
		}


		/**
		 * Function to update the database with the status of the replay game played
		 * by the players who have entered into a tie
		 *
		 * @param $newDistancePoints - The collection of the new distance of points played
		 * by the players for the tie breaker game
		 *
		 * @return void
		 */
		function setReplayStatus( $newDistancePoints, $gameID ) {

			$game = Game:: where('id', '=', $gameID)->first();

			foreach ( $newDistancePoints as $point ) {
				if ( $point != null ) {
					TieBreakers:: where( [
						'user_id' => $point->user_id,
						'game_id' => $gameID,
					] )->update( [
						'breakerPlayed'   => 1,
						'breakerDistance' => $point->distance,
					] );
				}
			}
		}


		// Loop to check if all the players have played the game or not
		foreach ( $players as $player ) {
			$newDistancePoints[] = findNewGameDistance( $player->id, $gameID );
		}

		// Update the Distance Points in the pointer table to declare winner
		$sortedNewPoints = TieBreakers:: where( [
			'game_id' => $gameID,
		] )->orderBy( 'breakerDistance', 'asc' )->get();


		// Fill The Distance Points
		foreach ( $sortedNewPoints as $sortedPoint ) {
			if ( $sortedPoint->breakerPlayed == false ) {
				setReplayStatus( $newDistancePoints, $gameID );
			}
		}

		// Update the pointer Database if the Sorted Points are all filled now
		$statusChecker = false;
		$statusChecker = $sortedNewPoints->every( function ( $value, $key ) {
			if ( $value->breakerPlayed == true ) {
				return true;
			}
		});


		if($statusChecker == true){
			$counter = 0;
			$xCords = $game->xCords;
			$yCords = $game->yCords;
			foreach ($sortedNewPoints as $sorted){
				Pointer :: where ([
					'game_id' => $sorted->game_id,
					'user_id' => $sorted->user_id
				])->first()->update([
					'distance' => $counter,
					'yCords' => $yCords,
					'xCords' => $xCords,
				]);
				$counter = $counter + 0.1;
				$xCords = $xCords + 64;
				$yCords = $yCords + 64;
			}
		}
	}
}

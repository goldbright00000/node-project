<?php

if (!function_exists('getCouponValue')) {

    /**
     * Calculates and returns the value of the coupon based on the percentage discount
     *
     * @param $couponName - The name of the discount coupon
     * @return
     */
    function getCouponValue($couponName, $cartContent)
    {
	    $fetchCouponValue = DB::table( 'dicount_coupons' )->where( 'name', '=', $couponName )->first()->value;

	    function totalWithoutDiscount($cartContent){
		    $sumTotal = null;
		    foreach ($cartContent as $item){
		        $eachItemTotal = $item->getPriceSumWithConditions();
		        $sumTotal += $eachItemTotal;
		    }
		    return $sumTotal;
	    }

		$discountValue = totalWithoutDiscount($cartContent) * $fetchCouponValue / 100.00;
	    return $discountValue;
    }
}

<?php

use App\Mail\SuccessfulOrder;
use App\Mail\OrderInvoice;
use Illuminate\Support\Facades\Mail;

if ( ! function_exists( 'postCheckoutActions' ) ) {

	/**
	 * description
	 *
	 * @param
	 *
	 * @return
	 */
	function postCheckoutActions( $request, $invoice ) {
		// Remove Extra Variables from the session
		session()->forget( 'creditAdjusted' );
		session()->forget( 'dontDiscount' );
		session()->forget( 'data' );
		session()->save();

		// Destroy Cart
		\Cart::destroy();		

		// Send A Confirmation Email to the user
		Mail::to( $request->user() )->send( new SuccessfulOrder( $invoice->id ) );
		Mail::to( $request->user() )->send( new OrderInvoice( $invoice ) );
	}
}

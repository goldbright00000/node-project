<?php

use App\Pointer;
use App\Game;
use Carbon\Carbon;

if (!function_exists('pointersToDb')) {

    /**
     * Function to insert pointers to the database
     *
     * @param
     * @return
     */
    function pointersToDb($request) {
		
    	$pointer = new Pointer;
	    $distance = CalculateDistance($request);
	    // Get The Current timestamp
	    $currentTimeStamp = Carbon::now();

    	$pointer->game_id = FindCurrentGame()->id;
    	$pointer->xCords = (($request->xCords)* (FindCurrentGame()->imageMultiplier));
    	$pointer->yCords = (($request->yCords)* (FindCurrentGame()->imageMultiplier));
    	$pointer->distance = $distance;
    	$pointer->sessionID = session()->get( '_token' );
    	$pointer->productID = $request->productID;
    	$pointer->stampId = $request->stampId;
    	$pointer->stamped = true;
	    $test = $pointer->save();
    }
}

<?php

use App\Referral;
use App\GlobalSettings;
use App\User;
use Carbon\Carbon;

if ( ! function_exists( 'CheckReferral' ) ) {

	/**
	 * Function to check if the new registering user has been referred by some other user
	 *
	 * @param
	 *
	 * @return
	 */
	function CheckReferral( $request ) {
		$referral = Referral::where( 'email', '=', $request->email )->first();
		if ( $referral == null ) {
			// Check Complete No Action needed
		} else {
			// Find the referrer
			$referrer = Referral::find( $referral->id )->user;

			// Set the amount of credits to be given to the user
			$credit = GlobalSettings::first()->creditNewReferal;

			// Get the current credits in users account
			$currentValue = $referrer->initialDiscount;

			// Total Number of credits to be updated
			$updatedCredits = $currentValue + $credit;

			// Update the number of credits in Referrer's Account
			User::where( 'id', '=', $referrer->id )->update( [
				'initialDiscount' => $updatedCredits,
			] );

			// Change Referred User Status
			Referral::where( 'email', '=', $request->email )->first()->update( [
				'status' => 1,
				'accepted_on' => Carbon::now(),
			] );

		}
	}
}

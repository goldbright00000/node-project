<?php

use App\Pointer;

if (!function_exists('gameStatus')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function gameStatus( $stamp ){
	    $sessionID = session()->get( '_token' );
	    $pointer   = Pointer::where( [
		    [ 'sessionID', '=', $sessionID ],
		    [ 'stampId', '=', $stamp ],
		    ['invoice_id', '=', null ]
	    ] )->first();
	    return $pointer;
    }
}

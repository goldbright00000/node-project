<?php

if (!function_exists('CalculateDistance')) {

    /**
     * description
     * https://www.mathplanet.com/education/algebra-2/conic-sections/distance-between-two-points-and-the-midpoint
     * @param
     * @return
     */
    function CalculateDistance($request) {
		// Find the current game
		$game = FindCurrentGame();

		// Current Games Zoom Multiplier
		$multiplier = $game->imageMultiplier;

		// Coordinates of the winning position
		//$xCords1 = base64_decode($game->xCords);
		//$yCords1 = base64_decode($game->yCords);
		
		$xCords1 = $game->xCords;
		$yCords1 = $game->yCords;
		
		
		$xCords = intval(preg_replace('/[^\d.]/', '',$xCords1));
		$yCords1 = intval(preg_replace('/[^\d.]/', '',$yCords1));

		// Coordinates marked by the user
	    $xCords2 =  intval(preg_replace('/[^\d.]/', '', $request->xCords)) *  intval(preg_replace('/[^\d.]/', '', $multiplier));
	    $yCords2 = intval(preg_replace('/[^\d.]/', '', $request->yCords)) * intval(preg_replace('/[^\d.]/', '', $multiplier));
		
	    $distance =  sqrt( pow( ($xCords2 - $xCords1),2 ) + pow( ($yCords2 - $yCords1),2 ) );

		return $distance;

    }
}

<?php

if (!function_exists('totalTicketsValue')) {

    /**
     * Returns the value of the tickets after the individual volume discounts have been applied to tickets
     *
     * @param
     * @return
     */
    function totalTicketsValue($cartContent)
    {
    	function ticketTotal($cartContent){
			$cart = json_decode(json_encode($cartContent), true);
    	$sumTotal = null;
    		foreach ($cart as $item){
	            $itemTotal = $item['qty']*$item['price'];
	            $sumTotal += $itemTotal;
	        }
    		return $sumTotal;
	    }
	   return ticketTotal($cartContent);
    }
}

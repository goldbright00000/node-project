<?php
/*
 * Product Discount Calculator - Calculates the discounted price based on individual product slabs
 * @function ProductDiscount
 * @param $product - array[] the product array
 * @param $slab - the discount slab that is required.
 * https://code.tutsplus.com/tutorials/how-to-create-a-laravel-helper--cms-28537
 * */
namespace App\Helpers;

use Illuminate\Support\ServiceProvider;

class ProductDiscount extends ServiceProvider {


	public static function getDiscountedPrice($product, $slab){

		$discountSlab1 = $product->discount_slab1;
		$discountSlab2 = $product->discount_slab2;

		$price_slab2 = round($product->price_slab1 - ($product->price_slab1 * $discountSlab1/100),2);
		$price_slab3 = round($product->price_slab1 - ($product->price_slab1 * $discountSlab2/100),2);


		if ($slab == 2) {
			return $price_slab2;
		}
		elseif ($slab == 3) {
			return $price_slab3;
		}
		else {
			return 'Slab Not Defined';
		}
	}
}
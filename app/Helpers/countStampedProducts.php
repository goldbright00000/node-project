<?php


if (!function_exists('countStampedProducts')) {

    /**
     * Checks If all products have been stamped
     *
     * @param
     * @return
     */
    function countStampedProducts() {
    	// Get cart Items Quantity Total
    	$cartQuantity = \Cart::count();

	    // Get cart Items
    	$cart = \Cart::content();

    	// Get Stamped Products
		$stampedProductsCount = stampProducts($cart)->where('stamped','!=', null)->count();

		if($cartQuantity <= $stampedProductsCount){
			return true;
		}
    }
}

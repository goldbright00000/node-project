<?php

if ( ! function_exists( 'preparePaypalData' ) ) {

	/**
	 * Function to prepare the details for PayPal
	 *
	 * @param
	 *
	 * @return
	 */
	function preparePaypalData( $invoice ) {
		/*$discount       = [
			'name'  => 'Discount',
			'price' => '-' . $invoice->discount,
			'qty'   => '1'
		];
		$tax            = [
			'name'  => 'Sales Tax',
			'price' => $invoice->tax,
			'qty'   => '1'
		];
		$creditAdjusted = [
			'name'  => 'Credit Adjusted',
			'price' => '-' . $invoice->creditAdjusted,
			'qty'   => '1'
		];*/

		$data               = [];
		$data['items']      = $invoice->products;
		$data['return_url'] = url( '/paypal/express-checkout-success' );
		$data['cancel_url'] = route('paypal.failure');
		//$data['items'][] = $discount;
		//$data['items'][] = $tax;
		//$data['items'][] = $creditAdjusted;
		$data['total']      = $invoice->total;
		$data['invoice_id']          = $invoice->id;
		$data['invoice_description'] = $invoice->description;
		//dd($data);

		return $data;

	}
}

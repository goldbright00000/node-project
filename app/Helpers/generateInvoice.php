<?php

use App\Invoice;

if ( ! function_exists( 'generateInvoice' ) ) {

	/**
	 * Function to generate the invoice and add it to the database
	 *
	 * @param
	 *
	 * @return
	 */
	function generateInvoice( $user, $cart ) {

		/**
		 * Starting to prepare data for PayPal
		 */

		$data = [];

		foreach ( $cart as $item ) {
			$products        = [
				'name'  => $item->name,
				'price' => $item->price(),
				'qty'   => $item->qty
			];
			$data['items'][] = $products;
		}

		/*
		 * Starting to prepare data to generate an invoice
		 */

		// Fetch the discount from the Cart
		$discount = 0;

		// Set the Credit Adjusted Value if the same has been used in the Cart
		$creditAdjusted = 0;
		if ( session()->has( 'creditAdjusted' ) ) {
			$creditAdjusted = session()->get( 'creditAdjusted' );
		}
		
		// Update the invoice Database
		$invoice                 = new Invoice;
		$invoice->title          = 'Preparing';
		$invoice->description    = 'Preparing';
		$invoice->user_id        = $user->id;
		$invoice->products       = $data['items'];
		$invoice->productsTotal  = totalTicketsValue( \Cart::content() );
		$invoice->discount       = round( $discount, 2 );
		$invoice->total          = \Cart::subtotal();
		$invoice->tax            = 0;
		$invoice->creditAdjusted = $creditAdjusted;
		$invoice->save();

		// Get the ID From the previous insertion
		$newInvoice = $invoice->id;

		// Update the Invoice table with Correct Order ID and Description
		Invoice::where( 'id', '=', $newInvoice )->update( [
			'title'       => 'Order ID:' . $newInvoice,
			'description' => 'Invoice for the Order ID: ' . $newInvoice,
		] );

		$invoice = Invoice::where( 'id', '=', $newInvoice )->first();

		return $invoice;
	}
}

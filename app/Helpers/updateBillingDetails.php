<?php

use App\User;

if (!function_exists('updateBillingDetails')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function updateBillingDetails($request, $user) {
    	// Get the ID of the user
    	$userID = $user->id;

    	// Update the address of the user
		User:: where('id', '=', $userID)
			->update([
				'address1' => $request->address1,
				'address2' => $request->address2,
				'city' => $request->city,
				'zip' => $request->zip,
				'state' => $request->state,
				'country' => $request->country,
			]);
    }
}

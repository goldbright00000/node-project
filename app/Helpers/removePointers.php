<?php

use App\Pointer;


if (!function_exists('removePointers')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function removePointers() {
	    $sessionToken = session()->get( '_token' );
	    $currentGame = FindCurrentGame()->id;

		   Pointer:: where([
		    'game_id' => $currentGame,
		   'sessionID' => $sessionToken,
		   ])->delete();

    }
}

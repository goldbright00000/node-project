<?php
use Illuminate\Support\Facades\Auth;
use App\GlobalSettings;

if (!function_exists('CreditAdjuster')) {

    /**
     * Checks the user
     *
     * @param
     * @return
     */
    function CreditAdjuster($user){

	    $dicountBalance = $user->initialDiscount;
	    $creditAdjusted = null;

	    $cartGrandTotal = \Cart::subtotal();

	    if ( $dicountBalance > 0 ) {
		    if ( $cartGrandTotal > $dicountBalance ) {
			    $deductCredit   = new CartCondition ( [
				    'name'   => 'Inital Credit',
				    'type'   => 'initial',
				    'target' => 'total',
				    'value'  => '-' . $dicountBalance,
				    'order'  => '10',
			    ] );
			    $creditAdjusted = $dicountBalance;
			    \Cart::condition( $deductCredit );
		    } else {
			    $applyNow = $cartGrandTotal;
			    $balance  = $dicountBalance - $cartGrandTotal;
			    
			    // Update Credit adjusted
			    $creditAdjusted = $applyNow;
			    // Update the credit balance
			   // DB::table( 'users' )->where( 'id', '=', $user->id )->update( [ 'initialDiscount' => $balance ] );
			    // Apply Condition on the cart
			    \Cart::content( );
		    }
		    // Saving To Session
			session()->put('creditAdjusted', $creditAdjusted);
		    session()->put('dontDiscount', 'true');
			session()->save();
	    }


	}
}

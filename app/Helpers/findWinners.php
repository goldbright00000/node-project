<?php

use App\Pointer;
use App\Game;
use App\Invoice;
use Illuminate\Support\Facades\DB;
use App\DeclareWinner;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\IntimateAboutTie;
use App\TieBreakers;

if ( ! function_exists( 'findWinners' ) ) {

	/**
	 * Function to calculate the winners for a given game
	 *
	 * @param
	 *
	 * @return
	 */
	function findWinners( $game ) {

		$pointer = DB:: table( 'invoices' )->join( 'pointers', 'pointers.invoice_id', '=', 'invoices.id' )->select( 'invoices.*', 'pointers.*' )->where( [
			'invoices.payment_status' => 'completed',
			'pointers.game_id'        => $game->id,
		] )->orderBy( 'pointers.distance', 'asc' )->take( $game->runnerUps )->get();

		// Filter Out Only Distance Points
		$distance = [];
		foreach ( $pointer as $point ) {
			$distance[] = number_format( ( $point->distance ), 2 );
		}
		// Create an array with the count for duplicate values
		$duplicatesCombine = array_count_values( $distance );

		// Create an array of only the duplicate values
		$duplicateValues = [];

		foreach ( $duplicatesCombine as $key => $value ) {
			if ( $value > 1 ) {
				$duplicateValues[] = floatval( $key );
			}
		}

		// Collection of position of duplicates
		$positions = collect( [] );

		// Find the position of duplicate values in the collection and add them to $positions Collection
		$index = - 1;
		foreach ( $pointer as $point ) {
			$index ++;
			foreach ( $duplicateValues as $key => $value ) {
				if ( $point->distance == $value ) {
					$positions[] = $index;
				}
			}
		}

		function findPlayers( $duplicateValues, $pointer ) {

			$playerID = collect( [] );
			$players  = collect( [] );

			foreach ( $pointer as $point ) {
				foreach ( $duplicateValues as $key => $value ) {
					if ( $point->distance == $value ) {
						$playerID [] = $point->user_id;
					}
				}
			}

			// Filter Out Unique values
			$playersUnique = $playerID->unique();

			foreach ( $playersUnique as $key => $value ) {
				$players [] = User:: where( 'id', '=', $value )->first();
			}

			// Return Players
			return $players;
		}


		// Start The Process Of Intimating the users under
		if ( $positions->contains( 0 ) ) {

			$players = findPlayers( $duplicateValues, $pointer );

			// Add Credits To Users which have a tie And Set Pointers
			foreach ( $players as $player ) {

				// Check if Tie Breaker Function Has An Entry
				$currentGameID    = $game->id;
				$playerPlaying    = $player->id;
				$fetchTieBreakers = TieBreakers:: where( [
					'user_id' => $playerPlaying,
					'game_id' => $currentGameID
				] )->first();


				if ( $fetchTieBreakers == null ) {

					// Find Previous Pointers And Save in database somewhere Preferrable Pointers Table
					$existingPointers = Pointer:: where( [
						'game_id' => FindCurrentGame()->id,
						'user_id' => $player->id,
					] )->get()->count();


					// Create a new TieBreaker Row
					$tieBreaker             = new TieBreakers;
					$tieBreaker->game_id    = $currentGameID;
					$tieBreaker->pointCount = $existingPointers;
					$tieBreaker->user_id    = $playerPlaying;
					$tieBreaker->save();

					// Find The User and Credit His Account
					User:: where( 'id', '=', $player->id )->update( [
						'initialDiscount' => ( $player->initialDiscount + 1 ),
					] );

					// Send Email Notification To Each User
					Mail::to( $player->email )->send( new IntimateAboutTie( $game ) );
				}
			}


			// See the New Distance Points that have been set in the new game
			tieBreaker( $game, $players );


		} else {
			$players = null;
		}

		$finalInformation = collect( [
			'pointers' => $pointer,
			'distance' => $distance,
			'players'  => $players,
		] );

		return $finalInformation;
	}
}

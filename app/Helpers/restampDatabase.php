<?php
use App\Pointer;


if (!function_exists('restampDatabase')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function restampDatabase($request, $cart) {		
		$shopping_cart=\Cart::content();
		$rowId = $request->productId;		
	    $sessionID = session()->get( '_token' );
		$quantityInCart = null;
		$shopping_cart->search(function ($cartItem, $rowId) {
			$quantityInCart =$cartItem->qty;
		});
		

	    // Find The existing Stamps in the database
    	$existingStamps = Pointer::where([
		    'sessionID' => $sessionID,
		    'productID'   => $request->productId,
		    'invoice_id' => null,
	    ])->get();

    	// Key value of the pointer that needs to be removed from the database
	    $keyValue = $existingStamps->where('stampId',$request->stampId)->keys()->first();

	    // Get the total Count of the pointers in the database
	    $existingCount = $existingStamps->count();

	    // Remove the corresponding pointer from the database
	    if($existingCount <= $keyValue ){

	    	// Remove the pointer from the database
		    Pointer::where([
			    'sessionID' => $sessionID,
			    'stampId'   => $request->stampId,
			    'invoice_id' => null,
		    ])->delete();

	    }
	    elseif ($existingCount > $keyValue){
		    $pointers =  Pointer::where([
			    'sessionID' => $sessionID,
			    'productID'   => $request->productId,
			    'invoice_id' => null,
		    ])->get();


		    // Remove the pointer first from the database
	    	Pointer::where([
			    'sessionID' => $sessionID,
			    'stampId'   => $request->stampId,
			    'invoice_id' => null,
		    ])->delete();

	    	// Get the pointers again from the database
		    $newCollection = Pointer::where([
			    'sessionID' => $sessionID,
			    'productID'   => $request->productId,
			    'invoice_id' => null,
		    ])->get();

		    // Count the number of elements in new collection
			$newCount = ($newCollection->count()) - 1;

			// Loop through all entries and update the stamps
			for ($x=0; $x <= $newCount; $x++) {

				// Get the element that needs to be updated
				$currentElement = $newCollection[$x];

				// Update The Element
				Pointer::where([
					'stampId' => $currentElement->stampId,
					'sessionID' => $currentElement->sessionID,
					'productID'   => $currentElement->productID,
					'invoice_id' => null,
				])->update([
					'stampId' => 'id' . $request->productId . ($x+1),
				]);
			}

	    }
    }
}








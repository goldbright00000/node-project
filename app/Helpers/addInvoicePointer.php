<?php

use App\Pointer;
use Illuminate\Support\Facades\Auth;

if ( ! function_exists( 'addInvoicePointer' ) ) {

	/**
	 * description
	 *
	 * @param
	 *
	 * @return
	 */
	function addInvoicePointer( $invoice ) {
		$user      = Auth::user()->id;
		$sessionID = session()->get( '_token' );

		// Get The invoice ID
		$invoiceID = $invoice->id;

		Pointer::where( 'sessionID', '=', $sessionID )->update( [
				'invoice_id' => $invoiceID,
				'user_id'    => $user,
			] );
	}
}

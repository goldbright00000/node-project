<?php
use App\User;

if (!function_exists('compareUserDetails')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function compareUserDetails($user, $request) {

		$userID = $user->id;
		$dbUser = User::where('id', '=', $userID)->first();

		if($dbUser->address1 != $request->address1 ){
			User::where('id', '=', $userID)->update([
				'address1' => $request->address1,
				'address2' => $request->address2,
				'city' => $request->city,
				'state' => $request->state,
				'zip' => $request->zip,
				'countryCode' => 'US',
				'country' => 'United States Of America'
			]);
		}


    }
}

<?php

use App\Pointer;
use App\GlobalSettings;

if ( ! function_exists( 'CheckMaximumTickets' ) ) {

	/**
	 * Function to keep a check on the maximum number of tickets that can be played by a user for one game.
	 *
	 * @param
	 *
	 * @return
	 */
	function CheckMaximumTickets( $user, $request, $game ) {
		$ticketPlayed = null;

		// Function to check the number of tickets in the cart
		// For the users who are not logged in yet
		function ticketsNotLogedin() {
			$ticketsPlayed = \Cart::count();

			return $ticketsPlayed;
		}

		// Function to check the number of tickets that
		// have been played for the game by the logged in user
		function ticketsLogedin( $user, $game ) {
			$pointers     = Pointer::where( [
				'user_id' => $user->id,
				'game_id' => $game->id
			] )->get()->count();
			$cartCount    = \Cart::count();
			$pointerCount = $pointers + $cartCount;

			return $pointerCount;
		}

		// Function to calculate the remainging Tickets that can be purchased
		function checkRemainingTickets( $alreadyPlayed, $request ) {
			$totalPlayable = GlobalSettings::first()->maxTickets;
			$newRequested  = $request;
			$newtotal      = $newRequested + $alreadyPlayed;
			$extraPlayed   = $newtotal - $totalPlayable;

			if ( $totalPlayable >= $newtotal ) {
				return null;
			} else {
				$ticketStatus = collect( [
					'extraPlayed'   => $extraPlayed,
					'newtotal'      => $newtotal,
					'totalPlayable' => $totalPlayable,
					'alreadyPlayed' => $alreadyPlayed,
					'newRequested'  => $newRequested,
				] );
				session()->put( [
					'ticketsExceeded' => true,
					'ticketStatus'    => $ticketStatus
				] );
				session()->save();

				return false;
			}
		}

		// Conditionally Fire Functions Based on Whether the user is logged in or not
		if ( $user == null ) {
			$alreadyPlayed = ticketsNotLogedin();
			$final         = checkRemainingTickets( $alreadyPlayed, $request );

			return $final;
		} else {
			$alreadyPlayed = ticketsLogedin( $user, $game );
			$final         = checkRemainingTickets( $alreadyPlayed, $request );

			return $final;
		}
	}
}

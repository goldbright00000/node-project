<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclareWinner extends Model
{

	/**
	 * Function to declare a one to one relationship with Game Model
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function game()
	{
		return $this->belongsTo('App\Game');
	}
}

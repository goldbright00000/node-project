<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyWinners extends Mailable {
	use Queueable, SerializesModels;


	public $game;
	public $declareWinners;
	public $player;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct( $game, $declareWinners, $player ) {
		$this->game = $game;
		$this->declareWinners = $declareWinners;
		$this->player = $player;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		return $this->subject( 'WinYourDestiny®: Winners Have Been Declared For A Game You Played' )->markdown( 'emails.notifyWinners' );
	}
}

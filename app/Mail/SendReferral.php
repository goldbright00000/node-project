<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReferral extends Mailable
{
    use Queueable, SerializesModels;

    public $sendReferral;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sendReferral)
    {
        $this->sendReferral = $sendReferral;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('WinYourDestiny®: Yay! You Have An Invite')->markdown('emails.sendReferral');
    }
}

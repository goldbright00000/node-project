<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuccessfulOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $successfulOrder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($successfulOrder)
    {
        $this->successfulOrder = $successfulOrder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('WinYourDestiny: Thank Your For Playing')->markdown('emails.successfulOrder');
    }
}

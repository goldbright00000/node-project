<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class IntimateAboutTie extends Mailable
{
    use Queueable, SerializesModels;

	public $game;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($game)
    {
	    $this->game = $game;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
	    return $this->subject('WinYourDestiny®: You Have A Tie, Play The Tie Breaker')
	                ->markdown('emails.intimateAboutTie');
    }
}

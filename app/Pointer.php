<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pointer extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'distance',
	];



	/**
	 * Function to define relationship with the User Model
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	/**
	 * Function to define relationship with the Game Model
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function game()
	{
		return $this->belongsTo('App\Game');
	}

	/**
	 * Function to define relationship with the Invoice Model
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function invoice()
	{
		return $this->belongsTo('App\Invoice');
	}

	public function users() {
		return $this->belongsToMany('App\User', 'winners');
	}


	public function games() {
		return $this->belongsToMany('App\Game', 'winners');
	}


}

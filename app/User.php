<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail {
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
		'newsConfirmation',
		'lastName',
		'countryCode',
		'mobile',
		'gender',
		'ageConfirmation',
		'provider',
		'provider_id',
		'initialDiscount',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * Function to associate Users with Invoices
	 */
	public function invoices() {
		return $this->hasMany( 'App\Invoice' );
	}

	/**
	 * Function to associate Users with Invoices
	 */
	public function referrals() {
		return $this->hasMany( 'App\Referral' );
	}

	/**
	 * Function to define one to many replationship with pointer model
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */

	public function pointers() {
		return $this->hasMany( 'App\Pointer' );
	}

	public function games() {
		return $this->belongsToMany( 'App\Game', 'winners' );
	}

	public function pointer() {
		return $this->belongsToMany('App\Pointer', 'winners');
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
	/**
	 * Function to define one to many replationship with pointer model
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function pointers()
	{
		return $this->hasMany('App\Pointer');
	}

	/**
	 * Function to define one to one replationship with Declare Winner
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function declareWinner()
	{
		return $this->hasOne('App\DeclareWinner');
	}

	/**
	 * Function to define relationship with the TieBreakers Model
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tiebreakers()
	{
		return $this->belongsTo('App\TieBreakers');
	}

	public function users() {
		return $this->belongsToMany('App\User', 'winners');
	}

	public function pointer() {
		return $this->belongsToMany('App\Pointer', 'winners');
	}
}

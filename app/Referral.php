<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model {
	/**
	 * Function to associate Referrals with User
	 */
	public function user(){
		return $this->belongsTo('App\User');
	}

	protected $fillable = [
		'name',
		'email',
		'status',
		'user_id',
		'accepted_on',
	];

	/**
	 * Attribute Casting To Fields
	 */
	protected $casts = [
		'accepted_on' => 'datetime',
	];
}

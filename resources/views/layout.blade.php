<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/gif" sizes="16x16">
    <title>Win Your Destiny</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/md-bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('css/md-bootstrap/css/mdb.min.css') }}" rel="stylesheet">
    <!-- Custom styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- Development Custom styles -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Magnify CSS  -->
    <link href="{{ asset('js/Magnifier.js-master/magnifier.css') }}" rel="stylesheet">-->
    <!-- IntroJS CSS File -->
    <link href="{{ asset('css/introjs.min.css') }}" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('css/md-bootstrap/js/jquery-3.3.1.min.js') }}"></script>

</head>

<body>

@include( 'sections.header' )

    <!--Main layout-->
    <main>

        @yield( 'content' )

    </main>
    <!-- /Main layout-->


@include( 'sections.footer' )




<!-- SCRIPTS -->
<!-- CSS -->
<!-- Stylesheet for Ekko Lightbox  -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/ekko-lightbox.css') }}" />
<!-- Stylesheet for Off Canvas Menu  -->
<link href="{{ asset('css/jquery.mmenu.all.css') }}" rel="stylesheet" />



<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{ asset('css/md-bootstrap/js/popper.min.js') }}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ asset('css/md-bootstrap/js/bootstrap.min.js') }}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ asset('css/md-bootstrap/js/mdb.min.js') }}"></script>

<!-- Ekko Lightbox For Bootstrap-->
<script type="text/javascript" src="{{ asset('js/ekko-lightbox.js') }}"></script>
<!-- Off Canvas Menu Scripts -->
<script src="{{ asset('js/jquery.mmenu.all.js') }}"></script>

<!-- DataTables CSS -->
<link href="{{ asset('css/md-bootstrap/css/addons/datatables.css') }}" rel="stylesheet">
<!-- DataTables Select CSS -->
<link href="{{ asset('css/md-bootstrap/css/addons/datatables-select.css') }}" rel="stylesheet">

<!-- Including the light slider CSS file -->
<link href="{{ asset('js/lightslider-master/dist/css/lightslider.css') }}" rel="stylesheet">


<!-- DataTables Js -->
<script src="{{ asset('css/md-bootstrap/js/addons/datatables.js') }}"></script>
<!-- DataTables Select Js -->
<script src="{{ asset('css/md-bootstrap/js/addons/datatables-select.js') }}"></script>

<!-- Include Raphael JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>
<!-- Intro JS File Include -->
<script src="{{ asset('js/intro.min.js') }}"></script>



<!-- Custom JS File Include -->
<script src="{{ asset('js/custom.js') }}"></script>




<!-- Page Specific Inline Js -->

<script>
	//Ekko Lightbox Trigger
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox();
	});

	// SideNav Button Initialization
	$(".button-collapse").sideNav({
		edge: 'right', // Choose the horizontal origin
		closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
	});

	// SideNav Scrollbar Initialization
	var sideNavScrollbar = document.querySelector('.custom-scrollbar');
	Ps.initialize(sideNavScrollbar);



	// Increment Form JS
	jQuery(document).ready(function(){
		// This button will increment the value
		$('.qtyplus').click(function(e){
			// Stop acting like a button
			e.preventDefault();
			// Get the field name
			fieldName = $(this).attr('field');
			// Get its current value
			var currentVal = parseInt($('input[name='+fieldName+']').val());
			// If is not undefined
			if (!isNaN(currentVal)) {
				// Increment
				$('input[name='+fieldName+']').val(currentVal + 1);
			} else {
				// Otherwise put a 0 there
				$('input[name='+fieldName+']').val(0);
			}
		});
		// This button will decrement the value till 0
		$(".qtyminus").click(function(e) {
			// Stop acting like a button
			e.preventDefault();
			// Get the field name
			fieldName = $(this).attr('field');
			// Get its current value
			var currentVal = parseInt($('input[name='+fieldName+']').val());
			// If it isn't undefined or its greater than 0
			if (!isNaN(currentVal) && currentVal > 0) {
				// Decrement one
				$('input[name='+fieldName+']').val(currentVal - 1);
			} else {
				// Otherwise put a 0 there
				$('input[name='+fieldName+']').val(0);
			}
		});
		
		@if(isset($cartHeaderAdd))
			if('{{$cartHeaderAdd}}' == 'headerAddCard')
			{
			  $("#wd-slideout").trigger("click");
			}
		@endif
		
		
		@if($errors->any() && !empty(\Cart::content()))

		if('{{$errors->first()}}' == 'cartRemove')
		{
          $("#wd-slideout").trigger("click");
		}
		@endif
	});


</script>

</body>

</html>

@component('mail::message')
# WinYourDestiny®: Successfully Placed Order No: {{ $successfulOrder }}

Thank you for playing at WinYourDestiny, you have successfully placed your your order. You will be notified about the winners email.

@component('mail::button', ['url' => 'https://winyourdestiny.com'])
Play More!
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

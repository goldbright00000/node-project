@component('mail::message')
# You Have A Tie With One Or More Players For Game Played During: {{ dateToString($game->startDate) . ' and ' . dateToString($game->endDate) }}

We have added **1 Credit** to your account so that you can play the tie breaker. Please visit the website to select *ANY PRODUCT* for value of $1 and play the most recent game that is live on  the website.

**PLEASE NOTE: You need to play the game to before {{ dateToString( $game-> tieDate) }}. This is just a tie breaker and you are playing forn the same stake as you selected during the game and the selection of the current product will *NOT IMPACT* the stake you played for previously.**

[Click Here To Select Any Product](https://winyourdestiny.com/products)

Thanks,<br>
{{ config('app.name') }}
@endcomponent

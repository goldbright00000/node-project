@component('mail::message')
# Hey {{ ' '. $player->name }}!

We are excited to announce that Winners Have been decalred for Game played between: {{ dateToString($game->startDate) . ' and ' . dateToString($game->endDate) }}

Please follow the link below to check the winners
[Click Here To See Winners](https://winyourdestiny.com/winners/{{ $declareWinners->slug }})

Thanks,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
# Invoice For Your Order No: {{ $orderInvoice->id }}

Thank you for playing with us.

## Invoice
@component('mail::table')
    | Particulars        |  Quantity         | Amount            |
    | ------------------ | :-------------:   |:-------------:    |
  @foreach($orderInvoice->products as $product)
    | {{$product['name'] }}  | {{$product['qty'] }}  | {{$product['price']}} |
  @endforeach
    |         |  (-) Discount              | {{ $orderInvoice->discount}}        |
    |         |  **Sub Total**               | {{$orderInvoice->productsTotal }}  |
    |         |  (+) Tax                   | {{$orderInvoice->tax}}             |
    |         |  (-) Credits Adjusted      | {{$orderInvoice->creditAdjusted}}  |
    |         |  **TOTAL**                   | {{$orderInvoice->total }}          |
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

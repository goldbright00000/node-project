@component('mail::message')
# Welcome To WinYourDestiny

Thank You for registering with WinYourDestiny. You will need to confirm your email address before you start playing the game. You will get a confirmation
link in another email. Click that and you are ready to go.

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent

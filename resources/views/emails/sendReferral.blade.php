@component('mail::message')
# WinYourDestiny®: Hey! {{ $sendReferral->get('referrer') }} Has Sent You An Invite

With WinYourDestiny® You get a chance to win what you desire by playing a simple game. It's amazing and we have an assured winner for every GamePlay. You get to choose the product you want to win and guess what we have included some free credits for you to start playing immediately.

This is what {{ $sendReferral->get('referrer') }} has to tell you
>>{{ $sendReferral->get('message') }}

Click on the button below to get registered
@component('mail::button', ['url' => 'https://winyourdestiny.com/register'])
Yes Accept The Invitation!
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

@extends ('layout')

@section('content')

    {{--{{ dd($user->gender) }}--}}

    <!--Container For The page-->
    <div class="container wd-first-container">
        <!--Row For The Personal Details -->
        <div class="row text-center wd-top-padding wd-bottom-padding">

            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">My Details</h2>
            </div>

            @if(session()->has('successPassword'))
                <div class="col-md-8 offset-md-2 success alert-success">
                    <p class="py-3 px-5">{{ session()->get('successPassword') }}</p>
                </div>
                {{ session()->forget('successPassword') }}
            @elseif (session()->has('passwordError'))
                <div class="col-md-8 offset-md-2 success alert-danger">
                    <p class="py-3 px-5">{{ session()->get('passwordError') }}</p>
                </div>
            {{ session()->forget('passwordError') }}
        @endif

        <!-- Column For Person Details -->
            <div class="col-sm-12 wd-my-account-section mt-5">

                <!-- Nested Row For Person Details -->
                <div class="row my-4">

                    <!-- Column For Person Details -->
                    <div class="col-sm-12 col-md-6 text-left">
                        <h4 class="font-weight-bold dark-grey-text">Personal Details</h4>
                        <form method="post" action="{{ route('home.update') }}" class="needs-validation">
                            @csrf
                            <div class="md-form my-5">
                                <label for="firstName">First Name</label>
                                <input value="{{ $user->name }}" type="text" id="firstName" name="firstName"
                                       class="form-control" required>
                            </div>
                            <div class="md-form my-5">
                                <label for="lastName">Last Name</label>
                                <input value="{{ $user->lastName }}" type="text" id="lastName" name="lastName"
                                       class="form-control" required>
                            </div>
                            <div class="md-form my-5">
                                <label for="phoneNumber">Country Code</label>
                                <input type="tel" name="phone" value="{{ $user->countryCode }}"
                                       id="countryCode" class="form-control" required>
                            </div>

                            <div class="md-form my-5">
                                <label for="phoneNumber">Phone Number</label>
                                <input type="tel" name="phone" value="{{ $user->mobile }}"
                                        id="phoneNumber" class="form-control"
                                       required>
{{--                                pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"--}}
                            </div>
                            <span class="my-2 py-0 d-block">Select Your Gender</span>
                            <span class="mb-0 d-block text-left">
                                    <div class="form-check form-check-inline">
                                        <input
                                                type="radio"
                                                class="form-check-input"
                                                id="genderMale"
                                                name="gender"
                                                value="male"
                                                @if($user->gender == 'male')
                                                checked="checked"
                                                class="form-check-input active"
                                                @else
                                                class="form-check-input"
                                        @endif
                                        >
                                        <label class="form-check-label" for="genderMale">Male</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input
                                                type="radio"
                                                class="form-check-input"
                                                id="genderFemale"
                                                name="gender"
                                                value="female"
                                                @if($user->gender == 'female')
                                                checked="checked"
                                                class="form-check-input active"
                                                @else
                                                class="form-check-input"
                                        @endif
                                        >
                                        <label class="form-check-label" for="genderFemale">Female</label>
                                    </div>
                                </span>
                            <button type="submit" class="btn btn-amber mt-5" id="wd-signup-form-submit">Update Profile
                            </button>
                        </form>
                    </div>
                    <!-- /Column For Person Details -->

                    <!-- Column For Profile Details -->
                    <div class="col-md-4 col-sm-12 offset-md-1 text-center wd-mobile-margin-top">
                        <h4 class="font-weight-bold dark-grey-text d-block mb-2">Profile Details</h4>
                        <img src="{{ asset('/') .'/'. $user->profileImage }}"
                             class="d-block mx-auto z-depth-1 rounded-circle wd-profile-max-width mb-2 mt-4"
                             alt="Profile Image">


                        <form class="md-form" method="post" action="{{ route('home.pic') }}"  enctype="multipart/form-data">
                            @csrf
                            <div class="file-field">
                                <div class="d-flex justify-content-center">
                                    <div class="btn btn-grey btn-rounded">
                                        <label
                                            style="color: #FFFFFF;
                                            font-size: 14px;
                                            position: relative;
                                            top:.35rem"
                                           for="picture">Add Photo</label>
                                        <input type="file" id="picture" class="form-control" name="picture" required/>
                                    </div>
                                </div>

                            </div>
                            <p class="text-center wd-very-small-text">For best results please upload a square image
                                min-size 300px x 300px</p>

                            <button type="submit" class="btn btn-amber btn-sm">Update</button>
                        </form>

                        <h4 class="font-weight-bold dark-grey-text d-block mt-5">Order Details</h4>
                        <span class="d-block text-center mt-3">
                                <a href="{{ route('order-history') }}" type="button" class="btn btn-dark btn-block">View Order History</a>
                        </span>
                        <span class="d-block text-center mt-3">
                                <a href="{{ route('credits-history') }}" type="button" class="btn btn-purple btn-block">Credits Dashboard</a>
                        </span>
                    </div>
                    <!-- /Column For Profile Details -->


                </div>
                <!-- /Nested Row For Person Details -->

            </div>
            <!-- /Column For Person Details -->


            <!-- Column For Billing Details -->
            <div class="col-sm-12  mt-5">

                <!-- Nested Row For Billing Details -->
                <div class="row my-4">

                    <!-- Column For Address Details -->
                    <div class="col-sm-12 col-md-6 wd-my-account-section text-left py-5 px-5">
                        <h4 class="font-weight-bold dark-grey-text">Billing Details</h4>
                        <form class="needs-validation" method="post" action="{{route('home.updateAddress')}}"
                              id="addressForm" >
                            @csrf
                            <div class="md-form my-5">
                                <label for="addressLineOne">Address Line 1 *</label>
                                <input
                                        @if($user->address1)
                                        placeholder="{{$user->address1}}"
                                        @else
                                        placeholder="Please update your address"
                                        @endif
                                        type="text"
                                        id="addressLineOne"
                                        name="address1"
                                        title="Please Enter a Valid Address"
                                        class="form-control" required>
                            </div>
                            <div class="md-form my-5">
                                <label for="addressLineTwo">Address Line 2 *</label>
                                <input
                                        @if($user->address2)
                                        placeholder="{{$user->address2}}"
                                        @else
                                        placeholder="Please update your address"
                                        @endif
                                        type="text"
                                        name="address2"
                                        title="Please Enter a Valid Address"
                                        id="addressLineTwo"
                                        class="form-control" required>
                            </div>
                            <div class="md-form my-5">
                                <label for="addressCity">City *</label>
                                <input
                                        @if($user->city)
                                        placeholder="{{$user->city}}"
                                        @else
                                        placeholder="Your City"
                                        @endif
                                        type="text"
                                        id="addressCity"
                                        title="Please Enter a Valid City"
                                        name="city"
                                        class="form-control" required>
                            </div>
                            <div class="md-form my-5">
                                <label for="addressState">State *</label>
                                <input
                                        @if($user->state)
                                        placeholder="{{$user->state}}"
                                        @else
                                        placeholder="Your State"
                                        @endif
                                        type="text"
                                        id="addressState"
                                        title="Please Enter a Valid State"
                                        name="state"
                                        class="form-control" required>
                            </div>
                            <div class="md-form my-5">
                                <label for="addressZipCode">Zip Code | Postal Code *</label>
                                <input
                                        @if($user->zip)
                                        placeholder="{{$user->zip}}"
                                        @else
                                        placeholder="Your Zip Code"
                                        @endif
                                        type="number"
                                        id="addressZipCode"
                                        title="Please Enter a Valid Zip Code"
                                        name="zip"
                                        class="form-control" required>
                            </div>
                            <div class="md-form my-5">
                                <label for="addressCountry">Country *</label>
                                <input
                                        @if($user->country)
                                        placeholder="{{$user->country}}"
                                        @else
                                        placeholder="Country"
                                        @endif
                                        type="text"
                                        id="addressCountry"
                                        name="country"
                                        value="United States Of America"
                                        class="form-control disabled" required>
                            </div>
                            <button type="submit" class="btn btn-amber mt-2" id="addressForm">Save Changes</button>
                        </form>
                    </div>
                    <!-- /Column For Address Details -->

                    <!-- Column For Card Details Disabled FOr Now  -->

                    {{--<div class="col-md-5 offset-md-1  wd-my-account-section col-sm-12  text-left wd-mobile-margin-top py-5 px-5">--}}
                    {{--<h4 class="font-weight-bold dark-grey-text d-block mb-2">PayPal Email Address</h4>--}}

                    {{--<h6 class="mt-5 mb-2">{{$user->email}}</h6>--}}
                    {{--<button type="submit" class="btn btn-amber mt-2" id="cardForm">Change PayPal ID</button>--}}


                    {{--<form class="needs-validation" id="cardForm" novalidate>--}}
                    {{--<div class="md-form my-5">--}}
                    {{--<label for="nameOnCard">Name On Card</label>--}}
                    {{--<input value="John Doe" type="text" id="nameOnCard" class="form-control">--}}
                    {{--</div>--}}
                    {{--<div class="md-form my-5">--}}
                    {{--<label for="cardNumber">Card Number</label>--}}
                    {{--<input value="1234456712346789" type="number" id="cardNumber" class="form-control">--}}
                    {{--</div>--}}
                    {{--<div class="md-form my-5">--}}
                    {{--<label for="cardCvv">CVV | CVC2 </label>--}}
                    {{--<input value="123" type="password" id="cardCvv" class="form-control">--}}
                    {{--</div>--}}
                    {{--<!-- Column For Expiry Month Selection -->--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-6 m-b-3 text-left">--}}

                    {{--<!--Month select-->--}}
                    {{--<select class="mdb-select colorful-select dropdown-warning">--}}
                    {{--<option value="1">January</option>--}}
                    {{--<option value="2">February</option>--}}
                    {{--<option value="3">March</option>--}}
                    {{--<option value="4">April</option>--}}
                    {{--<option value="5">May</option>--}}
                    {{--<option value="6">June</option>--}}
                    {{--<option value="7">July</option>--}}
                    {{--<option value="8">August</option>--}}
                    {{--<option value="9">September</option>--}}
                    {{--<option value="10">October</option>--}}
                    {{--<option value="11">November</option>--}}
                    {{--<option value="12">December</option>--}}
                    {{--</select>--}}
                    {{--<label>Expiry Month</label>--}}
                    {{--<!--/Month select-->--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6 m-b-3 text-left">--}}

                    {{--<!-- Year select-->--}}
                    {{--<select class="mdb-select colorful-select dropdown-warning">--}}
                    {{--<option value="1">2018</option>--}}
                    {{--<option value="2">2019</option>--}}
                    {{--<option value="3">2020</option>--}}
                    {{--<option value="4">2021</option>--}}
                    {{--<option value="5">2022</option>--}}
                    {{--<option value="6">2023</option>--}}
                    {{--</select>--}}
                    {{--<label>Expiry Year</label>--}}
                    {{--<!--/Year select-->--}}
                    {{--</div>--}}
                    {{--<!-- /Column For Expiry Month Selection -->--}}
                    {{--</div>--}}

                    {{--<span class="d-block text-left">--}}
                    {{--<button type="submit" class="btn btn-amber mt-2" value="Save Changes" id="cardForm">Save Changes</button>--}}
                    {{--</span>--}}
                    {{--</form>--}}

                    {{--</div>--}}
                    {{--<!-- /Column For Card Details -->--}}

                    <div class="col-md-5 offset-md-1 col-sm-12 wd-my-account-section py-5 px-5 text-left ">
                        <h4 class="font-weight-bold dark-grey-text d-block mb-2">Change Password</h4>
                        <form class="needs-validation" method="post" action="{{ route('home.updatePassword') }}">
                            @csrf
                            <div class="md-form my-5">
                                <label for="oldPassword"></label>
                                <input type="password"
                                       placeholder="Current Password *"
                                       id="current-password"
                                       name="current-password"
                                       class="form-control {{ $errors->has('oldPassword') ? ' is-invalid' : '' }}"
                                       required>
                                @if ($errors->has('oldPassword'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('oldPassword') }}</strong>
                            </span>
                                @endif

                            </div>


                            <div class="md-form my-5">
                                <label for="password"></label>
                                <input type="password"
                                       id="password"
                                       name="password"
                                       placeholder="New Password *"
                                       title="Minimum of 8 characters including at least 1 number, 1 capital letter, 1 lower case and 1 special character"
                                       pattern="((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!^&*()+-_?><~]).{6,20})"
                                       class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       required>
                                <p class="wd-very-small-text text-left mt-0">Minimum of 8 characters including at least 1 number, 1 capital letter, 1 lower case and 1 special character</p>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                @endif
                            </div>

                            <div class="md-form my-5">
                                <label for="confirm_password"></label>
                                <input type="password"
                                       placeholder="Confirm Password *"
                                       id="confirm_password"
                                       name="password_confirmation"
                                       class="form-control"
                                       title="Minimum of 8 characters including at least 1 number, 1 capital letter, 1 lower case and 1 special character"
                                       pattern="((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!^&*()+-_?><~]).{6,20})"
                                       required>
                                <p class="wd-very-small-text text-left mt-0">Minimum of 8 characters including at least 1 number, 1 capital letter, 1 lower case and 1 special character</p>
                            </div>
                            <span class="d-block text-left">
                                    <button type="submit" class="btn btn-amber mt-2" value="Save Changes"
                                            id="wd-signup-form-submit">Save Changes</button>
                                    </span>
                        </form>

                    </div>


                </div>
                <!-- /Nested Row For Billing Details -->

            </div>
            <!-- /Column For Billing Details -->


        </div>
        <!--/Row For The Personal Details -->


    </div>
    <!--/Container For The page-->

<script>
	var password = document.getElementById("password");
	var confirm_password = document.getElementById("confirm_password");

	function validatePassword(){
		if(password.value != confirm_password.value) {
			confirm_password.setCustomValidity("Passwords Don't Match");
		} else {
			confirm_password.setCustomValidity('');
		}
	}

	password.onchange = validatePassword;
	confirm_password.onkeyup = validatePassword;
</script>
@endsection

@extends ('layout')

@section('content')
    {{--    {{ dd($sections) }}--}}
    <div class="container wd-first-container wd-top-padding">
        <!--Row For The Personal Details -->
        <div class="row text-center wd-top-padding wd-bottom-padding">

            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Frequently Asked Questions</h2>
            </div>

            <div class="col-md-10 offset-md-1 col-sm-12 mt-3 mb-3">
                <p>You'll find the answers to some of our most Frequently Asked Questions below. If you can’t find the
                    answer you're looking for, please email us at info@winyourdestiny.com and we'll be delighted to help
                    you.</p>
            </div>

            @foreach ($sections as $key => $value)
                <div class="col-md-10 text-left offset-md-1 mt-5">
                    <h4>{{ $value }}</h4>
                    <!--Accordion wrapper-->
                    @foreach($faqs as $faq )
                        @if($value == $faq->section)
                            <div class="accordion md-accordion" id="accordion{{$faq->id}}" role="tablist"
                                 aria-multiselectable="true">
                                <!-- Accordion card -->
                                <div class="card">

                                    <!-- Card header -->
                                    <div class="card-header" role="tab" id="heading{{$faq->id}}">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion{{$faq->id}}"
                                           href="#collapse{{$faq->id}}"
                                           aria-expanded="false" aria-controls="collapse{{$faq->id}}">

                                            <h5 class="mb-0 dark-grey-text">
                                                {{ $faq->question }}
                                                <i class="fas fa fa-angle-down rotate-icon"></i>
                                            </h5>

                                        </a>
                                    </div>

                                    <!-- Card body -->
                                    <div id="collapse{{$faq->id}}" class="collapse" role="tabpanel"
                                         aria-labelledby="heading{{$faq->id}}"
                                         data-parent="#accordion{{$faq->id}}">

                                        <div class="card-body">
                                            {{ $faq->answer }}
                                        </div>
                                    </div>

                                </div>
                                <!-- Accordion card -->


                            </div>
                            <!-- Accordion wrapper -->
                        @endif
                    @endforeach

                </div>
            @endforeach


        </div>
        <!-- /Row For The Personal Details -->
    </div>

@endsection
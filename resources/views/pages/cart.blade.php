@extends ('layout')

@section('content')
    @php
        $cart = json_decode(json_encode(\Cart::content()), true);
    @endphp
    <div class="container wd-first-container">
        <!--Row For The Heading Section -->
        <div class="row text-center wd-top-padding">
            <div class="col-md-6 offset-md-3 mb-4 text-center">



                @if (session('success_message'))
                    <div class="alert alert-success">
                        {{ session('success_message') }}
                    </div>
                    {{ session()->forget('success_message') }}
                @elseif (session('removed_message'))
                    <div class="alert alert-warning">
                        {{ session('removed_message') }}
                    </div>
                    {{ session()->forget('removed_message') }}
                @endif
            </div>
            <!-- Column For heading -->

            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Shopping Cart</h2>
                <p>Psst! We would want you to play more. If you think you have nailed it already then click on
                    Checkout</p>
            </div>
            @if (empty($cart))
                <div class="col-md-8 offset-md-2 text-center mt-4">
                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">You Are Just Getting Started</h4>
                        <p>Currently Your Shopping is empty. Add a few tickets to your cart so that we start playing</p>
                    </div>
                </div>
        @endif

        <!-- Column For Order Summary -->
            <div class="col-md-12 col-sm-12 text-left mt-4 mb-2">

                <!-- Inner Column Declaration For Order Summary Section -->
                <div class="col-md-12 wd-my-account-section py-4">
                    <h4 class="font-weight-bold text-center dark-grey-text">Cart Summary</h4>

                    <!-- Editable table -->
                    <div class="card mt-4">

                        <div class="card-body">
                            <div id="table" class="table-editable">
                                <table class="table table-responsive-md table-striped text-center">
                                    <tr>
                                        <th class="text-center">Image</th>
                                        <th class="text-center">Product Name</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Amount</th>
                                        <th class="text-center">Total</th>
                                        <th class="text-center">Remove</th>
                                    </tr>

                                    <!-- Repeatable Table Row-->
                                    @foreach ($cart as $item)
                                        <tr>
                                            <td class="py-4 align-middle" contenteditable="false">
                                                <a href="{{ route('product-detail', $item['options']['attributes']['product_attr']['slug'] ) }}" class="">
                                                <img src="{{ $item['options']['attributes']['product_attr']['image'] }}"
                                                     class="mx-auto"
                                                     style="width:85px" alt="placeholder"/>
                                                </a>
                                            </td>
                                            <td class="py-4 align-middle" contenteditable="false">
                                                <a href="{{ route('product-detail', $item['options']['attributes']['product_attr']['slug'] ) }}" class="">
                                                    {{ $item['name'] }}
                                                </a>
                                                {{--<span class="d-block">X-2014 Y-418</span></td>--}}
                                            <td class="py-4 align-middle" contenteditable="false">{{ $item['qty'] }}</td>
                                            <td class="py-4 align-middle" contenteditable="false">$ {{ $item['price'] * $item['qty'] }}</td>
                                            <td class="py-4 align-middle" contenteditable="false">$ {{ $item['price'] * $item['qty'] * $item['qty'] }}</td>
                                            <td class="align-middle">
                                                <span class=" align-middle d-block">
                                                    <form action="{{ route( 'cart.destroy', $item['rowId'] ) }}" method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field( 'DELETE' )}}
                                                        <button type="submit" class="btn-floating btn-amber btn-sm my-0 py-0">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                        <!-- 'table-remove' class can be used with AJAX to remove Row -->
                                                    </form>
                                                </span>
                                            </td>
                                        </tr>
                                        <!-- /Repeatable Table Row-->
                                @endforeach

                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Editable table -->


                </div>
                <!-- /Inner Column Declaration For Order Summary Section -->

            </div>
            <!-- /Column For Order Summary -->
            <div class="col-md-6 text-left mb-5">
                <a href="{{ route('product-listing') }}" class="btn btn-grey btn-primary">Continue Shopping</a>
            </div>

            <div class="col-md-6 text-right mb-5">
                @if (Cart::count() > 0)
                    <form action="{{ route('checkout.index') }}">
                        @if (Request::has('previous'))
                            <input type="hidden" name="previous" value="{{ request()->previous }}">
                        @else
                            <input type="hidden" name="previous" value="{{ route('checkout.index') }}">
                        @endif
                            <button type="submit" class="btn btn-amber btn-primary">Proceed To Checkout</button>
                    </form>
                @endif
            </div>

        </div>
    </div>

@endsection
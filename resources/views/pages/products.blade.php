@extends('layout')

@section('content')

    @php
    if(session()->has('ticketStatus')){
    $ticketStatus = session()->get('ticketStatus');
    }
    @endphp

	<div class="d-none">
        {{ $cart = \Cart::content() }}
        {{ $headerStampedProducts = stampProducts($cart)  }}
    </div>
    <!--Container For The Prizes-->
    <div class="container wd-first-container">
        <!-- Row For The Prizes-->

		@if(\Cart::count()!==0)
			<div class="row margint-2">
				<div class="col-sm-3">
				</div>
				<div class="col-sm-9">
						<div class="alert alert-info alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Tickets</strong> have been added to cart.
							<div class="setDisplayInlineBlock">
								<a class="btn popupbtn btn-amber py-1 waves-effect waves-light" href="{{ route('pointer.index') }}">PROCEED TO PLAY</a>
							</div>
						</div>	
				</div>
			</div>
						
		@endif
        <div class="row wd-bottom-padding ">

            @if (session()->has('ticketsExceeded'))
                <div class="col-10 offset-1 mt-5">
                    <div class="alert-danger py-3 px-5 text-center">
                    We only allow Maximum Of {{ $ticketStatus['totalPlayable'] }} tickets to be played, per user, per game. You have already previously played/added {{ $ticketStatus['alreadyPlayed'] }} tickets and are requesting additional {{$ticketStatus['newRequested'] }} tickets which exceed maximum number by {{ $ticketStatus['extraPlayed'] }}. Please try adding tickets less than  {{ ($ticketStatus['totalPlayable']  -  $ticketStatus['alreadyPlayed']) }} to move ahead.
                    </div>
                    @php
                    session()->forget(['ticketStatus','ticketsExceeded']);
                    @endphp
                </div>
            @endif


            <div class="col-sm-12 py-5 d-none d-sm-block"></div>
            <!-- Column For Filters -->
            <div class="col-sm-3 wd-product-filters-section mb-5 ">
                <h5 class="font-weight-bold">Filters</h5>
                <!-- Search Bar -->
                <form action="{{ route('product-listing') }}" method="get">
                    <h6 class="font-weight-bold dark-grey-text mt-5">Ticket Price US$</h6>

                    <div class="d-flex justify-content-center my-3">
                        <span class="font-weight-bold amber-text mr-2">
                            @if(session('minimumPrice'))
                                {{'$' . session('minimumPrice') }}
                            @else
                                {{'$' . $products->min('price_slab1') }}
                            @endif

                        </span>
                        <div class="range-field w-100">
                            <input
                                    class="border-0"
                                    type="range"
                                    name="price"
                                    @if(session('minimumPrice'))
                                    min="{{ session('minimumPrice') }}"
                                    @else
                                    min="{{ $products->min('price_slab1') }}"
                                    @endif

                                    @if(session('maximumPrice'))
                                    max="{{ session('maximumPrice') }}"
                                    @else
                                    max="{{ $products->max('price_slab1') }}"
                                    @endif

                                    @if(session('lastPrice'))
                                    value="{{ session('lastPrice') }}"
                                    @else
                                    value="{{ $products->max('price_slab1') }}"
                                    @endif

                            />
                        </div>
                        <span class="font-weight-bold amber-text ml-2">
                            @if(session('maximumPrice'))
                                {{ '$' . session('maximumPrice') }}
                            @else
                                {{ '$' . $products->max('price_slab1') }}
                            @endif

                        </span>
                    </div>

                    <!-- Material checked -->
                    <h6 class="font-weight-bold dark-grey-text mt-3">Categories</h6>

                    @if(session()->has('requestedCategories'))
                        @foreach( $categories as $category)
                            <div class="form-check my-3">
                                <input
                                        type="checkbox"
                                        @if(session()->get('requestedCategories')->has($category->id))
                                        checked="checked"
                                        @endif
                                        class="form-check-input "
                                        name="{{ $category->id }}"
                                        id="{{ $category->slug }}">
                                <label
                                        @if(session()->get('requestedCategories')->has($category->id))
                                        class="form-check-label active"
                                        @else
                                        class="form-check-label active"
                                        @endif
                                        for="{{ $category->slug }}">
                                    {{ $category->name }}
                                </label>
                            </div>
                        @endforeach
                    @else
                        @foreach( $categories as $category)
                            <div class="form-check my-3">
                                <input
                                        type="checkbox"
                                        class="form-check-input"
                                        name="{{ $category->id }}"
                                        id="{{ $category->slug }}">
                                <label
                                        class="form-check-label"
                                        for="{{ $category->slug }}">
                                    {{ $category->name }}
                                </label>
                            </div>
                        @endforeach
                    @endif

                    <div class="text-center">
                        <button type="submit" value="filter" name="filter" class="btn btn-amber text-uppercase mt-4">
                            Apply Filter
                        </button>
                    </div>
                </form>

                <!-- Column For Filters -->
            </div>

            <div class="col-sm-9 wd-product-section text-center">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Choose A Prize And Win Your Destiny</h2>
                <span class="wd-counter-span-secondary my-4">
                        <div class="wd-countdown-pretext">LIMITED TIME LEFT. HURRY!</div>
                    <div data-countdown="2019/01/01" class="wd-counter-home"></div>
                </span>
                <!-- Nested Row For Products -->
                <div class="row text-center">

                @foreach($products as $product)
                    <!-- Grid column -->
                        <div class="col-lg-4 col-md-6 mb-4">
                            <!-- Card -->
                            <div class="card win-home-product align-items-center">
                                <!-- Card image -->
                                <div class="view overlay">
                                    <img src="{{ asset('/storage/') . '/' . $product -> product_image1}}"
                                         class="card-img-top wp-product-image" alt="">
                                    <a data-toggle="modal" data-target="{{ '#'. $product -> slug .'modal' }} ">
                                        <div class="mask rgba-white-slight"></div>
                                    </a>

                                </div>
                                <!-- Card image -->
                                <!-- Card content -->
                                <div class="card-body text-center">
                                    <!-- Category & Title -->
                                    <a href="{{ route('product-detail', $product -> slug ) }}" class="">
                                        <h3 class="wd-product-card-heading font-weight-bold">{{ $product -> name }}</h3>
                                    </a>

                                    <form action="{{ route('cart.store', $product) }}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="request_from" value="products">
                                        <input type="hidden" name="product_id" value="{{ $product -> id }}">
                                        <div class="btn-group btn-group-sm mb-3 wd-product-btn-grp" role="group"
                                             aria-label="Basic example">
                                            <button type="submit" name="product_quantity" value="1"
                                                    class="btn btn-amber btn-sm px-3 wd-btn-grp">1
                                            </button>
                                            <button type="submit" name="product_quantity" value="5"
                                                    class="btn btn-amber btn-sm px-3 wd-btn-grp">5
                                            </button>
                                            <button type="submit" name="product_quantity" value="10"
                                                    class="btn btn-amber btn-sm px-3 wd-btn-grp">10
                                            </button>
                                            <button type="submit" name="product_quantity" value="20"
                                                    class="btn btn-amber btn-sm px-3 wd-btn-grp">20
                                            </button>
                                            <button type="submit" name="product_quantity" value="50"
                                                    class="btn btn-amber btn-sm px-3 wd-btn-grp">50
                                            </button>
                                        </div>
                                    </form>

                                    <div class="wp-price-section-product mb-2">
                                <span>
                                    <span class="wd-price-hilight">1</span>{{'$'.  $product -> price_slab1 }} |
                                    <span class="wd-price-hilight">5+</span>{{'$'. ProductDiscount::getDiscountedPrice($product,2) }} |
                                    <span class="wd-price-hilight">10+</span>{{'$'. ProductDiscount::getDiscountedPrice($product,3) }}
                                </span>
                                    </div>
									<!--@if (\Cart::count() > 0)
										<div class="wp-price-section-product mb-2">
											<a class="btn btn-amber py-1 waves-effect waves-light" href="{{ route('pointer.index') }}">PROCEED TO PLAY</a>
										</div>
									@endif -->
                                </div>
                                <!-- Card content -->
                            </div>
                            <!-- Card -->
                        </div>
                        <!-- Grid column -->

                        <!-- Product Modal -->
                        <div class="modal fade " id="{{$product -> slug .'modal' }}" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalCenterTitle"
                             aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            id="{{$product -> slug .'modalTitle' }}">{{ $product -> name }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <div id="{{$product -> slug .'controls' }}" class="carousel slide"
                                                     data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="carousel-item active">
                                                            <img class="d-block w-100"
                                                                 src="{{ asset('/storage/') . '/' . $product -> product_image1}}"
                                                                 alt="First slide">
                                                        </div>
                                                        <div class="carousel-item">
                                                            <img class="d-block w-100"
                                                                 src="{{ asset('/storage/') . '/' . $product -> product_image2}}"
                                                                 alt="Second slide">
                                                        </div>
                                                        <div class="carousel-item">
                                                            <img class="d-block w-100"
                                                                 src="{{ asset('/storage/') . '/' . $product -> product_image3}}"
                                                                 alt="Third slide">
                                                        </div>
                                                        <div class="carousel-item">
                                                            <img class="d-block w-100"
                                                                 src="{{ asset('/storage/') . '/' . $product -> product_image4}}"
                                                                 alt="Third slide">
                                                        </div>
                                                        <div class="carousel-item">
                                                            <img class="d-block w-100"
                                                                 src="{{ asset('/storage/') . '/' . $product -> product_image5}}"
                                                                 alt="Third slide">
                                                        </div>
                                                    </div>
                                                    <a class="carousel-control-prev"
                                                       href="{{'#'. $product -> slug .'controls' }}" role="button"
                                                       data-slide="prev">
                                                        <span class="carousel-control-prev-icon "
                                                              aria-hidden="true"></span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="carousel-control-next "
                                                       href="{{'#'. $product -> slug .'controls' }}" role="button"
                                                       data-slide="next">
                                                        <span class="carousel-control-next-icon"
                                                              aria-hidden="true"></span>
                                                        <span class="sr-only">Next</span>
                                                    </a>


                                                </div>
                                            </div>
                                            <div class="col-lg-7">
                                                <p class="text-left">{{ $product -> description }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a type="button" href="{{ route('product-detail', $product -> slug ) }}"
                                           class="btn btn-primary btn-amber">See Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Product Modal -->
                @endforeach



                <!-- Column For Pagination -->
                    <div class="col-sm-12 wd-pagination">
                        <!--/Pagination amber-->
                        {{ $products->links() }}
                    </div>
                    <!-- Column For Pagination -->
                </div>
                <!-- /Nested Row For Products -->

            </div>


        </div>
        <!-- /Row For The Prizes-->

    </div>
    <!--/Container For The Form-->

@endsection
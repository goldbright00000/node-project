@extends ('layout')

@section('content')

    {{--{{ dd($user) }}--}}

    <!--Container For The page-->
    <div class="container wd-first-container">
        <!--Row For The Personal Details -->
        <div class="row text-center wd-top-padding wd-bottom-padding">

            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3  col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="mt-4 wd-black-heading">WinYourDestiny® Invoice Number: {{ $invoice->id }}</h2>
                <h4>Dated: {{ $invoice->created_at->toFormattedDateString() }} </h4>
                <p class="mt-3">Address:4787 Pratt Avenue, Olympia, WA Washington, 98501, USA</p>
                <p class="mt-0 pt-0">Reg. No: 3755182 | info@winyourdestiny.com | +44 (0)207 371 88 66</p>
            </div>

            @if($invoice->payment_status == 'Pending')
                <div class="col-md-12 my-4  text-center">
                    <form
                            action="{{ route('paypal.express-checkout') }}"
                            method="post">
                        @csrf
                        <button
                            type="submit"
                            name="invoice_id"
                            value="{{ $invoice->id }}"
                            class="btn btn-red btn-primary text-center"
                        >
                            Pay Now
                        </button>
                    </form>
                </div>
            @endif

            <div class="col-md-6 offset-md-1 mt-2">
                <table id="InvoiceTable" class="table table-striped table-bordered text-left" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="font-weight:bold; color: #000;">Name</td>
                            <td>{{ $user->name . ' ' . $user->lastName  }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; color: #000;">Billing Address</td>
                            <td>{{ $user->address1 . ', ' . $user->address2 }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; color: #000;">City, State</td>
                            <td>{{ $user->city . ', ' . $user->state }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; color: #000;">Country, Zip</td>
                            <td>{{ $user->country . ', ' . $user->zip }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-4 mt-2">
                <table id="InvoiceTable" class="table table-striped table-bordered text-left" cellspacing="0" width="100%">
                    <tbody>
                    <tr>
                        <td style="font-weight:bold; color: #000;">Email Address</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold; color: #000;">Mobile Number</td>
                        <td>{{ $user->mobile }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold; color: #000;">Payment Method</td>
                        <td>{{ $invoice->paymentMethod}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold; color: #000;">Payment Status</td>
                        <td>{{ $invoice->payment_status}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>



            <div class="col-md-10 offset-md-1">
                <table id="InvoiceTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm">Particulars
                        </th>
                        <th class="th-sm">Quantity
                        </th>
                        <th class="th-sm">Per Ticket
                        </th>
                        <th class="th-sm">Amount
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach($invoice->products as $product)
                            <tr>
                                <td>{{ $product['name'] }}</td>
                                <td>{{ $product['qty'] }}</td>
                                <td>${{ $product['price'] }}</td>
                                <td>${{ $product['qty'] * $product['price'] }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td>(-) Discount</td>
                            <td>${{ $invoice->discount }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td  style="font-style: italic; font-weight:bold; color: #000; font-size: 16px;">Sub Total</td>
                            <td style="font-style: italic; font-weight:bold; color: #000; font-size: 16px;">${{ $invoice->productsTotal }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>(+) Tax </td>
                            <td>${{ $invoice->tax }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>(-) Credits Adjusted</td>
                            <td>${{ $invoice->creditAdjusted }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="font-weight:bold; color: #000; font-size: 18px;">Grand Total</td>
                            <td style="font-weight:bold; color: #000; font-size: 18px;">${{ $invoice->total }}</td>
                        </tr>
                    </tbody>
                </table>

            </div>

            @if($invoice->payment_status == 'Pending')
                <div class="col-md-12 my-4  text-center">
                    <form
                            action="{{ route('paypal.express-checkout') }}"
                            method="post">
                        @csrf
                        <button
                                type="submit"
                                name="invoice_id"
                                value="{{ $invoice->id }}"
                                class="btn btn-red btn-primary text-center"
                        >
                            Pay Now
                        </button>
                    </form>
                </div>
            @endif

        </div>
        <!--/Row For The Personal Details -->




    </div>
    <!--/Container For The page-->


    @endsection

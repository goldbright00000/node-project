@extends ('layout')

@section('content')

    {{--{{ dd($invoices) }}--}}

    <!--Container For The page-->
    <div class="container wd-first-container">
        <!--Row For The Personal Details -->
        <div class="row text-center wd-top-padding wd-bottom-padding">

            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Order History</h2>
            </div>

            <div class="col-md-12 mt-5">
                <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm">Date
                        </th>
                        <th class="th-sm">Order ID
                        </th>
                        <th class="th-sm">Payment Status
                        </th>
                        <th class="th-sm">Amount
                        </th>
                        <th class="th-sm">Action
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($invoices as $invoice)

                        <tr>
                            <td>{{ $invoice->created_at->toFormattedDateString() }}</td>
                            <td>{{ $invoice->id }}</td>
                            <td>{{ $invoice->payment_status }}</td>
                            <td>${{ $invoice->total }}</td>
                            <td>
                                @if($invoice->payment_status == 'Completed')
                                <form
                                    action="{{ route('invoice') }}"
                                    method="post">
                                    @csrf
                                        <button
                                            type="submit"
                                            name="invoice_id"
                                            value="{{ $invoice->id }}"
                                            class="btn btn-green btn-sm text-center"
                                            formtarget="_blank"
                                        >
                                            View Invoice
                                        </button>
                                </form>
                                @else()
                                    <form
                                            action="{{ route('invoice') }}"
                                            method="post">
                                        @csrf
                                        <button
                                                type="submit"
                                                name="invoice_id"
                                                value="{{ $invoice->id }}"
                                                class="btn btn-red btn-sm text-center"
                                        >
                                            Pay Now
                                        </button>
                                    </form>
                                    @endif

                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="th-sm">Date
                        </th>
                        <th class="th-sm">Order ID
                        </th>
                        <th class="th-sm">Payment Status
                        </th>
                        <th class="th-sm">Amount
                        </th>
                        <th class="th-sm">Invoice
                        </th>
                    </tr>
                    </tfoot>
                </table>

            </div>
            <div class="col-md-12 text-right">
                <a href="{{ route('home') }}" class="btn btn-primary btn-grey text-right">Go Back To Dashboard</a>
            </div>

        </div>
        <!--/Row For The Personal Details -->




    </div>
    <!--/Container For The page-->


    @endsection

@extends ('layout')


@section('content')


    <!--Container For The page-->
    <div class="container wd-first-container eventBinding" id="winGameContainer">

        <!--Row For The Heading Section -->
        <div class="row text-center wd-top-padding ">


            <!-- Column For heading -->
            <div class="col-md-10 offset-md-1 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading" data-step="1" data-intro="Lets Start Playing The Game. You Just Have To Guess The Position Of The Ball In
                    The Image">
                    {{ $winner->title }}</h2>
                <h3>{{ $winner->subTitle }}</h3>
                <h5 class="mt-3">
                    {{ $game->name . ' played from ' . dateToString($game->startDate) . ' to ' . dateToString($game->endDate) }}
                </h5>
            </div>

            <div class="col-md-12 text-center mt-5">
                <iframe
                        width="100%"
                        height="640px"
                        src="https://www.youtube.com/embed/{{ $winner->video }}?controls=0"
                        frameborder="0"
                        allow="accelerometer;
						autoplay;
						encrypted-media;
						gyroscope;
						picture-in-picture"
                        allowfullscreen>
                </iframe>
            </div>
            <div class="col-md-12 text-left">
                <h2
                        class="mt-5 py-4 px-4 mb-0"
                        style="background-color: #b3b3b3;"
                >
                    {{ $winner->title }}
                </h2>

                <h6
                        class="mb-5 py-4 px-4"
                        style="background-color: #dddddd;
								line-height: 24px;"
                >
                    {{ $winner->description }}
                </h6>
            </div>

        </div>

        <div class="row mb-1">
            <div class="col-lg-10">
                <div id="win-winner-image-container">
                    <span
                            style="background-color: rgba(0,0,0,.5);
                         width:895px; 
                         height: 500px;
                         position: absolute;">
                    </span>
                    <img
                            src="{{ asset('/storage/') . '/' . $game->imageUrl }}"
                            width="895px"
                            height="500px"
                    />
                </div>
            </div>
            <div class="col-lg-2 wd-grey-backgound">
                <h4 class="my-3">Result Guides </h4>
                <div class="row mt-5">
                    <div class="col-4">
                        <div style="width: 100%; height: 30px; background-color: #ff1500;">

                            <div class="form-check" style="padding-left: 7px; padding-top: 2px;">
                                <input type="checkbox" class="form-check-input" id="judgePoints" checked>
                                <label class="form-check-label" for="judgePoints"></label>
                            </div>

                        </div>
                    </div>
                    <div class="col-8">
                        Winning Point As Per Judges
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col-4 ">
                        <div style="width: 100%; height: 30px; background-color: #7dff32; ">

                            <div class="form-check" style="padding-left: 7px; padding-top: 2px;">
                                <input type="checkbox" class="form-check-input" id="winnerPoints" checked>
                                <label class="form-check-label" for="winnerPoints"></label>
                            </div>

                        </div>
                    </div>
                    <div class="col-8">
                        The Winning Point
                    </div>
                </div>


                <div class="row">
                    @if($userPoints !== null)
                        <div class="col-4">
                            <div style="width: 100%; height: 30px; background-color: #38bcff;">

                                <div class="form-check" style="padding-left: 7px; padding-top: 2px;">
                                    <input type="checkbox" class="form-check-input" style="background-color: #ffffff"
                                           id="UserPoints" checked>
                                    <label class="form-check-label" for="UserPoints"></label>
                                </div>

                            </div>
                        </div>
                        <div class="col-8">
                            Your Points
                        </div>
                    @endif


                </div>

                <div class="row">
                    <div class="col-12 ">
                        <h5 class="mt-5">Login to view your Points </h5>
                        <p><strong>Please Note:</strong> You need to login inorder to view your points.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p>**The coordinates displayed here are scaled for representational puposes</p>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mt-3">
                <h3>The List Of Winners</h3>

                <table class="table table-striped table-bordered mt-3 mb-5" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm">Position
                        </th>
                        <th class="th-sm">Name
                        </th>
                        <th class="th-sm">City
                        </th>
                        <th class="th-sm">State
                        </th>
                        <th class="th-sm">Playing For
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($positions as $position)
                        <tr>
                            <td>{{ $position->position }}</td>
                            @foreach($userDetails as $user)
                                @if( $user['user_id'] == $position->user_id && $user['pointer_id'] == $position->pointer_id )
                                    <td> {{ $user['name'] }}</td>
                                    <td> {{ $user['city'] }}</td>
                                    <td> {{ $user['state'] }}</td>
                                @endif
                            @endforeach
                            @foreach($productDetails as $product)
                                @if ($product['pointer_id'] == $position->pointer_id )
                                    <td> {{  $product['product'] }}</td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>



    <script>
		$('#win-winner-image-container').append(
			$('<span class=""> <i class="fas fa fa-crosshairs judges" style="color: #ff1500;"></i></span>').css({
				position: 'absolute',
				'z-index': '90',
				top: '{{ (($game->yCords)/8) - 10 . 'px'}}',
				left: '{{ (($game->xCords)/8) - 10 . 'px'}}',
			})
		);
    </script>

    <script>
		$('#win-winner-image-container').append(
			$('<span class=""> <i class="fas fa fa-crosshairs winner" style="color: #7dff32;"></i></span>').css({
				position: 'absolute',
				'z-index': '90',
				top: '{{ (($pointers[0]->yCords)/8) - 10 . 'px'}}',
				left: '{{ (($pointers[0]->xCords)/8) - 10 . 'px'}}',
			})
		);
    </script>

    @if($userPoints !== null)
        @foreach ($userPoints as $userPoint)
            <script>
				$('#win-winner-image-container').append(
					$('<span class=""> <i class="fas fa fa-crosshairs currentUser" style="color: #38bcff;"></i></span>').css({
						position: 'absolute',
						'z-index': '90',
						top: '{{ (($userPoint->yCords)/8) - 10 . 'px'}}',
						left: '{{ (($userPoint->xCords)/8) - 10 . 'px'}}',
					})
				);
            </script>
        @endforeach
    @endif

    <script>
        $(document).ready(function(){
        	$('#judgePoints').click(function(){
        		if($(this).prop("checked") == true){
        			$('.judges').css({
						'color': '#ff1500',
						'display': 'inline-block',
					});
                }
        		else{
					console.log('false');
					$('.judges').css({
						'color': 'rgba(0,0,0,0)',
						'display': 'none',
					});
                }
            })
        });

		$(document).ready(function(){
			$('#winnerPoints').click(function(){
				if($(this).prop("checked") == true){
					$('.winner').css({
						'color': '#7dff32',
						'display': 'inline-block',
					});
				}
				else{
					console.log('false');
					$('.winner').css({
						'color': 'rgba(0,0,0,0)',
						'display': 'none',
					});
				}
			})
		});

		$(document).ready(function(){
			$('#UserPoints').click(function(){
				if($(this).prop("checked") == true){
					$('.currentUser').css({
						'color': '#38bcff',
						'display': 'inline-block',
					});
				}
				else{
					console.log('false');
					$('.currentUser').css({
						'color': 'rgba(0,0,0,0)',
						'display': 'none',
					});
				}
			})
		});

    </script>



@endsection
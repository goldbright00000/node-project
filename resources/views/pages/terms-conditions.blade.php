@extends ('layout')

@section('content')

    <!--Container For The page-->
    <div class="container wd-first-container my-5 wd-bottom-padding wd-top-padding win-order">
        <h3 class="mt-5 mb-4"> WIN YOUR DESTINY - STANDARD COMPETITION TERMS</h3>
        <ol class="">
            <li><strong>Services</strong>
                <ol>
                    <li>Win Your Destiny provides skill based competitions to its account holder on purchase of tickets.
                        Win Your Destiny (‘WYUD’, 'Promoter', 'our(s)') operate competitions – skilled prize
                        competitions resulting in the allocation of prizes in accordance with these terms and conditions
                        on the website www.winyourdestiny.com (the 'Website') - (the 'Competition(s)').
                        Registered users of our services are “Members” and unregistered users are “Visitors”. These
                        terms and conditions applies to both Member and Visitors. Also these terms and conditions
                        applies to users
                    </li>
                </ol>
            </li>
            <li><strong> Qualifying Persons</strong>
                <ol>
                    <li>The Competitions are open to all persons aged 18 and over and the age of majority in their
                        country of
                        residence except the Promoter's employees or members of their immediate family, agents or any
                        other person
                        who is connected with the creation or administration of our Competitions.
                    </li>
                </ol>
            </li>


            <li><strong>Legal Undertaking</strong>
                <ol>
                    <li>By entering a Competition the entrant (“User”, 'Entrant', 'you', 'your(s)') will be deemed to
                        have legal
                        capacity to do so, you will have read and understood these terms and conditions and you will be
                        bound by
                        them and by any other requirements set out in any related promotional material.
                    </li>
                    <li>Competitions are governed by US Law and any matters relating to the Competition will be resolved
                        under
                        US Law and the Courts of US shall have exclusive jurisdiction.
                    </li>
                    <li>In the event that you participate in a Competition online via the Website, and by accepting
                        these terms
                        and conditions you confirm that you are not breaching any laws in your country of residence
                        regarding the
                        legality of entering our Competitions. The Promoter will not be held responsible for any Entrant
                        entering
                        any of our Competitions unlawfully. If in any doubt you should immediately leave the Website and
                        check with
                        the relevant authorities in your country.
                    </li>
                </ol>
            </li>

            <li><strong>Competition Entry</strong>
                <ol>
                    <li>Competitions may be entered online via the Website. One or more Competitions may be operated at
                        the same time and each Competition will have specific prize options.
                    </li>
                    <li>
                        Availability and pricing of Competitions and tickets is at the discretion of the Promoter and
                        will be specified at the point of sale in our Physical Locations and on the Website. 5+ and 10+
                        ticket price categories are available in the Competition. Pricing will not be applied
                        retrospectively to orders already completed. Competition Credit (see 7.1(i)) won will be applied
                        relative to the purchase price of each ticket.
                    </li>
                    <li>User Account(s)
                        <p>
                            You will need to register an account with Win Your Destiny in order to enter into a
                            Competition. Your
                            account lets you sign in to buy competition tickets, refer friends and play the game of
                            skills. Creating an account: You can register an account in two ways:</p>
                        <ol>
                            <li>either online at www.winyourdestiny.com</li>
                            <li>To register an account online you will be asked to provide an email address</li>
                            <li>No two accounts can be created with single email ID</li>
                        </ol>
                    </li>
                    <li>When playing a Competition online via the Website, follow the on-screen instructions to:
                        <ol>
                            <li>select the Competition(s) you wish to enter, choose your tickets and play the Spot the
                                Ball Challenge
                            </li>
                            <li>and when you are ready to purchase your Ticket(s), provide your contact and payment
                                details. You will
                                need to check your details carefully and tick the declaration at checkout, confirming
                                you have read and
                                understood the Competition terms and conditions
                            </li>
                            <li>once your payment has cleared we will email you to confirm your entry into the
                                Competition. Please note
                                that when entering online you will not be deemed entered into the Competition until we
                                confirm your
                                Ticket(s) order back to you by email. ('Website Entries' together with 'Physical
                                Location Entries', referred
                                to as 'Entr(y)(ies)')
                            </li>
                        </ol>
                    </li>
                    <li>The Promoter reserves the right to refuse or disqualify any incomplete Entries if it has
                        reasonable grounds for believing that an Entrant has contravened any of these terms and
                        conditions. To the extent permitted by applicable law, all Entries become our property and will
                        not be returned.
                    </li>
                    <li>Competitions: Entries (tickets) are limited to one hundred (100) per person, per Competition.
                        Abuse of
                        this limit will not be tolerated and if the Promoter has reasonable grounds to believe that an
                        individual is
                        exceeding this limit, for example by entering a Competition using multiple accounts, it reserves
                        the
                        right
                        at its sole discretion to void any orders, any Credit won or to award any prize to the next
                        closest
                        entrant.
                        In addition, if any person using a single account exceeds 100 entries in a single Competition
                        for
                        any
                        reason, the Promoter reserves the right at its sole discretion, to void any tickets without
                        notice
                        firstly
                        on a chronological basis with respect to orders, and then on a sequential basis with respect to
                        ticket
                        references contained within an order, so as to void any tickets exceeding the first 100 paid
                        tickets
                        entered
                        into the Competition.
                    </li>
                </ol>
            </li>


            <li>
                <strong>Promotion Periods</strong>
                <ol>
                    <li>Each Competition will run for a specified period. Please see each Competition for details of
                        start and
                        end times and dates ('Promotion Period(s)').
                    </li>
                </ol>
            </li>


            <li><strong>Competition Judgement</strong>
                <ol>
                    <li>Destiny Competitions: The position of the centre of the ball will be determined before the close
                        of the
                        Competition by an independent Panel of Judges (sporting experts appointed by the Promoter – the
                        'Judge')
                        using their sporting experience and in the presence of a qualified lawyer and a representative
                        from the
                        Promoter ('Judge's Position'). In certain cases and at the discretion of the Promoter, the
                        position of the
                        Judge's Position may be determined by the Judge before the Competition starts, and will
                        immediately be
                        encrypted and stored securely on our database.
                    </li>
                    <li>Within a week of the end of the respective Competition, the Judge's Position will be computed
                        against
                        the database of coordinates to mathematically calculate which of the valid and eligible Entries
                        received by
                        the Promoter is closest to the Judge's Position, and therefore the winning Entrant (the 'First
                        Prize
                        Winner') of the prize. The First Prize Winner will be the person who correctly identifies, or if
                        no one is
                        correct, who is closest to, the Judge's Position. For the avoidance of doubt, distance will be
                        measured by
                        calculating the straight line distance from each valid and eligible Entry to the Judge's
                        Position
                        ('Distance').
                    </li>

                    <li>Competitions: In the event that there are two or more Entrants with valid and eligible Entries
                        that are
                        equally close to or exactly match the Judge's Position (each a 'Tie Break Entry'), the entrants
                        to whom such
                        Tie Break Entries belong, identified by their WYUD Account (the 'Tie Break Entrants') will
                        participate in a
                        tie break mechanic to determine the First Prize Winner for such Competition as follows:
                        <ol>
                            <li>Tie Break Scenario – in the event that one or more of the Tie Break Entrants has only
                                made one
                            </li>
                            <li> Entry
                                to the Competition or in the event that Tie Break 1 results in a tie break: the Tie
                                Break Entrants or the
                                TB1 Tie Break Entrants (as relevant) will be asked to take part in another Spot the Ball
                                Challenge using a
                                new sporting photograph in order to determine a single First Prize Winner (the 'Tie
                                Break 2'). Tie Break 2
                                will be entered and judged on the same terms as the original Spot the Ball Challenge and
                                there will be no
                                additional payment due. In the event that Tie Break 2 results in a tie break, the Tie
                                Break 2 process will
                                be repeated with the respective tied Entrants until there is a single First Prize
                                Winner. In the event that
                                a single Entrant has more than one Entry involved in a Tie Break, each Entry will be
                                eligible for a separate
                                Tie Break 2 Entry. If the Promoter is unable to contact one or more of the Tie Break
                                Entrants or the TB1 Tie
                                Break Entrants (as relevant) for Tie Break 2, within three
                            </li>
                            <li>(3) days (which may be extended at the sole
                                discretion of the Promoter) of identifying such Tie Break Entrants (or TB1 Tie Break
                                Entrants as relevant),
                                Tie Break 2 will be concluded without that particular Tie Break Entrant or the TB1 Tie
                                Break Entrant (as
                                relevant). All Tie Break Entrants, including those that we were unable to contact will
                                be automatically
                                allocated a tie break prize in accordance with rule 7.1(j). Tie Break 1 and Tie Break 2,
                                together referred
                                to as "Tie Break".
                            </li>
                        </ol>
                    </li>
                    <li>Runners Up in the Competitions will be chosen using the same process as set out in accordance
                        with rules 6.2 and 6.3.
                    </li>
                    <li>The First Prize Winner, the Runners Up, the Tie Break Entrants and Destiny Credit Winners are
                        all
                        referred to as 'Winner(s)'.
                    </li>
                    <li>The Promoter will attempt to contact Winner(s) using the telephone numbers and email address
                        provided at
                        the time of Entry (or as subsequently updated) and held securely in our database. It is the
                        Entrant's sole
                        responsibility to check and update these details. If for any reason they are taken down
                        incorrectly,
                        the
                        Promoter will not be held responsible. Entrants must carefully check their contact details have
                        been
                        recorded correctly.
                    </li>
                    <li>If for any reason the Promoter is unable to contact a Winner within 5 days (which may be
                        extended at the
                        sole discretion of the Promoter) of the end of a Competition or the Winner fails to confirm
                        acceptance of
                        the prize or the Winner is disqualified as a result of contravening any of these terms and
                        conditions, the
                        Winner will forfeit the prize and it will be awarded to the Entrant with the next closest
                        coordinates, as
                        defined in the judging process. For the avoidance of doubt once the prize has been forfeited the
                        original
                        Winner, Runners Up or Tie Break Entrants will have no further claims against the Promoter.
                    </li>
                    <li>In the event that the Promoter closes a Competition early, the Winner will be selected from all
                        valid
                        and eligible Entries received by the Promoter prior to the date of closure, except that the
                        Promoter
                        reserves the right, at its sole discretion, to close a Competition early without selecting a
                        Winner.
                        In the
                        event that a Competition is closed without selecting a Winner, the Promoter will give all
                        entrants
                        Destiny
                        Credit to enable them to replay equivalent tickets in a subsequent competition. The Promoter
                        also
                        reserves
                        the right at its sole discretion to extend the closing date of any Competition.
                    </li>
                    <li>All Entrants are automatically entered onto the Promoter's database for the purpose of
                        conveying
                        information as to the status of their Competition, as well as any future Promotions or
                        Competitions
                        offered
                        by the Promoter.
                    </li>
                </ol>

            </li>

            <li><strong>Winner's Details</strong>
                <ol>
                    <li>The First Prize Winner will be required to send a copy of their passport to the Promoter to
                        confirm
                        their identity, age and also to prove that if the purchase was made by credit card that the card
                        was
                        legally
                        theirs or that they had authorisation to use it, before any prize will be paid or delivered. Any
                        failure to
                        meet these obligations may result in the First Prize Winner being disqualified and the Promoter
                        choosing an
                        alternate winner.
                    </li>
                    <li>All Winners will also be required to provide photographs and/or pose for photographs and videos,
                        which
                        may be used in future marketing and public relations by the Promoter in connection with the
                        Competition and
                        in identifying them as a winner of a Competition.
                    </li>
                    <li>Following receipt and verification of the details requested above by the Promoter, the Winners
                        will be
                        contacted in order to make arrangements for delivery of the prize. At this point the Winners
                        must
                        choose
                        between the prizes available (as detailed in the Competition prize information on the Website)
                        and
                        notify
                        the Promoter of their choice in writing.
                    </li>
                    <li>Surprise Contact Details: On your account page you have the option to add the contact details of
                        an
                        individual who we can contact to help arrange with the surprise if you are a Winner. We will
                        categorically
                        not share or pass these details on to any third party but in adding them you are confirming that
                        you
                        have
                        the consent from the person to pass these details to WYUD.
                    </li>
                </ol>
            </li>
            <li><strong>Competition Prizes</strong>
                <ol>
                    <li>Destiny Competitions: Destiny Competitions are comprised of many different products. For the
                        avoidance
                        of doubt, there will be only one prize awarded for each Destiny Competition ('First Prize') and
                        not one per
                        product listed. Please also note the following:
                    </li>
                    <li>
                        <ol>
                            <li>During the course of a Destiny Competition, if due to circumstances beyond the
                                Promoter's control, the
                                Promoter is unable to provide the stated First Prize, the Promoter reserves the right to
                                award a similar
                                product of equal or greater value.
                            </li>
                            <li>If the First Prize Winner would like to take the product, an order will be placed by the
                                Promoter.
                                Please note that if the Winner decides to do this, delivery may take up to 1 month for
                                certain products. All
                                products will ordinarily be ordered in the USA from USA dealers. Bespoke options and
                                upgrades can be added
                                at the First Prize Winner's request but must be paid for by the First Prize Winner in
                                advance. The First
                                Prize Winner can choose the colour of the product (subject to manufacturer availability
                                and terms and
                                conditions). The First Prize Winner will not win Destiny Credit on their competition
                                entries.
                            </li>
                            <li>In the USA the First Prize will be delivered with USA VAT paid. In all other countries
                                the First Prize
                                will be shipped (net of all taxes) to the nearest port and any applicable import duties,
                                registration costs,
                                or any further taxes or duties of any nature due in the destination country will be the
                                responsibility of
                                the First Prize Winner. The Promoter reserves the right not to deliver to certain
                                countries. Please note
                                that all our products are supplied in the USA and therefore if exported may not be road
                                legal in the
                                destination country. It is the First Prize Winner's responsibility to check this and
                                choose an alternative
                                product or take the cash alternative if appropriate. For the avoidance of doubt, the
                                maximum value that The
                                Promoter will be liable to pay for or towards any prize is the USA RRP (or local
                                currency equivalent) of the
                                prize as advertised on our website. WYUD will only transact with manufacturer
                                recommended principal dealers.
                            </li>

                            <li>Tie Break Entrants: For the avoidance of doubt, the product won by the First Prize
                                Winner will be the
                                product attributed to the ticket that was equidistant from the judge’s position and not
                                any product
                                attributed to subsequent entries that may used to establish the First Prize Winner. In
                                the event that a
                                single Entrant has more than one Entry involved in a Tie Break, one of which is
                                identified as the First
                                Prize Winner, as a result of Tie Break Scenario 1 or Tie Break Scenario 2, the First
                                Prize will be awarded
                                as follows. The product that they will receive will be any product up to a maximum RRP
                                of the simple average
                                USA RRPs of their Entries involved in the Tie Break, or if they elect for to take the
                                Cash Alternative, a
                                simple average of the Cash Alternatives per 7.1 (f) of their Entries involved in the Tie
                                Break. Anyone
                                involved in a Competition Tie Break, except the First Prize Winner, will automatically
                                win $20 of Credits
                                points
                            </li>
                        </ol>
                    </li>
                    <li>All other expenses not expressly specified herein are the sole responsibility of the Winners.
                    </li>
                    <li>All Prizes are subject to the terms and conditions of the prize provider, manufacturer or
                        supplier
                    </li>
                    <li>Each First Prize must be accepted as awarded and is non-transferable or convertible to other
                        substitutes
                        and cannot be used in conjunction with any other vouchers, offers or discounts, including
                        without
                        limitation
                        any vouchers or offers of the Promoter or other prize suppliers.
                    </li>
                </ol>
            </li>

            <li><strong>Refer a Friend</strong>
                <ol>
                    <li>The Refer a Friend scheme is operated in good faith to reward customers for referring genuine
                        friends and
                        acquaintances as new customers to the Promoter. Abuse of this system will not be tolerated and
                        if the
                        Promoter has reasonable grounds to believe that new users are not genuine individuals it
                        reserves the right
                        at its sole discretion to void any orders and/or WYD Credit earned as a result of the scheme,
                        and to
                        disqualify such individual as a Prize Winner. Credit earned is subject to the terms of the Refer
                        a Friend
                        scheme.
                    </li>
                </ol>
            <li>


            <li><strong>Storage</strong>
                <ol>
                    <li>
                        The Promoter can store the chosen prize free of charge for 30 days after notifying the First
                        Prize
                        Winner,
                        at the end of which time the First Prize will be delivered to the Winner.
                    </li>
                </ol>

            <li><strong>Winners' Personal Data</strong>
                <ol>
                    <li>Acceptance of the prize by the Winner will mean they are required to have their photo and video
                        taken
                        by the Promoter for promotional purposes (Public Relations and Marketing), both immediately
                        after their
                        win
                        and in the future for use in accordance with rule 6.2, unless prohibited by law.
                    </li>
                    <li>By entering a Competition, you agree to the use of your name, address, and/or photograph or
                        other
                        likeness, as well as your appearance at publicity events without any additional compensation
                        (save for
                        reasonable travel expenses) and as required by the Promoter if you are declared a Winner.
                    </li>
                </ol>
            </li>

            <li><strong>Limitation of Liability</strong>
                <ol>
                    <li>WYUD is not responsible or liable for any failure to perform or delay in performing its
                        obligations
                        under
                        these Terms to the extent that the failure or delay is caused by circumstances beyond WYUD’s
                        reasonable
                        control (such as labour disputes, acts of God, war or terrorist activity, accidents or
                        compliance with
                        any
                        applicable law or government order). WYUD will endeavor to minimize the effects of any of these
                        events
                        and
                        to perform the obligations that aren’t affected.
                        WYUD makes no representations or warranties as to the quality/suitability of any of the goods or
                        services
                        offered as prizes.
                        WYUD shall not be liable for any loss suffered or sustained to person or property including, but
                        not
                        limited
                        to, consequential (including economic) loss by reason of any act or omission by the Promoter, or
                        its
                        servants or agents, in connection with the arrangement for supply, or the supply, of any goods
                        by any
                        person
                        to the prize Winner(s) and, where applicable, to any family/persons accompanying the Winner(s),
                        or in
                        connection with any of the Competitions promoted by the Promoter.
                    </li>
                </ol>
            </li>

            <li><strong>Electronic Communications</strong>
                <ol>
                    <li>
                        No responsibility will be accepted for failed, partial or garbled computer transmissions, for
                        any
                        computer,
                        telephone, cable, network, electronic or internet hardware or software malfunctions, failures,
                        connections,
                        availability, for the acts or omissions of any service provider, internet accessibility or
                        availability
                        or
                        for traffic congestion or unauthorised human act, including any errors or mistakes. The Promoter
                        shall
                        use
                        its best endeavours to award the prize for a Competition to the correct Entrant. If due to
                        reasons
                        of
                        hardware, software or other computer related failure, or due to human error the prize is awarded
                        incorrectly, the Promoter reserves the right to reclaim the Competition prize and award it to
                        the
                        correct
                        Entrant, at its sole discretion and without admission of liability. In the event that the
                        Promoter
                        closes a
                        Competition early, the Winner will be selected from all valid and eligible Entries received by
                        the
                        Promoter
                        prior to the date of closure, except that the Promoter reserves the right, at its sole
                        discretion,
                        to
                        close
                        a Competition early without selecting a Winner. In the event that a Competition is closed
                        without
                        selecting
                        a Winner, the Promoter will give all entrants Destiny Credit to enable them to replay equivalent
                        tickets
                        in
                        a subsequent competition. The Promoter also reserves the right at its sole discretion to extend
                        the
                        closing
                        date of any Competition. The Promoter shall not be liable for any economic or other
                        consequential
                        loss
                        suffered or sustained to any persons to whom an award has been incorrectly made, and no
                        compensation
                        shall
                        be due. The Promoter shall use its best endeavours to ensure that the software and website(s)
                        used
                        to
                        operate its Competitions perform correctly and accurately across the latest versions of popular
                        internet,
                        tablet and mobile browsers. For the avoidance of doubt, only the ticket coordinates recorded in
                        our
                        systems,
                        howsoever displayed or calculated, shall be entered into the relevant Competition and the
                        Promoter
                        shall
                        not
                        be held liable for any competition entries that occur as a result of malfunctioning software or
                        other
                        event.
                        Competition coordinates may be checked at any time by accessing your account at
                        winyourdestiny.com.
                    </li>
                </ol>
            </li>

            <li><strong>Data Protection Notice</strong>
                <ol>
                    <li>Any personal data that you supply to the Promoter or authorise the Promoter to obtain from a
                        third
                        party,
                        for example, a credit card company, will be used by the Promoter to administer the Competition
                        and
                        fulfil
                        prizes where applicable. In order to process, record and use your personal data the Promoter may
                        disclose it
                        to </br>
                        (i) any credit card company whose name you give;</br>
                        (ii) any person to whom the Promoter proposes to transfer any of the Promoter's rights and/or
                        responsibilities under any agreement the Promoter may have with you;</br>
                        (iii) any person to whom the Promoter proposes to transfer its business or any part of it;</br>
                        (iv) comply with any legal or regulatory requirement of the Promoter in any country; and</br>
                        (v) prevent, detect or prosecute fraud and other crime. In order to process, use, record and
                        disclose
                        your</br>
                        personal data the Promoter may need to transfer such information outside the USA, in which event
                        the
                        Promoter is responsible for ensuring that your personal data continues to be adequately
                        protected during
                        the
                        course of such transfer.
                    </li>
                </ol>
            </li>

            <li><strong>Promoter</strong>
                <ol>
                    <li>Win Your Destiny USA email: info@winyourdestiny.com Web: www.winyourdestiny.com. A list of
                        winners and
                        their
                        home town will be available for one (1) month after the end of each Competition by sending an
                        email to
                        the
                        Promoter.
                        Date of Last Revision: April 30, 2019
                    </li>
                </ol>
            </li>
        </ol>

    </div>

@endsection
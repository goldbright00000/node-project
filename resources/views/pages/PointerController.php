<?php

namespace App\Http\Controllers;

use App\Http\Middleware\Authenticate;
use Darryldecode\Cart\CartCondition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\Pointer;
use App\Game;
use Carbon\Carbon;
use Log;

class PointerController extends Controller {

	public $cart;
	public $nextPosition;
	public $nextStamp;
	public $gamePlay;
	public $lastStampId;
	public $count;


	/**
	 * Function to set the mandatory properties needed by the class
	 */
	private function setClassProperties() {
		$this->cart = \Cart::getContent();
	}


	/**
	 * Function to fund the position of the next ticket in Stamped Products
	 */
	private function findPosition( $stampedProducts, $stampId ) {

		$itemCount = ( $stampedProducts->count() - 1 );
		
		$stampedProducts->each( function ( $item, $key ) use ( $stampId, $itemCount, $stampedProducts ) {

			if ( $item['stamp'] == $stampId && $key <= $itemCount ) {
				$this->nextPosition = $key ;
				$positionFetch      = $this->nextPosition;
				$this->nextStamp    = $stampId;

				return false;
			} else {
				return null;
			}
		} );
	}





	/**
	 * Index method to return view
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {

		// Set the $gamePlay Properties
		$this->gamePlay = FindCurrentGame();

		// Update Session With New Session ID
		updatePointerSession();

		// Set the class properties
		$this->setClassProperties();

		// Create a Unique Stamp For Each Product
		$stampedProducts = stampProducts( $this->cart );

		return view( 'pages.play' )->with([
			'stampedProducts' => $stampedProducts,
			'gamePlay' => $this->gamePlay,
		]);
	}


	public function store( Request $request ) {

		session()->forget( 'nextStamp' );
		session()->save();
	    $lastStampId = 0;

		// Set the class properties
		$this->setClassProperties();

		// Get And Set the Coordinates and product user is playing for
		$pointerCollectionFromDb = collect( [] );
		$addCondition            = false;

		// Prepare A Collection Of Pointers From Current Status in DB
		$pointerCollectionFromDb = pointerCollectionFromDb( $request );

		// Compare Db Pointers to status of products in the shopping cart
		$addCondition = comparePointersToDb( $pointerCollectionFromDb, $this->cart, $request );

		// Add the pointers to the database if all the conditions have been met
		if ( $addCondition == true ) {
			pointersToDb( $request );
		};

		// Collect Pointers From DB After The Update
		$sessionID = session()->get( '_token' );
		$pointers  = Pointer::where( [
			[ 'sessionID', '=', $sessionID ],
			[ 'game_id', '=', '1' ],
			[ 'invoice_id', '=', null ],
		] )->get()->map( function ( $item, $key ) {
			return collect( $item )->except( [ 'sessionID', 'created_at', 'updated_at' ] )->toArray();
		} );

		// Collect Stamped products once again from DB
		$stampedProducts = stampProducts( $this->cart );
		$this->count = 0;
		//new Function 
		$stampedProducts->each( function ( $item, $key ){
			
			if( $item['stamped'] == null)
			{
				$this->count = $this->count +1;
				$count = $this->count;
				if($count == 1)
				{
					$this->lastStampId=  $item['stamp'];
				}
			}
		});
		
		$lastStampId = $this->lastStampId;
		// Return the postion of the next ticket to be played
		$this->findPosition( $stampedProducts, $lastStampId );
		$nextPosition = $this->nextPosition;
		// Saving Next Position to the session to be used by the slider
		session()->put( 'nextPosition', $nextPosition );
		session()->save();

		// Check If All Products Are Stamped
		$allProductsStamped = countStampedProducts();

		return response()->json( [
			'success'            => 'Data is successfully added',
			'pointers'           => $pointers,
			'stampedProducts'    => $stampedProducts,
			'nextPosition'       => $nextPosition,
			'nextStamp'          => $this->nextStamp,
			'cart'               => $this->cart,
			'allProductsStamped' => $allProductsStamped,
		] );
	}

	/**
	 * Function to remove the ticket from the cart and coordinates from the Database
	 *
	 * @param Request from the form submission
	 *
	 * @return back with stamped products
	 */
	public function remove( Request $request ) {

		// Set the class properties
		$this->setClassProperties();

		// Get The Cart Item
		$cartItem = \Cart::get($request->productId);


		if ($cartItem->quantity > 1){
			// Update the cart and reduce the quantity of the product
			\Cart::update( $request->productId, [
				'quantity' => - 1,
			] );
		}
		elseif ($cartItem->quantity == 1) {
			\Cart::remove($request->productId);
		}

		// To get all applied conditions on a cart, use below:
		$cart = \Cart::getContent();

		// Update The Discount Condition On The Product based on the current quantity of the product
		updateDiscountConditions($cartItem);

		// Restamp the database based on the new sequence
		restampDatabase( $request, $this->cart );

		// Stamp the products once again
		$stampedProducts = stampProducts( $this->cart );

		// Return the user back
		return back()->with( 'stampedProducts', $stampedProducts )->withErrors(['cartRemove']);
	}


	/**
	 * Function to update the quantity of the product
	 */
	public function update( Request $request ) {

		// Set the class properties
		$this->setClassProperties();

		// Update the cart and reduce the quantity of the product
		\Cart::update( $request->productId, [
			'quantity' => - 1,
		] );

		// Stamp the products once again
		$stampedProducts = stampProducts( $this->cart );

		// Return the user back
		return back()->with( 'stampedProducts', $stampedProducts );

	}

	public function destroy() {
		removePointers();
		return back();
	}
	
	public function destroyTicket($id) {
		$sessionToken = session()->get( '_token' );
	    $currentGame = FindCurrentGame()->id;

		   Pointer:: where([
		   'id' => $id,
		   ])->delete();
		return back();
	}

}

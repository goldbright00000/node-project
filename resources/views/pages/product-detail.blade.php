@extends('layout')

@section('content')

    <!--Container For The Prizes-->
    <div class="container wd-first-container">
        <!-- Row For The Prizes-->
        <div class="col-sm-12 py-5 d-none d-sm-block"></div>
        <div class="row wd-bottom-padding ">

            <div class="col-md-12 wd-product-section text-center">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">{{ $product->name }}</h2>
            </div>
            <div class="col-md-12 wd-product-section text-center">
                <span class="wd-heading-separator"></span>
                <h3 class="mb-5">Add Tickets To Cart And Start Playing</h3>
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center wp-price-section-product">
                        <p><span class="wd-price-hilight">Upto 5 Tickets:</span><strong> {{ $product->price_slab1 }} </strong></p>
                        <p><span class="wd-price-hilight">5+ Tickets: </span><strong> {{ ProductDiscount::getDiscountedPrice($product,2) }} </strong> | <span
                                    class="wd-price-hilight"> 10+ Tickets: </span><strong> {{ ProductDiscount::getDiscountedPrice($product,3) }} </strong></p>
                    </div>
                </div>

                <form action="{{ route('cart.store', $product) }}" method="post" class="mt-2">
                    {{ csrf_field() }}
                    <div class="value-button" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
                    <input type="hidden" name="product_id" value="{{ $product->id }}" />
                    <input type="hidden" name="request_from" value="product-detail" />
                    <input type="number" name="product_quantity" id="number" value="1" max="99" min="1"/>
                    <div class="value-button" id="increase" onclick="increaseValue()" value="Increase Value">+</div>
                    <div>
                        <button class="btn mt-3 btn-amber waves-effect waves-light" type="submit"> Add To Cart</button>
                    </div>

                </form>
            </div>

            <!-- Column for the main product slide show  -->
            <div class="col-md-10 offset-md-1 mt-4">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('/storage/') . '/' . $product -> product_image1}}" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('/storage/') . '/' . $product -> product_image2}}" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('/storage/') . '/' . $product -> product_image3}}" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('/storage/') . '/' . $product -> product_image4}}" alt="Fourth slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('/storage/') . '/' . $product -> product_image5}}" alt="Fifth slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!-- / Column for the main product slide show -->

            <!-- / Column for the product description -->
            <div class="col-sm-12 py-4 d-none d-sm-block"></div>
            <div class="col-md-12 wd-product-section text-center">
                <span class="wd-heading-separator"></span>
                <h3 class="">More Information</h3>
            </div>
            <p class="mt-2 text-center">{{ $product -> description  }}</p>
            <!-- / Column for the product description -->

        </div>
    </div>
@endsection
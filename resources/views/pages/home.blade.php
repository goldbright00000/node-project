@extends ('layout')

@section('content')


    <!-- Hero Section -->
    <div class="wd-home-hero-bg" style="background-image: url({{ env('APP_STORAGE', 'https://winyourdestiny.com/storage/') . $homeSettings->heroImage }});">
        <div class="container">
            <div class="wd-hero-inner-section pt-5">
                <div class="row">
                    <section class="col-md-7 offset-md-2  col-sm-12">
                        <h1 class="wd-hero-section-heading">{{ $homeSettings->heroContent }}</h1>
                    </section>
                    <section class="col-md-10 offset-md-2 col-sm-12">
                        <div class="wd-countdown-pretext">LIMITED TIME LEFT. HURRY!</div>
                        <div data-countdown="{{ $endDate->year }}/{{ $endDate->month }}/{{ $endDate->day . ' ' . $endDate->hour . ':' . $endDate->minute . ':' . $endDate->second }}" class="wd-counter-home"></div>
                        <a href="{{ route('product-listing') }}">
                            <button type="button" class="btn btn-lg btn-amber mt-5">PLAY NOW!</button>
                        </a>
                    </section>
                    <div class="col-md-3 col-sm-6 offset-md-8 offset-sm-3 mt-4">
                        <img src="{{ asset('images/trust_pilot_white.png') }}" alt="Trust Pilot Reviews"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Hero Section -->

    <section class="container wd-top-padding">
        <div class="col-md-10 offset-md-1 col-sm-12 wd-heading-container">
            <span class="wd-heading-separator"></span>
            <h2 class="wd-black-heading">Choose A Prize And Win Your Destiny</h2>
            <span class="wd-counter-span-secondary my-4">
                <div class="wd-countdown-pretext">LIMITED TIME LEFT. HURRY!</div>
                <div data-countdown="{{ $endDate->year }}/{{ $endDate->month }}/{{ $endDate->day }}" class="wd-counter-home"></div>
            </span>
        </div>
    </section>


    <!-- Grid column -->
    <section class="container wd-bottom-padding">
        <!-- Grid row -->
        <div class="row text-center">
            @foreach ($products as $product)
                <div class="col-lg-3 col-md-6 mb-lg-0 mb-4">
                    <!-- Card -->
                    <div class="card win-home-product align-items-center">
                        <!-- Card image -->
                        <div class="view overlay">
                            <img src="{{ asset('/storage/') . '/' . $product -> product_image1}}"
                                 class="card-img-top wp-product-image" alt="">
                            <a data-toggle="modal" data-target="{{ '#'. $product -> slug .'modal' }} ">
                                <div class="mask rgba-white-slight"></div>
                            </a>

                        </div>
                        <!-- Card image -->
                        <!-- Card content -->
                        <div class="card-body text-center">
                            <!-- Category & Title -->
                            <a href="{{ route('product-detail', $product -> slug )  }}" class="">
                                <h3 class="wd-product-card-heading font-weight-bold">{{ $product -> name }}</h3>
                            </a>
                            <form action="{{ route('cart.store', $product) }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="request_from" value="products">
                                <input type="hidden" name="product_id" value="{{ $product -> id }}">
                                <div class="btn-group btn-group-sm mb-3 wd-product-btn-grp" role="group"
                                     aria-label="Basic example">
                                    <button type="submit" name="product_quantity" value="1"
                                            class="btn btn-amber btn-sm px-3 wd-btn-grp">1
                                    </button>
                                    <button type="submit" name="product_quantity" value="5"
                                            class="btn btn-amber btn-sm px-3 wd-btn-grp">5
                                    </button>
                                    <button type="submit" name="product_quantity" value="10"
                                            class="btn btn-amber btn-sm px-3 wd-btn-grp">10
                                    </button>
                                    <button type="submit" name="product_quantity" value="20"
                                            class="btn btn-amber btn-sm px-3 wd-btn-grp">20
                                    </button>
                                    <button type="submit" name="product_quantity" value="50"
                                            class="btn btn-amber btn-sm px-3 wd-btn-grp">50
                                    </button>
                                </div>
                            </form>
                            <div class="wp-price-section-product mb-2">
                                <span>
                                    <span class="wd-price-hilight">1</span>{{'$'.  $product -> price_slab1 }} |
                                    <span class="wd-price-hilight">5+</span>{{'$'.  ProductDiscount::getDiscountedPrice($product,2) }} |
                                    <span class="wd-price-hilight">10+</span>{{'$'. ProductDiscount::getDiscountedPrice($product,3) }}
                                </span>
                            </div>

                        </div>
                        <!-- Card content -->
                    </div>
                    <!-- Card -->
                </div>
                <!-- Grid column -->

                <!-- Product Modal -->
                <div class="modal fade " id="{{$product -> slug .'modal' }}" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalCenterTitle"
                     aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"
                                    id="{{$product -> slug .'modalTitle' }}">{{ $product -> name }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div id="{{$product -> slug .'controls' }}" class="carousel slide"
                                             data-ride="carousel">
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img class="d-block w-100"
                                                         src="{{ asset('/storage/') . '/' . $product -> product_image1}}"
                                                         alt="First slide">
                                                </div>
                                                <div class="carousel-item">
                                                    <img class="d-block w-100"
                                                         src="{{ asset('/storage/') . '/' . $product -> product_image2}}"
                                                         alt="Second slide">
                                                </div>
                                                <div class="carousel-item">
                                                    <img class="d-block w-100"
                                                         src="{{ asset('/storage/') . '/' . $product -> product_image3}}"
                                                         alt="Third slide">
                                                </div>
                                                <div class="carousel-item">
                                                    <img class="d-block w-100"
                                                         src="{{ asset('/storage/') . '/' . $product -> product_image4}}"
                                                         alt="Third slide">
                                                </div>
                                                <div class="carousel-item">
                                                    <img class="d-block w-100"
                                                         src="{{ asset('/storage/') . '/' . $product -> product_image5}}"
                                                         alt="Third slide">
                                                </div>
                                            </div>
                                            <a class="carousel-control-prev"
                                               href="{{'#'. $product -> slug .'controls' }}" role="button"
                                               data-slide="prev">
                                                <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next "
                                               href="{{'#'. $product -> slug .'controls' }}" role="button"
                                               data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>


                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <p class="text-left">{{ $product -> description }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a type="button" href="{{ route('product-detail', $product -> slug ) }}"
                                   class="btn btn-primary btn-amber">Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Product Modal -->

            @endforeach


        </div>
        <!-- Grid row -->


        <!-- Grid row -->
        <div class="row">
            <!-- Button Column -->
            <div class="col-sm-12 col-md-12 text-center">
                <!-- Button -->
                <a href="{{ route('product-listing') }}">
                    <button type="button" class="btn btn-lg btn-amber mt-5">BROWSE ALL PRODUCTS</button>
                </a>
                <!-- /Button -->
            </div>
            <!-- Button Column -->
        </div>
        <!-- Grid row -->
    </section>



    <!-- Videos Section -->
    <div class="wd-grey-backgound wd-top-padding wd-bottom-padding">
        <section class="container text-center">
            <!-- Row For Videos Section -->
            <div class="row">
                <!-- Column For Heading  -->
                <div class="col-md-8 col-sm-12 offset-md-2">
                    <span class="wd-heading-separator"></span>
                    <h2 class="wd-black-heading">Videos Of Past Winners</h2>
                    <!-- /Column For Heading  -->
                </div>

				<div class="row">
                <!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-lg-0 mt-5 mb-3">
                    <!-- Card -->
                    <div class="card align-items-center">
                        <!-- Card image -->
                        <div class="view overlay">
                            <iframe width="350" height="198"
                                    src="https://www.youtube.com/embed/{{$homeSettings->videoOne}}?controls=0"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <!-- Card image -->
                        <!-- Card content -->
                        <div class="card-body text-center">
                            <!-- Category & Title -->
                            <a href="" class="">
                                <h3 class="wd-product-card-heading font-weight-bold">{{ $homeSettings->videoOneTitle }}</h3>
                            </a>
                            <span class="">
                                    <!--Facebook-->
                                    <span data-href="https://www.youtube.com/watch?v={{ $homeSettings->videoOne }}"
                                          data-size="large">
                                    <a type="button"
                                       href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoOne }}&amp;src=sdkpreparse"
                                       target="_blank"
                                       class="btn-floating btn btn-fb fb-xfbml-parse-ignore">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </span>

                                <!--Twitter-->
                                    <a type="button"
                                       href="https://twitter.com/intent/tweet?text=Win%20Your%20Destiny%20https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoOne }}"
                                       target="_blank"
                                       data-size="large" class="btn-floating btn btn-tw"><i
                                                class="fa fa-twitter"></i></a>

                                </span>

                        </div>
                        <!-- Card content -->
                    </div>
                    <!-- Card -->
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-lg-0 mt-5 mb-3">
                    <!-- Card -->
                    <div class="card align-items-center">
                        <!-- Card image -->
                        <div class="view overlay">
                            <iframe width="350" height="198"
                                    src="https://www.youtube.com/embed/{{$homeSettings->videoTwo}}?controls=0"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <!-- Card image -->
                        <!-- Card content -->
                        <div class="card-body text-center">
                            <!-- Category & Title -->
                            <a href="" class="">
                                <h3 class="wd-product-card-heading font-weight-bold">{{$homeSettings->videoTwoTitle}}</h3>
                            </a>
                            <span class="">
                                    <!--Facebook-->
                                    <span data-href="https://www.youtube.com/watch?v={{ $homeSettings->videoTwo }}"
                                          data-size="large">
                                    <a type="button"
                                       href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoTwo }}&amp;src=sdkpreparse"
                                       target="_blank"
                                       class="btn-floating btn btn-fb fb-xfbml-parse-ignore">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </span>

                                <!--Twitter-->
                                    <a type="button"
                                       href="https://twitter.com/intent/tweet?text=Win%20Your%20Destiny%20https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoTwo }}"
                                       target="_blank"
                                       data-size="large" class="btn-floating btn btn-tw"><i
                                                class="fa fa-twitter"></i></a>

                                </span>

                        </div>
                        <!-- Card content -->
                    </div>
                    <!-- Card -->
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-lg-0 mt-5 mb-3">
                    <!-- Card -->
                    <div class="card align-items-center">
                        <!-- Card image -->
                        <div class="view overlay">
                            <iframe width="350" height="198"
                                    src="https://www.youtube.com/embed/{{$homeSettings->videoThree}}?controls=0"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <!-- Card image -->
                        <!-- Card content -->
                        <div class="card-body text-center">
                            <!-- Category & Title -->
                            <a href="" class="">
                                <h3 class="wd-product-card-heading font-weight-bold">{{ $homeSettings->videoThreeTitle }}</h3>
                            </a>
                            <span class="">
                                    <!--Facebook-->
                                    <span data-href="https://www.youtube.com/watch?v={{ $homeSettings->videoThree }}"
                                          data-size="large">
                                    <a type="button"
                                       href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoThree }}&amp;src=sdkpreparse"
                                       target="_blank"
                                       class="btn-floating btn btn-fb fb-xfbml-parse-ignore">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </span>

                                <!--Twitter-->
                                    <a type="button"
                                       href="https://twitter.com/intent/tweet?text=Win%20Your%20Destiny%20https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoThree }}"
                                       target="_blank"
                                       data-size="large" class="btn-floating btn btn-tw"><i
                                                class="fa fa-twitter"></i></a>

                                </span>

                        </div>
                        <!-- Card content -->
                    </div>
                    <!-- Card -->
                </div>
                <!-- Grid column -->
				</div>


				<div class="row addDisplayNone" id="browseVideos">
				<!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-lg-0 mt-5 mb-3">
                    <!-- Card -->
                    <div class="card align-items-center">
                        <!-- Card image -->
                        <div class="view overlay">
                            <iframe width="350" height="198"
                                    src="https://www.youtube.com/embed/{{$homeSettings->videoFour}}?controls=0"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <!-- Card image -->
                        <!-- Card content -->
                        <div class="card-body text-center">
                            <!-- Category & Title -->
                            <a href="" class="">
                                <h3 class="wd-product-card-heading font-weight-bold">{{ $homeSettings->videoFourTitle }}</h3>
                            </a>
                            <span class="">
                                    <!--Facebook-->
                                    <span data-href="https://www.youtube.com/watch?v={{ $homeSettings->videoFour }}"
                                          data-size="large">
                                    <a type="button"
                                       href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoFour }}&amp;src=sdkpreparse"
                                       target="_blank"
                                       class="btn-floating btn btn-fb fb-xfbml-parse-ignore">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </span>

                                <!--Twitter-->
                                    <a type="button"
                                       href="https://twitter.com/intent/tweet?text=Win%20Your%20Destiny%20https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoFour }}"
                                       target="_blank"
                                       data-size="large" class="btn-floating btn btn-tw"><i
                                                class="fa fa-twitter"></i></a>

                                </span>

                        </div>
                        <!-- Card content -->
                    </div>
                    <!-- Card -->
                </div>
                <!-- Grid column -->
				
				<!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-lg-0 mt-5 mb-3">
                    <!-- Card -->
                    <div class="card align-items-center">
                        <!-- Card image -->
                        <div class="view overlay">
                            <iframe width="350" height="198"
                                    src="https://www.youtube.com/embed/{{$homeSettings->videoFive}}?controls=0"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <!-- Card image -->
                        <!-- Card content -->
                        <div class="card-body text-center">
                            <!-- Category & Title -->
                            <a href="" class="">
                                <h3 class="wd-product-card-heading font-weight-bold">{{ $homeSettings->videoFiveTitle }}</h3>
                            </a>
                            <span class="">
                                    <!--Facebook-->
                                    <span data-href="https://www.youtube.com/watch?v={{ $homeSettings->videoFive }}"
                                          data-size="large">
                                    <a type="button"
                                       href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoFive }}&amp;src=sdkpreparse"
                                       target="_blank"
                                       class="btn-floating btn btn-fb fb-xfbml-parse-ignore">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </span>

                                <!--Twitter-->
                                    <a type="button"
                                       href="https://twitter.com/intent/tweet?text=Win%20Your%20Destiny%20https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoFive }}"
                                       target="_blank"
                                       data-size="large" class="btn-floating btn btn-tw"><i
                                                class="fa fa-twitter"></i></a>

                                </span>

                        </div>
                        <!-- Card content -->
                    </div>
                    <!-- Card -->
                </div>
                <!-- Grid column -->
				
				<!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-lg-0 mt-5 mb-3">
                    <!-- Card -->
                    <div class="card align-items-center">
                        <!-- Card image -->
                        <div class="view overlay">
                            <iframe width="350" height="198"
                                    src="https://www.youtube.com/embed/{{$homeSettings->videoSix}}?controls=0"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <!-- Card image -->
                        <!-- Card content -->
                        <div class="card-body text-center">
                            <!-- Category & Title -->
                            <a href="" class="">
                                <h3 class="wd-product-card-heading font-weight-bold">{{ $homeSettings->videoSixTitle }}</h3>
                            </a>
                            <span class="">
                                    <!--Facebook-->
                                    <span data-href="https://www.youtube.com/watch?v={{ $homeSettings->videoSix }}"
                                          data-size="large">
                                    <a type="button"
                                       href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoSix }}&amp;src=sdkpreparse"
                                       target="_blank"
                                       class="btn-floating btn btn-fb fb-xfbml-parse-ignore">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </span>

                                <!--Twitter-->
                                    <a type="button"
                                       href="https://twitter.com/intent/tweet?text=Win%20Your%20Destiny%20https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D{{ $homeSettings->videoSix }}"
                                       target="_blank"
                                       data-size="large" class="btn-floating btn btn-tw"><i
                                                class="fa fa-twitter"></i></a>

                                </span>

                        </div>
                        <!-- Card content -->
                    </div>
                    <!-- Card -->
                </div>
                <!-- Grid column -->
				</div>
				
                <!-- Button Column -->
                <div class="col-sm-12 col-md-12 text-center">
                    <!-- Button -->
                    <a>  <!--href="{{ route('product-listing') }}" -->
                        <button type="button" class="btn btn-lg btn-amber mt-5" onclick="toggleBrowseVideos()">BROWSE ALL VIDEOS</button>
                    </a>
                    <!-- /Button -->
                </div>
                <!-- Button Column -->

                <!-- /Row For Videos Section -->
            </div>

        </section>
    </div>

    <!-- Countdown Timer For Jquery -->
    <script type="text/javascript" src="{{ asset('js/jquery.countdown.js') }}"></script>

    <script>
		//Final Countdown Initialization
		$('[data-countdown]').each(function() {
			var $this = $(this), finalDate = $(this).data('countdown');
			$this.countdown(finalDate, function(event) {
				$this.html(event.strftime( ''
					+ '<div class="wd-countdown-cont"><span class="wd-countdown-title">%D</span> <span class="wd-countdown-sub-title">DAYS</span></div> '
					+ '<div class="wd-countdown-cont"><span class="wd-countdown-title">%H</span> <span class="wd-countdown-sub-title">HOURS</span></div>'
					+ '<div class="wd-countdown-cont"><span class="wd-countdown-title">%M</span> <span class="wd-countdown-sub-title">MINUTES</span></div>'
					+ '<div class="wd-countdown-cont"><span class="wd-countdown-title">%S</span> <span class="wd-countdown-sub-title">SECONDS</span></div>'
				));
			});
		});
		
		function toggleBrowseVideos()
		{
			$("#browseVideos").removeClass("addDisplayNone");
		}
		
    </script>

@endsection
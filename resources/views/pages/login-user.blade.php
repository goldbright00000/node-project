@extends ('layout')

@section('content')
{{ dd(session()->all()) }}
    <!--Container For The Form-->
    <div class="container wd-first-container">
        <!--Row For The Form-->
        <div class="row text-center wd-top-padding wd-bottom-padding">
            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Login To Get Started</h2>
            </div>
            <!-- Column For Form -->
            <div class="col-md-6 offset-md-3 col-sm-12">

                <form action="{{ route('login') }}">
                    @if (Request::has('previous'))
                    <input type="hidden" name="previous" value="{{ request()->previous }}">
                    @endif
                    <button type="submit" class="btn-lg btn-amber btn-block mt-5">LOGIN WITH EMAIL</button>
                </form>

                <p class="pt-5 pb-3 grey-text">or Login With</p>
            </div>
            <div class="col-sm-6 mb-3">
                <a href="{{ url('/login/facebook') }}">
                    <button type="button" class="btn btn-fb btn-block"><i class="fa fa-facebook pr-1"></i> Facebook</button>
                </a>
            </div>

            <div class="col-sm-6 mb-3">
                <a href="{{ url('/login/google') }}">
                    <button type="button" class="btn btn-gplus btn-block"><i class="fa fa-google-plus pr-1"></i> Google</button>
                </a>
            </div>

            <div class="col-md-6 offset-md-3 col-sm-12">
                <p class="py-3">
                    Don’t Have An Account Yet? <a href="{{ route('register') }}" class="amber-text font-weight-bold">Signup Here</a>
                </p>
            </div>
            <!-- /Column For Form -->
        </div>
        <!--/Row For The Form-->
    </div>
    <!--/Container For The Form-->

@endsection

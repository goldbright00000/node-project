@extends ('layout')


@section('content')

    <!--Container For The page-->
    <div class="container wd-first-container eventBinding" id="winGameContainer">

        <!--Row For The Heading Section -->
        <div class="row text-center wd-top-padding wd-bottom-padding">


            <!-- Column For heading -->
            <div class="col-md-10 offset-md-1 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading" data-step="1" data-intro="Lets Start Playing The Game. You Just Have To Guess The Position Of The Ball In
                    The Image">
                    Winners</h2>
                <p>An Archive Of The Past Winners Of All Competitions</p>
            </div>

            <div class="col-md-12 text-left mt-5">
                <!-- Section: Blog v.3 -->
                <section class="my-5">
                @foreach ($games as $game)

                    <!-- Grid row -->
                        <div class="row mt-5">
                            <!-- Grid column -->
                            <div class="col-lg-4 offset-lg-1 text-right">

                                <!-- Featured image -->
                                <div class="h-100 mb-4 text-right" style="width: 330px;">
                                    @if ($game->declareWinner_id != null)
                                        @foreach($winners as $winner)
                                            @if ($game->declareWinner_id == $winner->id)
                                                <iframe
                                                        width="320px"
                                                        height="186px"
                                                        src="https://www.youtube.com/embed/{{ $winner->video }}?controls=0"
                                                        frameborder="0"
                                                        allow="accelerometer;
												autoplay;
												encrypted-media;
												gyroscope;
												picture-in-picture"
                                                        allowfullscreen>
                                                </iframe>
                                            @endif
                                        @endforeach
                                    @else
                                        <div
                                                style="
                                                        height: 70%;
                                                        width: 100%;
                                                        background-image: url('{{ $game->imageUrl }}');
                                                        background-position: center center;
                                                        background-size: cover;
                                                        ">
                                            <p
                                                    style="
													font-size: 18px;
													text-align: center;
													color: #ffffff;
    												height: 100%;
    												display: block;
    												background-color: rgba(0,0,0,.7);
    												padding-top: 30px;
												">
                                                The results for this game will be announced
                                                on {{ dateToString($game->winnerDate)}}
                                            </p>
                                        </div>
                                    @endif
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>

                            </div>
                            <!-- Grid column -->

                            <!-- Grid column -->
                            <div class="col-lg-6">

                                <!-- Post title -->
                                <h3 class="font-weight-bold mb-3">
                                    <strong>
                                        {{ $game->name . ' played from ' . dateToString($game->startDate) . ' to ' . dateToString($game->endDate) }}
                                    </strong>
                                </h3>
                                <!-- Excerpt -->
                                <p class="dark-grey-text">{{ $game->description }}</p>

                            @php
                                if($game->declareWinner_id != null){
                                    $slug = $winners->where('id', '=', $game->declareWinner_id)->first()->slug;
                                }
                                else {
                                $slug = null;
                                }
                            @endphp

                            <!-- Read more button -->
                                @if($slug !== null)
                                    <a href="{{ route('winners.detail', $slug) }}"
                                       class="btn btn-primary btn-md btn-amber">View Details</a>
                                @endif
                            </div>
                            <!-- Grid column -->
                        </div>
                        <!-- Grid row -->
                        <div class="col-lg-10 offset-lg-1 mt-4 mb-3">
                            <hr/>
                        </div>
                    @endforeach


                </section>
                <!-- Section: Blog v.3 -->
            </div>
            <div class="col-lg-12 wd-pagination">
                <!--/Pagination amber-->
                {{ $games->links() }}
            </div>
        </div>

    </div>

@endsection
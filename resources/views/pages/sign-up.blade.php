@extends ('layout')

@section('content')

    <!--Container For The Form-->
    <div class="container wd-first-container">
        <!--Row For The Form-->
        <div class="row text-center wd-top-padding wd-bottom-padding">
            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Sign Up For An Account</h2>
            </div>
            <!-- Column For Form -->
            <div class="col-md-6 offset-md-3 col-sm-12 py-4">
                <form class="" action="" id="">
                    <input class="form-control form-control-lg my-4" type="text" placeholder="First Name *" required>
                    <input class="form-control form-control-lg my-4" type="text" placeholder="Last Name *" required>
                    <input class="form-control form-control-lg my-4" type="email" placeholder="Email Address *" required>
                    <input class="form-control form-control-lg my-4" type="email" placeholder="Confirm Email Address *" required>
                    <input class="form-control form-control-lg my-4" type="password" placeholder="Password *" required>
                    <p class="wd-very-small-text text-left mt-0">Minimum of 8 characters including at least 1 number, 1 capital letter and 1 special
                        character.</p>
                    <input class="form-control form-control-lg my-4" type="password" placeholder="Confirm Password *" required>
                    <div class="form-row mb-4">
                        <div class="col">
                            <!-- First name -->
                            <input type="text" id="defaultRegisterFormFirstName" class="form-control form-control-lg" placeholder="+1">
                            <p class="text-left text-sm-left">Country Code in Format +1</p>
                        </div>
                        <div class="col-9">
                            <!-- Last name -->
                            <input type="text" id="defaultRegisterFormLastName" class="form-control form-control-lg" placeholder="Mobile Number *">
                            <p class="text-left">Your Mobile Number</p>
                        </div>
                    </div>
                    <input class="form-check-input text-left my-4" type="checkbox" value="" id="wd-age-agreement" required>
                    <label class="form-check-label text-left" for="wd-age-agreement">
                        I AGREE THAT I AM OVER 18 YEARS OF AGE
                    </label>
                    <p class="text-left my-4">By creating your account, you agree to our <a href="" class="amber-text font-weight-bold">Terms and Conditions</a> & <a
                                href="" class="amber-text font-weight-bold">Privacy
                            Policy.</a>
                    </p>
                    <p class="text-left my-4">We do not share your details with any third parties for them to market their products/services to you.</p>
                    <input class="form-check-input text-left my-4" type="checkbox" value="" id="wd-news-signup">
                    <label class="form-check-label " for="wd-news-signup">
                        We would like to keep in touch with you about our products and services. If you do not want to hear from us please tick the box.
                    </label>
                    <button type="submit" class="btn-lg btn-amber btn-block mt-5" value="SIGN UP" id="wd-signup-form-submit">SIGN UP</button>
                </form>
                <!-- /Column For Form -->
            </div>
            <!--/Row For The Form-->
        </div>
        <!--/Container For The Form-->
    </div>


@endsection
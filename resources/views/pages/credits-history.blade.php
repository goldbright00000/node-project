@extends ('layout')

@section('content')


    <!--Container For The page-->
    <div class="container wd-first-container">
        <!--Row For The Personal Details -->
        <div class="row text-center wd-top-padding wd-bottom-padding">

            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Credits History</h2>
            </div>


            <div class="col-md-6 offset-md-3 col-sm-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-md-6 offset-md-3 mt-5 col-sm-12">
                @if ( session('success') )
                    <div class="alert alert-success">{{ session('success') }}</div>
                @endif
                {{ session()->forget('success') }}
            </div>

            <div class="col-md-8 mt-5 wd-grey-backgound">
                <h4 class="mt-5">Refer Your Friends And Earn Credits</h4>
                <p> Refer new friends and get credited for each referal. Earn Credits and keep playing</p>
                <div class="row">
                    <div class="col-10 offset-md-1">

                        <form
                            class="needs-validation"
                            id="inviteUsers"
                            method="post"
                            action="{{ route('referral.store') }}"
                        >
                            @csrf

                                <div class="md-form mt-3">
                                    <label for="name">Name</label>
                                    <input  name="name" type="text" id="name" class="form-control">
                                </div>
                                <div class="md-form mt-3">
                                    <label for="email">Email</label>
                                    <input name="email" type="email" id="email" class="form-control">
                                </div>

                                <div class="md-form">
                                    <textarea
                                        type="text"
                                        id="message"
                                        name="message"
                                        class="md-textarea form-control"
                                        rows="3"></textarea>
                                    <label for="message">Personal Message</label>
                                </div>

                                <button
                                    type="submit"
                                    class="btn btn-amber mt-2 mb-5"
                                    id="wd-signup-form-submit">
                                    Invite Now
                                </button>
                        </form>

                    </div>
                </div>
            </div>

            <div class="col-md-3 offset-md-1 mt-5 text-center">
                <div class="row">
                    <div class="col-12 wd-credits-heading py-2">Total Credits In Your Account</div>
                    <div class="col-12 wd-credits-counter wd-grey-backgound ">{{ $user->initialDiscount }}</div>

                    <div class="col-12 wd-credits-heading py-2 mt-5">How Does It Work</div>
                    <div class="col-12 wd-grey-backgound ">
                        <ul class="mt-3 text-left">
                            <li>Enter the name and correct email ID of your friend</li>
                            <li>Your friend will get an email</li>
                            <li>Your friend would then need to accept the invitation through register/ signing up to the website.</li>
                            <li>Once your friend register/ sign up, you will get credit to your account.</li>
                            <li>Enjoy playing</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <ul class="nav nav-tabs md-tabs nav-justified orange" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#sent" role="tab">Sent Requests</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#accepted" role="tab">Accepted Requests</a>
                    </li>

                </ul>
                <!-- Tab panels -->
                <div class="tab-content">


                    <!--Panel 1 Sent Requests -->
                    <div class="tab-pane  show active" id="sent" role="tabpanel">

                        <table id="dtsentRequest" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th class="th-sm">Name
                                </th>
                                <th class="th-sm">Email
                                </th>
                                <th class="th-sm">Sent On
                                </th>
                                <th class="th-sm text-center">Current Status
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sentRequests as $sentRequest)
                                <tr>
                                    <td>{{ $sentRequest->name }}</td>
                                    <td>{{ $sentRequest->email }}</td>
                                    <td>{{ $sentRequest->created_at->toFormattedDateString() }}</td>
                                    <td class="text-center">
                                        <a href="" class="btn btn-sm btn-red" >
                                            Pending
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!--/.Panel 1-->



                    <!--Panel Accepted Requests-->
                    <div class="tab-pane fade" id="accepted" role="tabpanel">

                        <table id="dtacceptedPayments" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="th-sm">Name
                                    </th>
                                    <th class="th-sm">Email
                                    </th>
                                    <th class="th-sm">Sent On
                                    </th>
                                    <th class="th-sm text-center">Current Status
                                    </th>
                                </tr>

                            </thead>
                            <tbody>

                            @foreach($acceptedRequests as $acceptedRequest)
                                <tr>
                                    <td>{{ $acceptedRequest->name }}</td>
                                    <td>{{ $acceptedRequest->email }}</td>
                                    <td>{{ $acceptedRequest->accepted_on->toFormattedDateString() }}</td>
                                    <td class="text-center">
                                        <a href="" class="btn btn-sm btn-green" >
                                            Accepted
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>
                    <!--/.Panel 2-->


                </div>


            </div>
        </div>
    </div>
    <!--/Row For The Personal Details -->





@endsection

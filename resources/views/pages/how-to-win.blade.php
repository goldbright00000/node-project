@extends ('layout')

@section('content')

    <!--Container For The page-->
    <div class="container wd-first-container wd-top-padding">
        <!--Row For The Personal Details -->
        <div class="row text-center wd-top-padding wd-bottom-padding">

            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">How To Win </h2>
            </div>

            <!-- Column For Cards -->
            <div class="col-md-4 wd-top-padding">
                <!-- Card -->
                <div class="card pb-4">

                    <!-- Card image -->
                    <i class="fas fa fa-shopping-bag pt-5 pb-3" style="font-size: 150px;"></i>

                    <!-- Card content -->
                    <div class="card-body">

                        <!-- Title -->
                        <h5 class="card-title"><a>SELECT TICKETS FOR THE PRODUCT YOU WISH TO WIN
                            </a></h5>
                        <!-- Text -->
                        <p class="card-text">Multi product categories to choose from</p>


                    </div>

                </div>
                <!-- Card -->
            </div>
            <!-- /Column For Cards -->


            <!-- Column For Cards -->
            <div class="col-md-4 wd-top-padding">
                <!-- Card -->
                <div class="card pb-4">

                    <!-- Card image -->
                    <i class="fas fa  fa-crosshairs pt-5 pb-3" style="font-size: 150px;"></i>

                    <!-- Card content -->
                    <div class="card-body">

                        <!-- Title -->
                        <h5 class="card-title"><a>PLAY "SPOT THE BALL" WITH EACH TICKET
                            </a></h5>
                        <!-- Text -->
                        <p class="card-text">Use your analytical skills to determine the centre of the ball</p>


                    </div>

                </div>
                <!-- Card -->
            </div>
            <!-- /Column For Cards -->

            <!-- Column For Cards -->
            <div class="col-md-4 wd-top-padding">
                <!-- Card -->
                <div class="card pb-4">

                    <!-- Card image -->
                    <i class="fas fa fa-gavel pt-5 pb-3" style="font-size: 150px;"></i>

                    <!-- Card content -->
                    <div class="card-body">

                        <!-- Title -->
                        <h5 class="card-title"><a>THE COMPETITION IS JUDGED
                            </a></h5>
                        <!-- Text -->
                        <p class="card-text">The closest to the judges position wins their destiny product</p>


                    </div>

                </div>
                <!-- Card -->
            </div>
            <!-- /Column For Cards -->

        </div>
        <!-- /Row For The Personal Details -->
    </div>

@endsection
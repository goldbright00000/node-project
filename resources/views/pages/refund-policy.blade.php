@extends ('layout')

@section('content')

    <!--Container For The page-->
    <div class="container wd-first-container my-5 wd-bottom-padding wd-top-padding">

        @markdown
        ### Refund Policy

        Please refer to the terms and conditions for details related to  Win Your Destinay competitions


        For any refund claim please liaise WYD on info@winyourdestiny.com ; and somebody from WYD will contact you to understand the grivience and to take the necessary corrective. The companhy holds the right to determine the corrective action which may include refund of money or providing credit points


        @endmarkdown

    </div>

@endsection
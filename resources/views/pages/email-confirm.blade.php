@extends ('layout')

@section('content')

    <!--Container For The Form-->
    <div class="container wd-first-container">
        <!--Row For The Form-->
        <div class="row text-center wd-top-padding wd-bottom-padding">
            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Thank You For Signing Up!</h2>
                <h5>You need to confirm your email account before you can play the game. Please check your email account for the
                    confirmation link.</h5>
            </div>

        </div>
        <!--/Row For The Form-->
    </div>
    <!--/Container For The Form-->

@endsection

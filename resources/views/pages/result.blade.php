@extends ('layout')

@section('content')

    <section class="container wd-bottom-padding wd-top-padding my-5 ">
        <div class="col-md-6 offset-md-3 mb-4 text-center">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            <a class="btn btn-primary btn-amber" href="{{ route('product-listing') }}">Play More</a>
                {{ session()->forget('success') }}

            @elseif (session('failure'))
                <div class="alert alert-danger">
                    {{ session('failure') }}
                </div>
                {{ session()->forget('failure') }}
            @endif
        </div>
    </section>

@endsection
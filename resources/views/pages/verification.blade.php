@extends ('layout')

@section('content')

    <!--Container For The Form-->
    <div class="container wd-first-container">
        <!--Row For The Form-->
        <div class="row text-center wd-top-padding wd-bottom-padding">
            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Please Confirm Your Email</h2>
                <h5>You need to confirm your email account before you can proceed. We have already sent you a confirmation link, please check your email account for the same.</h5>
                <p>If you need to generate a new link please click on the button below to proceed.</p>
                <a class="btn btn-primary btn-amber" href="{{ route('resend.verification') }}">Resend Email</a>
            </div>

        </div>
        <!--/Row For The Form-->
    </div>
    <!--/Container For The Form-->

@endsection


@extends ('layout')
@section('content')
    <style type="text/css">
        .round-loope {border:4px solid #cccccc !important;}
    </style>
	{{--    	{{ dd(session()->all()) }}--}}
	
	@if(count($stampedProducts)==0)
	{{-- @if(empty(\Cart::content())) --}}
        <div class="container">
            <div class="row my-5">
                <div class="col-12 my-5">
                    <div class="row my-5">
                        <div class="col-lg-6 offset-lg-3 my-5 text-center alert-danger">
							<h5 class="p-5">PSSSSST!! Your
                                Cart Is Empty. Please Add A Few Tickets To Your Cart To Start Playing And To
                                WinYourDestiny</h5></div>
                    </div>
                </div>
            </div>
        </div>

	@else	
<script>
</script>

        <div id="ajaxSpinner"></div>
        <!-- / Closing the spinner tag  -->

        <!--Container For The page-->
        <div class="container wd-first-container eventBinding" id="winGameContainer">

            <!--Row For The Heading Section -->
            <div class="row text-center wd-top-padding wd-bottom-padding">
                <!-- Column For heading -->
                <div class="col-md-10 offset-md-1 col-sm-12">
                    <span class="wd-heading-separator"></span>
                    <h2 class="wd-black-heading" data-step="1" data-intro="Lets Start Playing The Game. You Just Have To Spot The Position Of The Ball In
                    The Image">
                        Spot The Location Of The Ball</h2>
                    <p>Using your skill, place a marker for each ticket in the place where you think the middle of the
                        ball
                        should be.</p>
                </div>

                <!-- /Column For The Game Play Area -->

                <div id="acab" class="eventBinding">
						<span id="offsetDisplay" style="
						color: #ffffff;
						background-color: rgba(0,0,0,0.8);
						padding:8px 10px;"
                        ></span>
                </div>

                <!-- Column For Small Device -->
                <div class="col-lg-12 setDisplayFlex d-lg-none mt-4 mb-5"
                     style="">

                    <div class="container">
                        <div class="row" id="gameRow">
                            <div class="col-lg-12 wd-playing-tools"
                                data-toggle="tooltip"
                                data-placement="top"
                                data-intro="You can Use These Tools To Make Spot The Position Of The Ball"
                                data-step="2"
                                data-tooltipClass="secord-tooltip"
                                style="height:500px; position: relative;">
                            <h4 class="text-white mt-4">TOOLS</h4>
                            <div class="my-4 wd-tools-edit text-center "
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Edit Tool To Select Colors Of Guides"
                                    data-step="3"
                                    data-intro="Use Pencil Tool To Draw Guides And Select The Color Of Guides"
                                    id="wdGameTools">
                                <i class="fa fa-pencil wd-tool-icons wd-spl-tool"></i>
                            </div>
                            <div id="wd-tools-reset"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Roll back Previous Changes"
                                    data-step="4"
                                    data-intro="Roll Back The Last Draw Line Using The Roll Back Tool"
                                    class="my-4 wd-tools-reset">
                                <i class="fa fa-undo wd-tool-icons"></i>
                            </div>
                            <div
                                    id="wd-tools-hide"
                                    data-step="5"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Toggle Display | Hide Guides"
                                    data-intro="Toggle To Display And Hide The Guides"
                                    class="my-4 wd-tools-hide ">
                                <i class="fa fa-eye wd-tool-icons wd-spl-tool"></i>
                            </div>
                            <div id="wd-tools-clear"
                                    data-step="6"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Delete All Guides From The Play Area"
                                    data-intro="Erase All The Guides Drawn By You"
                                    class="my-4 wd-tools-clear">
                                <i class="fa wd-tool-icons fa-times"></i>
                            </div>
								
                                <div class="my-4 wd-tools-information "
                                     data-toggle="modal"
                                     data-target="#myModal"
                                     id="wdLastTool">
                                    <i class="fa wd-tool-icons fa-info"></i>
                                </div>
								<label class="switch" data-step="6"
                                     data-toggle="tooltip"
                                     data-placement="top" title="Magnifying Glass">
								  <input type="checkbox" class="maginifer_status" checked>
								  <span class="slider round"></span>
								</label>


                                <!-- The Modal -->
                                <div class="modal" id="myModal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h4 class="modal-title">Please Note:</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;
                                                </button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <p>Exact pixel mapping can be viewed through the magnifier. Other (lower
                                                    resolution) views are for reference
                                                    only, and the exact coordinates you choose will be recorded in your
                                                    basket and in your order.</p>

                                                <p>The behaviour of the line drawing tools may not be consistent across
                                                    all
                                                    devices, browsers and screen
                                                    resolutions, and are provided only to assist with your coordinate
                                                    selection on desktop (non-touch
                                                    screen) devices.</p>
                                            </div>

                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-amber" data-dismiss="modal">Close
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div id="wd-tools-container" class="wd-tools-container wdGameToolsHide">
                                    <a id="paintRed"><img src="{{ asset('images/red.png') }}" class="my-2 colorActive"/></a>
                                    <a id="paintMagenta"><img src="{{ asset('images/magento.png') }}" class="my-2"/></a>
                                    <a id="paintPurple"><img src="{{ asset('images/purple.png') }}" class="my-2"/></a>
                                    <a id="paintBlue"><img src="{{ asset('images/blue.png') }}" class="my-2"/></a>
                                    <a id="paintGreen"><img src="{{ asset('images/green.png') }}" class="my-2"/></a>
                                    <a id="paintYellow"><img src="{{ asset('images/yellow.png') }}" class="my-2"/></a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-10 win-crosshair-over" class="eventBinding">

                        @if (countStampedProducts() == true)
                            <div id="preventermobile" class="preventer">
								<script>
									$(".preventer").css({"background-image": "none" ,"background-color": "black","opacity":"0.5"});
								
								</script>
                            </div>
                        @else
                            <div id="preventermobile" class="d-none">You Have Played All Tickets In Your Cart. Please Add More
                                Tickets Or Proceed To Checkout
                            </div>
                        @endif

                        <div id="gameBackImage">
                            <div id="canvasGame" class="eventBinding">
                                <div id="findMagnifier" class="magnifier-thumb-wrapper">
                                    <img class="zoom_04" src="{{ asset('/storage/') . '/' . $gamePlay->imageUrl}}"
     data-zoom-image="{{ asset('/storage/') . '/' . $gamePlay->imageUrl}}" style="height:500px;width:895px;"/>

                                    <img class="zoom_05 d-none" src="{{ asset('/storage/') . '/' . $gamePlay->imageUrl}}"
     data-zoom-image="{{ asset('/storage/') . '/' . $gamePlay->imageUrl}}" style="height:500px;width:895px;"/>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /Column For Small Device Error Message -->
                @if(countStampedProducts() == true)
                    <div class=" col-8 offset-2 alert-danger py-4"> You Have Played All The Tickets. Please add more
                        tickets to play or proceed to checkout
                    </div>
            @endif
            <!-- New Slider Starts -->
                <div class="col-md-12 mt-5 d-lg-flex <!--d-none-->" id="winTicketSlider">
                   
                    <ul id="ticketsSlider" class="">                        
                        @foreach($stampedProducts as $product)
                            @if ($product['stamped'] == null)
                                <li id="{{ $product['stamp'] }}" class="unStamped">
                            @else
                                <li id="{{ $product['stamp'] }}"
                                    data-xcord="{{ (intval($product['stamped']->xCords)) + 8  }}"
                                    data-ycord="{{ (intval($product['stamped']->yCords)) - 10 }}"
                                    class="stamped">
                                    @endif

                                    <div class="card mb-2 pt-2 slider-cardheight" style="">
                                        <img class="card-img-top mx-auto" style="max-width: 100px;"
                                             src="{{ asset('/storage/') . '/' . $product['image'] }}">
                                        <div class="card-body">
                                            <h6 class="card-title mb-1 ">{{ $product['name'] }}</h6>
                                            <table class="table mb-0 table-borderless">
                                                <tbody>
                                                <tr class="">
                                                    @if ($product['stamped'] == null)
                                                        <td class="font-weight-bold pt-2 grey-text playingText"><i
                                                                    class="fa fa-check mr-2 "></i>To Play
                                                        </td>
                                                    @else
                                                        <td class="font-weight-bold pt-2 red-text playingText">
                                                            {{ 'X: '. intval($product['stamped']->xCords) . ' | ' }}
                                                            {{ 'Y: '. intval($product['stamped']->yCords) }}
                                                        </td>
                                                    @endif


                                                    <td class="font-weight-bold pt-2">
                                                        <form method="post" action=" {{ route('pointer.remove') }} ">
                                                            @csrf
                                                            <input type="hidden" name="productId"
                                                                   value="{{  $product['productID'] }}">
                                                            <input type="hidden" name="stampId"
                                                                   value=" {{ $product['stamp'] }}">
                                                            <input type="hidden"  name="rowId" value="{{  $product['rowId'] }}">

                                                            <button class="wd-remove-ticket-button" type="submit">
                                                                <i class="fa fa-times mr-2"></i>
                                                                Remove
                                                            </button>
														@if ($product['stamped'] != null)
															<br>
                                                            <a class="wd-remove-ticket-button" href="{{ route('pointer.destroyTicket', $product['stamped']->id) }}">
																<i class="fa fa-repeat mr-2"></i>
																Replay
															</a>
                                                            
														@endif
                                                        </form>
                                                    </td>

                                                </tr>
                                                </tbody>
                                            </table>
											
                                        </div>
                                        <p class="text-center text-success">Ticket {{$loop->index + 1}}
                                            Of {{$loop->count}}</p>
                                    </div>


                                    <form id="{{ $product['stamp'].'form' }}" method="get" style="display: none;">
                                        <input type="hidden" id="{{ $product['stamp'] . 'xCords'}}" name="xCords"
                                               placeholder="xCords">
                                        <input type="hidden" id="{{ $product['stamp'] . 'yCords' }}" name="yCords"
                                               placeholder="yCords">
                                        <input type="hidden" id="{{ $product['stamp'] . 'productID' }}" name="productID"
                                               placeholder="productID" value="{{ $product['productID'] }}">
                                        <input type="hidden" name="rowId" id="{{ $product['stamp'] . 'rowID' }}" value="{{  $product['rowId'] }}">
                                        <input type="hidden" id="stampId" name="stampId" placeholder="stampId"
                                               value="{{ $product['stamp'] }}">
                                        <button id="cordsSubmit" type="submit"></button>
                                    </form>

                                </li>
                                @endforeach
                    </ul>
                </div>
                <!-- / New Slider Ends -->

            </div>


            <!-- Wrapper For Buttons -->
            <div class="mb-5 row"> <!-- d-none d-lg-flex-->
                <!-- Column For Add Tickets Button -->
                <div class="col-md-6">
                    <a href="{{ route('product-listing') }}"
                       class="btn btn-block btn-dark btn-primary text-uppercase  btn-lg">
                        <i class="fa fa-chevron-left mr-4"></i>
                        ADD MORE TICKETS
                    </a>
                </div>
                <!-- /Column For Add Tickets Button -->

                <!-- Column For Checkout Button -->
                <div class="col-md-6 ">
                    @if (\Cart::count() > 0)
                        <form action="{{ route('checkout.index') }}">
                            <input
                                    type="hidden"
                                    name="previous"
                                    value="{{ route('checkout.index') }}">

                            @if (countStampedProducts() == true)
                                <button
                                        class="btn btn-block btn-amber btn-primary text-uppercase text-uppercase btn-lg"
                                        style="cursor: pointer"
                                        type="submit">
                                    Proceed To Checkout
                                    <i class="fa fa-chevron-right ml-4"></i>
                                </button>
                            @else
                                <button
                                        class="btn btn-block btn-amber disabled btn-primary text-uppercase text-uppercase btn-lg"
                                        style="cursor: pointer"
                                        id="playCheckout"
                                        type="submit">
                                    Proceed To Checkout
                                    <i class="fa fa-chevron-right ml-4"></i>
                                </button>
                                <p id="playCheckoutChild" class="text-center grey-text">Please Play All Tickets To
                                    Checkout</p>
                            @endif
                        </form>
                    @endif
                </div>
                <!-- /Column For Checkout Button -->

            </div>
            <!-- /Wrapper For Buttons -->

        </div>

        <!--/Row For The Heading Section -->

        <!--/Container For The page-->
        @foreach($stampedProducts as $item)
            @if($item['stamped'] !=null)
                <script>
					$('.win-crosshair-over').append(
						$('<span> <i class="fas fa fa-crosshairs"></i></span>').css({
							color: 'red',
							position: 'absolute',
							'z-index': '999',
							top: '{{ (($item['stamped']->yCords)/8) - 8 . 'px'}}',
							left: '{{ (($item['stamped']->xCords)/8) + 8 . 'px'}}',
						})
					);
                </script>
            @endif
        @endforeach

        <script src="{{ asset('js/zoomsl.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/game-core.js') }}"></script>
		<script type="text/javascript">
			function toggleMagnifier()
			{
				//$("#findMagnifier").toggleClass("addDisplayNone");
			}

        </script>

        <!-- Setting For LightSlider -->
        <script type="text/javascript">
			var slider;
                    @if ($stampedProducts->count() > 4)
			var itemCount = 4;
                    @else
			var itemCount = ("{{ ($stampedProducts->count()) }}");

            @endif
			$(document).ready(function () {

                // $('.maginifer_status').click(function(){
                //     if($(this).prop("checked") == true){   
                    
                //     }
                //     else if($(this).prop("checked") == false){
                //         console.log('check true')         
                    
                //     }
                // });

                $(".zoom_04").imagezoomsl({
                    innerzoommagnifier: true,
                    classmagnifier: window.external ? "round-loope" : "", 
                    magnifierborder: "5px solid #000000",                 
                    zoomrange: [2, 2],
                    zoomstart: 2,
                    magnifiersize: [70, 70],
                    cursorshade: true,
                    magnifycursor: 'crosshair',
                    statusdivborder: '5px solid #000000',
                });
				slider = $("#ticketsSlider").lightSlider({
					item:itemCount,
					slideMove: 1,
					slideMargin: 15,
					mode: "slide",
					useCSS: true,
					cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
					easing: 'linear', //'for jquery animation',////
					speed: 400, //ms'
					auto: false,
					loop: false,
					slideEndAnimation: true,
					keyPress: false,
					controls: true,
					prevHtml: '<span style="padding:15px 8px; background-color:rgba(0,0,0,0.7)"><i style="color: #ffffff;" class="fas fa fa-chevron-left"></i></span>',
					nextHtml: '<span style="padding:15px 8px; background-color:rgba(0,0,0,0.7)"><i style="color: #ffffff;" class="fas fa fa-chevron-right"></i></span>',
					enableTouch: false,
					enableDrag: false,
					pager: false,
				});
			});

        </script>

        <script>
			// Important Variables For a Global Scope
			var playedOnce = false;
			var stamp = null;
			var stampFromSession = null;
			var nextPosition = null;
			var imagePath = " {{ asset('/storage/') . '/' }}";
			var removeRoute = '{{ route('pointer.remove') }}';
			var magnifyImage = '{{ asset('/storage/') . '/' . $gamePlay->imageUrl}}';
			var stampedCount = '{{ $stampedProducts->count() }}';
			var zoom = '{{ FindCurrentGame()->imageMultiplier }}';

			// Function to set the global variable for checking playing slide count
			$(document).ready(function () {
				$('.eventBinding').on('click', function () {
					if (playedOnce == false) {
						stamp = $('#ticketsSlider').find('.unStamped').first().attr('id');
					} else {
						stamp = stampFromSession;
					}
				});
			});


			// Function to Add A Marker to the Image OnClick and process further click events
			$(document).ready(function () {
				$('.win-crosshair-over').on('click', '.eventBinding', function (e) {
					//debugger;
					$('#canvasGame svg').css({'z-index': '1111', 'margin-left': '-30px'});
					// Get the coordinates and mouse position based on user click
					var offset = $(this).offset();
					x = ((event.pageX - offset.left));
					y = ((event.pageY - offset.top));
				
					zoomx = x * zoom;
					zoomy = y * zoom;
					
					//debugger;
					if ($('#canvasGame').hasClass('canvasActive') == false && zoomy < 3950 && zoomy > 64 && zoomx < 7100 && zoomx > 64) {
						// Create a corsshair mark on the image where the user clicked
                        //debugger;
                        
						$('.win-crosshair-over').append(
							$('<span> <i class="fas fa fa-crosshairs"></i></span>').css({
								color: 'red',
								position: 'absolute',
								'z-index': '999',
								top: (y - 10) + 'px',
								left: (x + 8) + 'px',
							})
                        );
                        
                        console.log('green')

						// Trigger the AJAX Call To The Server
						ajaxCall(stamp, x, y);

						// Reset the Played Once Variable to True
						playedOnce = true;

					} else if ($('#canvasGame').hasClass('canvasActive') == false && (zoomy > 3950 || zoomy < 64 || zoomx > 7100 || zoomx < 64)) {
						//debugger;
						alert('Sorry You Either Trying To Place A Coordinate Outside the playing Area, Place Your Pointer Within The Playing Area. ')
					} else if ($('#canvasGame').hasClass('canvasActive') == true) {
                        
						//debugger;
                        alert('The Guides or any of other tools are enabled on the playing area. Please disable the same to place your pointer');
          
					}
				})
                
			});

            
			// Function to trigger an AJAX call
			function ajaxCall(stamp, xCords, yCords) {
                console.log(jQuery('#' + $.trim(stamp) + 'rowID').val());
				jQuery('#ajaxSpinner').addClass('ajaxSpinnedShow');
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				jQuery.ajax({
					url: "{{ route('pointer.store') }}",
					method: 'get',
					data: {
						xCords: xCords,
						yCords: yCords,
						stampId: stamp,
						productID: jQuery('#' + $.trim(stamp) + 'productID').val(),
                        rowId: jQuery('input[name="rowId"]').val()
					},
					success: function (result) { // After Successful Execution of the AJAX Call
						stampFromSession = result.nextStamp;
						// Remove All Old HTML
						jQuery('#ticketsSlider li').remove();

						// Set the base Ticket number
						var ticketNumber = 1;
						var differenceTickets = (4 - stampedCount);
						nextPosition = result.nextPosition;

						// Calculate the card width based on the stamp count
						var cardWidth;
						if (stampedCount > 4) {
							cardWidth = '266.25px';
						} else {
							cardWidth = '225px';
						}

						// console.log('This is the Stamp Count -> ' + differenceTickets);

						$.each(result.stampedProducts, function (key, data) {

							if (data.stamped == null && key === nextPosition) {
								var html = '<li id="' + data.stamp + '" class="unStamped unStampedActive active " style="width:' + cardWidth + '; margin-right: 15px;" >';
							} else if (data.stamped == null && key !== nextPosition) {
								var html = '<li id="' + data.stamp + '" class="unStamped" style="width:' + cardWidth + '; margin-right: 15px;" >';
							} else if (data.stamped != null) {
								var html = '<li id="' + data.stamp + '" data-xcord="' + data.stamped.xCords + '"' +
									' data-ycord=" ' + data.stamped.yCords + ' " class="stamped" style="width:' + cardWidth + '; margin-right: 15px;">';
							}
							html += '<div class="card mb-2 pt-2 slider-cardheight" style="">';
							html += '<img class="card-img-top mx-auto" style="max-width: 100px;" src="' + imagePath + data.image + '" >';
							html += '<div class="card-body">';
							html += '<h6 class="card-title mb-1">' + data.name + '</h6>';
							html += '<table class="table mb-0 table-borderless">';
							html += '<tbody>';
							html += '<tr class="">';
							if (data.stamped == null) {
								html += '<td class="font-weight-bold pt-2 grey-text playingText"><i class="fa fa-check mr-2 "></i>To Play</td>';
							} else if (data.stamped != null) {
								html += '<td class="font-weight-bold pt-2 red-text playingText">' +
									'X: ' + Math.round(data.stamped.xCords) + ' | Y: ' + data.stamped.yCords +
									'</td>';
							}
							
							html += '<td class="font-weight-bold pt-2">' +
								'<form method="post" action="' + removeRoute + '">' +
								'<input type="hidden"  name="_token"  value="' + $('meta[name="csrf-token"]').attr('content') + '">' +
								'<input type="hidden" name="productId" value="' + data.productID + '">' +
								'<input type="hidden" name="stampId" value="' + data.stamp + '">' +
                                '<input type="hidden" name="rowId" value="'+data.rowId+'">'+
								'<button class="wd-remove-ticket-button" type="submit">' +
								'<i class="fa fa-times mr-2"></i>' +
								'Remove' +
								'</button>';
							if (data.stamped != null) {
                                var route_val="{{ url('/') }}/pointer/destroyTicket/"+data.stamped.id;
                                html += '<br><a class="wd-remove-ticket-button" href="'+route_val+'">' +
								'<i class="fa fa-repeat mr-2"></i>' +
								'Replay' +
								'</a>';
							}
							html +=	'</form>' +
								'</td>';

							html += '</td>';
							html += '</tr>';
							html += '</tbody>';
							html += '</table>';
							/* if (data.stamped != null) {
								html += '<a href="/pointer/destroyTicket/'+data.stamp+'"><i class="fa fa-repeat wd-tool-icons"></i></a>';
							} */ 
							html += '</div>';
							html += '<p class="text-center">Ticket ' + (ticketNumber++) + ' of ' + result.stampedProducts.length + ' </p>';
							html += '</div>';
							html += '<form id="' + data.stamp + 'form' + '" method="get" style="display: none;">';
							html += '<input type="hidden" id="' + data.stamp + 'xCords' + '" name="xCords" placeholder="xCords">';
							html += '<input type="hidden" id="' + data.stamp + 'yCords' + '" name="yCords" placeholder="yCords">';
							html += '<input type="hidden" id="' + data.stamp + 'productID' + '" name="productID" placeholder="productID" value="' + data.productID + '">';
							html += '<input type="hidden" id="stampId" name="stampId" placeholder="stampId" value="' + data.stamp + '">';
							html += '<button id="cordsSubmit" type="submit"></button>';
							html += '</form>';
							html += '</li>';

							// Append new HTML
							jQuery('#ticketsSlider').append(html);

						});


						// Clear the HTML within the sidebar navigation
						jQuery('#sideSlide').empty();

						$.each(result.cart, function (key, data) {
                            
							// Declare Local Variables to Generate The Sidebar
							var sideHtml = null;

							// Product Detail Route Declaration
							var route = 'product-detail' + data.options.attributes.product_attr.slug;
							//var price = ((data.price * data.quantity) - (data.qty * data.conditions.parsedRawValue));
							var price = data.price*data.qty;
							var productID = data.id;  
                            var rowID = data.rowId;

							// Generating HTML for The Sidebar
							sideHtml += '<div class="row" id="sideSlideContent">';
							sideHtml += '<div class="col-md-12  wd-cart-element text-center">';
							sideHtml += '<div class="row mt-5">';
							sideHtml += '<div class="col-12">';
							sideHtml += '<h5 class="dark-grey-text mt-2">' + data.name;
							sideHtml += '</h5>';
							sideHtml += '<div class="table-responsive">';
							sideHtml += '<table class="table" style="border:0px;">';
							sideHtml += '<tbody>';
							sideHtml += '<tr>';
							sideHtml += '<td>';
							sideHtml += '<a href="' + route + '" class="">';
							sideHtml += '<img src="' + imagePath + data.options.attributes.product_attr.image + '" style="width:70px">';
							sideHtml += '</a>';
							sideHtml += '</td>';
							sideHtml += '<td>';
							sideHtml += '<p class="text-center dark-grey-text mt-3">QTY: ' + data.qty + '</p>';
							sideHtml += '</td>';
							sideHtml += '<td class="align-middle dark-grey-text">Total <span class="font-weight-bold">$ ' + price + ' </span></td>';
							sideHtml += '</tr>';
							sideHtml += '</tbody>';
							sideHtml += '</table>';
							sideHtml += '</div>';
							sideHtml += '</div>';
							sideHtml += '<span class="mb-03" style="width: 100%;">';

							$.each(result.stampedProducts, function (key, data) {

								// var remove Route
								var removeRoute = "{{ route('pointer.remove') }}";
								if (productID == data.productID && data.stamped != null) {
									sideHtml += '<div class="col-10 offset-1 mb-1 wp-mark-points-sidebar">';
									sideHtml += '<p style="display: inline;" class="dark-grey-text mt-1 mb-1">' +
										data.name + ' X:' + data.stamped.xCords + ' | Y:' + data.stamped.yCords;
									sideHtml += '</p>';
									sideHtml += '<form method="post" class="wd-side-remove-form" action="' + removeRoute + '">' +
										'<input type="hidden"  name="_token"  value="' + $('meta[name="csrf-token"]').attr('content') + '">' +
										'<input type="hidden" name="productId" value="' + data.productID + '">' +
                                        '<input type="hidden" name="rowId" value="' + data.rowId + '">'+
										'<input type="hidden" name="stampId" value="' + data.stamp + '">' +
										'<button class="wd-side-remove" type="submit">' +
										'<i class="fas fa fa-trash"></i>' +
										'</button>' +
										'</form>';
									sideHtml += '</div>';
								} else if (productID == data.productID && data.stamped == null) {
									sideHtml += '<div class="col-10 offset-1 mb-1 wp-mark-points-sidebar">';
									sideHtml += '<p style="display: inline;" class="dark-grey-text mt-1 mb-1">' +
										'<i class="fa fa-check mr-2 "></i> To Play';
									sideHtml += '</p>';
									sideHtml += '<form method="post" class="wd-side-remove-form" action="' + removeRoute + '">' +
										'<input type="hidden"  name="_token"  value="' + $('meta[name="csrf-token"]').attr('content') + '">' +
										'<input type="hidden" name="productId" value="' + data.productID + '">' +
										'<input type="hidden" name="rowId" value="' + data.rowId + '">' +
										'<input type="hidden" name="stampId" value="' + data.stamp + '">' +
										'<button class="wd-side-remove" type="submit">' +
										'<i class="fas fa fa-trash"></i>' +
										'</button>' +
										'</form>';
									sideHtml += '</div>';
								}
							});
							sideHtml += '</span>';
							sideHtml += '</div>';
							sideHtml += '</div>';
							sideHtml += '</div>';

							// Append new HTML
							jQuery('#sideSlide').append(sideHtml);
						});


						// Declaring a variable for Checkout Route
						var checkoutRoute = "{{ route('checkout.index') }}";

						var afterSide = '<div class="row">';
						afterSide += '<div class="col-12 text-center my-5">';

						// Check If all the tickets have been stamped or not
						if (result.allProductsStamped == true) {
							afterSide += '<a href="' + checkoutRoute + '" class="btn btn-amber btn-primary text-uppercase">PROCEED TO CHECKOUT</a>';
						} else {
							afterSide += '<a type="submit" class="btn disabled btn-amber btn-primary text-uppercase">PROCEED TO CHECKOUT</a>';
						}


						afterSide += '</div>';
						afterSide += '</div>';

						jQuery('#sideSlide').append(afterSide);

						// Add Preventer If all tickets have been stamped
						if (result.allProductsStamped == true) {
							$('#preventer').addClass('preventer');
							$('#preventer').removeClass('d-none');
							$('#findMagnifier').remove();
							$('#playCheckout').removeClass('disabled');
							$('#playCheckoutChild').addClass('d-none');
						}
						if (result.allProductsStamped == true) {
							$('#preventermobile').addClass('preventer');
							$('#preventermobile').removeClass('d-none');
							$('#findMagnifier').remove();
							$('#playCheckout').removeClass('disabled');
							$('#playCheckoutChild').addClass('d-none');
						}

						// Focus and move the slider to the correct slide
						if(nextPosition >= 4)
						{
							slider.goToSlide(Number(nextPosition));
						}

						// Add Playing Text To The Ticket
						var playingElement = $('#' + stampFromSession).find('.playingText');

						// Append the Playing HTMl to the Current Ticket
						playingElement.html('<span style="color:green;"><i class="fas fa fa-crosshairs"></i> Now Playing </span>');

						$.each(result.pointers, function (key, data) {

						})                                                
					},
					error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
						console.log(JSON.stringify(jqXHR));
						console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
					},
				});
				jQuery('#ajaxSpinner').removeClass('ajaxSpinnedShow');                
			}
        </script>


        {{--		Script To Change Element Color On Hover --}}
        <script>
			// Method to add the coordinate
			$(document).ready(function () {
				$('.stamped').mouseover(function () {
					var xcord = (($(this).data("xcord")) / zoom) - 10;
					var ycord = (($(this).data("ycord")) / zoom) - 10;
					var atID = $(this).attr('id');
					$('.win-crosshair-over').append(
						$('<span id="p' + atID + '"> <i class="fas fa fa-crosshairs"></i></span>').css({
                            color: '#d5ff3d',
                            margin:'16px',
							position: 'absolute',
							'z-index': '1200',
							top: ycord -13,
							left: xcord,
						})
					);
				});

				$('.stamped').mouseout(function () {
					var atID = $(this).attr('id');
					$('#p' + atID).remove();
				});

			});


			$(document).on('click', function () {
				$('.stamped').mouseover(function () {
					var xcord = (($(this).data("xcord")) / zoom) + 8;
					var ycord = (($(this).data("ycord")) / zoom) - 10;
					var atID = $(this).attr('id');
					$('.win-crosshair-over').append(
						$('<span id="p' + atID + '"> <i class="fas fa fa-crosshairs"></i></span>').css({
							color: '#d5ff3d',
							position: 'absolute',
							'z-index': '1200',
							top: ycord,
							left: xcord,
						})
					);
				});

				$('.stamped').mouseout(function () {
					var atID = $(this).attr('id');
					$('#p' + atID).remove();
				});
			});


        </script>


        <!-- Attach The Required JS Files Needed By This Page -->
        <!-- The Code Login For the AJAX Request  -->
        <script type="text/javascript" src="{{ asset('js/request.js')}}"></script>
        <!-- Light Sliders For the PlayPage  -->
        <script type="text/javascript" src="{{ asset('js/lightslider-master/dist/js/lightslider.js') }}"></script>


        <script type="javascript">

        </script>



        @if(session()->get('introDisplayed') != true)
            <script>
				// Instantiate IntroJS
				$(document).ready(function () {
                    
                    introJs().start();
				});
            </script>
        @endif
        {{ session()->put('introDisplayed', true) }}
        {{ session()->save() }}



    @endif
@endsection
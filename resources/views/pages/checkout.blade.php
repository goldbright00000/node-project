@extends ('layout')

@section('content')

{{--    {{ dd(session()->all()) }}--}}

    <!--Container For The page-->
    <div class="container wd-first-container">
        <!--Row For The Heading Section -->
        <div class="row text-center wd-top-padding">

            <div class="col-md-6 offset-md-3 mb-4 text-center">
                @if(session('couponFlash'))
                    <div class="alert alert-warning">
                        {{ session('couponFlash') }}
                    </div>
                    {{ session()->forget('couponFlash') }}

                @elseif (session('couponSuccess'))
                    <div class="alert alert-success">
                        {{ session('couponSuccess') }}
                    </div>
                    {{ session()->forget('couponSuccess') }}
                @endif
            </div>

            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Checkout</h2>
                <p>You're almost there! Simply choose your payment option and checkout.</p>
            </div>

            <!-- Column For Order Summary -->
            <div class="col-md-8 col-sm-12 text-left mt-4 mb-5">

                <!-- Inner Column Declaration For Order Summary Section -->
                <div class="col-md-12 wd-my-account-section py-4">
                    <h4 class="font-weight-bold dark-grey-text">Order Summary</h4>

                    <!-- Editable table -->
                    <div class="card mt-4">

                        <div class="card-body">
                            <div id="table" class="table-editable">
                                <table class="table table-responsive-md table-striped text-center">
                                    <tr>
                                        <th class="text-center">Image</th>
                                        <th class="text-center">Product Name</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Amount</th>
                                        <th class="text-center">Total</th>
                                    </tr>

                                    <!-- Repeatable Table Row-->
                                    @php
                                        $cart = json_decode(json_encode(\Cart::content()), true);
                                    @endphp
                                    @foreach ($cart as $item)
                                        <tr>
                                            <td class="py-4 align-middle" contenteditable="false">
                                                <a href="{{ route('product-detail', $item['options']['attributes']['product_attr']['slug'] ) }}"
                                                   class="">
                                                    <img src="{{ asset('/storage/') . '/' .  $item['options']['attributes']['product_attr']['image'] }}"
                                                         class="mx-auto"
                                                         style="width:85px" alt="placeholder"/>
                                                </a>
                                            </td>
                                            <td class="py-4 align-middle" contenteditable="false">
                                                <a href="{{ route('product-detail', $item['options']['attributes']['product_attr']['slug'] ) }}"
                                                   class="">
                                                    {{ $item['name'] }}
                                                </a>
                                            {{--<span class="d-block">X-2014 Y-418</span></td>--}}
                                            <td class="py-4 align-middle"
                                                contenteditable="false">{{ $item['qty'] }}</td>
                                            <td class="py-4 align-middle" contenteditable="false">
                                                $ {{ $item['qty']*$item['price'] }}</td>
                                            <td class="py-4 align-middle" contenteditable="false">
                                                $ {{ $item['qty']*$item['price'] }}</td>

                                        </tr>
                                        <!-- /Repeatable Table Row-->
                                    @endforeach

                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Editable table -->

                </div>
                <!-- /Inner Column Declaration For Order Summary Section -->

            </div>
            <!-- /Column For Order Summary -->


            <!-- Column For Order Total -->
            <div class="col-md-4 col-sm-12 my-4">

                <!-- Declaration of inner column for Order Total Section -->
                <div class="col-md-12 col-sm-12 wd-my-account-section text-left py-4">
                    <h4 class="font-weight-bold dark-grey-text">Order Total</h4>
                    <!--Table-->

                    <table id="tablePreview" class="table table-borderless table-sm">
                        <!--Table head-->
                        <thead>
                        <tr>
                            <th>Particulars</th>
                            <th>Value</th>
                        </tr>
                        </thead>
                        <!--Table head-->
                        <!--Table body-->
                        <tbody>
                        <tr>
                            <td scope="row">Tickets Total</td>
                            <td>${{ totalTicketsValue(\Cart::content()) }}</td>
                        </tr>


                        <tr>
                            <th>Grand Total</th>
                            <th>${{ Cart::subtotal() }}</th>
                        </tr>
                        </tbody>
                        <!--Table body-->
                    </table>
                    <!--Table-->


                    <form id="discountCheckout" method="post" action="{{ route('checkout.discount') }}">
                        @csrf
                        <input type="text" placeholder="Discount Code" name="discountCoupon" class="form-control">
                        <button type="submit" class="btn btn-dark text-left mt-3">Apply Discount</button>
                    </form>
                </div>
                <!-- /Declaration of inner column for Order Total Section -->


            </div>
            <!-- /Column For Order Total -->


        </div>
        <!--/Row For The Heading Section -->


        <!-- Row For The Payment Section -->
        <div class="row">


            <!-- Column For The Billing Section -->
            <div class="col-md-8 col-sm-12 mb-5">
                <form
                        class="needs-validation"
                        id="paymentForm"
                        method="post"
                        action="{{ route('paypal.express-checkout') }}">

                @csrf

                <!-- Inner Column For The Billing Section -->
                    <div class="col-md-12 col-sm-12 wd-my-account-section text-center py-4">
                        <h4 class="font-weight-bold dark-grey-text mb-4">Billing Details</h4>


                        <div class="md-form my-5">
                            <label for="addressLineOne">Address Line 1 *</label>

                            <input
                                    @if($user->address1)
                                    value="{{$user->address1}}"
                                    @else
                                    placeholder="Please update your address"
                                    @endif
                                    type="text"
                                    id="addressLineOne"
                                    name="address1"
                                    class="form-control" required>
                        </div>
                        <div class="md-form my-5">
                            <label for="addressLineTwo">Address Line 2 *</label>
                            <input
                                    @if($user->address2)
                                    value="{{$user->address2}}"
                                    @else
                                    placeholder="Please update your address"
                                    @endif
                                    type="text"
                                    id="addressLineTwo"
                                    name="address2"
                                    class="form-control" required>
                        </div>
                        <div class="md-form my-5">
                            <label for="addressCity">City *</label>
                            <input
                                    @if($user->city)
                                    value="{{$user->city}}"
                                    @else
                                    placeholder="Please update your city"
                                    @endif
                                    type="text"
                                    id="addressCity"
                                    name="city"
                                    class="form-control" required>
                        </div>
                        <div class="md-form my-5">
                            <label for="addressState">State *</label>
                            <input
                                    @if($user->state)
                                    value="{{$user->state}}"
                                    @else
                                    placeholder="Please update your state"
                                    @endif
                                    type="text"
                                    id="addressState"
                                    name="state"
                                    class="form-control" required>
                        </div>
                        <div class="md-form my-5">
                            <label for="addressZipCode">Zip Code | Postal Code *</label>
                            <input
                                    @if($user->zip)
                                    value="{{$user->zip}}"
                                    @else
                                    placeholder="Please update your address"
                                    @endif
                                    type="number"
                                    id="addressZipCode"
                                    name="zip"
                                    class="form-control"
                                    required>
                        </div>
                        <div class="md-form my-5">

                            <label for="country">Country</label>
                            <Input class="form-control disabled"
                                   type="text"
                                   name="country"
                                   id="country"
                                   value="United States Of America"
                                   required>
                        </div>
                    </div>
                    <!-- Inner Column For The Billing Section -->


                    <!-- Column For The Payment Section -->
                    <div class="col12 mb-5 mt-5">

                        <!-- Inner Column For The Payment Section -->
                        <div class="col12 col-sm-12 wd-my-account-section text-center py-4">
                            <h4 class="font-weight-bold dark-grey-text mb-4">Payment Method</h4>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs md-tabs nav-justified orange" role="tablist">
                                {{--<li class="nav-item">--}}
                                {{--<a class="nav-link active" data-toggle="tab" href="#panel5" role="tab"><i--}}
                                {{--class="fa far fa-credit-card"></i> Credit | Debit Card</a>--}}
                                {{--</li>--}}
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#paypal" role="tab"><i
                                                class="fa fab fa-paypal"></i> PayPal</a>
                                </li>

                            </ul>
                            <!-- Tab panels -->
                            <div class="tab-content">
                            {{--<!--Panel 1-->--}}
                            {{--<div class="tab-pane fade in show active" id="panel5" role="tabpanel">--}}

                            {{--<form class="needs-validation" id="cardForm" novalidate>--}}
                            {{--<div class="md-form my-5">--}}
                            {{--<label for="nameOnCard">Name On Card</label>--}}
                            {{--<input value="John Doe" type="text" id="nameOnCard" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="md-form my-5">--}}
                            {{--<label for="cardNumber">Card Number</label>--}}
                            {{--<input value="1234456712346789" type="number" id="cardNumber" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="md-form my-5">--}}
                            {{--<label for="cardCvv">CVV | CVC2 </label>--}}
                            {{--<input value="123" type="password" id="cardCvv" class="form-control">--}}
                            {{--</div>--}}
                            {{--<!-- Column For Expiry Month Selection -->--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-6 m-b-3 text-left">--}}

                            {{--<!--Month select-->--}}
                            {{--<select class="mdb-select colorful-select dropdown-warning">--}}
                            {{--<option value="1">January</option>--}}
                            {{--<option value="2">February</option>--}}
                            {{--<option value="3">March</option>--}}
                            {{--<option value="4">April</option>--}}
                            {{--<option value="5">May</option>--}}
                            {{--<option value="6">June</option>--}}
                            {{--<option value="7">July</option>--}}
                            {{--<option value="8">August</option>--}}
                            {{--<option value="9">September</option>--}}
                            {{--<option value="10">October</option>--}}
                            {{--<option value="11">November</option>--}}
                            {{--<option value="12">December</option>--}}
                            {{--</select>--}}
                            {{--<label>Expiry Month</label>--}}
                            {{--<!--/Month select-->--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6 m-b-3 text-left">--}}

                            {{--<!-- Year select-->--}}
                            {{--<select class="mdb-select colorful-select dropdown-warning">--}}
                            {{--<option value="1">2018</option>--}}
                            {{--<option value="2">2019</option>--}}
                            {{--<option value="3">2020</option>--}}
                            {{--<option value="4">2021</option>--}}
                            {{--<option value="5">2022</option>--}}
                            {{--<option value="6">2023</option>--}}
                            {{--</select>--}}
                            {{--<label>Expiry Year</label>--}}
                            {{--<!--/Year select-->--}}
                            {{--</div>--}}
                            {{--<!-- /Column For Expiry Month Selection -->--}}
                            {{--</div>--}}

                            {{--<input class="form-check-input text-left my-4" type="checkbox" value=""--}}
                            {{--id="wd-news-signup">--}}
                            {{--<label class="form-check-label text-left my-4 " for="wd-news-signup">--}}
                            {{--Securely store credit details for quicker checkout--}}
                            {{--</label>--}}

                            {{--</form>--}}

                            {{--</div>--}}
                            {{--<!--/.Panel 1-->--}}
                            <!--Panel 2-->
                                <div class="tab-pane fade" id="paypal" role="tabpanel">
                                    <br>
                                    <p>Please proceed to Checkout and you would be redirected to PayPal for
                                        Payments </p>
                                </div>
                                <!--/.Panel 2-->

                            </div>

                        </div>
                        <!-- /Inner Column For The Payment Section -->
                    </div>
                    <!-- /Column For The Payment Section -->

                    <!-- Column For The Agreement Section -->
                    <div class="col-12 col-sm-12 mb-5 text-center">
                        <input class="form-check-input text-left" type="checkbox" value="agreed" name="TermsConditions"
                               id="wd-terms-agree" checked="checked" required>
                        <label class="form-check-label text-left" for="wd-terms-agree">
                    <span class="text-left my-4 d-inline">I Agree With <a href="" class="amber-text font-weight-bold">Terms
                            and Conditions</a> & <a
                                href="" class="amber-text font-weight-bold">Privacy
                            Policy.</a>
                    </span>
                        </label>
                        <input type="hidden" value="PayPal" name="PaymentMethod">
                        <button
                                type="submit"
                                class="wd-header-button-font btn btn-amber btn-block my-4">
                            <i class="fas fa fa-angle-double-right pr-3" style="font-size: 22px"></i>
                            FINISH AND PAY
                        </button>


                    </div>
                    <!-- /Column For The Agreement Section -->
                </form>
            </div>
            <!-- /Column For The Billing Section -->

        </div>
        <!--/Row For The Payment Section -->

    </div>
    <!--/Container For The page-->



@endsection
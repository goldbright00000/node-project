
<!--Footer-->
<!-- Footer -->
<footer class="page-footer d-print-none font-small wd-footer-wrapper pt-1">

    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Footer links -->
        <div class="row text-center text-md-left mt-3 ">

            <!-- Grid column -->
            <div class="col-md-3 wd-footer-extra-padding mx-auto mt-2 mb-1">
                <div class="wd-footer-extra-padding">
                    <p>
                        <a href="{{ route('winners.index') }}">Winners</a>
                    </p>
                    <p>
                        <a href="{{ route('faq.index') }}">Frequently Asked Questions</a>
                    </p>
                    <p>
                        <a href="#">Reviews</a>
                    </p>
                    <p>
                        <a href="/privacy-policy">Privacy Policy</a>
                    </p>
                    <p>
                        <a href="/terms-conditions">Terms and Conditions</a>
                    </p>
                    <p>
                        <a href="/refund-policy">Refund Policy</a>
                    </p>
                </div>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 mx-auto mb-1">
                <a href="#">
                    <img src="{{ asset('images/trust_pilot_white.png') }}" alt="trust pilot reviews" />
                </a>
                {{--<p class="mt-3">--}}
                    {{--<i class="fa fa-home mr-3"></i> 4787  Pratt Avenue, Olympia, WA--}}
                    {{--Washington, 98501, USA</p>--}}
                {{--<p>--}}
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-2">
                <h6 class="text-uppercase mb-2 wd-poppins">We Are Social</h6>
                <!-- Social buttons -->
                <div class="text-md-left text-center mb-3">
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item text-left">
                            <a href="https://www.facebook.com/Win-Your-Destiny-2099700090113794 " class="btn-floating btn-sm rgba-white-slight mx-1 wd-copyright-social-buttons">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://twitter.com/winyourdestiny" class="btn-floating btn-sm rgba-white-slight mx-1 wd-copyright-social-buttons">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/winyourdestiny/" class="btn-floating btn-sm rgba-white-slight mx-1 wd-copyright-social-buttons">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="btn-floating btn-sm rgba-white-slight mx-1 wd-copyright-social-buttons">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <h6 class="text-uppercase  mt-4 mb-4 wd-poppins">We Accept</h6>
                <img src="{{ asset('images/paypal-icon.png') }}" class="paypalLogo" alt="credit cards accepted" />
            </div>
            <!-- Grid column -->

        </div>
        <!-- Footer links -->

    </div>
    <!-- Footer Links -->

    <!-- Grid row -->
    <div class="wd-copyright-section mt-4">
        <div class="container">
            <div class="row align-items-center pt-3 ">

                <!-- Grid column -->
                <div class="col-md-4">
                    <!--Copyright-->
                    <p class="text-center text-md-left">© Copyright 2016-2017 WinYourDestinty.com</p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-5 offset-md-3">
                    <p class="text-center text-md-right">info@winyourdestiny.com </p>

                </div>
                <!-- Grid column -->
            </div>
        </div>
    </div>
    <!-- Grid row -->
    <div id="fb-root"></div>
</footer>
<!-- Footer -->
<!--Footer-->

<!-- Google Analytics  -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135709511-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-135709511-1');
</script>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2"></script>

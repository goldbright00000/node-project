@php
    $cart = json_decode(json_encode(\Cart::content()), true);
@endphp
<!--Main Navigation-->
<header>
    <!-- Fetching Data needed in the Navigation -->
    <div class="d-none">
        {{ $headerStampedProducts = stampProducts($cart)  }}
        {{ updatePointerSession() }}
    </div>

    <nav class="navbar navbar-expand-xl navbar-toggleable-md fixed-top navbar-dark wd-main-navigation py-sm-0">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">
                <img src={{ asset('images/logo-white.png') }} height="40" alt="Win Your Destiny Logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
                    aria-controls="navbarSupportedContent-4"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse  navbar-collapse mt-4 mt-xl-0" id="navbarSupportedContent-4">
                <ul class="navbar-nav ml-auto wn-main-navigation">
                    <li class="nav-item ">
                        <a class="nav-link px-sm-3 py-4 waves-effect waves-light" href="{{ route('product-listing') }}">TICKETS</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link px-sm-3 py-4 waves-effect waves-light" href="{{ route('pointer.index') }}">PLAY
                            NOW</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link px-sm-3 py-4 waves-effect waves-light" href="/how-to-win">HOW TO WIN</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link px-sm-3 py-4 waves-effect waves-light" href="/about">ABOUT</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link px-sm-3 py-4 waves-effect waves-light text-uppercase"
                           href="{{ route('winners.index') }}">Winners</a>
                    </li>


                    <!-- Authentication Links -->
                    @guest
                        <span class="form-inline d-none d-xl-flex ">

                        <a href="{{ route('login') . '?previous=' . Request::fullUrl() }}"><button type="button"
                                                                                                   class="btn btn-amber py-1"
                                                                                                   style="border: 2px solid #ffa000;">LOGIN</button></a>
                        @if (Route::has('register'))
                                <a href="{{ route('register') }}"><button
                                            class="btn btn-outline-amber py-1">SIGN UP</button></a>
                    </span>
                    @endif
                    @else
                        <span class="form-inline d-none d-xl-flex ">
                            <a href="{{ route('home') }}"><button type="button" class="btn btn-amber py-1"
                                                                  style="border: 2px solid #ffa000;">MY
                                    DASHBOARD</button></a>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <button class="btn btn-outline-amber py-1">LOGOUT</button></a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </span>
                    @endguest


                    <li class="nav-item d-none d-xl-inline">
                        <a href="#" data-activates="slide-out" id="wd-slideout"
                           class="nav-link waves-effect waves-light button-collapse wd-header-cart-button">
                            <i class="fa fa-shopping-cart"></i>
                            @if (\Cart::count() > 0)
                                <span class="win-cart-counter">{{ \Cart::count() }}</span>
                            @endif
                        </a>
                    </li>
                    @guest
                    <!-- Mobile Only Buttons -->
                        <a href="{{ route('login') . '?previous=' . Request::fullUrl() }}">
                            <button type="button" class="btn btn-amber py-1 d-block w-100 d-xl-none py-3">LOGIN</button>
                        </a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">
                                <button class="btn btn-yellow  py-1 d-block w-100 d-xl-none py-3">SIGN UP</button>
                            </a>
                        @endif
                    @else
                        <a href="{{ route('home') }}">
                            <button type="button" class="btn btn-amber w-100 py-1 d-block d-xl-none py-3">MY DASHBOARD
                            </button>
                        </a>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <button class="btn btn-amber w-100 py-1 d-block d-xl-none py-3">LOGOUT</button>
                        </a>
                    @endguest
                    <button type="button" class="btn btn-white py-1 d-block d-xl-none py-3 mb-4">Shopping Cart</button>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Sidebar navigation -->
    <div id="slide-out" class="side-nav side-nav-light wd-sidebar">
        <ul class="custom-scrollbar dark-grey-text">
            <!-- Logo -->
            <li class="text-center my-3">
                <h2>Cart Details</h2>
              
            </li>
        </ul>

        <div class="container" id="sideSlide">

            {{-- @if(empty(Cart::content())) --}}
            @if(count($cart)==0)
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="alert-danger px-5 py-3 text-center"> You Cart Is Empty Please try adding some
                            tickets to the Cart
                        </div>
                    </div>
                </div>
            </div>
          
            @else
                @foreach($cart as $item)
                
                    <div class="row" id="sideSlideContent">
                        <div class="col-md-12  wd-cart-element text-center">
                            <div class="row mt-5">

                                <div class="col-12">

                                    <h5 class="dark-grey-text mt-2">
                                        {{ $item['name'] }}
                                    </h5>

                                    <div>
                                        <i
                                                class="fas fa fa-trash dark-grey-text"
                                                style="font-size: 18px;">
                                        </i>
                                        <form method="post" style="display: inline-block;" action="{{ route('cart.destroy', $item['rowId']) }}">
                                            @csrf
                                            <input type="hidden" name="_method" value="delete" />

                                            <input
                                                    type="submit"
                                                    value="Remove Product"
                                                    class="wd-side-remove"
                                                    href="#"
                                                    style="font-size: 15px;
                                            font-family: Arial, Helvetica, sans-serif"
                                            />
                                        </form>

                                    </div>

                                    <div class='table-responsive'>
                                        <table class="table" style="border:0px;">
                                            <tbody>
                                            <tr>
                                                <td class="setPaddingTable">
                                                    <a href="{{ route('product-detail', $item['options']['attributes']['product_attr']['slug'] ) }}"
                                                       class="">
                                                        <img src="{{asset('/storage/') . '/' .  $item['options']['attributes']['product_attr']['image']}}"
                                                             style="width:70px">
                                                    </a>
                                                </td>
                                                <td>
                                                    <p class="text-center dark-grey-text mt-3">
                                                        QTY: {{ $item['qty'] }}    </p>
                                                    {{--<form id='cartQtyForm' method='get' action='{{ route('pointer.remove')  }}'>--}}
                                                    {{--<input type='button' value='-' class='qtyminus' field='quantity'/>--}}
                                                    {{--<input type='text' name='quantity' value='{{ $item->quantity }}' class='qty'/>--}}
                                                    {{--<input type='button' value='+' class='qtyplus' field='quantity'/>--}}
                                                    {{--<button type="submit"class="btn-amber">Test</button>--}}
                                                    {{--</form>--}}
                                                </td>
												<td><form action="{{ route('cart.store',[$item['rowId']]) }}" method="post">
														{{ csrf_field() }}
														<input type="hidden" name="request_from" value="products">
														<input type="hidden" name="product_id" value="{{ $item['id'] }}">
														<input type="hidden" name="product_quantity" value="1">
                                                        <input type="hidden" name="to_do" value="updateCartVal" >
														<input type="hidden" name="headerAddCard" value="headerAddCard">
														<button type="submit" class="btn btn-amber btn-sm px-3 wd-btn-grp waves-effect waves-light"><i class="fa fa-plus"></i></button>
													</form></td>
                                                <td class="align-middle dark-grey-text">Total <span
                                                            class="font-weight-bold">${{ $item['price'] * $item['qty'] }}</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <span class="mb-03" style="width: 100%;">
                            @foreach ($headerStampedProducts as $pointer)
                                        @if (($item['id'] ==  $pointer['productID']) && $pointer['stamped'] != null)
                                            <div class="col-10 offset-1 mb-1 wp-mark-points-sidebar">
                                        <p style="display: inline;" class="dark-grey-text mt-1 mb-1">
                                            {{ $item['name'] .' X:' . intval($pointer['stamped']->xCords) . '  Y: ' . intval($pointer['stamped']->yCords) }}
                                        </p>

                                        <form method="post" class="wd-side-remove-form"
                                              action=" {{ route('pointer.remove') }} ">
                                            @csrf
                                            <input type="hidden" name="productId" value="{{  $pointer['productID'] }}">
                                            <input type="hidden" name="stampId" value=" {{ $pointer['stamp'] }}">
                                            <input type="hidden" name="rowId" value=" {{ $pointer['rowId'] }}">
                                            <button class="wd-side-remove" type="submit">
                                                <i class="fas fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </div>
                                        @elseif (($item['id'] ==  $pointer['productID']) && $pointer['stamped'] == null)
                                            <div class="col-10 offset-1 mb-1 wp-mark-points-sidebar">
                                        <p style="display: inline;" class="dark-grey-text mt-1 mb-1">
                                            <i class="fa fa-check mr-2 "></i>To Play
                                        </p>
                                        <form method="post" class="wd-side-remove-form"
                                              action=" {{ route('pointer.remove') }} ">
                                            @csrf
                                            <input type="hidden" name="productId" value="{{ $pointer['productID'] }}">
                                            <input type="hidden" name="stampId" value=" {{ $pointer['stamp'] }}">
                                            <input type="hidden" name="rowId" value=" {{ $pointer['rowId'] }}">

                                            <button class="wd-side-remove" type="submit">
                                                <i class="fas fa fa-trash"></i>
                                            </button>
                                        </form>

                                    </div>
                                        @endif
                                    @endforeach
                        </span>
                            </div>


                        </div>


                    </div>
                @endforeach
            @endif
            <div class="row">
                <div class="col-12 text-center my-5">
                    @if (\Cart::count() > 0)
                            @if (countStampedProducts() == true)
                                <a href="{{ route('checkout.index') }}" class="btn btn-amber btn-primary text-uppercase">PROCEED TO CHECKOUT</a>
                            @else
                                <a href="{{ route('pointer.index') }}" class="btn btn-amber btn-primary text-uppercase">PROCEED TO PLAY
                                </a>
                            @endif

                    @endif
                </div>
            </div>


        </div>
        <div class="sidenav-bg side-nav-light"></div>
    </div>
    <!--/. Sidebar navigation -->


</header>
<!--Main Navigation-->

<script>
	// Function to update the side menu on click
	$(document).ready(function () {


	});
</script>

@extends('voyager::master')
@section('content')

    <!-- Linking A Custom Stylesheet For Admin Panel -->
    <link href="{{ asset('css/adminStyle.css') }}" rel="stylesheet">

    @if(session()->has('success'))
        <div class="winnerContainer px-30 mb-30" style="background-color:green;">
            <h5 class="py-30" style="color:white;">{{ session()->get('success') }}</h5>
        </div>

        {{ session()->forget('success') }}
    @endif

    @php
        $currentDate = \Carbon\Carbon :: now();
    @endphp

    <div class="winnerContainer px-30">
        <h2 class="py-30 ">Games And Winner Annoucements</h2>
        <div class="winnerInner mb-30">
            <div class="datagrid">
                <table class="">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Winner Date</th>
                        <th>Tie Date</th>
                        <th>Winner Status</th>
                        <th>Results Page</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $games as $game)
                        <tr>
                            <td>{{ $game->name }}</td>
                            <td>{{ dateToString($game->startDate) }}</td>
                            <td>{{ dateToString($game->endDate) }}</td>
                            <td>{{ dateToString($game->winnerDate) }}</td>
                            <td>{{ dateToString($game->tieDate) }}</td>
                            <td>
                                @if($game->resultStatus == 0  && $game->endDate <= $currentDate)
                                    {{-- && $game->endDate <= $currentDate--}}
                                    <form action="{{ route('voyager.winner-list') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="game" value="{{ $game->id }}">
                                        <button type="submit" class="redButton">Check Winner</button>
                                    </form>
                                @elseif ($game->resultStatus == 0 && $game->endDate >= $currentDate)
                                    <p style="color:red;">Game Is Being Played or Yet To Be Played</p>
                                @else
                                    <p style="color:green;">Already Declared</p>
                                @endif
                            </td>
                            <td>
                                @if($game->resultStatus == 0)
                                    <button class="redButton">Not Declared</button>
                                @else
                                    @foreach ($winners as $winner)
                                        @if($winner->id == $game->declareWinner_id  )
                                            <a class="greenButton" href="{{ route('winners.detail', $winner->slug) }}">View
                                                Winners</a>
                                        @endif
                                    @endforeach
                                @endif
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        <div class="col-sm-12 wd-pagination">
            <!--/Pagination amber-->
            {{ $games->links() }}
        </div>
    </div>
@stop
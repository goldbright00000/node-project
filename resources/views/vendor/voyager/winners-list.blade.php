@extends('voyager::master')
@section('content')

    <!-- Linking A Custom Stylesheet For Admin Panel -->
    <link href="{{ asset('css/adminStyle.css') }}" rel="stylesheet">

    <div class="winnerContainer px-30">
        @if($winnerData['pointers']->isEmpty())
            <h2 class="py-30 ">The game has not been played by anyone as yet</h2>
        @else
            @if ($winnerData['players'] == null)
                <h2 class="py-30 ">Here are The Clear Winners In Sequence</h2>
                <div class="datagrid">
                    <table class="">
                        <thead>
                        <tr>
                            <th>Position</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>X-Cord</th>
                            <th>Y-Cord</th>
                            <th>Playing For</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($winnerData['pointers'] as $point)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $users->where('id', '=', $point->user_id)->first()->name}}</td>
                                <td>{{ $users->where('id', '=', $point->user_id)->first()->lastName}}</td>
                                <td>{{ $users->where('id', '=', $point->user_id)->first()->email}}</td>
                                <td>
                                    {{ $users->where('id', '=', $point->user_id)->first()->countryCode . ' ' .$users->where('id', '=', $point->user_id)->first()->mobile }}
                                </td>
                                <td>{{ $point->xCords }}</td>
                                <td>{{ $point->yCords }}</td>
                                <td>{{ $products->where ('id', '=', $point->productID)->first()->name }}</td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            @else

                <h2 class="py-30 ">These Players Are Competing For The First Position </h2>
                <div class="datagrid">
                    <table class="">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Tie Email Status</th>
                            <th>Tie Breaker Status</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($winnerData['players'] as $player)
                            <tr>
                                <td>{{ $player->name}}</td>
                                <td>{{ $player->lastName}}</td>
                                <td>{{ $player->email}}</td>
                                <td>
                                    {{ $player->countryCode . ' ' .$player->mobile }}
                                </td>
                                <td><p style="color:green">Sent</p></td>
                                @php
                                $status = false;
                                    foreach($tieBreakers as $breaker){
                                        if($breaker->user_id == $player->id && $breaker->breakerPlayed == true){
                                            $status = true;
                                        }
                                    }
                                @endphp
                                @if ($status == true)
                                    <td><p style="color:green">Played</p></td>
                                @else
                                    <td><p style="color:red">Not Played</p></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            @endif
        @endif
        <span class="mb-30 mt-30"><a href="{{ route('voyager.games') }}" class="redButton mb-30 mt-30 ">Go back</a></span>

        <span class="mb-30 mt-30">
            <form method="post" style="display: inline;" action=" {{ route('voyager.winner-declare') }}">
                @csrf
                <input type="hidden" name="game" value="{{ $game_id }}">
                <input type="hidden" name="winnerData" value=" {{ collect($winnerData) }}">
                <input type="hidden" name="users" value=" {{ $users }}">
                <input type="hidden" name="products" value=" {{ $products }}">
                <button type="submit"  class="greenButton mb-30 mt-30 ">Declare Winners</button>
            </form>
        </span>
    </div>

@stop
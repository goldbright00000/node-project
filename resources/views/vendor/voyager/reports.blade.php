@extends('voyager::master')
@section('content')

    <!-- Linking A Custom Stylesheet For Admin Panel -->
    <link href="{{ asset('css/adminStyle.css') }}" rel="stylesheet">

    @if(session()->has('success'))
        <div class="winnerContainer px-30 mb-30" style="background-color:green;">
            <h5 class="py-30" style="color:white;">{{ session()->get('success') }}</h5>
        </div>

        {{ session()->forget('success') }}
    @endif

    @php
        $currentDate = \Carbon\Carbon :: now();
    @endphp

    <div class="winnerContainer px-30">
		<div class="row">
			<div class="col-sm-10">
				<h2 class=" ">User List</h2>
			</div>
			<div class="col-sm-2" style="margin-top: 20px;">
				<a href="/admin/csv" class="btn btn-info btn-lg" download>
					<span class="glyphicon glyphicon-download-alt"></span> CSV
				</a>
			</div>
		</div>

        <div class="winnerInner mb-30">
            <div class="datagrid">
                <table class="">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Mobile</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $userList as $user)
					<td>{{$user->name}} {{$user->lastName}}</td>
					<td>{{$user->email}}</td>
					<td>{{$user->gender}}</td>
					<td>{{$user->mobile}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        <div class="col-sm-12 wd-pagination">
            <!--/Pagination amber-->
            {{ $userList->links() }}
        </div>
    </div>
@stop
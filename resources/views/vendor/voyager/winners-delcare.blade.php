@extends('voyager::master')
@section('content')

    <!-- Linking A Custom Stylesheet For Admin Panel -->
    <link href="{{ asset('css/adminStyle.css') }}" rel="stylesheet">


    <div class="winnerContainer px-30">

        <h3 class="py-30">Please fill in the form to declare the winners and create a page</h3>
        <form class="form-edit-add" method="post"  action=" {{ route('voyager.winner-post') }} " enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="gameid" value="{{ $game->id }}">
            <div class="form-group  col-md-12">
                <label for="title">The Title Of The Page</label>
                <input class="form-control disabled" type="text" id="title" name="title" value="{{'Winner Of ' . $game->name}}"  required>
            </div>

            <div class="form-group  col-md-12">
                <label for="subTitle">The Sub-Title Of The Page</label>
                <input class="form-control" type="text" id="subTitle" name="subTitle" required>
            </div>
            <div class="form-group  col-md-12">
                <label for="slug">Slug (This is the URL and should be unique, without spaces and like:
                    <strong>game-name-1</strong></label>
                <input class="form-control" type="text" id="slug" name="slug" required>
            </div>
            <div class="form-group col-md-12">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" rows="6" required></textarea>
            </div>
            <div class="form-group col-md-12">
                <label for="image1">Winner Image 1:</label>
                <input type="file" id="image1" class="form-control" name="image1" required/>
            </div>
            <div class="form-group col-md-12">
                <label for="image2">Winner Image 2:</label>
                <input type="file" id="image2" class="form-control" name="image2" />
            </div>
            <div class="form-group col-md-12">
                <label for="image3">Winner Image 3:</label>
                <input type="file" id="image3" class="form-control" name="image3" />
            </div>

            <div class="form-group col-md-12">
                <label for="video">YouTube Video ID (Just The ID)</label>
                <input class="form-control" type="text" id="video" name="video" required>
            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-primary save">Publish</button>
            </div>
        </form>

    </div>

@stop
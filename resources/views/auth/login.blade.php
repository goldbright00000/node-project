@extends ('layout')

@section('content')

    <!--Container For The Form-->
    <!--Container For The Form-->
    <div class="container wd-first-container">
        <!--Row For The Form-->
        <div class="row text-center wd-top-padding wd-bottom-padding">
            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">LOGIN WITH EMAIL</h2>
            </div>
            <!-- Column For Form -->
            <div class="col-md-6 offset-md-3 col-sm-12 py-4">
                    @if(\Session::get('error_message'))
                    <div class="alert alert-danger">
                        {{ \Session::get('error_message') }}
                    </div>
                    @endif
                
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                            <input id="email" type="email" placeholder="Email Address" class="form-control form-control form-control-lg my-4{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   name="email" value="{{ old('email') }}" required autofocus>


                            <input id="password" type="password" placeholder="Password *" class="form-control form-control form-control-lg my-4{{ $errors->has('password') ? '
                            is-invalid' : '' }}" name="password"
                                   required>


                            @if (Request::has('previous'))
                                <input type="hidden" name="previous" value="{{ Request::get('previous') }}">
                            @else
                                <input type="hidden" name="previous" value="{{ URL::previous() }}">
                            @endif


                            <input class="form-check-input text-left d-inline mt-4" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label text-left float-left mb-4" for="remember">
                                {{ __('Remember Me') }}
                            </label>

                            <a class="grey-text d-inline float-right" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>

                            <button type="submit"  id="wd-signup-form-submit" class="btn-lg btn-amber btn-block mt-5">
                                {{ __('Login') }}
                            </button>

                    </form>

                <p class="py-3">Don’t Have An Account Yet? <a href="{{ route('register') }}" class="amber-text font-weight-bold ">Signup Here</a></p>
                <!-- /Column For Form -->
            </div>
            <!--/Row For The Form-->
        </div>
        <!--/Container For The Form-->
    </div>
@endsection

@extends ('layout')


@section('content')
    <div class="container wd-first-container">
        <!--Row For The Form-->
        <div class="row text-center wd-top-padding wd-bottom-padding">
            <!-- Column For heading -->
            <div class="col-md-6 offset-md-3 col-sm-12">
                <span class="wd-heading-separator"></span>
                <h2 class="wd-black-heading">Sign Up For An Account</h2>
            </div>
            <div class="col-md-6 offset-md-3 col-sm-12 py-4">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <input id="name" type="text" placeholder="First Name *" class="form-control form-control-lg my-4{{ $errors->has('name') ? ' is-invalid' : '' }}"
                               name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                        <input id="lastName" type="text" placeholder="Last Name *" class="form-control form-control-lg my-4{{ $errors->has('lastName') ? '
                        is-invalid' : '' }}" name="lastName" value="{{ old('lastName') }}" required autofocus>

                        @if ($errors->has('lastName'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lastName') }}</strong>
                                </span>
                        @endif


                        <input id="email" type="email" placeholder="Email Address *" class="form-control form-control-lg my-4{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif


                        <input id="password" type="password" placeholder="Password *" title="Minimum of 8 characters including at least 1 number, 1 capital letter, 1 lower case and 1 special character" pattern="((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!^&*()+-_?><~]).{6,20})"
                               class="form-control form-control-lg my-4{{ $errors->has('password') ?
                        ' is-invalid' : ''
                        }}" name="password" required>
                        <p class="wd-very-small-text text-left mt-0">Minimum of 8 characters including at least 1 number, 1 capital letter, 1 lower case and 1 special character</p>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                        <input id="password-confirm" type="password" class="form-control form-control-lg my-4" placeholder="Confirm Password *"
                               name="password_confirmation" required>
                    <div class="my-4 text-left">
                        <h5>Select Your Gender</h5>
                        <span class="mb-0 d-block text-left">
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" id="genderMale"
                                       name="gender" value="male">
                                <label class="form-check-label" for="genderMale">Male</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" id="genderFemale"
                                       name="gender" value="female">
                                <label class="form-check-label" for="genderFemale">Female</label>
                            </div>
                        </span>
                        @if ($errors->has('gender'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('gender') }}</strong>
                                </span>
                        @endif
                    </div>

                        <div class="form-row mb-4">
                            <div class="col">
                                <!-- First name -->
                                <input type="text" id="countryCode" class="form-control form-control-lg{{ $errors->has('countryCode') ? ' is-invalid' : ''
                                }}" placeholder="+1" name="countryCode" value="+1" required>
                                <p class="text-left text-sm-left">Country Code in Format +1</p>

                                @if ($errors->has('countryCode'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('countryCode') }}</strong>
                                </span>
                                @endif

                            </div>
                            <div class="col-9">
                                <!-- Last name -->
                                <input type="text" id="mobile" class="form-control form-control-lg{{ $errors->has('mobile') ? ' is-invalid' : ''
                                }}" placeholder="Mobile Number *"  name="mobile" value="{{ old('mobile') }}"
                                       required>
                                <p class="text-left">Your Mobile Number</p>

                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>

                        <input class="form-check-input text-left my-4{{ $errors->has('ageConfirmation') ? ' is-invalid' : ''}}" type="checkbox" name="ageConfirmation" value="1" id="wd-age-agreement" required>
                        <label class="form-check-label text-left" for="wd-age-agreement">
                            I AGREE THAT I AM OVER 18 YEARS OF AGE
                        </label>
                        @if ($errors->has('ageConfirmation'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ageConfirmation') }}</strong>
                                </span>
                        @endif
                        <p class="text-left my-4">By creating your account, you agree to our <a href="{{ route('terms-conditions') }}" class="amber-text font-weight-bold">Terms and Conditions</a> & <a
                                    href="{{route('privacy-policy')}}" class="amber-text font-weight-bold">Privacy Policy.</a>
                        </p>


                        <p class="text-left my-4">We do not share your details with any third parties for them to market their products/services to you.</p>
                        <input class="form-check-input text-left my-4{{ $errors->has('newsConfirmation') ? ' is-invalid' : ''}}" type="checkbox"
                               name="newsConfirmation" value="1" type="checkbox" checked="checked" id="wd-news-signup">


                        <label class="form-check-label " for="wd-news-signup">
                            We would like to keep in touch with you about our products and services. If you do not want to hear from us please tick the box.
                        </label>
                        @if ($errors->has('newsConfirmation'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('newsConfirmation') }}</strong>
                                </span>
                        @endif


                        <button type="submit" class="btn-lg btn-amber btn-block mt-5" id="wd-signup-form-submit">
                            {{ __('SIGN UP') }}
                        </button>



                    </form>
                <!-- /Column For Form -->
            </div>
        </div>
        <!--/Row For The Form-->
    </div>
    <!--/Container For The Form-->
@endsection

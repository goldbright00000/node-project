<?php

use Illuminate\Database\Seeder;

class DataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('data_types')->delete();
        
        \DB::table('data_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'users',
                'slug' => 'users',
                'display_name_singular' => 'User',
                'display_name_plural' => 'Users',
                'icon' => 'voyager-person',
                'model_name' => 'TCG\\Voyager\\Models\\User',
                'policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy',
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:25',
                'updated_at' => '2019-04-10 05:26:25',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'menus',
                'slug' => 'menus',
                'display_name_singular' => 'Menu',
                'display_name_plural' => 'Menus',
                'icon' => 'voyager-list',
                'model_name' => 'TCG\\Voyager\\Models\\Menu',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:25',
                'updated_at' => '2019-04-10 05:26:25',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'roles',
                'slug' => 'roles',
                'display_name_singular' => 'Role',
                'display_name_plural' => 'Roles',
                'icon' => 'voyager-lock',
                'model_name' => 'TCG\\Voyager\\Models\\Role',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:25',
                'updated_at' => '2019-04-10 05:26:25',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'global_settings',
                'slug' => 'global_settings',
                'display_name_singular' => 'Global Setting',
                'display_name_plural' => 'Global Settings',
                'icon' => '',
                'model_name' => 'App\\GlobalSettings',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:26',
                'updated_at' => '2019-04-10 05:26:26',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'dicount_coupons',
                'slug' => 'dicount_coupons',
                'display_name_singular' => 'Discount Coupon',
                'display_name_plural' => 'Discount Coupons',
                'icon' => '',
                'model_name' => 'App\\DicountCoupon',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:26',
                'updated_at' => '2019-04-10 05:26:26',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'categories',
                'slug' => 'categories',
                'display_name_singular' => 'Categories',
                'display_name_plural' => 'Categories',
                'icon' => '',
                'model_name' => 'App\\Categories',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:26',
                'updated_at' => '2019-04-10 05:26:26',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'games',
                'slug' => 'games',
                'display_name_singular' => 'Games',
                'display_name_plural' => 'Games',
                'icon' => '',
                'model_name' => 'App\\Game',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:27',
                'updated_at' => '2019-04-10 05:26:27',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'invoices',
                'slug' => 'invoices',
                'display_name_singular' => 'Invoices',
                'display_name_plural' => 'Invoices',
                'icon' => '',
                'model_name' => 'App\\Invoice',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:27',
                'updated_at' => '2019-04-10 05:26:27',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'products',
                'slug' => 'products',
                'display_name_singular' => 'Product',
                'display_name_plural' => 'Products',
                'icon' => '',
                'model_name' => 'App\\Product',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:27',
                'updated_at' => '2019-04-10 05:26:27',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'homepage_settings',
                'slug' => 'homepage_settings',
                'display_name_singular' => 'HomepageSetting',
                'display_name_plural' => 'HomepageSettings',
                'icon' => '',
                'model_name' => 'App\\HomepageSetting',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-04-10 05:26:27',
                'updated_at' => '2019-04-10 05:26:27',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'faqs',
                'slug' => 'faqs',
                'display_name_singular' => 'Faq',
                'display_name_plural' => 'Faqs',
                'icon' => 'voyager-check',
                'model_name' => 'App\\Faq',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}',
                'created_at' => '2019-04-10 05:27:31',
                'updated_at' => '2019-04-10 05:28:05',
            ),
        ));
        
        
    }
}
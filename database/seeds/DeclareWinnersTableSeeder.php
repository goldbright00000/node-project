<?php

use Illuminate\Database\Seeder;

class DeclareWinnersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table( 'declare_winners' )->insert( [
			'title'        => 'Winner Of Game 1',
			'slug'         => 'winner-game-1',
			'subTitle'     => 'John Doe Won - Porce 911',
			'video'        => 'DjIe6U4TyG0',
			'winnerImage1' => 'https://win.test/storage/March2019/homepage-hero-image.jpg',
			'winnerImage2' => 'https://win.test/storage/March2019/homepage-hero-image.jpg',
			'winnerImage3' => 'https://win.test/storage/March2019/homepage-hero-image.jpg',
			'description'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin facilisis vel nibh vitae commodo. Aliquam maximus commodo dui, porta commodo nisi tempor at. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti. Integer vehicula ante sapien, sit amet volutpat elit facilisis sit amet. Aliquam ut lacinia orci. Fusce vulputate justo id viverra maximus. Maecenas eget condimentum purus.',
		] );
	}
}

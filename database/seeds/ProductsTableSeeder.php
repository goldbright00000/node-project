<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		for ($i=1; $i <= 20; $i++){
			Product::create( [
				'name'           => 'Car' . $i,
				'slug'           => 'car-'. $i,
				'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec mauris pulvinar, dignissim felis id, venenatis nunc. Curabitur vel suscipit elit. Praesent commodo hendrerit iaculis. Ut at elit a risus pulvinar tempor. Praesent in euismod elit, quis mattis ex. Maecenas eu sem neque. Suspendisse mauris lectus, placerat in vehicula quis, dignissim id nisi. Nulla bibendum ultricies velit id pretium. Suspendisse potenti. Etiam lacinia mi lectus, vitae bibendum urna consequat quis. ',
				'price_slab1'    => rand(1, 3),
				'discount_slab1' => rand(3, 6),
				'discount_slab2' => rand(7, 10),
				'product_image1' => 'oldImages/product-images/'. rand(1,8) . '.jpg',
				'product_image2' => 'oldImages/product-images/'. rand(11,14) . '.jpg',
				'product_image3' => 'oldImages/product-images/'. rand(11,14) . '.jpg',
				'product_image4' => 'oldImages/product-images/'. rand(11,14) . '.jpg',
				'product_image5' => 'oldImages/product-images/'. rand(11,14) . '.jpg',
			] )->categories()->attach(rand(1,5));
		}
			$product = Product::find( 1 );
			$product->categories()->attach( 2 );
	}
}

<?php

use Illuminate\Database\Seeder;
use VoyagerBread\Traits\BreadSeeder;

class GamesBreadSeeder extends Seeder
{
    use BreadSeeder;

    public function bread()
    {
        return [
            // usually the name of the table
            'name'                  => 'games',
            'display_name_singular' => 'Games',
            'display_name_plural'   => 'Games',
            'icon'                  => '',
            'model_name'            => 'App\Game',
            'controller'            => '',
            'generate_permissions'  => 1,
            'description'           => '',
        ];
    }

    public function inputFields()
    {
        return [
            'id' => [
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 1,
            ],
            'created_at' => [
                'type'         => 'timestamp',
                'display_name' => 'created_at',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 2,
            ],
            'updated_at' => [
                'type'         => 'timestamp',
                'display_name' => 'updated_at',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 3,
            ],
            'name' => [
	            'type'         => 'text',
	            'display_name' => 'Name Of The Game (Please give a unique name as will bee visible to audience in results)',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 4,
            ],
            'startDate' => [
	            'type'         => 'timestamp',
	            'display_name' => 'Start Date (Select the start date and time for the game) ex: 2019-03-01 00:00:00',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 4,
            ],

            'endDate' => [
	            'type'         => 'timestamp',
	            'display_name' => 'End Date (Select the end date and time for the game) example: 2019-03-01 00:00:00',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 4,
            ],
            'imageUrl' => [
		        'type'         => 'image',
		        'display_name' => 'Image ( The size of the image should be 7160px x 4000px at 150DPI for clarity)',
		        'required'     => 1,
		        'browse'       => 1,
		        'read'         => 1,
		        'edit'         => 1,
		        'add'          => 1,
		        'delete'       => 1,
		        'details'      => '',
		        'order'        => 4,
	        ],
            'xCords' => [
		        'type'         => 'number',
		        'display_name' => 'Winning X-Cordinate (The X Coordinate for the winning position)',
		        'required'     => 1,
		        'browse'       => 0,
		        'read'         => 1,
		        'edit'         => 0,
		        'add'          => 1,
		        'delete'       => 1,
		        'details'      => '',
		        'order'        => 4,
	        ],
            'yCords' => [
	            'type'         => 'number',
	            'display_name' => 'Winning Y-Cordinate (The X Coordinate for the winning position)',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 4,
            ],
            'winnerDate' => [
	            'type'         => 'timestamp',
	            'display_name' => 'Timestamp For Winner Declaration Date example: 2019-03-01 00:00:00',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 4,
            ],
            'tieDate' => [
	            'type'         => 'timestamp',
	            'display_name' => 'Date Till When A Tie Should Be Resolved example: 2019-03-01 00:00:00',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 4,
            ],
            'runnerUps' => [
	            'type'         => 'number',
	            'display_name' => 'The number of runnerups needed for this game',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 4,
            ],
            'description' => [
	            'type'         => 'text_area',
	            'display_name' => 'Enter the description for the game',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 4,
            ],
        ];
    }

    public function menuEntry()
    {
        return [
            'role'      => 'admin',
            'title'      => 'Games',
            'url'        => '',
            'route'      => 'voyager.games.index',
            'target'     => '_self',
            'icon_class' => 'voyager-helm',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 8,
        ];
    }
}

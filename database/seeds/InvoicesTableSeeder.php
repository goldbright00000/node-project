<?php

use Illuminate\Database\Seeder;

class InvoicesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('invoices')->delete();
        
        \DB::table('invoices')->insert(array (
            0 => 
            array (
                'id' => 1145,
                'title' => 'Order ID:75',
                'description' => 'Invoice For Order ID:75',
                'products' => '[{"name":"Car17","price":"30.00","qty":"1"}]',
                'productsTotal' => 30.0,
                'discount' => 5.0,
                'tax' => 5.0,
                'creditAdjusted' => 0.0,
                'total' => 30.0,
                'paymentMethod' => 'PayPal',
                'payment_status' => 'Completed',
                'user_id' => 1,
                'created_at' => '2019-02-01 06:58:32',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 1146,
                'title' => 'Order ID:75',
                'description' => 'Invoice For Order ID:75',
                'products' => '[{"name":"Car17","price":"30.00","qty":"1"}]',
                'productsTotal' => 30.0,
                'discount' => 5.0,
                'tax' => 5.0,
                'creditAdjusted' => 0.0,
                'total' => 30.0,
                'paymentMethod' => 'PayPal',
                'payment_status' => 'Completed',
                'user_id' => 1,
                'created_at' => '2019-02-01 06:58:32',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 1147,
                'title' => 'Order ID:75',
                'description' => 'Invoice For Order ID:75',
                'products' => '[{"name":"Car17","price":"30.00","qty":"1"}]',
                'productsTotal' => 30.0,
                'discount' => 5.0,
                'tax' => 5.0,
                'creditAdjusted' => 0.0,
                'total' => 30.0,
                'paymentMethod' => 'PayPal',
                'payment_status' => 'Pending',
                'user_id' => 2,
                'created_at' => '2019-02-01 06:58:32',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 1148,
                'title' => 'Order ID:75',
                'description' => 'Invoice For Order ID:75',
                'products' => '[{"name":"Car17","price":"30.00","qty":"1"}]',
                'productsTotal' => 30.0,
                'discount' => 5.0,
                'tax' => 5.0,
                'creditAdjusted' => 0.0,
                'total' => 30.0,
                'paymentMethod' => 'PayPal',
                'payment_status' => 'Pending',
                'user_id' => 1,
                'created_at' => '2019-02-01 06:58:32',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
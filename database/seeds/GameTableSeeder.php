<?php

use Illuminate\Database\Seeder;


class GameTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table( 'games' )->insert( [
			'name'             => 'Game 1',
			'startDate'        => '2018-11-01 00:00:00',
			'endDate'          => '2018-11-20 00:00:00',
			'xCords'           => '3500',
			'yCords'           => '2500',
			'imageURL'         => 'oldImages/gameplay-image.jpg',
			'imageWidth'       => '1200',
			'imageHeight'      => '800',
			'runnerUps'        => '10',
			'tieDate'          => '2019-02-05 00:00:00',
			'winnerDate'       => '2018-11-21 00:00:00',
			'videoURL'         => 'fAdkeJeeYG4',
			'resultStatus'     => '1',
			'declareWinner_id' => '1',
			'description'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin facilisis vel nibh vitae commodo. Aliquam maximus commodo dui, porta commodo nisi tempor at. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti. Integer vehicula ante sapien, sit amet volutpat elit facilisis sit amet. Aliquam ut lacinia orci. Fusce vulputate justo id viverra maximus. Maecenas eget condimentum purus.',
		] );
		DB::table( 'games' )->insert( [
			'name'             => 'Game 2',
			'startDate'        => '2018-11-20 00:00:00',
			'endDate'          => '2018-12-10 00:00:00',
			'xCords'           => '3500',
			'yCords'           => '2500',
			'imageURL'         => 'oldImages/gameplay-image.jpg',
			'imageWidth'       => '1200',
			'imageHeight'      => '800',
			'runnerUps'        => '10',
			'tieDate'          => '2019-02-05 00:00:00',
			'winnerDate'       => '2018-12-11 00:00:00',
			'videoURL'         => 'oeBzN3XmoR4',
			'resultStatus'     => '1',
			'declareWinner_id' => '1',
			'description'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin facilisis vel nibh vitae commodo. Aliquam maximus commodo dui, porta commodo nisi tempor at. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti. Integer vehicula ante sapien, sit amet volutpat elit facilisis sit amet. Aliquam ut lacinia orci. Fusce vulputate justo id viverra maximus. Maecenas eget condimentum purus.',
		] );
		DB::table( 'games' )->insert( [
			'name'             => 'Game 3',
			'startDate'        => '2018-12-10 00:00:00',
			'endDate'          => '2018-12-30 00:00:00',
			'xCords'           => '3500',
			'yCords'           => '2500',
			'imageURL'         => 'oldImages/gameplay-image.jpg',
			'imageWidth'       => '1200',
			'imageHeight'      => '800',
			'runnerUps'        => '10',
			'tieDate'          => '2019-02-05 00:00:00',
			'winnerDate'       => '2018-12-31 00:00:00',
			'videoURL'         => 'dqAwzCXlRWs',
			'resultStatus'     => '1',
			'declareWinner_id' => '1',
			'description'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin facilisis vel nibh vitae commodo. Aliquam maximus commodo dui, porta commodo nisi tempor at. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti. Integer vehicula ante sapien, sit amet volutpat elit facilisis sit amet. Aliquam ut lacinia orci. Fusce vulputate justo id viverra maximus. Maecenas eget condimentum purus.',
		] );
		DB::table( 'games' )->insert( [
			'name'             => 'Game 4',
			'startDate'        => '2018-12-30 00:00:00',
			'endDate'          => '2019-02-28 00:00:00',
			'xCords'           => '3500',
			'yCords'           => '2500',
			'imageURL'         => 'oldImages/gameplay-image.jpg',
			'imageWidth'       => '1200',
			'runnerUps'        => '10',
			'tieDate'          => '2019-02-05 00:00:00',
			'imageHeight'      => '800',
			'winnerDate'       => '2019-03-01 00:00:00',
			'videoURL'         => 'dqAwzCXlRWs',
			'resultStatus'     => '1',
			'declareWinner_id' => '1',
			'description'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin facilisis vel nibh vitae commodo. Aliquam maximus commodo dui, porta commodo nisi tempor at. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti. Integer vehicula ante sapien, sit amet volutpat elit facilisis sit amet. Aliquam ut lacinia orci. Fusce vulputate justo id viverra maximus. Maecenas eget condimentum purus.',
		] );
		DB::table( 'games' )->insert( [
			'name'        => 'Game 5',
			'startDate'   => '2019-04-01 00:00:00',
			'endDate'     => '2019-04-30 11:30:00',
			'xCords'      => '3500',
			'yCords'      => '2500',
			'imageURL'    => 'oldImages/gameplay-image.jpg',
			'imageWidth'  => '1200',
			'imageHeight' => '800',
			'runnerUps'   => '10',
			'winnerDate'  => '2019-04-30 13:30:00',
			'tieDate'     => '2019-04-30 12:30:00',
			'videoURL'    => '',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin facilisis vel nibh vitae commodo. Aliquam maximus commodo dui, porta commodo nisi tempor at. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti. Integer vehicula ante sapien, sit amet volutpat elit facilisis sit amet. Aliquam ut lacinia orci. Fusce vulputate justo id viverra maximus. Maecenas eget condimentum purus.',
		] );
	}
}

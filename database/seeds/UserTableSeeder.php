<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table( 'users' )->insert( [
			'name'            => 'John',
			'lastName'        => 'Doe',
			'email'           => 'john@win.com',
			'password'        => bcrypt( 'password' ),
			'countryCode'     => '+1',
			'initialDiscount' => '100',
			'gender'          => 'male',
			'mobile'          => '1234567890',
			'address1'        => 'Address Line One Added In Database',
			'address2'        => 'Address Line Two Added In Database',
			'city'            => 'New York',
			'state'           => 'New York',
			'zip'             => '12345',
			'country'         => 'United States Of America',
		] );
		DB::table( 'users' )->insert( [
			'name'            => 'Mark',
			'lastName'        => 'Doe',
			'email'           => 'mark@win.com',
			'password'        => bcrypt( 'password' ),
			'countryCode'     => '+1',
			'initialDiscount' => '100',
			'gender'          => 'male',
			'mobile'          => '1234567890',
			'address1'        => 'Address Line One Added In Database',
			'address2'        => 'Address Line Two Added In Database',
			'city'            => 'New York',
			'state'           => 'New York',
			'zip'             => '12345',
			'country'         => 'United States Of America',
		] );
		DB::table( 'users' )->insert( [
			'name'            => 'Bob',
			'lastName'        => 'Doe',
			'email'           => 'bob@win.com',
			'password'        => bcrypt( 'password' ),
			'countryCode'     => '+1',
			'initialDiscount' => '100',
			'gender'          => 'male',
			'mobile'          => '1234567890',
			'address1'        => 'Address Line One Added In Database',
			'address2'        => 'Address Line Two Added In Database',
			'city'            => 'New York',
			'state'           => 'New York',
			'zip'             => '12345',
			'country'         => 'United States Of America',
		] );
		DB::table( 'users' )->insert( [
			'name'            => 'Anuj',
			'lastName'        => 'Singhal',
			'email'           => 'admin@win.com',
			'password'        => bcrypt( 'password' ),
			'initialDiscount' => '100',
			'countryCode'     => '+1',
			'gender'          => 'male',
			'mobile'          => '1234567890',
			'address1'        => 'Address Line One Added In Database',
			'address2'        => 'Address Line Two Added In Database',
			'city'            => 'New York',
			'state'           => 'New York',
			'zip'             => '12345',
			'country'         => 'United States Of America',
			'role_id'         => '1',
		] );
	}
}

<?php

use Illuminate\Database\Seeder;

class HomepageSetting extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table( 'homepage_settings' )->insert( [
			'heroImage'  => 'March2019/homepage-hero-image.jpg',
			'heroContent'  => 'Win A Macbook Pro® This Week',
			'videoOne'   => 'fAdkeJeeYG4',
			'videoOneTitle'   => 'Title Of The First Video',
			'videoTwo'   => 'oeBzN3XmoR4',
			'videoTwoTitle'   => 'Title Of The Second Video',
			'videoThree' => 'dqAwzCXlRWs',
			'videoThreeTitle' => 'Title Of The Third Video',
		] );
	}
}

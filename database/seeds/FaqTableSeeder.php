<?php

use Illuminate\Database\Seeder;

class FaqTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table( 'faqs' )->insert( [
			'section'  => 'HOW DOES IT WORK?',
			'question' => 'How to buy tickets and enter the competion?',
			'answer'   => 'Simply add tickets to your basket for the prizes you\'d like to win, play Spot the Ball to enter and then proceed to checkout. You can buy tickets from our website at www.winyourdestiny.com ',
		] );
		DB::table( 'faqs' )->insert( [
			'section'  => 'HOW DOES IT WORK?',
			'question' => 'How is the winner decided?',
			'answer'   => 'At the begiinning of the competition, the "Spot the Ball" picture on which you would choose your coordinates is shown to a judging panel. In the presence of an independent lawyer they mark where they think, in their professional opinion, the centre of the ball should be. The winner is simply the person who is closest to the judged position.
',
		] );

		DB::table( 'faqs' )->insert( [
			'section'  => 'WHO ARE WE?',
			'question' => 'Where is WYUD based?',
			'answer'   => 'Our Head Office address is 12 Sheryl Drive, Shrewsbury, US, and we can be contacted  via email at info@winyourdestiny.com. Tweet us @Winyourdestiny or get in touch through our Facebook page - facebook.com/WYUD.
',
		] );

		DB::table( 'faqs' )->insert( [
			'section'  => 'WHO ARE WE?',
			'question' => 'Who are WYUD?',
			'answer'   => 'WYUD (Win Your Destiny LLC) has been running competitions since 2019. Every month there are guaranteed Product Competition. We\'re a US-based entity , and are therefore required to adhere to the highest legal and audit standards.',
		] );

		DB::table( 'faqs' )->insert( [
			'section'  => 'WINNERS AND PRIZES?',
			'question' => 'Are there winners of every product?',
			'answer'   => 'Entrants can choose from the multiple products in the competition line-up. At the end of each competition, one product is won. This is determined by matching the winning coordinates from the Spot the Ball game (as chosen by our panel of expert judges) to all the entries.',
		] );

		DB::table( 'faqs' )->insert( [
			'section'  => 'WINNERS AND PRIZES?',
			'question' => 'How can I get further information?',
			'answer'   => 'Feel free to  email us at info@winyourdestiny.com and we will get straight back to you.',
		] );
	}
}

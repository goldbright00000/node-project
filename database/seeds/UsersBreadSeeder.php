<?php

use Illuminate\Database\Seeder;
use VoyagerBread\Traits\BreadSeeder;

class UserBreadSeeder extends Seeder
{
    use BreadSeeder;

    public function bread()
    {
        return [
            // usually the name of the table
            'name'                  => 'users',
            'display_name_singular' => 'User',
            'display_name_plural'   => 'Users',
            'icon'                  => '',
            'model_name'            => 'App\User',
            'controller'            => '',
            'generate_permissions'  => 1,
            'description'           => '',
        ];
    }

    public function inputFields()
    {
        return [
            'id' => [
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 1,
            ],
            'name' => [
	            'type'         => 'text',
	            'display_name' => 'First Name',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'lastName' => [
	            'type'         => 'text',
	            'display_name' => 'Last Name',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'email' => [
	            'type'         => 'text',
	            'display_name' => 'Email',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'gender' => [
	            'type'         => 'text',
	            'display_name' => 'Gender',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'countryCode' => [
	            'type'         => 'integer',
	            'display_name' => 'Phone Country Code',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'mobile' => [
	            'type'         => 'integer',
	            'display_name' => 'Phone Number',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'address1' => [
	            'type'         => 'text',
	            'display_name' => 'Address Line 1',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'address2' => [
	            'type'         => 'text',
	            'display_name' => 'Address Line 2',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'city' => [
	            'type'         => 'text',
	            'display_name' => 'City',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'state' => [
	            'type'         => 'text',
	            'display_name' => 'State',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'zip' => [
	            'type'         => 'text',
	            'display_name' => 'Zip',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'country' => [
	            'type'         => 'text',
	            'display_name' => 'Country',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
            'initialDiscount' => [
	            'type'         => 'number',
	            'display_name' => 'Credits',
	            'required'     => 1,
	            'browse'       => 0,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 1,
            ],
        ];
    }

    public function menuEntry()
    {
        return [
            'role'      => 'admin',
            'title'      => 'Users',
            'url'        => '',
            'route'      => 'voyager.users.index',
            'target'     => '_self',
            'icon_class' => 'voyager-basket',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 8,
        ];
    }
}

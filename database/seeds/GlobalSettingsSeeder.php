<?php

use Illuminate\Database\Seeder;

class GlobalSettingsSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table( 'global_settings' )->insert( [
			'userDiscount' => 2,
			'salesTax'     => 7,
			'maxTickets' => 100,
		] );
	}
}

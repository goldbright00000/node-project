<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table( 'categories' )->insert( [
			'name' => 'Cars',
			'slug' => 'cars',
		] );
		DB::table( 'categories' )->insert( [
			'name' => 'Laptops',
			'slug' => 'laptops',
		] );
		DB::table( 'categories' )->insert( [
			'name' => 'Mobile Phones',
			'slug' => 'mobile-phones',
		] );
		DB::table( 'categories' )->insert( [
			'name' => 'Digital Cameras',
			'slug' => 'digital-cameras',
		] );

		DB::table( 'categories' )->insert( [
			'name' => 'Watches',
			'slug' => 'watches',
		] );
	}
}

<?php

use Illuminate\Database\Seeder;

class DiscountCouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table( 'dicount_coupons' )->insert([
		    'name'          => 'Disc10',
			'value'         => '10',
		    'type'          => 'coupon',
		    'target'        => 'subtotal',
	    ]);
	    DB::table( 'dicount_coupons' )->insert( [
		    'name'          => 'Disc20',
		    'value'         => '20',
		    'type'          => 'coupon',
		    'target'        => 'subtotal',
	    ] );
    }
}

<?php

use Illuminate\Database\Seeder;
use VoyagerBread\Traits\BreadSeeder;

class InvoicesBreadSeeder extends Seeder
{
    use BreadSeeder;

    public function bread()
    {
        return [
            // usually the name of the table
            'name'                  => 'invoices',
            'display_name_singular' => 'Invoices',
            'display_name_plural'   => 'Invoices',
            'icon'                  => '',
            'model_name'            => 'App\Invoice',
            'controller'            => '',
            'generate_permissions'  => 1,
            'description'           => '',
        ];
    }

    public function inputFields()
    {
        return [
            'id' => [
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 1,
            ],
            'created_at' => [
                'type'         => 'timestamp',
                'display_name' => 'created_at',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 2,
            ],
            'updated_at' => [
                'type'         => 'timestamp',
                'display_name' => 'updated_at',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 3,
            ],
            'user_id' => [
	            'type'         => 'number',
	            'display_name' => 'User ID',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 0,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],
            'title' => [
	            'type'         => 'text',
	            'display_name' => 'Title',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],
            'description' => [
	            'type'         => 'text_area',
	            'display_name' => 'Description',
	            'required'     => 0,
	            'browse'       => 0,
	            'read'         => 0,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],
            'products' => [
	            'type'         => 'code',
	            'display_name' => 'Products',
	            'required'     => 0,
	            'browse'       => 0,
	            'read'         => 0,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],
            'productsTotal' => [
	            'type'         => 'text',
	            'display_name' => 'Sub Total',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],
            'discount' => [
	            'type'         => 'text',
	            'display_name' => 'Discount',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],
            'tax' => [
	            'type'         => 'text',
	            'display_name' => 'Tax',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],
            'creditAdjusted' => [
	            'type'         => 'text',
	            'display_name' => 'Credit Adjusted',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],

            'total' => [
	            'type'         => 'text',
	            'display_name' => 'Grand Total',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],

            'payment_status' => [
	            'type'         => 'text',
	            'display_name' => 'Payment Status',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 0,
	            'add'          => 0,
	            'delete'       => 0,
	            'details'      => '',
	            'order'        => 3,
            ],
        ];
    }

    public function menuEntry()
    {
        return [
            'role'      => 'admin',
            'title'      => 'Invoices',
            'url'        => '',
            'route'      => 'voyager.invoices.index',
            'target'     => '_self',
            'icon_class' => 'voyager-wallet',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 8,
        ];
    }
}

<?php

use Illuminate\Database\Seeder;
use VoyagerBread\Traits\BreadSeeder;

class HomepageSettingBreadSeeder extends Seeder {
	use BreadSeeder;

	public function bread() {
		return [
			// usually the name of the table
			'name'                  => 'homepage_settings',
			'display_name_singular' => 'HomepageSetting',
			'display_name_plural'   => 'HomepageSettings',
			'icon'                  => '',
			'model_name'            => 'App\HomepageSetting',
			'controller'            => '',
			'generate_permissions'  => 1,
			'description'           => '',
		];
	}

	public function inputFields() {
		return [

			'heroImage'       => [
				'type'         => 'image',
				'display_name' => 'Hompage Hero Image (Size 1920px * 1080px)',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => '',
				'order'        => 1,
			],
			'heroContent' => [
				'type'         => 'text',
				'display_name' => 'Win A Macbook Pro® This Week',
				'required'     => 1,
				'browse'       => 0,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => '',
				'order'        => 1,
			],
			'videoOneTitle'   => [
				'type'         => 'text',
				'display_name' => 'Title For The First Video',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => '',
				'order'        => 1,
			],
			'videoOne'        => [
				'type'         => 'text',
				'display_name' => 'YouTube ID Of The First Video',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => '',
				'order'        => 1,
			],
			'videoTwoTitle'   => [
				'type'         => 'text',
				'display_name' => 'Title For The Second Video',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => '',
				'order'        => 1,
			],
			'videoTwo'        => [
				'type'         => 'text',
				'display_name' => 'YouTube ID Of The Second Video',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => '',
				'order'        => 1,
			],
			'videoThreeTitle' => [
				'type'         => 'text',
				'display_name' => 'Title For The Third Video',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => '',
				'order'        => 1,
			],
			'videoThree'      => [
				'type'         => 'text',
				'display_name' => 'YouTube ID Of The Third Video',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => '',
				'order'        => 1,
			],
		];
	}

	public function menuEntry() {
		return [
			'role'       => 'admin',
			'title'      => 'HomepageSettings',
			'url'        => '',
			'route'      => 'voyager.homepage_settings.index',
			'target'     => '_self',
			'icon_class' => 'voyager-basket',
			'color'      => null,
			'parent_id'  => null,
			'order'      => 8,
		];
	}
}

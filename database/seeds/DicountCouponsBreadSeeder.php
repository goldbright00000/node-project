<?php

use Illuminate\Database\Seeder;
use VoyagerBread\Traits\BreadSeeder;

class DicountCouponBreadSeeder extends Seeder {
	use BreadSeeder;

	public function bread() {
		return [
			// usually the name of the table
			'name'                  => 'dicount_coupons',
			'display_name_singular' => 'Discount Coupon',
			'display_name_plural'   => 'Discount Coupons',
			'icon'                  => '',
			'model_name'            => 'App\DicountCoupon',
			'controller'            => '',
			'generate_permissions'  => 1,
			'description'           => '',
		];
	}

	public function inputFields() {
		return [
			'id'         => [
				'type'         => 'number',
				'display_name' => 'ID',
				'required'     => 1,
				'browse'       => 0,
				'read'         => 0,
				'edit'         => 0,
				'add'          => 0,
				'delete'       => 0,
				'details'      => '',
				'order'        => 1,
			],
			'created_at' => [
				'type'         => 'timestamp',
				'display_name' => 'created_at',
				'required'     => 0,
				'browse'       => 0,
				'read'         => 0,
				'edit'         => 0,
				'add'          => 0,
				'delete'       => 0,
				'details'      => '',
				'order'        => 2,
			],
			'updated_at' => [
				'type'         => 'timestamp',
				'display_name' => 'updated_at',
				'required'     => 0,
				'browse'       => 0,
				'read'         => 0,
				'edit'         => 0,
				'add'          => 0,
				'delete'       => 0,
				'details'      => '',
				'order'        => 3,
			],
			'name'       => [
				'type'         => 'text',
				'display_name' => 'Coupon Code',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => 'The name of the discount coupon',
				'order'        => 4,
			],
			'value'      => [
				'type'         => 'number',
				'display_name' => 'Discount % (ONLY NUMBER NEED NO NEED FOR % SYMBOL) ',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => 'The %age value of the coupon. (ONLY NUMBER NEED NO NEED FOR % SYMBOL)',
				'order'        => 5,
			],
			'type'       => [
				'type'         => 'select_dropdown',
				'display_name' => 'Type Of Discount',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => [
					'default' => 'coupon',
					'options' => [
						'option1' => 'coupon',
					],
				],
			],
			'Target'     => [
				'type'         => 'select_dropdown',
				'display_name' => 'Taget Value For Discount',
				'required'     => 1,
				'browse'       => 1,
				'read'         => 1,
				'edit'         => 1,
				'add'          => 1,
				'delete'       => 1,
				'details'      => [
					'default' => 'subtotal',
					'options' => [
						'option1' => 'subtotal',
						'option2' => 'total'
					],
				],
				'order'        => 5,
			]
		];
	}

	public function menuEntry() {
		return [
			'role'       => 'admin',
			'title'      => 'Discount Coupons',
			'url'        => '',
			'route'      => 'voyager.dicount_coupons.index',
			'target'     => '_self',
			'icon_class' => 'voyager-bell',
			'color'      => null,
			'parent_id'  => null,
			'order'      => 8,
		];
	}
}

<?php

use Illuminate\Database\Seeder;
use VoyagerBread\Traits\BreadSeeder;

class GlobalSettingsBreadSeeder extends Seeder
{
    use BreadSeeder;

    public function bread()
    {
        return [
            // usually the name of the table
            'name'                  => 'global_settings',
            'display_name_singular' => 'Global Setting',
            'display_name_plural'   => 'Global Settings',
            'icon'                  => '',
            'model_name'            => 'App\GlobalSettings',
            'controller'            => '',
            'generate_permissions'  => 1,
            'description'           => '',
        ];
    }

    public function inputFields()
    {
        return [
	        'id' => [
		        'type'         => 'number',
		        'display_name' => 'ID',
		        'required'     => 1,
		        'browse'       => 0,
		        'read'         => 0,
		        'edit'         => 0,
		        'add'          => 0,
		        'delete'       => 0,
		        'details'      => '',
		        'order'        => 1,
	        ],
	        'created_at' => [
		        'type'         => 'timestamp',
		        'display_name' => 'created_at',
		        'required'     => 0,
		        'browse'       => 0,
		        'read'         => 0,
		        'edit'         => 0,
		        'add'          => 0,
		        'delete'       => 0,
		        'details'      => '',
		        'order'        => 2,
	        ],
	        'updated_at' => [
		        'type'         => 'timestamp',
		        'display_name' => 'updated_at',
		        'required'     => 0,
		        'browse'       => 0,
		        'read'         => 0,
		        'edit'         => 0,
		        'add'          => 0,
		        'delete'       => 0,
		        'details'      => '',
		        'order'        => 3,
	        ],
	        'userDiscount' => [
		        'type'         => 'number',
		        'display_name' => 'Welcome Credits (The the default credits given to a user upon registration)',
		        'required'     => 0,
		        'browse'       => 1,
		        'read'         => 1,
		        'edit'         => 1,
		        'add'          => 1,
		        'delete'       => 0,
		        'details'      => '',
		        'order'        => 3,
	        ],
	        'salesTax' => [
		        'type'         => 'number',
		        'display_name' => 'Sales Tax (No Need for % sign)',
		        'required'     => 0,
		        'browse'       => 1,
		        'read'         => 1,
		        'edit'         => 1,
		        'add'          => 1,
		        'delete'       => 0,
		        'details'      => '',
		        'order'        => 4,
	        ],
	        'maxTickets' => [
		        'type'         => 'number',
		        'display_name' => 'The maximum number of tickets that can be played by a user per game',
		        'required'     => 1,
		        'browse'       => 0,
		        'read'         => 1,
		        'edit'         => 1,
		        'add'          => 1,
		        'delete'       => 0,
		        'details'      => '',
		        'order'        => 4,
	        ],
	        'creditNewReferal' => [
		        'type'         => 'number',
		        'display_name' => 'New Referral Credit',
		        'required'     => 0,
		        'browse'       => 1,
		        'read'         => 1,
		        'edit'         => 1,
		        'add'          => 1,
		        'delete'       => 0,
		        'details'      => 'The percentage of tax applied on the invoice. % symbol is not needed',
		        'order'        => 4,
	        ],
        ];
    }

    public function menuEntry()
    {
        return [
            'role'      => 'admin',
            'title'      => 'GlobalSettings',
            'url'        => '',
            'route'      => 'voyager.global_settings.index',
            'target'     => '_self',
            'icon_class' => 'voyager-sun',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 8,
        ];
    }
}

<?php

use Illuminate\Database\Seeder;
use VoyagerBread\Traits\BreadSeeder;

class CategoriesBreadSeeder extends Seeder
{
    use BreadSeeder;

    public function bread()
    {
        return [
            // usually the name of the table
            'name'                  => 'categories',
            'display_name_singular' => 'Categories',
            'display_name_plural'   => 'Categories',
            'icon'                  => '',
            'model_name'            => 'App\Categories',
            'controller'            => '',
            'generate_permissions'  => 1,
            'description'           => '',
        ];
    }

    public function inputFields()
    {
        return [
            'id' => [
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 1,
            ],
            'created_at' => [
                'type'         => 'timestamp',
                'display_name' => 'created_at',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 2,
            ],
            'updated_at' => [
                'type'         => 'timestamp',
                'display_name' => 'updated_at',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 3,
            ],
            'name' => [
		        'type'         => 'text',
		        'display_name' => 'Category Name',
		        'required'     => 1,
		        'browse'       => 1,
		        'read'         => 1,
		        'edit'         => 1,
		        'add'          => 1,
		        'delete'       => 1,
		        'details'      => '',
		        'order'        => 4,
	        ],
            'slug' => [
	            'type'         => 'text',
	            'display_name' => 'Slug',
	            'required'     => 1,
	            'browse'       => 1,
	            'read'         => 1,
	            'edit'         => 1,
	            'add'          => 1,
	            'delete'       => 1,
	            'details'      => '',
	            'order'        => 5,
            ],
        ];
    }

    public function menuEntry()
    {
        return [
            'role'      => 'admin',
            'title'      => 'Categories',
            'url'        => '',
            'route'      => 'voyager.categories.index',
            'target'     => '_self',
            'icon_class' => 'voyager-wand',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 8,
        ];
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {
		$this->call( VoyagerDatabaseSeeder::class );
		$this->call( CategoriesTableSeeder::class );
		$this->call( ProductsTableSeeder::class );
		$this->call( DiscountCouponSeeder::class );
		$this->call( UserTableSeeder::class );
		$this->call( GlobalSettingsSeeder::class );
		$this->call( InvoicesTableSeeder::class );
		$this->call( ReferralTableSeeder::class );
		$this->call( DeclareWinnersTableSeeder::class );
		$this->call( GameTableSeeder::class );
		$this->call( GlobalSettingsBreadSeeder::class );
		$this->call( DicountCouponBreadSeeder::class );
		$this->call( CategoriesBreadSeeder::class );
		$this->call( GamesBreadSeeder::class );
		$this->call( InvoicesBreadSeeder::class );
		$this->call( ProductBreadSeeder::class );
		$this->call( HomepageSettingBreadSeeder::class );
		$this->call( UserBreadSeeder::class );
		$this->call( PermissionRoleTableSeeder::class );
		$this->call( HomepageSetting::class );
		$this->call( CustomizedMenuItemsTableSeeder::class );
		$this->call( FaqTableSeeder::class );

    }
}

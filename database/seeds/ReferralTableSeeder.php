<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Faker\Provider\DateTime;

class ReferralTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();

		for ( $i = 0; $i < 100; $i ++ ) {
			App\Referral::create( [
				'name'        => $faker->name,
				'email'       => $faker->email,
				'message'     => $faker->text,
				'status'      => (bool) random_int( 0, 1 ),
				'user_id'     => rand( 1, 3 ),
				'accepted_on' => $faker->dateTimeBetween( $startDate = '-1 month', $endDate = 'now', $timezone = null ),
			] );
		}

	}
}

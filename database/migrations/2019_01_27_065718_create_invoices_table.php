<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'invoices', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' )->unsigned();
			$table->string( 'title' );
			$table->text( 'description' );
			$table->text( 'products' );
			$table->float( 'productsTotal' );
			$table->float( 'discount' );
			$table->float( 'tax' );
			$table->float( 'creditAdjusted' );
			$table->float( 'total' );
			$table->string( 'paymentMethod' )->nullable();
			$table->string( 'payment_status' )->default( 'Pending' );
			$table->unsignedInteger( 'user_id' );
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );
			$table->timestamps();
		} );
		DB::update( "ALTER TABLE invoices AUTO_INCREMENT = 1215;" );//1215

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'invoices' );
	}
}

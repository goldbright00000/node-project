<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DiscountCoupons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dicount_coupons', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name')->unique();
	        $table->integer('value');
	        $table->string('type')->default('coupon');
	        $table->string('target')->default('subtotal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dicount_coupons');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomepageSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepage_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->text('heroImage');
            $table->text('heroContent');
            $table->text('videoOneTitle');
            $table->text('videoOne');
            $table->text('videoTwoTitle');
            $table->text('videoTwo');
            $table->text('videoThreeTitle');
            $table->text('videoThree');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homepage_settings');
    }
}

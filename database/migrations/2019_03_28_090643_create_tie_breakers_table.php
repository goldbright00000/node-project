<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTieBreakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tie_breakers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pointCount');
	        $table->unsignedInteger( 'game_id' )->nullable();
	        $table->unsignedInteger( 'user_id' )->nullable();
	        $table->boolean( 'breakerPlayed' )->default(false);
	        $table->float( 'breakerDistance' )->nullable();
	        $table->foreign( 'game_id' )->references( 'id' )->on( 'games' )->onDelete( 'cascade' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tie_breakers');
    }
}

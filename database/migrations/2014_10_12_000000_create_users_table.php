<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
	        $table->string('lastName')->nullable(); //additional field added for Last Name
            $table->string('email')->unique();
            $table->string('gender')->nullable();
            $table->timestamp('email_verified_at')->nullable()->unique();
            $table->string('password');
	        $table->string('provider_id')->nullable();
	        $table->string('provider')->nullable();
            $table->rememberToken();
            $table->timestamps();
	        $table->string('mobile')->nullable(); //additional field added for mobile number
	        $table->string('countryCode')->nullable(); //additional field added for country code
	        $table->string('address1')->nullable(); //additional field added for address line 1
	        $table->string('address2')->nullable(); //additional field added for address line 2
	        $table->string('city')->nullable(); //additional field added for city
	        $table->string('state' )->nullable(); //additional field added for state
	        $table->string('zip')->nullable(); //additional field added for zipcode
	        $table->string('country')->nullable(); //additional field added for country
	        $table->boolean('ageConfirmation')->nullable(); //additional field added for age confirmation
	        $table->boolean('newsConfirmation')->nullable(); //additional field added for news letter confirmation
	        $table->string('profileImage')->default('images/avatar.png'); //additional field
	        // added for profile image
	        $table->integer('initialDiscount')->default(2); //additional field added for Initial Discount
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

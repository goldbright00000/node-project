<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->dateTime('startDate');
            $table->dateTime('endDate');
            $table->integer('xCords')->nullable();
            $table->integer('yCords')->nullable();
            $table->string('imageUrl');
            $table->integer('imageWidth')->nullable();
            $table->integer('imageHeight')->nullable();
            $table->integer('imageMultiplier')->default(8);
            $table->dateTime('winnerDate');
            $table->dateTime('tieDate');
	        $table->string('videoURL')->nullable();
	        $table->longText('description')->nullable();
	        $table->integer('runnerUps')->default(20);
	        $table->boolean('resultStatus')->default(false);
	        $table->unsignedInteger( 'declareWinner_id' )->nullable();
	        $table->foreign( 'declareWinner_id' )->references( 'id' )->on( 'declare_winners' )->onDelete( 'cascade' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}

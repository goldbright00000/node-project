<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('position');
	        $table->unsignedInteger( 'user_id' );
	        $table->unsignedInteger( 'game_id' );
	        $table->unsignedInteger( 'pointer_id' );
	        $table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
	        $table->foreign( 'game_id' )->references( 'id' )->on( 'games' );
	        $table->foreign( 'pointer_id' )->references( 'id' )->on( 'pointers' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winners');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclareWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declare_winners', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('subTitle');
            $table->text('slug');
            $table->longText('description');
            $table->string('winnerImage1');
            $table->string('winnerImage2')->nullable();
            $table->string('winnerImage3')->nullable();
            $table->string('video')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declare_winners');
    }
}

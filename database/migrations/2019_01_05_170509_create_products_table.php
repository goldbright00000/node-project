<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name')->unique();
	        $table->string('slug')->unique();
	        $table->text('description');
	        $table->float('price_slab1');
	        $table->float('discount_slab1');
	        $table->float('discount_slab2');
	        $table->string('product_image1');
	        $table->string('product_image2')->nullable();
	        $table->string('product_image3')->nullable();
	        $table->string('product_image4')->nullable();
	        $table->string('product_image5')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('pointers', function (Blueprint $table) {
	        $table->engine = 'InnoDB';
        	$table->increments('id');
            $table->float('xCords');
	        $table->float('yCords');
	        $table->float('distance');
	        $table->string('sessionID')->nullable();
	        $table->integer('productID')->nullable();
	        $table->unsignedInteger( 'user_id' )->nullable();
	        $table->bigInteger( 'invoice_id' )->unsigned()->nullable()->default(null);
	        $table->unsignedInteger( 'game_id' )->nullable();
	        $table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
	        $table->foreign( 'invoice_id' )->references( 'id' )->on( 'invoices' );
	        $table->foreign( 'game_id' )->references( 'id' )->on( 'games' );
	        $table->string('stampId')->nullable();
	        $table->boolean('stamped')->default(false);
	        $table->boolean('removed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pointers');
    }
}

/*
*
* Custom.js
* This file contains all the JS related functions
* @since version 1.0.0
* @author Cloudaffle
*
* */

/*
* Code for Dropdown Select
* */
$(document).ready(function() {
    $('.mdb-select').material_select();
});

/*
* Script For Removing Product Items From The Cart
*
**/
var $TABLE = $('#table');
var $BTN = $('#export-btn');
var $EXPORT = $('#export');

$('.table-add').click(function () {
    var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
    $TABLE.find('table').append($clone);
});

$('.table-remove').click(function () {
    $(this).parents('tr').detach();
});

$('.table-up').click(function () {
    var $row = $(this).parents('tr');
    if ($row.index() === 1) return; // Don't go above the header
    $row.prev().before($row.get(0));
});

$('.table-down').click(function () {
    var $row = $(this).parents('tr');
    $row.next().after($row.get(0));
});

// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.click(function () {
    var $rows = $TABLE.find('tr:not(:hidden)');
    var headers = [];
    var data = [];

    // Get the headers (add special header logic here)
    $($rows.shift()).find('th:not(:empty)').each(function () {
        headers.push($(this).text().toLowerCase());
    });

    // Turn all existing rows into a loopable array
    $rows.each(function () {
        var $td = $(this).find('td');
        var h = {};

        // Use the headers from earlier to name our hash keys
        headers.forEach(function (header, i) {
            h[header] = $td.eq(i).text();
        });

        data.push(h);
    });

    // Output the result
    $EXPORT.text(JSON.stringify(data));
});



// Number Button JS
function increaseValue() {
	var value = parseInt(document.getElementById('number').value, 10);
	value = isNaN(value) ? 1 : value;
	value++;
	document.getElementById('number').value = value;
	console.log(value);
}

function decreaseValue() {
	var value = parseInt(document.getElementById('number').value, 10);
	value = isNaN(value) ? 1 : value;
	value < 1 ? value = 1 : '';
	value--;
	document.getElementById('number').value = value;
	console.log(value);
}


// Js For Pagination Of Invoice Table
$(document).ready(function () {
	if($('#dtBasicExample')){
		$('#dtBasicExample').DataTable({
			"paging": true,
			"pagingType": "numbers",
			"ordering": false,
			"searching": false,
		});
		$('.dataTables_length').addClass('bs-select');
	}
});

// Js For Datatable for Accepted requests
$(document).ready(function () {
	if($('#dtacceptedPayments')){
		$('#dtacceptedPayments').DataTable({
			"paging": true,
			"pagingType": "numbers",
			"ordering": false,
			"searching": false,
		});
		$('.dataTables_length').addClass('bs-select');
	}
});

// Js For Datatable for Accepted requests
$(document).ready(function () {
	if($('#dtsentRequest')){
		$('#dtsentRequest').DataTable({
			"paging": true,
			"pagingType": "numbers",
			"ordering": false,
			"searching": false,
		});
		$('.dataTables_length').addClass('bs-select');
	}
});


/**
 * Script to trigger the Magnify Plugin
 *
 * **/
$(document).ready(function(){
	var evt = new Event();
	m = new Magnifier(evt);
	m.attach({
		thumb: '#thumb',
		large: magnifyImage,
		largeWrapper: 'preview',
		zoom: zoom,
		zoomable: false,
		mode: 'inside',
	});
});

//Click Function For Displaying Tools
$(document).ready(function () {
	$("#wdGameTools").click(function () {
	
		if ($("#wd-tools-container").hasClass("wdGameToolsHide")) {
			$("#wd-tools-container").removeClass("wdGameToolsHide");
			$("#canvasGame").addClass("canvasActive");
		} else {
			$("#wd-tools-container").addClass("wdGameToolsHide");
			$("#canvasGame").removeClass("canvasActive");
		}
	});

	$("#btn2").click(function () {
		$("img").after("<b>This text is on the right side</b>");
	});
});

/***
 * New Game Play Canvas Code
 *
 */
function Line(startX, startY, endX, endY, color, paper) {

	var start = {
		x: startX,
		y: startY,
	};


	var end = {
		ex: endX,
		ey: endY
	};


	var getEndX = function () {
		return end.ex;
	};

	var getEndy = function () {
		return end.ey;
	};

	var getPath = function () {
		return "M" + start.x + "," + start.y + "L" + getEndX() + "," + getEndy();
	};

	var redraw = function () {
		node.attr("path", getPath());
	};


	var node = paper.path(getPath());

	node.attr({
		'fill': color,
		'stroke': color,
		"stroke-width": "2"
	});

	return {
		updateStart: function (x, y) {
			start.x = x;
			start.y = y;
			redraw();
			return this;
		},
		updateEnd: function (x, y) {
			end.ex = x;
			end.ey = y;
			redraw();
			return this;
		},
		clear: function () {
			node.remove();
		}
	};
}

$(function () {
	var $paper = $("#canvasGame");
	var $gameCont = $("#gameContainer");
	var $controls = $('.control');
	var paper = Raphael(document.getElementById('canvasGame'), 895, 500);
	var color = "#ff0000";

	$("#paintRed").click(function () {
		$("#wd-tools-container>a>img").removeClass("colorActive");
		$("#paintRed>img").addClass("colorActive");
		return color = "#ff0000";
	});
	$("#paintMagenta").click(function () {
		$("#wd-tools-container>a>img").removeClass("colorActive");
		$("#paintMagenta>img").addClass("colorActive");
		return color = "#ff00e4";
	});
	$("#paintPurple").click(function () {
		$("#wd-tools-container>a>img").removeClass("colorActive");
		$("#paintPurple>img").addClass("colorActive");
		return color = "#a200ff";
	});
	$("#paintBlue").click(function () {
		$("#wd-tools-container>a>img").removeClass("colorActive");
		$("#paintBlue>img").addClass("colorActive");
		return color = "#0068ff";
	});
	$("#paintGreen").click(function () {
		$("#wd-tools-container>a>img").removeClass("colorActive");
		$("#paintGreen>img").addClass("colorActive");
		return color = "#00d821";
	});
	$("#paintYellow").click(function () {
		$("#wd-tools-container>a>img").removeClass("colorActive");
		$("#paintYellow>img").addClass("colorActive");
		return color = "#ffdc00";
	});

	$(".wd-spl-tool").click(function () {
		if ($(this).hasClass("wd-tool-icons-active")) {
			$(this).removeClass("wd-tool-icons-active");
		} else {
			$(this).addClass("wd-tool-icons-active");
		}
	});


	// $paper.mousedown(
	// 	function(e) {
	// 		e.stopPropagation();
	// 		c = color;
	// 		x = e.offsetX;
	// 		y = e.offsetY;
	// 		line = Line(x, y, x, y, c, paper);
	// 		$paper.bind('mousemove', function(e) {
	// 			x = e.offsetX;
	// 			y = e.offsetY;
	// 			line.updateEnd(x, y);
	// 		});
	// 	});

	$('body').on('mousedown', '.canvasActive',  function (e) {
		e.preventDefault();
		e.stopPropagation();


		var c = color;
		var offset = $('#canvasGame').offset();
		var x = e.pageX - offset.left;
		var y = e.pageY - offset.top;

		var line = Line(x, y, x, y, c, paper);
		$paper.bind('mousemove', function (e) {
			x = e.pageX - offset.left;
			y = e.pageY - offset.top;
			line.updateEnd(x, y);
		});
	});

	$paper.mouseup(
		function (e) {
			e.stopPropagation();
			$paper.unbind('mousemove');
		});


	// Function To Clear The Paper
	$("#wd-tools-clear").click(function () {
		paper.clear();
	});

	// Function To Hide all the guides drawn on the Canvas
	$("#wd-tools-hide").click(function () {
		if ($("#canvasGame").hasClass("win-hide-canvas")) {
			$("#canvasGame").removeClass("win-hide-canvas")
		} else {
			$("#canvasGame").addClass("win-hide-canvas")
		}
	});

	//Function to remove the last drawn line
	$("#wd-tools-reset").click(function () {
		if ($("svg").has("path")) {
			var path = $("svg path").last();
			path.remove();
		}
	});
});

//ToolTip Instantiation
$(function () {
	$('[data-toggle="tooltip"]').tooltip()
});


// Create a Floating DIv on Mousemove in Game Area
$('.win-crosshair-over').on('mousemove', '#canvasGame', function (e) {
	$('#acab').css({
		left: e.pageX,
		top: e.pageY
	});
});

// Calculate Coordinates for image
jQuery(function ($) {
	var x, y;
	$(".win-crosshair-over").on('mousemove', '#canvasGame', function (event) {
		var offset = $(this).offset();
		x = ((event.pageX - offset.left) * 8);
		y = ((event.pageY - offset.top) * 8);
		$("#offsetDisplay").show();
		if(x>0 && y>0){
			$("#offsetDisplay").html("(X: " + x + ", Y: " + y + ")");
		} else {
			$("#offsetDisplay").hide();
		}		
	});
});


// Add Class to highlight the active ticket being played
function addActiveClass() {

	var activeTicket = $('#ticketsSlider').find('.unStamped').first();
	var playingElement = activeTicket.children().find('.playingText');

	// Add Active Class to the element
	activeTicket.addClass('unStampedActive active');

	// Replace the text to playing
	playingElement.html('<span style="color:green;"><i class="fas fa fa-crosshairs"></i> Now Playing </span>');
}

// Trigger the active class in case the page reloads.
$(document).ready(function () {
	addActiveClass();
});

jQuery.fn.getMaxOccupiedLocation = function()
{
    var maxX = 0, maxY = 0, tmpX, tmpY, elem;
    this.each( function(i,v)
    {
        elem = $("#canvasGame"),
        tmpX = elem .offset().left + elem.width(),
        maxX = (tmpX > maxX) ? tmpX : maxX,
        tmpY = elem .offset().top + elem.height(),
        maxY = (tmpY > maxY) ? tmpY : maxY;
    });
    console.log(maxX+','+maxY);    
};





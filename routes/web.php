<?php

/*
|--------------------------------------------------------------------------
| Force SSL For All Pages
|--------------------------------------------------------------------------
|
| This forces Laravel to use SSL for all routes if the production constant
| is set to true in the env file.
|
*/


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route for the home page
Route::get('/', 'HomePageController@index')->name('home');
Route::get('/resend-verificatrion', 'HomePageController@verify')->name('resend.verification');

// Route for the product listing page
Route::get('/products','ProductListingController@index')->name('product-listing');
Route::post('/products','ProductListingController@update')->name('product.update');

// Route For the product detail
Route::get('/products/{product}','ProductListingController@show')->name('product-detail');

// Route For the Shopping Cart
Route::get('/cart','CartController@index')->name('cart');
Route::post('/cart/{product}','CartController@store')->name('cart.store');
Route::delete('/cart/{product}','CartController@destroy')->name('cart.destroy');
Route::post('/cart/delete','CartController@delete')->name('cart.delete');

// Routes For Checkout
Route::get('/checkout','CheckoutController@index')->name('checkout.index')->middleware('auth');
Route::post('/checkout','CheckoutController@discount')->name('checkout.discount')->middleware('auth');


// PayPal Routes
Route::post('paypal/express-checkout', 'PaypalController@expressCheckout')->name('paypal.express-checkout');
Route::get('paypal/express-checkout-success', 'PaypalController@expressCheckoutSuccess');
//Route::post('paypal/notify', 'PaypalController@GeFailNotification')->name('paypal.failure');
Route::get('paypal/notify', ['uses' => 'PaypalController@GeFailNotification', 'as' => 'paypal.failure']);



// Temporary Empty Cart Function
Route::get('empty', function(){
	Cart::clear();
});

// Session Flush Temporary
Route::get('session', function(){
	Session::flush();
});


Route::get('/how-to-win', function () {
	return view('pages.how-to-win');
});

Route::get('/about', function () {
	return view('pages.about');
});

Route::get('/sign-up-user', function () {
	return view('pages.sign-up');
});

Route::get('/login-user', function () {
	return view('pages.login-user');
})->name('login-user');


Route::get('/product-detail', function () {
	return view('pages.product-detail');
});


Route::get('/privacy-policy', function () {
	return view('pages.privacy-policy');
})->name('privacy-policy');

Route::get('/terms-conditions', function () {
	return view('pages.terms-conditions');
})->name('terms-conditions');

Route::get('/refund-policy', function () {
	return view('pages.refund-policy');
});
Route::get('/email-confirm', function () {
	return view('pages.email-confirm');
});

// Routes For the Winners Page
Route::get('/faqs', 'FaqController@index')->name('faq.index');


// Email Verification Route
Route::get('/verification', function () {
	return view('pages.verification');
})->name('verification');


// Routes For the Winners Page
Route::get('/winners', 'GameController@index')->name('winners.index');
Route::get('/winners/{winner}', 'GameController@detail')->name('winners.detail');


Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


//Auth::routes();
Auth::routes(['verify' => true]);

// Routes to Ensure that only the verified users have the access to the profile.
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/order-history', 'HomeController@orderHistory')->name('order-history');
Route::post('/invoice', 'HomeController@invoice')->name('invoice');
Route::post('/home-profile', 'HomeController@update')->name('home.update');
Route::post('/home-address', 'HomeController@updateAddress')->name('home.updateAddress');
Route::post('/home-password', 'HomeController@updatePassword')->name('home.updatePassword');
Route::post('/home-pic', 'HomeController@profilePic')->name('home.pic');

// Routes For Referral Controller
Route::get('/credits-history', 'ReferralController@index')->name('credits-history');
Route::post('/credits-history', 'ReferralController@store')->name('referral.store');

// Routes For The Pointer Controller
Route::get('/play', 'PointerController@index')->name('pointer.index');
Route::get('/pointer/get', 'PointerController@store')->name('pointer.store');
Route::post('/pointer/remove', 'PointerController@remove')->name('pointer.remove');
Route::get('/pointer/destroy', 'PointerController@destroy')->name('pointer.destroy');
Route::post('/pointer/update', 'PointerController@update')->name('pointer.update');
Route::get('/pointer/destroyTicket/{id}', 'PointerController@destroyTicket')->name('pointer.destroyTicket');

// Admin Panel Views
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
	

});

Route::get('/vendor/voyager/gravy','GameController@gravy')->name('voyager.games');
Route::post('/vendor/voyager/winners','GameController@winners')->name('voyager.winner-list');
Route::post('/vendor/voyager/winners-declare','GameController@declare')->name('voyager.winner-declare');
Route::post('/vendor/voyager/winners-post','GameController@post')->name('voyager.winner-post');

